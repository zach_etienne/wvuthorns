#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "math.h"
/**
 * Perform BSSN-to-ADM conversion. Useful for diagnostics.
 */
void BaikalVacuum_BSSN_to_ADM(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS_BaikalVacuum_BSSN_to_ADM;
  DECLARE_CCTK_PARAMETERS;

#pragma omp parallel for
  for (int i2 = 0; i2 < cctk_lsh[2]; i2++) {
    for (int i1 = 0; i1 < cctk_lsh[1]; i1++) {
      for (int i0 = 0; i0 < cctk_lsh[0]; i0++) {
        alp[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        betax[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        betay[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        betaz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        dtbetax[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        dtbetay[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        dtbetaz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL cf = cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL trK = trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD00 = hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD01 = hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD02 = hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD11 = hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD12 = hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD22 = hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL aDD00 = aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL aDD01 = aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL aDD02 = aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL aDD11 = aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL aDD12 = aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL aDD22 = aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL tmp0 = (1.0 / ((cf) * (cf)));
        const CCTK_REAL tmp7 = (1.0 / 3.0) * trK;
        const CCTK_REAL tmp1 = tmp0 * (hDD00 + 1);
        const CCTK_REAL tmp4 = tmp0 * (hDD11 + 1);
        const CCTK_REAL tmp6 = tmp0 * (hDD22 + 1);
        gxx[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = tmp1;
        gxy[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = hDD01 * tmp0;
        gxz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = hDD02 * tmp0;
        gyy[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = tmp4;
        gyz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = hDD12 * tmp0;
        gzz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = tmp6;
        kxx[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = aDD00 * tmp0 + tmp1 * tmp7;
        kxy[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = aDD01 * tmp0 + hDD01 * tmp0 * tmp7;
        kxz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = aDD02 * tmp0 + hDD02 * tmp0 * tmp7;
        kyy[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = aDD11 * tmp0 + tmp4 * tmp7;
        kyz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = aDD12 * tmp0 + hDD12 * tmp0 * tmp7;
        kzz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = aDD22 * tmp0 + tmp6 * tmp7;
      } // END LOOP: for (int i0 = 0; i0 < cctk_lsh[0]; i0++)
    } // END LOOP: for (int i1 = 0; i1 < cctk_lsh[1]; i1++)
  } // END LOOP: for (int i2 = 0; i2 < cctk_lsh[2]; i2++)
}
