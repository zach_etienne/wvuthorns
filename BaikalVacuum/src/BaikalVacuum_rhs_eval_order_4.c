#include "./simd/simd_intrinsics.h"
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "math.h"
/**
 * Finite difference function for operator dD0, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dD0_fdorder4(const REAL_SIMD_ARRAY FDPROTO_i0m1, const REAL_SIMD_ARRAY FDPROTO_i0m2,
                                                                             const REAL_SIMD_ARRAY FDPROTO_i0p1, const REAL_SIMD_ARRAY FDPROTO_i0p2,
                                                                             const REAL_SIMD_ARRAY invdxx0) {
  const double dblFDPart1_Rational_1_12 = 1.0 / 12.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_12 = ConstSIMD(dblFDPart1_Rational_1_12);

  const double dblFDPart1_Rational_2_3 = 2.0 / 3.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_2_3 = ConstSIMD(dblFDPart1_Rational_2_3);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(invdxx0, FusedMulAddSIMD(FDPart1_Rational_1_12, SubSIMD(FDPROTO_i0m2, FDPROTO_i0p2),
                                                                     MulSIMD(FDPart1_Rational_2_3, SubSIMD(FDPROTO_i0p1, FDPROTO_i0m1))));

  return FD_result;
}
/**
 * Finite difference function for operator dD1, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dD1_fdorder4(const REAL_SIMD_ARRAY FDPROTO_i1m1, const REAL_SIMD_ARRAY FDPROTO_i1m2,
                                                                             const REAL_SIMD_ARRAY FDPROTO_i1p1, const REAL_SIMD_ARRAY FDPROTO_i1p2,
                                                                             const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Rational_1_12 = 1.0 / 12.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_12 = ConstSIMD(dblFDPart1_Rational_1_12);

  const double dblFDPart1_Rational_2_3 = 2.0 / 3.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_2_3 = ConstSIMD(dblFDPart1_Rational_2_3);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(invdxx1, FusedMulAddSIMD(FDPart1_Rational_1_12, SubSIMD(FDPROTO_i1m2, FDPROTO_i1p2),
                                                                     MulSIMD(FDPart1_Rational_2_3, SubSIMD(FDPROTO_i1p1, FDPROTO_i1m1))));

  return FD_result;
}
/**
 * Finite difference function for operator dD2, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dD2_fdorder4(const REAL_SIMD_ARRAY FDPROTO_i2m1, const REAL_SIMD_ARRAY FDPROTO_i2m2,
                                                                             const REAL_SIMD_ARRAY FDPROTO_i2p1, const REAL_SIMD_ARRAY FDPROTO_i2p2,
                                                                             const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_12 = 1.0 / 12.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_12 = ConstSIMD(dblFDPart1_Rational_1_12);

  const double dblFDPart1_Rational_2_3 = 2.0 / 3.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_2_3 = ConstSIMD(dblFDPart1_Rational_2_3);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(invdxx2, FusedMulAddSIMD(FDPart1_Rational_1_12, SubSIMD(FDPROTO_i2m2, FDPROTO_i2p2),
                                                                     MulSIMD(FDPart1_Rational_2_3, SubSIMD(FDPROTO_i2p1, FDPROTO_i2m1))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD00, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD00_fdorder4(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i0m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0m2, const REAL_SIMD_ARRAY FDPROTO_i0p1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p2, const REAL_SIMD_ARRAY invdxx0) {
  const double dblFDPart1_Rational_1_12 = 1.0 / 12.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_12 = ConstSIMD(dblFDPart1_Rational_1_12);

  const double dblFDPart1_Rational_4_3 = 4.0 / 3.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_3 = ConstSIMD(dblFDPart1_Rational_4_3);

  const double dblFDPart1_Rational_5_2 = 5.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_5_2 = ConstSIMD(dblFDPart1_Rational_5_2);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(invdxx0, invdxx0),
              FusedMulSubSIMD(FDPart1_Rational_4_3, AddSIMD(FDPROTO_i0m1, FDPROTO_i0p1),
                              FusedMulAddSIMD(FDPROTO, FDPart1_Rational_5_2, MulSIMD(FDPart1_Rational_1_12, AddSIMD(FDPROTO_i0m2, FDPROTO_i0p2)))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD01, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD01_fdorder4(
    const REAL_SIMD_ARRAY FDPROTO_i0m1_i1m1, const REAL_SIMD_ARRAY FDPROTO_i0m1_i1m2, const REAL_SIMD_ARRAY FDPROTO_i0m1_i1p1,
    const REAL_SIMD_ARRAY FDPROTO_i0m1_i1p2, const REAL_SIMD_ARRAY FDPROTO_i0m2_i1m1, const REAL_SIMD_ARRAY FDPROTO_i0m2_i1m2,
    const REAL_SIMD_ARRAY FDPROTO_i0m2_i1p1, const REAL_SIMD_ARRAY FDPROTO_i0m2_i1p2, const REAL_SIMD_ARRAY FDPROTO_i0p1_i1m1,
    const REAL_SIMD_ARRAY FDPROTO_i0p1_i1m2, const REAL_SIMD_ARRAY FDPROTO_i0p1_i1p1, const REAL_SIMD_ARRAY FDPROTO_i0p1_i1p2,
    const REAL_SIMD_ARRAY FDPROTO_i0p2_i1m1, const REAL_SIMD_ARRAY FDPROTO_i0p2_i1m2, const REAL_SIMD_ARRAY FDPROTO_i0p2_i1p1,
    const REAL_SIMD_ARRAY FDPROTO_i0p2_i1p2, const REAL_SIMD_ARRAY invdxx0, const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Rational_1_144 = 1.0 / 144.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_144 = ConstSIMD(dblFDPart1_Rational_1_144);

  const double dblFDPart1_Rational_1_18 = 1.0 / 18.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_18 = ConstSIMD(dblFDPart1_Rational_1_18);

  const double dblFDPart1_Rational_4_9 = 4.0 / 9.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_9 = ConstSIMD(dblFDPart1_Rational_4_9);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(
      invdxx0,
      MulSIMD(invdxx1,
              FusedMulAddSIMD(
                  FDPart1_Rational_1_18,
                  AddSIMD(AddSIMD(FDPROTO_i0m2_i1p1, FDPROTO_i0p1_i1m2),
                          AddSIMD(FDPROTO_i0p2_i1m1, SubSIMD(FDPROTO_i0m1_i1p2, AddSIMD(AddSIMD(FDPROTO_i0m1_i1m2, FDPROTO_i0m2_i1m1),
                                                                                        AddSIMD(FDPROTO_i0p1_i1p2, FDPROTO_i0p2_i1p1))))),
                  FusedMulAddSIMD(FDPart1_Rational_4_9,
                                  AddSIMD(FDPROTO_i0p1_i1p1, SubSIMD(FDPROTO_i0m1_i1m1, AddSIMD(FDPROTO_i0m1_i1p1, FDPROTO_i0p1_i1m1))),
                                  MulSIMD(FDPart1_Rational_1_144,
                                          AddSIMD(FDPROTO_i0p2_i1p2, SubSIMD(FDPROTO_i0m2_i1m2, AddSIMD(FDPROTO_i0m2_i1p2, FDPROTO_i0p2_i1m2))))))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD02, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD02_fdorder4(
    const REAL_SIMD_ARRAY FDPROTO_i0m1_i2m1, const REAL_SIMD_ARRAY FDPROTO_i0m1_i2m2, const REAL_SIMD_ARRAY FDPROTO_i0m1_i2p1,
    const REAL_SIMD_ARRAY FDPROTO_i0m1_i2p2, const REAL_SIMD_ARRAY FDPROTO_i0m2_i2m1, const REAL_SIMD_ARRAY FDPROTO_i0m2_i2m2,
    const REAL_SIMD_ARRAY FDPROTO_i0m2_i2p1, const REAL_SIMD_ARRAY FDPROTO_i0m2_i2p2, const REAL_SIMD_ARRAY FDPROTO_i0p1_i2m1,
    const REAL_SIMD_ARRAY FDPROTO_i0p1_i2m2, const REAL_SIMD_ARRAY FDPROTO_i0p1_i2p1, const REAL_SIMD_ARRAY FDPROTO_i0p1_i2p2,
    const REAL_SIMD_ARRAY FDPROTO_i0p2_i2m1, const REAL_SIMD_ARRAY FDPROTO_i0p2_i2m2, const REAL_SIMD_ARRAY FDPROTO_i0p2_i2p1,
    const REAL_SIMD_ARRAY FDPROTO_i0p2_i2p2, const REAL_SIMD_ARRAY invdxx0, const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_144 = 1.0 / 144.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_144 = ConstSIMD(dblFDPart1_Rational_1_144);

  const double dblFDPart1_Rational_1_18 = 1.0 / 18.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_18 = ConstSIMD(dblFDPart1_Rational_1_18);

  const double dblFDPart1_Rational_4_9 = 4.0 / 9.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_9 = ConstSIMD(dblFDPart1_Rational_4_9);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(
      invdxx0,
      MulSIMD(invdxx2,
              FusedMulAddSIMD(
                  FDPart1_Rational_1_18,
                  AddSIMD(AddSIMD(FDPROTO_i0m2_i2p1, FDPROTO_i0p1_i2m2),
                          AddSIMD(FDPROTO_i0p2_i2m1, SubSIMD(FDPROTO_i0m1_i2p2, AddSIMD(AddSIMD(FDPROTO_i0m1_i2m2, FDPROTO_i0m2_i2m1),
                                                                                        AddSIMD(FDPROTO_i0p1_i2p2, FDPROTO_i0p2_i2p1))))),
                  FusedMulAddSIMD(FDPart1_Rational_4_9,
                                  AddSIMD(FDPROTO_i0p1_i2p1, SubSIMD(FDPROTO_i0m1_i2m1, AddSIMD(FDPROTO_i0m1_i2p1, FDPROTO_i0p1_i2m1))),
                                  MulSIMD(FDPart1_Rational_1_144,
                                          AddSIMD(FDPROTO_i0p2_i2p2, SubSIMD(FDPROTO_i0m2_i2m2, AddSIMD(FDPROTO_i0m2_i2p2, FDPROTO_i0p2_i2m2))))))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD11, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD11_fdorder4(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i1m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1m2, const REAL_SIMD_ARRAY FDPROTO_i1p1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p2, const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Rational_1_12 = 1.0 / 12.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_12 = ConstSIMD(dblFDPart1_Rational_1_12);

  const double dblFDPart1_Rational_4_3 = 4.0 / 3.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_3 = ConstSIMD(dblFDPart1_Rational_4_3);

  const double dblFDPart1_Rational_5_2 = 5.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_5_2 = ConstSIMD(dblFDPart1_Rational_5_2);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(invdxx1, invdxx1),
              FusedMulSubSIMD(FDPart1_Rational_4_3, AddSIMD(FDPROTO_i1m1, FDPROTO_i1p1),
                              FusedMulAddSIMD(FDPROTO, FDPart1_Rational_5_2, MulSIMD(FDPart1_Rational_1_12, AddSIMD(FDPROTO_i1m2, FDPROTO_i1p2)))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD12, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD12_fdorder4(
    const REAL_SIMD_ARRAY FDPROTO_i1m1_i2m1, const REAL_SIMD_ARRAY FDPROTO_i1m1_i2m2, const REAL_SIMD_ARRAY FDPROTO_i1m1_i2p1,
    const REAL_SIMD_ARRAY FDPROTO_i1m1_i2p2, const REAL_SIMD_ARRAY FDPROTO_i1m2_i2m1, const REAL_SIMD_ARRAY FDPROTO_i1m2_i2m2,
    const REAL_SIMD_ARRAY FDPROTO_i1m2_i2p1, const REAL_SIMD_ARRAY FDPROTO_i1m2_i2p2, const REAL_SIMD_ARRAY FDPROTO_i1p1_i2m1,
    const REAL_SIMD_ARRAY FDPROTO_i1p1_i2m2, const REAL_SIMD_ARRAY FDPROTO_i1p1_i2p1, const REAL_SIMD_ARRAY FDPROTO_i1p1_i2p2,
    const REAL_SIMD_ARRAY FDPROTO_i1p2_i2m1, const REAL_SIMD_ARRAY FDPROTO_i1p2_i2m2, const REAL_SIMD_ARRAY FDPROTO_i1p2_i2p1,
    const REAL_SIMD_ARRAY FDPROTO_i1p2_i2p2, const REAL_SIMD_ARRAY invdxx1, const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_144 = 1.0 / 144.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_144 = ConstSIMD(dblFDPart1_Rational_1_144);

  const double dblFDPart1_Rational_1_18 = 1.0 / 18.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_18 = ConstSIMD(dblFDPart1_Rational_1_18);

  const double dblFDPart1_Rational_4_9 = 4.0 / 9.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_9 = ConstSIMD(dblFDPart1_Rational_4_9);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(
      invdxx1,
      MulSIMD(invdxx2,
              FusedMulAddSIMD(
                  FDPart1_Rational_1_18,
                  AddSIMD(AddSIMD(FDPROTO_i1m2_i2p1, FDPROTO_i1p1_i2m2),
                          AddSIMD(FDPROTO_i1p2_i2m1, SubSIMD(FDPROTO_i1m1_i2p2, AddSIMD(AddSIMD(FDPROTO_i1m1_i2m2, FDPROTO_i1m2_i2m1),
                                                                                        AddSIMD(FDPROTO_i1p1_i2p2, FDPROTO_i1p2_i2p1))))),
                  FusedMulAddSIMD(FDPart1_Rational_4_9,
                                  AddSIMD(FDPROTO_i1p1_i2p1, SubSIMD(FDPROTO_i1m1_i2m1, AddSIMD(FDPROTO_i1m1_i2p1, FDPROTO_i1p1_i2m1))),
                                  MulSIMD(FDPart1_Rational_1_144,
                                          AddSIMD(FDPROTO_i1p2_i2p2, SubSIMD(FDPROTO_i1m2_i2m2, AddSIMD(FDPROTO_i1m2_i2p2, FDPROTO_i1p2_i2m2))))))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD22, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD22_fdorder4(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2m2, const REAL_SIMD_ARRAY FDPROTO_i2p1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2p2, const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_12 = 1.0 / 12.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_12 = ConstSIMD(dblFDPart1_Rational_1_12);

  const double dblFDPart1_Rational_4_3 = 4.0 / 3.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_3 = ConstSIMD(dblFDPart1_Rational_4_3);

  const double dblFDPart1_Rational_5_2 = 5.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_5_2 = ConstSIMD(dblFDPart1_Rational_5_2);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(invdxx2, invdxx2),
              FusedMulSubSIMD(FDPart1_Rational_4_3, AddSIMD(FDPROTO_i2m1, FDPROTO_i2p1),
                              FusedMulAddSIMD(FDPROTO, FDPart1_Rational_5_2, MulSIMD(FDPart1_Rational_1_12, AddSIMD(FDPROTO_i2m2, FDPROTO_i2p2)))));

  return FD_result;
}
/**
 * Finite difference function for operator dKOD0, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dKOD0_fdorder4(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i0m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0m2, const REAL_SIMD_ARRAY FDPROTO_i0m3,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p1, const REAL_SIMD_ARRAY FDPROTO_i0p2,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p3, const REAL_SIMD_ARRAY invdxx0) {
  const double dblFDPart1_Rational_15_64 = 15.0 / 64.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_15_64 = ConstSIMD(dblFDPart1_Rational_15_64);

  const double dblFDPart1_Rational_1_64 = 1.0 / 64.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_64 = ConstSIMD(dblFDPart1_Rational_1_64);

  const double dblFDPart1_Rational_3_32 = 3.0 / 32.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_3_32 = ConstSIMD(dblFDPart1_Rational_3_32);

  const double dblFDPart1_Rational_5_16 = 5.0 / 16.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_5_16 = ConstSIMD(dblFDPart1_Rational_5_16);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(invdxx0, FusedMulAddSIMD(FDPart1_Rational_1_64, AddSIMD(FDPROTO_i0m3, FDPROTO_i0p3),
                                       FusedMulSubSIMD(FDPart1_Rational_15_64, AddSIMD(FDPROTO_i0m1, FDPROTO_i0p1),
                                                       FusedMulAddSIMD(FDPROTO, FDPart1_Rational_5_16,
                                                                       MulSIMD(FDPart1_Rational_3_32, AddSIMD(FDPROTO_i0m2, FDPROTO_i0p2))))));

  return FD_result;
}
/**
 * Finite difference function for operator dKOD1, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dKOD1_fdorder4(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i1m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1m2, const REAL_SIMD_ARRAY FDPROTO_i1m3,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p1, const REAL_SIMD_ARRAY FDPROTO_i1p2,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p3, const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Rational_15_64 = 15.0 / 64.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_15_64 = ConstSIMD(dblFDPart1_Rational_15_64);

  const double dblFDPart1_Rational_1_64 = 1.0 / 64.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_64 = ConstSIMD(dblFDPart1_Rational_1_64);

  const double dblFDPart1_Rational_3_32 = 3.0 / 32.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_3_32 = ConstSIMD(dblFDPart1_Rational_3_32);

  const double dblFDPart1_Rational_5_16 = 5.0 / 16.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_5_16 = ConstSIMD(dblFDPart1_Rational_5_16);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(invdxx1, FusedMulAddSIMD(FDPart1_Rational_1_64, AddSIMD(FDPROTO_i1m3, FDPROTO_i1p3),
                                       FusedMulSubSIMD(FDPart1_Rational_15_64, AddSIMD(FDPROTO_i1m1, FDPROTO_i1p1),
                                                       FusedMulAddSIMD(FDPROTO, FDPart1_Rational_5_16,
                                                                       MulSIMD(FDPart1_Rational_3_32, AddSIMD(FDPROTO_i1m2, FDPROTO_i1p2))))));

  return FD_result;
}
/**
 * Finite difference function for operator dKOD2, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dKOD2_fdorder4(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2m2, const REAL_SIMD_ARRAY FDPROTO_i2m3,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2p1, const REAL_SIMD_ARRAY FDPROTO_i2p2,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2p3, const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_15_64 = 15.0 / 64.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_15_64 = ConstSIMD(dblFDPart1_Rational_15_64);

  const double dblFDPart1_Rational_1_64 = 1.0 / 64.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_64 = ConstSIMD(dblFDPart1_Rational_1_64);

  const double dblFDPart1_Rational_3_32 = 3.0 / 32.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_3_32 = ConstSIMD(dblFDPart1_Rational_3_32);

  const double dblFDPart1_Rational_5_16 = 5.0 / 16.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_5_16 = ConstSIMD(dblFDPart1_Rational_5_16);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(invdxx2, FusedMulAddSIMD(FDPart1_Rational_1_64, AddSIMD(FDPROTO_i2m3, FDPROTO_i2p3),
                                       FusedMulSubSIMD(FDPart1_Rational_15_64, AddSIMD(FDPROTO_i2m1, FDPROTO_i2p1),
                                                       FusedMulAddSIMD(FDPROTO, FDPart1_Rational_5_16,
                                                                       MulSIMD(FDPart1_Rational_3_32, AddSIMD(FDPROTO_i2m2, FDPROTO_i2p2))))));

  return FD_result;
}
/**
 * Finite difference function for operator ddnD0, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_ddnD0_fdorder4(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i0m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0m2, const REAL_SIMD_ARRAY FDPROTO_i0m3,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p1, const REAL_SIMD_ARRAY invdxx0) {
  const double dblFDPart1_Rational_1_12 = 1.0 / 12.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_12 = ConstSIMD(dblFDPart1_Rational_1_12);

  const double dblFDPart1_Rational_1_2 = 1.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_2 = ConstSIMD(dblFDPart1_Rational_1_2);

  const double dblFDPart1_Rational_1_4 = 1.0 / 4.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_4 = ConstSIMD(dblFDPart1_Rational_1_4);

  const double dblFDPart1_Rational_3_2 = 3.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_3_2 = ConstSIMD(dblFDPart1_Rational_3_2);

  const double dblFDPart1_Rational_5_6 = 5.0 / 6.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_5_6 = ConstSIMD(dblFDPart1_Rational_5_6);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(invdxx0, FusedMulAddSIMD(FDPROTO_i0m2, FDPart1_Rational_1_2,
                                       FusedMulAddSIMD(FDPROTO_i0p1, FDPart1_Rational_1_4,
                                                       FusedMulSubSIMD(FDPROTO, FDPart1_Rational_5_6,
                                                                       FusedMulAddSIMD(FDPROTO_i0m1, FDPart1_Rational_3_2,
                                                                                       MulSIMD(FDPROTO_i0m3, FDPart1_Rational_1_12))))));

  return FD_result;
}
/**
 * Finite difference function for operator ddnD1, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_ddnD1_fdorder4(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i1m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1m2, const REAL_SIMD_ARRAY FDPROTO_i1m3,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p1, const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Rational_1_12 = 1.0 / 12.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_12 = ConstSIMD(dblFDPart1_Rational_1_12);

  const double dblFDPart1_Rational_1_2 = 1.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_2 = ConstSIMD(dblFDPart1_Rational_1_2);

  const double dblFDPart1_Rational_1_4 = 1.0 / 4.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_4 = ConstSIMD(dblFDPart1_Rational_1_4);

  const double dblFDPart1_Rational_3_2 = 3.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_3_2 = ConstSIMD(dblFDPart1_Rational_3_2);

  const double dblFDPart1_Rational_5_6 = 5.0 / 6.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_5_6 = ConstSIMD(dblFDPart1_Rational_5_6);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(invdxx1, FusedMulAddSIMD(FDPROTO_i1m2, FDPart1_Rational_1_2,
                                       FusedMulAddSIMD(FDPROTO_i1p1, FDPart1_Rational_1_4,
                                                       FusedMulSubSIMD(FDPROTO, FDPart1_Rational_5_6,
                                                                       FusedMulAddSIMD(FDPROTO_i1m1, FDPart1_Rational_3_2,
                                                                                       MulSIMD(FDPROTO_i1m3, FDPart1_Rational_1_12))))));

  return FD_result;
}
/**
 * Finite difference function for operator ddnD2, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_ddnD2_fdorder4(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2m2, const REAL_SIMD_ARRAY FDPROTO_i2m3,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2p1, const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_12 = 1.0 / 12.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_12 = ConstSIMD(dblFDPart1_Rational_1_12);

  const double dblFDPart1_Rational_1_2 = 1.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_2 = ConstSIMD(dblFDPart1_Rational_1_2);

  const double dblFDPart1_Rational_1_4 = 1.0 / 4.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_4 = ConstSIMD(dblFDPart1_Rational_1_4);

  const double dblFDPart1_Rational_3_2 = 3.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_3_2 = ConstSIMD(dblFDPart1_Rational_3_2);

  const double dblFDPart1_Rational_5_6 = 5.0 / 6.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_5_6 = ConstSIMD(dblFDPart1_Rational_5_6);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(invdxx2, FusedMulAddSIMD(FDPROTO_i2m2, FDPart1_Rational_1_2,
                                       FusedMulAddSIMD(FDPROTO_i2p1, FDPart1_Rational_1_4,
                                                       FusedMulSubSIMD(FDPROTO, FDPart1_Rational_5_6,
                                                                       FusedMulAddSIMD(FDPROTO_i2m1, FDPart1_Rational_3_2,
                                                                                       MulSIMD(FDPROTO_i2m3, FDPart1_Rational_1_12))))));

  return FD_result;
}
/**
 * Finite difference function for operator dupD0, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dupD0_fdorder4(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i0m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p1, const REAL_SIMD_ARRAY FDPROTO_i0p2,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p3, const REAL_SIMD_ARRAY invdxx0) {
  const double dblFDPart1_Rational_1_12 = 1.0 / 12.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_12 = ConstSIMD(dblFDPart1_Rational_1_12);

  const double dblFDPart1_Rational_1_2 = 1.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_2 = ConstSIMD(dblFDPart1_Rational_1_2);

  const double dblFDPart1_Rational_1_4 = 1.0 / 4.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_4 = ConstSIMD(dblFDPart1_Rational_1_4);

  const double dblFDPart1_Rational_3_2 = 3.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_3_2 = ConstSIMD(dblFDPart1_Rational_3_2);

  const double dblFDPart1_Rational_5_6 = 5.0 / 6.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_5_6 = ConstSIMD(dblFDPart1_Rational_5_6);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(
      invdxx0,
      FusedMulAddSIMD(FDPROTO_i0p3, FDPart1_Rational_1_12,
                      FusedMulSubSIMD(FDPROTO_i0p1, FDPart1_Rational_3_2,
                                      FusedMulAddSIMD(FDPROTO_i0m1, FDPart1_Rational_1_4,
                                                      FusedMulAddSIMD(FDPROTO_i0p2, FDPart1_Rational_1_2, MulSIMD(FDPROTO, FDPart1_Rational_5_6))))));

  return FD_result;
}
/**
 * Finite difference function for operator dupD1, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dupD1_fdorder4(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i1m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p1, const REAL_SIMD_ARRAY FDPROTO_i1p2,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p3, const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Rational_1_12 = 1.0 / 12.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_12 = ConstSIMD(dblFDPart1_Rational_1_12);

  const double dblFDPart1_Rational_1_2 = 1.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_2 = ConstSIMD(dblFDPart1_Rational_1_2);

  const double dblFDPart1_Rational_1_4 = 1.0 / 4.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_4 = ConstSIMD(dblFDPart1_Rational_1_4);

  const double dblFDPart1_Rational_3_2 = 3.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_3_2 = ConstSIMD(dblFDPart1_Rational_3_2);

  const double dblFDPart1_Rational_5_6 = 5.0 / 6.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_5_6 = ConstSIMD(dblFDPart1_Rational_5_6);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(
      invdxx1,
      FusedMulAddSIMD(FDPROTO_i1p3, FDPart1_Rational_1_12,
                      FusedMulSubSIMD(FDPROTO_i1p1, FDPart1_Rational_3_2,
                                      FusedMulAddSIMD(FDPROTO_i1m1, FDPart1_Rational_1_4,
                                                      FusedMulAddSIMD(FDPROTO_i1p2, FDPart1_Rational_1_2, MulSIMD(FDPROTO, FDPart1_Rational_5_6))))));

  return FD_result;
}
/**
 * Finite difference function for operator dupD2, with FD accuracy order 4.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dupD2_fdorder4(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2p1, const REAL_SIMD_ARRAY FDPROTO_i2p2,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2p3, const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_12 = 1.0 / 12.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_12 = ConstSIMD(dblFDPart1_Rational_1_12);

  const double dblFDPart1_Rational_1_2 = 1.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_2 = ConstSIMD(dblFDPart1_Rational_1_2);

  const double dblFDPart1_Rational_1_4 = 1.0 / 4.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_4 = ConstSIMD(dblFDPart1_Rational_1_4);

  const double dblFDPart1_Rational_3_2 = 3.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_3_2 = ConstSIMD(dblFDPart1_Rational_3_2);

  const double dblFDPart1_Rational_5_6 = 5.0 / 6.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_5_6 = ConstSIMD(dblFDPart1_Rational_5_6);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(
      invdxx2,
      FusedMulAddSIMD(FDPROTO_i2p3, FDPart1_Rational_1_12,
                      FusedMulSubSIMD(FDPROTO_i2p1, FDPart1_Rational_3_2,
                                      FusedMulAddSIMD(FDPROTO_i2m1, FDPart1_Rational_1_4,
                                                      FusedMulAddSIMD(FDPROTO_i2p2, FDPart1_Rational_1_2, MulSIMD(FDPROTO, FDPart1_Rational_5_6))))));

  return FD_result;
}

/**
 * Set RHSs for the BSSN evolution equations.
 */
void BaikalVacuum_rhs_eval_order_4(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS_BaikalVacuum_rhs_eval_order_4;

  const REAL_SIMD_ARRAY invdxx0 CCTK_ATTRIBUTE_UNUSED = ConstSIMD(1.0 / CCTK_DELTA_SPACE(0));
  const REAL_SIMD_ARRAY invdxx1 CCTK_ATTRIBUTE_UNUSED = ConstSIMD(1.0 / CCTK_DELTA_SPACE(1));
  const REAL_SIMD_ARRAY invdxx2 CCTK_ATTRIBUTE_UNUSED = ConstSIMD(1.0 / CCTK_DELTA_SPACE(2));
  const CCTK_REAL *param_PI CCTK_ATTRIBUTE_UNUSED = CCTK_ParameterGet("PI", "BaikalVacuum", NULL);
  const REAL_SIMD_ARRAY PI CCTK_ATTRIBUTE_UNUSED = ConstSIMD(*param_PI);
  const CCTK_REAL *param_eta CCTK_ATTRIBUTE_UNUSED = CCTK_ParameterGet("eta", "BaikalVacuum", NULL);
  const REAL_SIMD_ARRAY eta CCTK_ATTRIBUTE_UNUSED = ConstSIMD(*param_eta);

  const CCTK_REAL *param_diss_strength CCTK_ATTRIBUTE_UNUSED = CCTK_ParameterGet("diss_strength", "BaikalVacuum", NULL);
  const REAL_SIMD_ARRAY diss_strength CCTK_ATTRIBUTE_UNUSED = ConstSIMD(*param_diss_strength);
#pragma omp parallel for
  for (int i2 = cctk_nghostzones[2]; i2 < cctk_lsh[2] - cctk_nghostzones[2]; i2++) {
    for (int i1 = cctk_nghostzones[1]; i1 < cctk_lsh[1] - cctk_nghostzones[1]; i1++) {
      for (int i0 = cctk_nghostzones[0]; i0 < cctk_lsh[0] - cctk_nghostzones[0]; i0 += simd_width) {
        /*
         * NRPy+-Generated GF Access/FD Code, Step 1 of 3:
         * Read gridfunction(s) from main memory and compute FD stencils as needed.
         */
        const REAL_SIMD_ARRAY RbarDD00 = ReadSIMD(&RbarDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY RbarDD01 = ReadSIMD(&RbarDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY RbarDD02 = ReadSIMD(&RbarDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY RbarDD11 = ReadSIMD(&RbarDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY RbarDD12 = ReadSIMD(&RbarDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY RbarDD22 = ReadSIMD(&RbarDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i2m3 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY aDD00_i2m2 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY aDD00_i2m1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD00_i1m3 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY aDD00_i1m2 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY aDD00_i1m1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0m3 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0m2 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0m1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0p1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0p2 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0p3 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i1p1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i1p2 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY aDD00_i1p3 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY aDD00_i2p1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD00_i2p2 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY aDD00_i2p3 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY aDD01_i2m3 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY aDD01_i2m2 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY aDD01_i2m1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD01_i1m3 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY aDD01_i1m2 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY aDD01_i1m1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0m3 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0m2 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0m1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0p1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0p2 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0p3 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i1p1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i1p2 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY aDD01_i1p3 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY aDD01_i2p1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD01_i2p2 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY aDD01_i2p3 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY aDD02_i2m3 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY aDD02_i2m2 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY aDD02_i2m1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD02_i1m3 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY aDD02_i1m2 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY aDD02_i1m1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0m3 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0m2 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0m1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0p1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0p2 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0p3 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i1p1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i1p2 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY aDD02_i1p3 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY aDD02_i2p1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD02_i2p2 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY aDD02_i2p3 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY aDD11_i2m3 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY aDD11_i2m2 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY aDD11_i2m1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD11_i1m3 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY aDD11_i1m2 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY aDD11_i1m1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0m3 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0m2 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0m1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0p1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0p2 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0p3 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i1p1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i1p2 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY aDD11_i1p3 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY aDD11_i2p1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD11_i2p2 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY aDD11_i2p3 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY aDD12_i2m3 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY aDD12_i2m2 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY aDD12_i2m1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD12_i1m3 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY aDD12_i1m2 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY aDD12_i1m1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0m3 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0m2 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0m1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0p1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0p2 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0p3 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i1p1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i1p2 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY aDD12_i1p3 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY aDD12_i2p1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD12_i2p2 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY aDD12_i2p3 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY aDD22_i2m3 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY aDD22_i2m2 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY aDD22_i2m1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD22_i1m3 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY aDD22_i1m2 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY aDD22_i1m1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0m3 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0m2 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0m1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0p1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0p2 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0p3 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i1p1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i1p2 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY aDD22_i1p3 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY aDD22_i2p1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD22_i2p2 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY aDD22_i2p3 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY alpha_i2m3 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY alpha_i1m2_i2m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 2)]);
        const REAL_SIMD_ARRAY alpha_i1m1_i2m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 2)]);
        const REAL_SIMD_ARRAY alpha_i0m2_i2m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY alpha_i0m1_i2m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY alpha_i2m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY alpha_i0p1_i2m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY alpha_i0p2_i2m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY alpha_i1p1_i2m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 2)]);
        const REAL_SIMD_ARRAY alpha_i1p2_i2m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 2)]);
        const REAL_SIMD_ARRAY alpha_i1m2_i2m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 1)]);
        const REAL_SIMD_ARRAY alpha_i1m1_i2m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY alpha_i0m2_i2m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY alpha_i0m1_i2m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY alpha_i2m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY alpha_i0p1_i2m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY alpha_i0p2_i2m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY alpha_i1p1_i2m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY alpha_i1p2_i2m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 1)]);
        const REAL_SIMD_ARRAY alpha_i1m3 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY alpha_i0m2_i1m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY alpha_i0m1_i1m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY alpha_i1m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY alpha_i0p1_i1m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY alpha_i0p2_i1m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY alpha_i0m2_i1m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0m1_i1m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY alpha_i1m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0p1_i1m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0p2_i1m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0m3 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0m2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0m1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY alpha = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0p3 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0m2_i1p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0m1_i1p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY alpha_i1p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0p1_i1p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0p2_i1p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY alpha_i0m2_i1p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY alpha_i0m1_i1p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY alpha_i1p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY alpha_i0p1_i1p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY alpha_i0p2_i1p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY alpha_i1p3 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY alpha_i1m2_i2p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 1)]);
        const REAL_SIMD_ARRAY alpha_i1m1_i2p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY alpha_i0m2_i2p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY alpha_i0m1_i2p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY alpha_i2p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY alpha_i0p1_i2p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY alpha_i0p2_i2p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY alpha_i1p1_i2p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY alpha_i1p2_i2p1 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 1)]);
        const REAL_SIMD_ARRAY alpha_i1m2_i2p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 2)]);
        const REAL_SIMD_ARRAY alpha_i1m1_i2p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 2)]);
        const REAL_SIMD_ARRAY alpha_i0m2_i2p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY alpha_i0m1_i2p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY alpha_i2p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY alpha_i0p1_i2p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY alpha_i0p2_i2p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY alpha_i1p1_i2p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 2)]);
        const REAL_SIMD_ARRAY alpha_i1p2_i2p2 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 2)]);
        const REAL_SIMD_ARRAY alpha_i2p3 = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY betU0_i2m3 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY betU0_i2m2 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY betU0_i2m1 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY betU0_i1m3 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY betU0_i1m2 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY betU0_i1m1 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY betU0_i0m3 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY betU0_i0m2 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY betU0_i0m1 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY betU0 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY betU0_i0p1 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY betU0_i0p2 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY betU0_i0p3 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY betU0_i1p1 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY betU0_i1p2 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY betU0_i1p3 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY betU0_i2p1 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY betU0_i2p2 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY betU0_i2p3 = ReadSIMD(&betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY betU1_i2m3 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY betU1_i2m2 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY betU1_i2m1 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY betU1_i1m3 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY betU1_i1m2 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY betU1_i1m1 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY betU1_i0m3 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY betU1_i0m2 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY betU1_i0m1 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY betU1 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY betU1_i0p1 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY betU1_i0p2 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY betU1_i0p3 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY betU1_i1p1 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY betU1_i1p2 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY betU1_i1p3 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY betU1_i2p1 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY betU1_i2p2 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY betU1_i2p3 = ReadSIMD(&betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY betU2_i2m3 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY betU2_i2m2 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY betU2_i2m1 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY betU2_i1m3 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY betU2_i1m2 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY betU2_i1m1 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY betU2_i0m3 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY betU2_i0m2 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY betU2_i0m1 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY betU2 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY betU2_i0p1 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY betU2_i0p2 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY betU2_i0p3 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY betU2_i1p1 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY betU2_i1p2 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY betU2_i1p3 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY betU2_i2p1 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY betU2_i2p2 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY betU2_i2p3 = ReadSIMD(&betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i1m2_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i1p2_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i1m2_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0m2_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0p2_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i1p2_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i1m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY cf = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i1p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY cf_i1m2_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0m2_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0p2_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i1p2_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i1m2_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i1p2_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i1m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i1m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i1m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i1m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i1m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i1m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY lambdaU0_i2m3 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY lambdaU0_i2m2 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY lambdaU0_i2m1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY lambdaU0_i1m3 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1m2 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1m1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0m3 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0m2 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0m1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0p1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0p2 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0p3 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1p1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1p2 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1p3 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i2p1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY lambdaU0_i2p2 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY lambdaU0_i2p3 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY lambdaU1_i2m3 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY lambdaU1_i2m2 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY lambdaU1_i2m1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY lambdaU1_i1m3 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1m2 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1m1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0m3 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0m2 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0m1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0p1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0p2 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0p3 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1p1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1p2 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1p3 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i2p1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY lambdaU1_i2p2 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY lambdaU1_i2p3 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY lambdaU2_i2m3 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY lambdaU2_i2m2 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY lambdaU2_i2m1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY lambdaU2_i1m3 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1m2 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1m1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0m3 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0m2 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0m1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0p1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0p2 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0p3 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1p1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1p2 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1p3 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i2p1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY lambdaU2_i2p2 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY lambdaU2_i2p3 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY trK_i2m3 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY trK_i2m2 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY trK_i2m1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY trK_i1m3 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY trK_i1m2 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY trK_i1m1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY trK_i0m3 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i0m2 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i0m1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY trK = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i0p1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i0p2 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i0p3 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i1p1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY trK_i1p2 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY trK_i1p3 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY trK_i2p1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY trK_i2p2 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY trK_i2p3 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY vetU0_i2m3 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY vetU0_i1m2_i2m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU0_i1m1_i2m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU0_i0m2_i2m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU0_i0m1_i2m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU0_i2m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU0_i0p1_i2m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU0_i0p2_i2m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU0_i1p1_i2m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU0_i1p2_i2m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU0_i1m2_i2m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU0_i1m1_i2m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU0_i0m2_i2m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU0_i0m1_i2m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU0_i2m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU0_i0p1_i2m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU0_i0p2_i2m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU0_i1p1_i2m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU0_i1p2_i2m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU0_i1m3 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0m2_i1m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0m1_i1m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU0_i1m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0p1_i1m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0p2_i1m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0m2_i1m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0m1_i1m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i1m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0p1_i1m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0p2_i1m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0m3 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0m2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0m1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY vetU0 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0p3 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0m2_i1p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0m1_i1p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i1p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0p1_i1p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0p2_i1p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0m2_i1p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0m1_i1p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU0_i1p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0p1_i1p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU0_i0p2_i1p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU0_i1p3 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY vetU0_i1m2_i2p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU0_i1m1_i2p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU0_i0m2_i2p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU0_i0m1_i2p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU0_i2p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU0_i0p1_i2p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU0_i0p2_i2p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU0_i1p1_i2p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU0_i1p2_i2p1 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU0_i1m2_i2p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU0_i1m1_i2p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU0_i0m2_i2p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU0_i0m1_i2p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU0_i2p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU0_i0p1_i2p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU0_i0p2_i2p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU0_i1p1_i2p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU0_i1p2_i2p2 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU0_i2p3 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY vetU1_i2m3 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY vetU1_i1m2_i2m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU1_i1m1_i2m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU1_i0m2_i2m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU1_i0m1_i2m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU1_i2m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU1_i0p1_i2m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU1_i0p2_i2m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU1_i1p1_i2m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU1_i1p2_i2m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU1_i1m2_i2m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU1_i1m1_i2m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU1_i0m2_i2m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU1_i0m1_i2m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU1_i2m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU1_i0p1_i2m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU1_i0p2_i2m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU1_i1p1_i2m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU1_i1p2_i2m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU1_i1m3 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0m2_i1m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0m1_i1m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU1_i1m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0p1_i1m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0p2_i1m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0m2_i1m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0m1_i1m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i1m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0p1_i1m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0p2_i1m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0m3 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0m2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0m1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY vetU1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0p3 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0m2_i1p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0m1_i1p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i1p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0p1_i1p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0p2_i1p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0m2_i1p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0m1_i1p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU1_i1p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0p1_i1p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU1_i0p2_i1p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU1_i1p3 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY vetU1_i1m2_i2p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU1_i1m1_i2p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU1_i0m2_i2p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU1_i0m1_i2p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU1_i2p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU1_i0p1_i2p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU1_i0p2_i2p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU1_i1p1_i2p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU1_i1p2_i2p1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU1_i1m2_i2p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU1_i1m1_i2p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU1_i0m2_i2p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU1_i0m1_i2p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU1_i2p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU1_i0p1_i2p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU1_i0p2_i2p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU1_i1p1_i2p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU1_i1p2_i2p2 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU1_i2p3 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY vetU2_i2m3 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY vetU2_i1m2_i2m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU2_i1m1_i2m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU2_i0m2_i2m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU2_i0m1_i2m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU2_i2m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU2_i0p1_i2m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU2_i0p2_i2m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU2_i1p1_i2m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU2_i1p2_i2m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 2)]);
        const REAL_SIMD_ARRAY vetU2_i1m2_i2m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU2_i1m1_i2m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU2_i0m2_i2m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU2_i0m1_i2m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU2_i2m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU2_i0p1_i2m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU2_i0p2_i2m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU2_i1p1_i2m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU2_i1p2_i2m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 1)]);
        const REAL_SIMD_ARRAY vetU2_i1m3 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0m2_i1m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0m1_i1m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU2_i1m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0p1_i1m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0p2_i1m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0m2_i1m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0m1_i1m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i1m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0p1_i1m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0p2_i1m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0m3 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0m2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0m1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY vetU2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0p3 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0m2_i1p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0m1_i1p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i1p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0p1_i1p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0p2_i1p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0m2_i1p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0m1_i1p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU2_i1p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0p1_i1p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU2_i0p2_i1p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY vetU2_i1p3 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY vetU2_i1m2_i2p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU2_i1m1_i2p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU2_i0m2_i2p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU2_i0m1_i2p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU2_i2p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU2_i0p1_i2p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU2_i0p2_i2p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU2_i1p1_i2p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU2_i1p2_i2p1 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU2_i1m2_i2p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU2_i1m1_i2p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU2_i0m2_i2p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU2_i0m1_i2p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU2_i2p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU2_i0p1_i2p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU2_i0p2_i2p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU2_i1p1_i2p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU2_i1p2_i2p2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 2)]);
        const REAL_SIMD_ARRAY vetU2_i2p3 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD000 =
            SIMD_fd_function_ddnD0_fdorder4(aDD00, aDD00_i0m1, aDD00_i0m2, aDD00_i0m3, aDD00_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD001 =
            SIMD_fd_function_ddnD1_fdorder4(aDD00, aDD00_i1m1, aDD00_i1m2, aDD00_i1m3, aDD00_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD002 =
            SIMD_fd_function_ddnD2_fdorder4(aDD00, aDD00_i2m1, aDD00_i2m2, aDD00_i2m3, aDD00_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD010 =
            SIMD_fd_function_ddnD0_fdorder4(aDD01, aDD01_i0m1, aDD01_i0m2, aDD01_i0m3, aDD01_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD011 =
            SIMD_fd_function_ddnD1_fdorder4(aDD01, aDD01_i1m1, aDD01_i1m2, aDD01_i1m3, aDD01_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD012 =
            SIMD_fd_function_ddnD2_fdorder4(aDD01, aDD01_i2m1, aDD01_i2m2, aDD01_i2m3, aDD01_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD020 =
            SIMD_fd_function_ddnD0_fdorder4(aDD02, aDD02_i0m1, aDD02_i0m2, aDD02_i0m3, aDD02_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD021 =
            SIMD_fd_function_ddnD1_fdorder4(aDD02, aDD02_i1m1, aDD02_i1m2, aDD02_i1m3, aDD02_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD022 =
            SIMD_fd_function_ddnD2_fdorder4(aDD02, aDD02_i2m1, aDD02_i2m2, aDD02_i2m3, aDD02_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD110 =
            SIMD_fd_function_ddnD0_fdorder4(aDD11, aDD11_i0m1, aDD11_i0m2, aDD11_i0m3, aDD11_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD111 =
            SIMD_fd_function_ddnD1_fdorder4(aDD11, aDD11_i1m1, aDD11_i1m2, aDD11_i1m3, aDD11_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD112 =
            SIMD_fd_function_ddnD2_fdorder4(aDD11, aDD11_i2m1, aDD11_i2m2, aDD11_i2m3, aDD11_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD120 =
            SIMD_fd_function_ddnD0_fdorder4(aDD12, aDD12_i0m1, aDD12_i0m2, aDD12_i0m3, aDD12_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD121 =
            SIMD_fd_function_ddnD1_fdorder4(aDD12, aDD12_i1m1, aDD12_i1m2, aDD12_i1m3, aDD12_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD122 =
            SIMD_fd_function_ddnD2_fdorder4(aDD12, aDD12_i2m1, aDD12_i2m2, aDD12_i2m3, aDD12_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD220 =
            SIMD_fd_function_ddnD0_fdorder4(aDD22, aDD22_i0m1, aDD22_i0m2, aDD22_i0m3, aDD22_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD221 =
            SIMD_fd_function_ddnD1_fdorder4(aDD22, aDD22_i1m1, aDD22_i1m2, aDD22_i1m3, aDD22_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_ddnD222 =
            SIMD_fd_function_ddnD2_fdorder4(aDD22, aDD22_i2m1, aDD22_i2m2, aDD22_i2m3, aDD22_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD000 =
            SIMD_fd_function_dupD0_fdorder4(aDD00, aDD00_i0m1, aDD00_i0p1, aDD00_i0p2, aDD00_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD001 =
            SIMD_fd_function_dupD1_fdorder4(aDD00, aDD00_i1m1, aDD00_i1p1, aDD00_i1p2, aDD00_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD002 =
            SIMD_fd_function_dupD2_fdorder4(aDD00, aDD00_i2m1, aDD00_i2p1, aDD00_i2p2, aDD00_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD010 =
            SIMD_fd_function_dupD0_fdorder4(aDD01, aDD01_i0m1, aDD01_i0p1, aDD01_i0p2, aDD01_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD011 =
            SIMD_fd_function_dupD1_fdorder4(aDD01, aDD01_i1m1, aDD01_i1p1, aDD01_i1p2, aDD01_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD012 =
            SIMD_fd_function_dupD2_fdorder4(aDD01, aDD01_i2m1, aDD01_i2p1, aDD01_i2p2, aDD01_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD020 =
            SIMD_fd_function_dupD0_fdorder4(aDD02, aDD02_i0m1, aDD02_i0p1, aDD02_i0p2, aDD02_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD021 =
            SIMD_fd_function_dupD1_fdorder4(aDD02, aDD02_i1m1, aDD02_i1p1, aDD02_i1p2, aDD02_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD022 =
            SIMD_fd_function_dupD2_fdorder4(aDD02, aDD02_i2m1, aDD02_i2p1, aDD02_i2p2, aDD02_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD110 =
            SIMD_fd_function_dupD0_fdorder4(aDD11, aDD11_i0m1, aDD11_i0p1, aDD11_i0p2, aDD11_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD111 =
            SIMD_fd_function_dupD1_fdorder4(aDD11, aDD11_i1m1, aDD11_i1p1, aDD11_i1p2, aDD11_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD112 =
            SIMD_fd_function_dupD2_fdorder4(aDD11, aDD11_i2m1, aDD11_i2p1, aDD11_i2p2, aDD11_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD120 =
            SIMD_fd_function_dupD0_fdorder4(aDD12, aDD12_i0m1, aDD12_i0p1, aDD12_i0p2, aDD12_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD121 =
            SIMD_fd_function_dupD1_fdorder4(aDD12, aDD12_i1m1, aDD12_i1p1, aDD12_i1p2, aDD12_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD122 =
            SIMD_fd_function_dupD2_fdorder4(aDD12, aDD12_i2m1, aDD12_i2p1, aDD12_i2p2, aDD12_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD220 =
            SIMD_fd_function_dupD0_fdorder4(aDD22, aDD22_i0m1, aDD22_i0p1, aDD22_i0p2, aDD22_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD221 =
            SIMD_fd_function_dupD1_fdorder4(aDD22, aDD22_i1m1, aDD22_i1p1, aDD22_i1p2, aDD22_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputaDD_dupD222 =
            SIMD_fd_function_dupD2_fdorder4(aDD22, aDD22_i2m1, aDD22_i2p1, aDD22_i2p2, aDD22_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputalpha_ddnD0 =
            SIMD_fd_function_ddnD0_fdorder4(alpha, alpha_i0m1, alpha_i0m2, alpha_i0m3, alpha_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputalpha_ddnD1 =
            SIMD_fd_function_ddnD1_fdorder4(alpha, alpha_i1m1, alpha_i1m2, alpha_i1m3, alpha_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputalpha_ddnD2 =
            SIMD_fd_function_ddnD2_fdorder4(alpha, alpha_i2m1, alpha_i2m2, alpha_i2m3, alpha_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputalpha_dupD0 =
            SIMD_fd_function_dupD0_fdorder4(alpha, alpha_i0m1, alpha_i0p1, alpha_i0p2, alpha_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputalpha_dupD1 =
            SIMD_fd_function_dupD1_fdorder4(alpha, alpha_i1m1, alpha_i1p1, alpha_i1p2, alpha_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputalpha_dupD2 =
            SIMD_fd_function_dupD2_fdorder4(alpha, alpha_i2m1, alpha_i2p1, alpha_i2p2, alpha_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_ddnD00 =
            SIMD_fd_function_ddnD0_fdorder4(betU0, betU0_i0m1, betU0_i0m2, betU0_i0m3, betU0_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_ddnD01 =
            SIMD_fd_function_ddnD1_fdorder4(betU0, betU0_i1m1, betU0_i1m2, betU0_i1m3, betU0_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_ddnD02 =
            SIMD_fd_function_ddnD2_fdorder4(betU0, betU0_i2m1, betU0_i2m2, betU0_i2m3, betU0_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_ddnD10 =
            SIMD_fd_function_ddnD0_fdorder4(betU1, betU1_i0m1, betU1_i0m2, betU1_i0m3, betU1_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_ddnD11 =
            SIMD_fd_function_ddnD1_fdorder4(betU1, betU1_i1m1, betU1_i1m2, betU1_i1m3, betU1_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_ddnD12 =
            SIMD_fd_function_ddnD2_fdorder4(betU1, betU1_i2m1, betU1_i2m2, betU1_i2m3, betU1_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_ddnD20 =
            SIMD_fd_function_ddnD0_fdorder4(betU2, betU2_i0m1, betU2_i0m2, betU2_i0m3, betU2_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_ddnD21 =
            SIMD_fd_function_ddnD1_fdorder4(betU2, betU2_i1m1, betU2_i1m2, betU2_i1m3, betU2_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_ddnD22 =
            SIMD_fd_function_ddnD2_fdorder4(betU2, betU2_i2m1, betU2_i2m2, betU2_i2m3, betU2_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_dupD00 =
            SIMD_fd_function_dupD0_fdorder4(betU0, betU0_i0m1, betU0_i0p1, betU0_i0p2, betU0_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_dupD01 =
            SIMD_fd_function_dupD1_fdorder4(betU0, betU0_i1m1, betU0_i1p1, betU0_i1p2, betU0_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_dupD02 =
            SIMD_fd_function_dupD2_fdorder4(betU0, betU0_i2m1, betU0_i2p1, betU0_i2p2, betU0_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_dupD10 =
            SIMD_fd_function_dupD0_fdorder4(betU1, betU1_i0m1, betU1_i0p1, betU1_i0p2, betU1_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_dupD11 =
            SIMD_fd_function_dupD1_fdorder4(betU1, betU1_i1m1, betU1_i1p1, betU1_i1p2, betU1_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_dupD12 =
            SIMD_fd_function_dupD2_fdorder4(betU1, betU1_i2m1, betU1_i2p1, betU1_i2p2, betU1_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_dupD20 =
            SIMD_fd_function_dupD0_fdorder4(betU2, betU2_i0m1, betU2_i0p1, betU2_i0p2, betU2_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_dupD21 =
            SIMD_fd_function_dupD1_fdorder4(betU2, betU2_i1m1, betU2_i1p1, betU2_i1p2, betU2_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputbetU_dupD22 =
            SIMD_fd_function_dupD2_fdorder4(betU2, betU2_i2m1, betU2_i2p1, betU2_i2p2, betU2_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputcf_ddnD0 = SIMD_fd_function_ddnD0_fdorder4(cf, cf_i0m1, cf_i0m2, cf_i0m3, cf_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputcf_ddnD1 = SIMD_fd_function_ddnD1_fdorder4(cf, cf_i1m1, cf_i1m2, cf_i1m3, cf_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputcf_ddnD2 = SIMD_fd_function_ddnD2_fdorder4(cf, cf_i2m1, cf_i2m2, cf_i2m3, cf_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputcf_dupD0 = SIMD_fd_function_dupD0_fdorder4(cf, cf_i0m1, cf_i0p1, cf_i0p2, cf_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputcf_dupD1 = SIMD_fd_function_dupD1_fdorder4(cf, cf_i1m1, cf_i1p1, cf_i1p2, cf_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputcf_dupD2 = SIMD_fd_function_dupD2_fdorder4(cf, cf_i2m1, cf_i2p1, cf_i2p2, cf_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD000 =
            SIMD_fd_function_ddnD0_fdorder4(hDD00, hDD00_i0m1, hDD00_i0m2, hDD00_i0m3, hDD00_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD001 =
            SIMD_fd_function_ddnD1_fdorder4(hDD00, hDD00_i1m1, hDD00_i1m2, hDD00_i1m3, hDD00_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD002 =
            SIMD_fd_function_ddnD2_fdorder4(hDD00, hDD00_i2m1, hDD00_i2m2, hDD00_i2m3, hDD00_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD010 =
            SIMD_fd_function_ddnD0_fdorder4(hDD01, hDD01_i0m1, hDD01_i0m2, hDD01_i0m3, hDD01_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD011 =
            SIMD_fd_function_ddnD1_fdorder4(hDD01, hDD01_i1m1, hDD01_i1m2, hDD01_i1m3, hDD01_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD012 =
            SIMD_fd_function_ddnD2_fdorder4(hDD01, hDD01_i2m1, hDD01_i2m2, hDD01_i2m3, hDD01_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD020 =
            SIMD_fd_function_ddnD0_fdorder4(hDD02, hDD02_i0m1, hDD02_i0m2, hDD02_i0m3, hDD02_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD021 =
            SIMD_fd_function_ddnD1_fdorder4(hDD02, hDD02_i1m1, hDD02_i1m2, hDD02_i1m3, hDD02_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD022 =
            SIMD_fd_function_ddnD2_fdorder4(hDD02, hDD02_i2m1, hDD02_i2m2, hDD02_i2m3, hDD02_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD110 =
            SIMD_fd_function_ddnD0_fdorder4(hDD11, hDD11_i0m1, hDD11_i0m2, hDD11_i0m3, hDD11_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD111 =
            SIMD_fd_function_ddnD1_fdorder4(hDD11, hDD11_i1m1, hDD11_i1m2, hDD11_i1m3, hDD11_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD112 =
            SIMD_fd_function_ddnD2_fdorder4(hDD11, hDD11_i2m1, hDD11_i2m2, hDD11_i2m3, hDD11_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD120 =
            SIMD_fd_function_ddnD0_fdorder4(hDD12, hDD12_i0m1, hDD12_i0m2, hDD12_i0m3, hDD12_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD121 =
            SIMD_fd_function_ddnD1_fdorder4(hDD12, hDD12_i1m1, hDD12_i1m2, hDD12_i1m3, hDD12_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD122 =
            SIMD_fd_function_ddnD2_fdorder4(hDD12, hDD12_i2m1, hDD12_i2m2, hDD12_i2m3, hDD12_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD220 =
            SIMD_fd_function_ddnD0_fdorder4(hDD22, hDD22_i0m1, hDD22_i0m2, hDD22_i0m3, hDD22_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD221 =
            SIMD_fd_function_ddnD1_fdorder4(hDD22, hDD22_i1m1, hDD22_i1m2, hDD22_i1m3, hDD22_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_ddnD222 =
            SIMD_fd_function_ddnD2_fdorder4(hDD22, hDD22_i2m1, hDD22_i2m2, hDD22_i2m3, hDD22_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD000 =
            SIMD_fd_function_dupD0_fdorder4(hDD00, hDD00_i0m1, hDD00_i0p1, hDD00_i0p2, hDD00_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD001 =
            SIMD_fd_function_dupD1_fdorder4(hDD00, hDD00_i1m1, hDD00_i1p1, hDD00_i1p2, hDD00_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD002 =
            SIMD_fd_function_dupD2_fdorder4(hDD00, hDD00_i2m1, hDD00_i2p1, hDD00_i2p2, hDD00_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD010 =
            SIMD_fd_function_dupD0_fdorder4(hDD01, hDD01_i0m1, hDD01_i0p1, hDD01_i0p2, hDD01_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD011 =
            SIMD_fd_function_dupD1_fdorder4(hDD01, hDD01_i1m1, hDD01_i1p1, hDD01_i1p2, hDD01_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD012 =
            SIMD_fd_function_dupD2_fdorder4(hDD01, hDD01_i2m1, hDD01_i2p1, hDD01_i2p2, hDD01_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD020 =
            SIMD_fd_function_dupD0_fdorder4(hDD02, hDD02_i0m1, hDD02_i0p1, hDD02_i0p2, hDD02_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD021 =
            SIMD_fd_function_dupD1_fdorder4(hDD02, hDD02_i1m1, hDD02_i1p1, hDD02_i1p2, hDD02_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD022 =
            SIMD_fd_function_dupD2_fdorder4(hDD02, hDD02_i2m1, hDD02_i2p1, hDD02_i2p2, hDD02_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD110 =
            SIMD_fd_function_dupD0_fdorder4(hDD11, hDD11_i0m1, hDD11_i0p1, hDD11_i0p2, hDD11_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD111 =
            SIMD_fd_function_dupD1_fdorder4(hDD11, hDD11_i1m1, hDD11_i1p1, hDD11_i1p2, hDD11_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD112 =
            SIMD_fd_function_dupD2_fdorder4(hDD11, hDD11_i2m1, hDD11_i2p1, hDD11_i2p2, hDD11_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD120 =
            SIMD_fd_function_dupD0_fdorder4(hDD12, hDD12_i0m1, hDD12_i0p1, hDD12_i0p2, hDD12_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD121 =
            SIMD_fd_function_dupD1_fdorder4(hDD12, hDD12_i1m1, hDD12_i1p1, hDD12_i1p2, hDD12_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD122 =
            SIMD_fd_function_dupD2_fdorder4(hDD12, hDD12_i2m1, hDD12_i2p1, hDD12_i2p2, hDD12_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD220 =
            SIMD_fd_function_dupD0_fdorder4(hDD22, hDD22_i0m1, hDD22_i0p1, hDD22_i0p2, hDD22_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD221 =
            SIMD_fd_function_dupD1_fdorder4(hDD22, hDD22_i1m1, hDD22_i1p1, hDD22_i1p2, hDD22_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputhDD_dupD222 =
            SIMD_fd_function_dupD2_fdorder4(hDD22, hDD22_i2m1, hDD22_i2p1, hDD22_i2p2, hDD22_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_ddnD00 =
            SIMD_fd_function_ddnD0_fdorder4(lambdaU0, lambdaU0_i0m1, lambdaU0_i0m2, lambdaU0_i0m3, lambdaU0_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_ddnD01 =
            SIMD_fd_function_ddnD1_fdorder4(lambdaU0, lambdaU0_i1m1, lambdaU0_i1m2, lambdaU0_i1m3, lambdaU0_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_ddnD02 =
            SIMD_fd_function_ddnD2_fdorder4(lambdaU0, lambdaU0_i2m1, lambdaU0_i2m2, lambdaU0_i2m3, lambdaU0_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_ddnD10 =
            SIMD_fd_function_ddnD0_fdorder4(lambdaU1, lambdaU1_i0m1, lambdaU1_i0m2, lambdaU1_i0m3, lambdaU1_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_ddnD11 =
            SIMD_fd_function_ddnD1_fdorder4(lambdaU1, lambdaU1_i1m1, lambdaU1_i1m2, lambdaU1_i1m3, lambdaU1_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_ddnD12 =
            SIMD_fd_function_ddnD2_fdorder4(lambdaU1, lambdaU1_i2m1, lambdaU1_i2m2, lambdaU1_i2m3, lambdaU1_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_ddnD20 =
            SIMD_fd_function_ddnD0_fdorder4(lambdaU2, lambdaU2_i0m1, lambdaU2_i0m2, lambdaU2_i0m3, lambdaU2_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_ddnD21 =
            SIMD_fd_function_ddnD1_fdorder4(lambdaU2, lambdaU2_i1m1, lambdaU2_i1m2, lambdaU2_i1m3, lambdaU2_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_ddnD22 =
            SIMD_fd_function_ddnD2_fdorder4(lambdaU2, lambdaU2_i2m1, lambdaU2_i2m2, lambdaU2_i2m3, lambdaU2_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_dupD00 =
            SIMD_fd_function_dupD0_fdorder4(lambdaU0, lambdaU0_i0m1, lambdaU0_i0p1, lambdaU0_i0p2, lambdaU0_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_dupD01 =
            SIMD_fd_function_dupD1_fdorder4(lambdaU0, lambdaU0_i1m1, lambdaU0_i1p1, lambdaU0_i1p2, lambdaU0_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_dupD02 =
            SIMD_fd_function_dupD2_fdorder4(lambdaU0, lambdaU0_i2m1, lambdaU0_i2p1, lambdaU0_i2p2, lambdaU0_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_dupD10 =
            SIMD_fd_function_dupD0_fdorder4(lambdaU1, lambdaU1_i0m1, lambdaU1_i0p1, lambdaU1_i0p2, lambdaU1_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_dupD11 =
            SIMD_fd_function_dupD1_fdorder4(lambdaU1, lambdaU1_i1m1, lambdaU1_i1p1, lambdaU1_i1p2, lambdaU1_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_dupD12 =
            SIMD_fd_function_dupD2_fdorder4(lambdaU1, lambdaU1_i2m1, lambdaU1_i2p1, lambdaU1_i2p2, lambdaU1_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_dupD20 =
            SIMD_fd_function_dupD0_fdorder4(lambdaU2, lambdaU2_i0m1, lambdaU2_i0p1, lambdaU2_i0p2, lambdaU2_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_dupD21 =
            SIMD_fd_function_dupD1_fdorder4(lambdaU2, lambdaU2_i1m1, lambdaU2_i1p1, lambdaU2_i1p2, lambdaU2_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputlambdaU_dupD22 =
            SIMD_fd_function_dupD2_fdorder4(lambdaU2, lambdaU2_i2m1, lambdaU2_i2p1, lambdaU2_i2p2, lambdaU2_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputtrK_ddnD0 = SIMD_fd_function_ddnD0_fdorder4(trK, trK_i0m1, trK_i0m2, trK_i0m3, trK_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputtrK_ddnD1 = SIMD_fd_function_ddnD1_fdorder4(trK, trK_i1m1, trK_i1m2, trK_i1m3, trK_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputtrK_ddnD2 = SIMD_fd_function_ddnD2_fdorder4(trK, trK_i2m1, trK_i2m2, trK_i2m3, trK_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputtrK_dupD0 = SIMD_fd_function_dupD0_fdorder4(trK, trK_i0m1, trK_i0p1, trK_i0p2, trK_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputtrK_dupD1 = SIMD_fd_function_dupD1_fdorder4(trK, trK_i1m1, trK_i1p1, trK_i1p2, trK_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputtrK_dupD2 = SIMD_fd_function_dupD2_fdorder4(trK, trK_i2m1, trK_i2p1, trK_i2p2, trK_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_ddnD00 =
            SIMD_fd_function_ddnD0_fdorder4(vetU0, vetU0_i0m1, vetU0_i0m2, vetU0_i0m3, vetU0_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_ddnD01 =
            SIMD_fd_function_ddnD1_fdorder4(vetU0, vetU0_i1m1, vetU0_i1m2, vetU0_i1m3, vetU0_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_ddnD02 =
            SIMD_fd_function_ddnD2_fdorder4(vetU0, vetU0_i2m1, vetU0_i2m2, vetU0_i2m3, vetU0_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_ddnD10 =
            SIMD_fd_function_ddnD0_fdorder4(vetU1, vetU1_i0m1, vetU1_i0m2, vetU1_i0m3, vetU1_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_ddnD11 =
            SIMD_fd_function_ddnD1_fdorder4(vetU1, vetU1_i1m1, vetU1_i1m2, vetU1_i1m3, vetU1_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_ddnD12 =
            SIMD_fd_function_ddnD2_fdorder4(vetU1, vetU1_i2m1, vetU1_i2m2, vetU1_i2m3, vetU1_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_ddnD20 =
            SIMD_fd_function_ddnD0_fdorder4(vetU2, vetU2_i0m1, vetU2_i0m2, vetU2_i0m3, vetU2_i0p1, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_ddnD21 =
            SIMD_fd_function_ddnD1_fdorder4(vetU2, vetU2_i1m1, vetU2_i1m2, vetU2_i1m3, vetU2_i1p1, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_ddnD22 =
            SIMD_fd_function_ddnD2_fdorder4(vetU2, vetU2_i2m1, vetU2_i2m2, vetU2_i2m3, vetU2_i2p1, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_dupD00 =
            SIMD_fd_function_dupD0_fdorder4(vetU0, vetU0_i0m1, vetU0_i0p1, vetU0_i0p2, vetU0_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_dupD01 =
            SIMD_fd_function_dupD1_fdorder4(vetU0, vetU0_i1m1, vetU0_i1p1, vetU0_i1p2, vetU0_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_dupD02 =
            SIMD_fd_function_dupD2_fdorder4(vetU0, vetU0_i2m1, vetU0_i2p1, vetU0_i2p2, vetU0_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_dupD10 =
            SIMD_fd_function_dupD0_fdorder4(vetU1, vetU1_i0m1, vetU1_i0p1, vetU1_i0p2, vetU1_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_dupD11 =
            SIMD_fd_function_dupD1_fdorder4(vetU1, vetU1_i1m1, vetU1_i1p1, vetU1_i1p2, vetU1_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_dupD12 =
            SIMD_fd_function_dupD2_fdorder4(vetU1, vetU1_i2m1, vetU1_i2p1, vetU1_i2p2, vetU1_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_dupD20 =
            SIMD_fd_function_dupD0_fdorder4(vetU2, vetU2_i0m1, vetU2_i0p1, vetU2_i0p2, vetU2_i0p3, invdxx0);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_dupD21 =
            SIMD_fd_function_dupD1_fdorder4(vetU2, vetU2_i1m1, vetU2_i1p1, vetU2_i1p2, vetU2_i1p3, invdxx1);
        const REAL_SIMD_ARRAY UpwindAlgInputvetU_dupD22 =
            SIMD_fd_function_dupD2_fdorder4(vetU2, vetU2_i2m1, vetU2_i2p1, vetU2_i2p2, vetU2_i2p3, invdxx2);
        const REAL_SIMD_ARRAY aDD_dKOD000 =
            SIMD_fd_function_dKOD0_fdorder4(aDD00, aDD00_i0m1, aDD00_i0m2, aDD00_i0m3, aDD00_i0p1, aDD00_i0p2, aDD00_i0p3, invdxx0);
        const REAL_SIMD_ARRAY aDD_dKOD001 =
            SIMD_fd_function_dKOD1_fdorder4(aDD00, aDD00_i1m1, aDD00_i1m2, aDD00_i1m3, aDD00_i1p1, aDD00_i1p2, aDD00_i1p3, invdxx1);
        const REAL_SIMD_ARRAY aDD_dKOD002 =
            SIMD_fd_function_dKOD2_fdorder4(aDD00, aDD00_i2m1, aDD00_i2m2, aDD00_i2m3, aDD00_i2p1, aDD00_i2p2, aDD00_i2p3, invdxx2);
        const REAL_SIMD_ARRAY aDD_dKOD010 =
            SIMD_fd_function_dKOD0_fdorder4(aDD01, aDD01_i0m1, aDD01_i0m2, aDD01_i0m3, aDD01_i0p1, aDD01_i0p2, aDD01_i0p3, invdxx0);
        const REAL_SIMD_ARRAY aDD_dKOD011 =
            SIMD_fd_function_dKOD1_fdorder4(aDD01, aDD01_i1m1, aDD01_i1m2, aDD01_i1m3, aDD01_i1p1, aDD01_i1p2, aDD01_i1p3, invdxx1);
        const REAL_SIMD_ARRAY aDD_dKOD012 =
            SIMD_fd_function_dKOD2_fdorder4(aDD01, aDD01_i2m1, aDD01_i2m2, aDD01_i2m3, aDD01_i2p1, aDD01_i2p2, aDD01_i2p3, invdxx2);
        const REAL_SIMD_ARRAY aDD_dKOD020 =
            SIMD_fd_function_dKOD0_fdorder4(aDD02, aDD02_i0m1, aDD02_i0m2, aDD02_i0m3, aDD02_i0p1, aDD02_i0p2, aDD02_i0p3, invdxx0);
        const REAL_SIMD_ARRAY aDD_dKOD021 =
            SIMD_fd_function_dKOD1_fdorder4(aDD02, aDD02_i1m1, aDD02_i1m2, aDD02_i1m3, aDD02_i1p1, aDD02_i1p2, aDD02_i1p3, invdxx1);
        const REAL_SIMD_ARRAY aDD_dKOD022 =
            SIMD_fd_function_dKOD2_fdorder4(aDD02, aDD02_i2m1, aDD02_i2m2, aDD02_i2m3, aDD02_i2p1, aDD02_i2p2, aDD02_i2p3, invdxx2);
        const REAL_SIMD_ARRAY aDD_dKOD110 =
            SIMD_fd_function_dKOD0_fdorder4(aDD11, aDD11_i0m1, aDD11_i0m2, aDD11_i0m3, aDD11_i0p1, aDD11_i0p2, aDD11_i0p3, invdxx0);
        const REAL_SIMD_ARRAY aDD_dKOD111 =
            SIMD_fd_function_dKOD1_fdorder4(aDD11, aDD11_i1m1, aDD11_i1m2, aDD11_i1m3, aDD11_i1p1, aDD11_i1p2, aDD11_i1p3, invdxx1);
        const REAL_SIMD_ARRAY aDD_dKOD112 =
            SIMD_fd_function_dKOD2_fdorder4(aDD11, aDD11_i2m1, aDD11_i2m2, aDD11_i2m3, aDD11_i2p1, aDD11_i2p2, aDD11_i2p3, invdxx2);
        const REAL_SIMD_ARRAY aDD_dKOD120 =
            SIMD_fd_function_dKOD0_fdorder4(aDD12, aDD12_i0m1, aDD12_i0m2, aDD12_i0m3, aDD12_i0p1, aDD12_i0p2, aDD12_i0p3, invdxx0);
        const REAL_SIMD_ARRAY aDD_dKOD121 =
            SIMD_fd_function_dKOD1_fdorder4(aDD12, aDD12_i1m1, aDD12_i1m2, aDD12_i1m3, aDD12_i1p1, aDD12_i1p2, aDD12_i1p3, invdxx1);
        const REAL_SIMD_ARRAY aDD_dKOD122 =
            SIMD_fd_function_dKOD2_fdorder4(aDD12, aDD12_i2m1, aDD12_i2m2, aDD12_i2m3, aDD12_i2p1, aDD12_i2p2, aDD12_i2p3, invdxx2);
        const REAL_SIMD_ARRAY aDD_dKOD220 =
            SIMD_fd_function_dKOD0_fdorder4(aDD22, aDD22_i0m1, aDD22_i0m2, aDD22_i0m3, aDD22_i0p1, aDD22_i0p2, aDD22_i0p3, invdxx0);
        const REAL_SIMD_ARRAY aDD_dKOD221 =
            SIMD_fd_function_dKOD1_fdorder4(aDD22, aDD22_i1m1, aDD22_i1m2, aDD22_i1m3, aDD22_i1p1, aDD22_i1p2, aDD22_i1p3, invdxx1);
        const REAL_SIMD_ARRAY aDD_dKOD222 =
            SIMD_fd_function_dKOD2_fdorder4(aDD22, aDD22_i2m1, aDD22_i2m2, aDD22_i2m3, aDD22_i2p1, aDD22_i2p2, aDD22_i2p3, invdxx2);
        const REAL_SIMD_ARRAY alpha_dD0 = SIMD_fd_function_dD0_fdorder4(alpha_i0m1, alpha_i0m2, alpha_i0p1, alpha_i0p2, invdxx0);
        const REAL_SIMD_ARRAY alpha_dD1 = SIMD_fd_function_dD1_fdorder4(alpha_i1m1, alpha_i1m2, alpha_i1p1, alpha_i1p2, invdxx1);
        const REAL_SIMD_ARRAY alpha_dD2 = SIMD_fd_function_dD2_fdorder4(alpha_i2m1, alpha_i2m2, alpha_i2p1, alpha_i2p2, invdxx2);
        const REAL_SIMD_ARRAY alpha_dDD00 = SIMD_fd_function_dDD00_fdorder4(alpha, alpha_i0m1, alpha_i0m2, alpha_i0p1, alpha_i0p2, invdxx0);
        const REAL_SIMD_ARRAY alpha_dDD01 =
            SIMD_fd_function_dDD01_fdorder4(alpha_i0m1_i1m1, alpha_i0m1_i1m2, alpha_i0m1_i1p1, alpha_i0m1_i1p2, alpha_i0m2_i1m1, alpha_i0m2_i1m2,
                                            alpha_i0m2_i1p1, alpha_i0m2_i1p2, alpha_i0p1_i1m1, alpha_i0p1_i1m2, alpha_i0p1_i1p1, alpha_i0p1_i1p2,
                                            alpha_i0p2_i1m1, alpha_i0p2_i1m2, alpha_i0p2_i1p1, alpha_i0p2_i1p2, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY alpha_dDD02 =
            SIMD_fd_function_dDD02_fdorder4(alpha_i0m1_i2m1, alpha_i0m1_i2m2, alpha_i0m1_i2p1, alpha_i0m1_i2p2, alpha_i0m2_i2m1, alpha_i0m2_i2m2,
                                            alpha_i0m2_i2p1, alpha_i0m2_i2p2, alpha_i0p1_i2m1, alpha_i0p1_i2m2, alpha_i0p1_i2p1, alpha_i0p1_i2p2,
                                            alpha_i0p2_i2m1, alpha_i0p2_i2m2, alpha_i0p2_i2p1, alpha_i0p2_i2p2, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY alpha_dDD11 = SIMD_fd_function_dDD11_fdorder4(alpha, alpha_i1m1, alpha_i1m2, alpha_i1p1, alpha_i1p2, invdxx1);
        const REAL_SIMD_ARRAY alpha_dDD12 =
            SIMD_fd_function_dDD12_fdorder4(alpha_i1m1_i2m1, alpha_i1m1_i2m2, alpha_i1m1_i2p1, alpha_i1m1_i2p2, alpha_i1m2_i2m1, alpha_i1m2_i2m2,
                                            alpha_i1m2_i2p1, alpha_i1m2_i2p2, alpha_i1p1_i2m1, alpha_i1p1_i2m2, alpha_i1p1_i2p1, alpha_i1p1_i2p2,
                                            alpha_i1p2_i2m1, alpha_i1p2_i2m2, alpha_i1p2_i2p1, alpha_i1p2_i2p2, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY alpha_dDD22 = SIMD_fd_function_dDD22_fdorder4(alpha, alpha_i2m1, alpha_i2m2, alpha_i2p1, alpha_i2p2, invdxx2);
        const REAL_SIMD_ARRAY alpha_dKOD0 =
            SIMD_fd_function_dKOD0_fdorder4(alpha, alpha_i0m1, alpha_i0m2, alpha_i0m3, alpha_i0p1, alpha_i0p2, alpha_i0p3, invdxx0);
        const REAL_SIMD_ARRAY alpha_dKOD1 =
            SIMD_fd_function_dKOD1_fdorder4(alpha, alpha_i1m1, alpha_i1m2, alpha_i1m3, alpha_i1p1, alpha_i1p2, alpha_i1p3, invdxx1);
        const REAL_SIMD_ARRAY alpha_dKOD2 =
            SIMD_fd_function_dKOD2_fdorder4(alpha, alpha_i2m1, alpha_i2m2, alpha_i2m3, alpha_i2p1, alpha_i2p2, alpha_i2p3, invdxx2);
        const REAL_SIMD_ARRAY betU_dKOD00 =
            SIMD_fd_function_dKOD0_fdorder4(betU0, betU0_i0m1, betU0_i0m2, betU0_i0m3, betU0_i0p1, betU0_i0p2, betU0_i0p3, invdxx0);
        const REAL_SIMD_ARRAY betU_dKOD01 =
            SIMD_fd_function_dKOD1_fdorder4(betU0, betU0_i1m1, betU0_i1m2, betU0_i1m3, betU0_i1p1, betU0_i1p2, betU0_i1p3, invdxx1);
        const REAL_SIMD_ARRAY betU_dKOD02 =
            SIMD_fd_function_dKOD2_fdorder4(betU0, betU0_i2m1, betU0_i2m2, betU0_i2m3, betU0_i2p1, betU0_i2p2, betU0_i2p3, invdxx2);
        const REAL_SIMD_ARRAY betU_dKOD10 =
            SIMD_fd_function_dKOD0_fdorder4(betU1, betU1_i0m1, betU1_i0m2, betU1_i0m3, betU1_i0p1, betU1_i0p2, betU1_i0p3, invdxx0);
        const REAL_SIMD_ARRAY betU_dKOD11 =
            SIMD_fd_function_dKOD1_fdorder4(betU1, betU1_i1m1, betU1_i1m2, betU1_i1m3, betU1_i1p1, betU1_i1p2, betU1_i1p3, invdxx1);
        const REAL_SIMD_ARRAY betU_dKOD12 =
            SIMD_fd_function_dKOD2_fdorder4(betU1, betU1_i2m1, betU1_i2m2, betU1_i2m3, betU1_i2p1, betU1_i2p2, betU1_i2p3, invdxx2);
        const REAL_SIMD_ARRAY betU_dKOD20 =
            SIMD_fd_function_dKOD0_fdorder4(betU2, betU2_i0m1, betU2_i0m2, betU2_i0m3, betU2_i0p1, betU2_i0p2, betU2_i0p3, invdxx0);
        const REAL_SIMD_ARRAY betU_dKOD21 =
            SIMD_fd_function_dKOD1_fdorder4(betU2, betU2_i1m1, betU2_i1m2, betU2_i1m3, betU2_i1p1, betU2_i1p2, betU2_i1p3, invdxx1);
        const REAL_SIMD_ARRAY betU_dKOD22 =
            SIMD_fd_function_dKOD2_fdorder4(betU2, betU2_i2m1, betU2_i2m2, betU2_i2m3, betU2_i2p1, betU2_i2p2, betU2_i2p3, invdxx2);
        const REAL_SIMD_ARRAY cf_dD0 = SIMD_fd_function_dD0_fdorder4(cf_i0m1, cf_i0m2, cf_i0p1, cf_i0p2, invdxx0);
        const REAL_SIMD_ARRAY cf_dD1 = SIMD_fd_function_dD1_fdorder4(cf_i1m1, cf_i1m2, cf_i1p1, cf_i1p2, invdxx1);
        const REAL_SIMD_ARRAY cf_dD2 = SIMD_fd_function_dD2_fdorder4(cf_i2m1, cf_i2m2, cf_i2p1, cf_i2p2, invdxx2);
        const REAL_SIMD_ARRAY cf_dDD00 = SIMD_fd_function_dDD00_fdorder4(cf, cf_i0m1, cf_i0m2, cf_i0p1, cf_i0p2, invdxx0);
        const REAL_SIMD_ARRAY cf_dDD01 = SIMD_fd_function_dDD01_fdorder4(
            cf_i0m1_i1m1, cf_i0m1_i1m2, cf_i0m1_i1p1, cf_i0m1_i1p2, cf_i0m2_i1m1, cf_i0m2_i1m2, cf_i0m2_i1p1, cf_i0m2_i1p2, cf_i0p1_i1m1,
            cf_i0p1_i1m2, cf_i0p1_i1p1, cf_i0p1_i1p2, cf_i0p2_i1m1, cf_i0p2_i1m2, cf_i0p2_i1p1, cf_i0p2_i1p2, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY cf_dDD02 = SIMD_fd_function_dDD02_fdorder4(
            cf_i0m1_i2m1, cf_i0m1_i2m2, cf_i0m1_i2p1, cf_i0m1_i2p2, cf_i0m2_i2m1, cf_i0m2_i2m2, cf_i0m2_i2p1, cf_i0m2_i2p2, cf_i0p1_i2m1,
            cf_i0p1_i2m2, cf_i0p1_i2p1, cf_i0p1_i2p2, cf_i0p2_i2m1, cf_i0p2_i2m2, cf_i0p2_i2p1, cf_i0p2_i2p2, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY cf_dDD11 = SIMD_fd_function_dDD11_fdorder4(cf, cf_i1m1, cf_i1m2, cf_i1p1, cf_i1p2, invdxx1);
        const REAL_SIMD_ARRAY cf_dDD12 = SIMD_fd_function_dDD12_fdorder4(
            cf_i1m1_i2m1, cf_i1m1_i2m2, cf_i1m1_i2p1, cf_i1m1_i2p2, cf_i1m2_i2m1, cf_i1m2_i2m2, cf_i1m2_i2p1, cf_i1m2_i2p2, cf_i1p1_i2m1,
            cf_i1p1_i2m2, cf_i1p1_i2p1, cf_i1p1_i2p2, cf_i1p2_i2m1, cf_i1p2_i2m2, cf_i1p2_i2p1, cf_i1p2_i2p2, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY cf_dDD22 = SIMD_fd_function_dDD22_fdorder4(cf, cf_i2m1, cf_i2m2, cf_i2p1, cf_i2p2, invdxx2);
        const REAL_SIMD_ARRAY cf_dKOD0 = SIMD_fd_function_dKOD0_fdorder4(cf, cf_i0m1, cf_i0m2, cf_i0m3, cf_i0p1, cf_i0p2, cf_i0p3, invdxx0);
        const REAL_SIMD_ARRAY cf_dKOD1 = SIMD_fd_function_dKOD1_fdorder4(cf, cf_i1m1, cf_i1m2, cf_i1m3, cf_i1p1, cf_i1p2, cf_i1p3, invdxx1);
        const REAL_SIMD_ARRAY cf_dKOD2 = SIMD_fd_function_dKOD2_fdorder4(cf, cf_i2m1, cf_i2m2, cf_i2m3, cf_i2p1, cf_i2p2, cf_i2p3, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD000 = SIMD_fd_function_dD0_fdorder4(hDD00_i0m1, hDD00_i0m2, hDD00_i0p1, hDD00_i0p2, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD001 = SIMD_fd_function_dD1_fdorder4(hDD00_i1m1, hDD00_i1m2, hDD00_i1p1, hDD00_i1p2, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD002 = SIMD_fd_function_dD2_fdorder4(hDD00_i2m1, hDD00_i2m2, hDD00_i2p1, hDD00_i2p2, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD010 = SIMD_fd_function_dD0_fdorder4(hDD01_i0m1, hDD01_i0m2, hDD01_i0p1, hDD01_i0p2, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD011 = SIMD_fd_function_dD1_fdorder4(hDD01_i1m1, hDD01_i1m2, hDD01_i1p1, hDD01_i1p2, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD012 = SIMD_fd_function_dD2_fdorder4(hDD01_i2m1, hDD01_i2m2, hDD01_i2p1, hDD01_i2p2, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD020 = SIMD_fd_function_dD0_fdorder4(hDD02_i0m1, hDD02_i0m2, hDD02_i0p1, hDD02_i0p2, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD021 = SIMD_fd_function_dD1_fdorder4(hDD02_i1m1, hDD02_i1m2, hDD02_i1p1, hDD02_i1p2, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD022 = SIMD_fd_function_dD2_fdorder4(hDD02_i2m1, hDD02_i2m2, hDD02_i2p1, hDD02_i2p2, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD110 = SIMD_fd_function_dD0_fdorder4(hDD11_i0m1, hDD11_i0m2, hDD11_i0p1, hDD11_i0p2, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD111 = SIMD_fd_function_dD1_fdorder4(hDD11_i1m1, hDD11_i1m2, hDD11_i1p1, hDD11_i1p2, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD112 = SIMD_fd_function_dD2_fdorder4(hDD11_i2m1, hDD11_i2m2, hDD11_i2p1, hDD11_i2p2, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD120 = SIMD_fd_function_dD0_fdorder4(hDD12_i0m1, hDD12_i0m2, hDD12_i0p1, hDD12_i0p2, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD121 = SIMD_fd_function_dD1_fdorder4(hDD12_i1m1, hDD12_i1m2, hDD12_i1p1, hDD12_i1p2, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD122 = SIMD_fd_function_dD2_fdorder4(hDD12_i2m1, hDD12_i2m2, hDD12_i2p1, hDD12_i2p2, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD220 = SIMD_fd_function_dD0_fdorder4(hDD22_i0m1, hDD22_i0m2, hDD22_i0p1, hDD22_i0p2, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD221 = SIMD_fd_function_dD1_fdorder4(hDD22_i1m1, hDD22_i1m2, hDD22_i1p1, hDD22_i1p2, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD222 = SIMD_fd_function_dD2_fdorder4(hDD22_i2m1, hDD22_i2m2, hDD22_i2p1, hDD22_i2p2, invdxx2);
        const REAL_SIMD_ARRAY hDD_dKOD000 =
            SIMD_fd_function_dKOD0_fdorder4(hDD00, hDD00_i0m1, hDD00_i0m2, hDD00_i0m3, hDD00_i0p1, hDD00_i0p2, hDD00_i0p3, invdxx0);
        const REAL_SIMD_ARRAY hDD_dKOD001 =
            SIMD_fd_function_dKOD1_fdorder4(hDD00, hDD00_i1m1, hDD00_i1m2, hDD00_i1m3, hDD00_i1p1, hDD00_i1p2, hDD00_i1p3, invdxx1);
        const REAL_SIMD_ARRAY hDD_dKOD002 =
            SIMD_fd_function_dKOD2_fdorder4(hDD00, hDD00_i2m1, hDD00_i2m2, hDD00_i2m3, hDD00_i2p1, hDD00_i2p2, hDD00_i2p3, invdxx2);
        const REAL_SIMD_ARRAY hDD_dKOD010 =
            SIMD_fd_function_dKOD0_fdorder4(hDD01, hDD01_i0m1, hDD01_i0m2, hDD01_i0m3, hDD01_i0p1, hDD01_i0p2, hDD01_i0p3, invdxx0);
        const REAL_SIMD_ARRAY hDD_dKOD011 =
            SIMD_fd_function_dKOD1_fdorder4(hDD01, hDD01_i1m1, hDD01_i1m2, hDD01_i1m3, hDD01_i1p1, hDD01_i1p2, hDD01_i1p3, invdxx1);
        const REAL_SIMD_ARRAY hDD_dKOD012 =
            SIMD_fd_function_dKOD2_fdorder4(hDD01, hDD01_i2m1, hDD01_i2m2, hDD01_i2m3, hDD01_i2p1, hDD01_i2p2, hDD01_i2p3, invdxx2);
        const REAL_SIMD_ARRAY hDD_dKOD020 =
            SIMD_fd_function_dKOD0_fdorder4(hDD02, hDD02_i0m1, hDD02_i0m2, hDD02_i0m3, hDD02_i0p1, hDD02_i0p2, hDD02_i0p3, invdxx0);
        const REAL_SIMD_ARRAY hDD_dKOD021 =
            SIMD_fd_function_dKOD1_fdorder4(hDD02, hDD02_i1m1, hDD02_i1m2, hDD02_i1m3, hDD02_i1p1, hDD02_i1p2, hDD02_i1p3, invdxx1);
        const REAL_SIMD_ARRAY hDD_dKOD022 =
            SIMD_fd_function_dKOD2_fdorder4(hDD02, hDD02_i2m1, hDD02_i2m2, hDD02_i2m3, hDD02_i2p1, hDD02_i2p2, hDD02_i2p3, invdxx2);
        const REAL_SIMD_ARRAY hDD_dKOD110 =
            SIMD_fd_function_dKOD0_fdorder4(hDD11, hDD11_i0m1, hDD11_i0m2, hDD11_i0m3, hDD11_i0p1, hDD11_i0p2, hDD11_i0p3, invdxx0);
        const REAL_SIMD_ARRAY hDD_dKOD111 =
            SIMD_fd_function_dKOD1_fdorder4(hDD11, hDD11_i1m1, hDD11_i1m2, hDD11_i1m3, hDD11_i1p1, hDD11_i1p2, hDD11_i1p3, invdxx1);
        const REAL_SIMD_ARRAY hDD_dKOD112 =
            SIMD_fd_function_dKOD2_fdorder4(hDD11, hDD11_i2m1, hDD11_i2m2, hDD11_i2m3, hDD11_i2p1, hDD11_i2p2, hDD11_i2p3, invdxx2);
        const REAL_SIMD_ARRAY hDD_dKOD120 =
            SIMD_fd_function_dKOD0_fdorder4(hDD12, hDD12_i0m1, hDD12_i0m2, hDD12_i0m3, hDD12_i0p1, hDD12_i0p2, hDD12_i0p3, invdxx0);
        const REAL_SIMD_ARRAY hDD_dKOD121 =
            SIMD_fd_function_dKOD1_fdorder4(hDD12, hDD12_i1m1, hDD12_i1m2, hDD12_i1m3, hDD12_i1p1, hDD12_i1p2, hDD12_i1p3, invdxx1);
        const REAL_SIMD_ARRAY hDD_dKOD122 =
            SIMD_fd_function_dKOD2_fdorder4(hDD12, hDD12_i2m1, hDD12_i2m2, hDD12_i2m3, hDD12_i2p1, hDD12_i2p2, hDD12_i2p3, invdxx2);
        const REAL_SIMD_ARRAY hDD_dKOD220 =
            SIMD_fd_function_dKOD0_fdorder4(hDD22, hDD22_i0m1, hDD22_i0m2, hDD22_i0m3, hDD22_i0p1, hDD22_i0p2, hDD22_i0p3, invdxx0);
        const REAL_SIMD_ARRAY hDD_dKOD221 =
            SIMD_fd_function_dKOD1_fdorder4(hDD22, hDD22_i1m1, hDD22_i1m2, hDD22_i1m3, hDD22_i1p1, hDD22_i1p2, hDD22_i1p3, invdxx1);
        const REAL_SIMD_ARRAY hDD_dKOD222 =
            SIMD_fd_function_dKOD2_fdorder4(hDD22, hDD22_i2m1, hDD22_i2m2, hDD22_i2m3, hDD22_i2p1, hDD22_i2p2, hDD22_i2p3, invdxx2);
        const REAL_SIMD_ARRAY lambdaU_dKOD00 = SIMD_fd_function_dKOD0_fdorder4(lambdaU0, lambdaU0_i0m1, lambdaU0_i0m2, lambdaU0_i0m3, lambdaU0_i0p1,
                                                                               lambdaU0_i0p2, lambdaU0_i0p3, invdxx0);
        const REAL_SIMD_ARRAY lambdaU_dKOD01 = SIMD_fd_function_dKOD1_fdorder4(lambdaU0, lambdaU0_i1m1, lambdaU0_i1m2, lambdaU0_i1m3, lambdaU0_i1p1,
                                                                               lambdaU0_i1p2, lambdaU0_i1p3, invdxx1);
        const REAL_SIMD_ARRAY lambdaU_dKOD02 = SIMD_fd_function_dKOD2_fdorder4(lambdaU0, lambdaU0_i2m1, lambdaU0_i2m2, lambdaU0_i2m3, lambdaU0_i2p1,
                                                                               lambdaU0_i2p2, lambdaU0_i2p3, invdxx2);
        const REAL_SIMD_ARRAY lambdaU_dKOD10 = SIMD_fd_function_dKOD0_fdorder4(lambdaU1, lambdaU1_i0m1, lambdaU1_i0m2, lambdaU1_i0m3, lambdaU1_i0p1,
                                                                               lambdaU1_i0p2, lambdaU1_i0p3, invdxx0);
        const REAL_SIMD_ARRAY lambdaU_dKOD11 = SIMD_fd_function_dKOD1_fdorder4(lambdaU1, lambdaU1_i1m1, lambdaU1_i1m2, lambdaU1_i1m3, lambdaU1_i1p1,
                                                                               lambdaU1_i1p2, lambdaU1_i1p3, invdxx1);
        const REAL_SIMD_ARRAY lambdaU_dKOD12 = SIMD_fd_function_dKOD2_fdorder4(lambdaU1, lambdaU1_i2m1, lambdaU1_i2m2, lambdaU1_i2m3, lambdaU1_i2p1,
                                                                               lambdaU1_i2p2, lambdaU1_i2p3, invdxx2);
        const REAL_SIMD_ARRAY lambdaU_dKOD20 = SIMD_fd_function_dKOD0_fdorder4(lambdaU2, lambdaU2_i0m1, lambdaU2_i0m2, lambdaU2_i0m3, lambdaU2_i0p1,
                                                                               lambdaU2_i0p2, lambdaU2_i0p3, invdxx0);
        const REAL_SIMD_ARRAY lambdaU_dKOD21 = SIMD_fd_function_dKOD1_fdorder4(lambdaU2, lambdaU2_i1m1, lambdaU2_i1m2, lambdaU2_i1m3, lambdaU2_i1p1,
                                                                               lambdaU2_i1p2, lambdaU2_i1p3, invdxx1);
        const REAL_SIMD_ARRAY lambdaU_dKOD22 = SIMD_fd_function_dKOD2_fdorder4(lambdaU2, lambdaU2_i2m1, lambdaU2_i2m2, lambdaU2_i2m3, lambdaU2_i2p1,
                                                                               lambdaU2_i2p2, lambdaU2_i2p3, invdxx2);
        const REAL_SIMD_ARRAY trK_dD0 = SIMD_fd_function_dD0_fdorder4(trK_i0m1, trK_i0m2, trK_i0p1, trK_i0p2, invdxx0);
        const REAL_SIMD_ARRAY trK_dD1 = SIMD_fd_function_dD1_fdorder4(trK_i1m1, trK_i1m2, trK_i1p1, trK_i1p2, invdxx1);
        const REAL_SIMD_ARRAY trK_dD2 = SIMD_fd_function_dD2_fdorder4(trK_i2m1, trK_i2m2, trK_i2p1, trK_i2p2, invdxx2);
        const REAL_SIMD_ARRAY trK_dKOD0 = SIMD_fd_function_dKOD0_fdorder4(trK, trK_i0m1, trK_i0m2, trK_i0m3, trK_i0p1, trK_i0p2, trK_i0p3, invdxx0);
        const REAL_SIMD_ARRAY trK_dKOD1 = SIMD_fd_function_dKOD1_fdorder4(trK, trK_i1m1, trK_i1m2, trK_i1m3, trK_i1p1, trK_i1p2, trK_i1p3, invdxx1);
        const REAL_SIMD_ARRAY trK_dKOD2 = SIMD_fd_function_dKOD2_fdorder4(trK, trK_i2m1, trK_i2m2, trK_i2m3, trK_i2p1, trK_i2p2, trK_i2p3, invdxx2);
        const REAL_SIMD_ARRAY vetU_dD00 = SIMD_fd_function_dD0_fdorder4(vetU0_i0m1, vetU0_i0m2, vetU0_i0p1, vetU0_i0p2, invdxx0);
        const REAL_SIMD_ARRAY vetU_dD01 = SIMD_fd_function_dD1_fdorder4(vetU0_i1m1, vetU0_i1m2, vetU0_i1p1, vetU0_i1p2, invdxx1);
        const REAL_SIMD_ARRAY vetU_dD02 = SIMD_fd_function_dD2_fdorder4(vetU0_i2m1, vetU0_i2m2, vetU0_i2p1, vetU0_i2p2, invdxx2);
        const REAL_SIMD_ARRAY vetU_dD10 = SIMD_fd_function_dD0_fdorder4(vetU1_i0m1, vetU1_i0m2, vetU1_i0p1, vetU1_i0p2, invdxx0);
        const REAL_SIMD_ARRAY vetU_dD11 = SIMD_fd_function_dD1_fdorder4(vetU1_i1m1, vetU1_i1m2, vetU1_i1p1, vetU1_i1p2, invdxx1);
        const REAL_SIMD_ARRAY vetU_dD12 = SIMD_fd_function_dD2_fdorder4(vetU1_i2m1, vetU1_i2m2, vetU1_i2p1, vetU1_i2p2, invdxx2);
        const REAL_SIMD_ARRAY vetU_dD20 = SIMD_fd_function_dD0_fdorder4(vetU2_i0m1, vetU2_i0m2, vetU2_i0p1, vetU2_i0p2, invdxx0);
        const REAL_SIMD_ARRAY vetU_dD21 = SIMD_fd_function_dD1_fdorder4(vetU2_i1m1, vetU2_i1m2, vetU2_i1p1, vetU2_i1p2, invdxx1);
        const REAL_SIMD_ARRAY vetU_dD22 = SIMD_fd_function_dD2_fdorder4(vetU2_i2m1, vetU2_i2m2, vetU2_i2p1, vetU2_i2p2, invdxx2);
        const REAL_SIMD_ARRAY vetU_dDD000 = SIMD_fd_function_dDD00_fdorder4(vetU0, vetU0_i0m1, vetU0_i0m2, vetU0_i0p1, vetU0_i0p2, invdxx0);
        const REAL_SIMD_ARRAY vetU_dDD001 =
            SIMD_fd_function_dDD01_fdorder4(vetU0_i0m1_i1m1, vetU0_i0m1_i1m2, vetU0_i0m1_i1p1, vetU0_i0m1_i1p2, vetU0_i0m2_i1m1, vetU0_i0m2_i1m2,
                                            vetU0_i0m2_i1p1, vetU0_i0m2_i1p2, vetU0_i0p1_i1m1, vetU0_i0p1_i1m2, vetU0_i0p1_i1p1, vetU0_i0p1_i1p2,
                                            vetU0_i0p2_i1m1, vetU0_i0p2_i1m2, vetU0_i0p2_i1p1, vetU0_i0p2_i1p2, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY vetU_dDD002 =
            SIMD_fd_function_dDD02_fdorder4(vetU0_i0m1_i2m1, vetU0_i0m1_i2m2, vetU0_i0m1_i2p1, vetU0_i0m1_i2p2, vetU0_i0m2_i2m1, vetU0_i0m2_i2m2,
                                            vetU0_i0m2_i2p1, vetU0_i0m2_i2p2, vetU0_i0p1_i2m1, vetU0_i0p1_i2m2, vetU0_i0p1_i2p1, vetU0_i0p1_i2p2,
                                            vetU0_i0p2_i2m1, vetU0_i0p2_i2m2, vetU0_i0p2_i2p1, vetU0_i0p2_i2p2, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY vetU_dDD011 = SIMD_fd_function_dDD11_fdorder4(vetU0, vetU0_i1m1, vetU0_i1m2, vetU0_i1p1, vetU0_i1p2, invdxx1);
        const REAL_SIMD_ARRAY vetU_dDD012 =
            SIMD_fd_function_dDD12_fdorder4(vetU0_i1m1_i2m1, vetU0_i1m1_i2m2, vetU0_i1m1_i2p1, vetU0_i1m1_i2p2, vetU0_i1m2_i2m1, vetU0_i1m2_i2m2,
                                            vetU0_i1m2_i2p1, vetU0_i1m2_i2p2, vetU0_i1p1_i2m1, vetU0_i1p1_i2m2, vetU0_i1p1_i2p1, vetU0_i1p1_i2p2,
                                            vetU0_i1p2_i2m1, vetU0_i1p2_i2m2, vetU0_i1p2_i2p1, vetU0_i1p2_i2p2, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY vetU_dDD022 = SIMD_fd_function_dDD22_fdorder4(vetU0, vetU0_i2m1, vetU0_i2m2, vetU0_i2p1, vetU0_i2p2, invdxx2);
        const REAL_SIMD_ARRAY vetU_dDD100 = SIMD_fd_function_dDD00_fdorder4(vetU1, vetU1_i0m1, vetU1_i0m2, vetU1_i0p1, vetU1_i0p2, invdxx0);
        const REAL_SIMD_ARRAY vetU_dDD101 =
            SIMD_fd_function_dDD01_fdorder4(vetU1_i0m1_i1m1, vetU1_i0m1_i1m2, vetU1_i0m1_i1p1, vetU1_i0m1_i1p2, vetU1_i0m2_i1m1, vetU1_i0m2_i1m2,
                                            vetU1_i0m2_i1p1, vetU1_i0m2_i1p2, vetU1_i0p1_i1m1, vetU1_i0p1_i1m2, vetU1_i0p1_i1p1, vetU1_i0p1_i1p2,
                                            vetU1_i0p2_i1m1, vetU1_i0p2_i1m2, vetU1_i0p2_i1p1, vetU1_i0p2_i1p2, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY vetU_dDD102 =
            SIMD_fd_function_dDD02_fdorder4(vetU1_i0m1_i2m1, vetU1_i0m1_i2m2, vetU1_i0m1_i2p1, vetU1_i0m1_i2p2, vetU1_i0m2_i2m1, vetU1_i0m2_i2m2,
                                            vetU1_i0m2_i2p1, vetU1_i0m2_i2p2, vetU1_i0p1_i2m1, vetU1_i0p1_i2m2, vetU1_i0p1_i2p1, vetU1_i0p1_i2p2,
                                            vetU1_i0p2_i2m1, vetU1_i0p2_i2m2, vetU1_i0p2_i2p1, vetU1_i0p2_i2p2, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY vetU_dDD111 = SIMD_fd_function_dDD11_fdorder4(vetU1, vetU1_i1m1, vetU1_i1m2, vetU1_i1p1, vetU1_i1p2, invdxx1);
        const REAL_SIMD_ARRAY vetU_dDD112 =
            SIMD_fd_function_dDD12_fdorder4(vetU1_i1m1_i2m1, vetU1_i1m1_i2m2, vetU1_i1m1_i2p1, vetU1_i1m1_i2p2, vetU1_i1m2_i2m1, vetU1_i1m2_i2m2,
                                            vetU1_i1m2_i2p1, vetU1_i1m2_i2p2, vetU1_i1p1_i2m1, vetU1_i1p1_i2m2, vetU1_i1p1_i2p1, vetU1_i1p1_i2p2,
                                            vetU1_i1p2_i2m1, vetU1_i1p2_i2m2, vetU1_i1p2_i2p1, vetU1_i1p2_i2p2, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY vetU_dDD122 = SIMD_fd_function_dDD22_fdorder4(vetU1, vetU1_i2m1, vetU1_i2m2, vetU1_i2p1, vetU1_i2p2, invdxx2);
        const REAL_SIMD_ARRAY vetU_dDD200 = SIMD_fd_function_dDD00_fdorder4(vetU2, vetU2_i0m1, vetU2_i0m2, vetU2_i0p1, vetU2_i0p2, invdxx0);
        const REAL_SIMD_ARRAY vetU_dDD201 =
            SIMD_fd_function_dDD01_fdorder4(vetU2_i0m1_i1m1, vetU2_i0m1_i1m2, vetU2_i0m1_i1p1, vetU2_i0m1_i1p2, vetU2_i0m2_i1m1, vetU2_i0m2_i1m2,
                                            vetU2_i0m2_i1p1, vetU2_i0m2_i1p2, vetU2_i0p1_i1m1, vetU2_i0p1_i1m2, vetU2_i0p1_i1p1, vetU2_i0p1_i1p2,
                                            vetU2_i0p2_i1m1, vetU2_i0p2_i1m2, vetU2_i0p2_i1p1, vetU2_i0p2_i1p2, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY vetU_dDD202 =
            SIMD_fd_function_dDD02_fdorder4(vetU2_i0m1_i2m1, vetU2_i0m1_i2m2, vetU2_i0m1_i2p1, vetU2_i0m1_i2p2, vetU2_i0m2_i2m1, vetU2_i0m2_i2m2,
                                            vetU2_i0m2_i2p1, vetU2_i0m2_i2p2, vetU2_i0p1_i2m1, vetU2_i0p1_i2m2, vetU2_i0p1_i2p1, vetU2_i0p1_i2p2,
                                            vetU2_i0p2_i2m1, vetU2_i0p2_i2m2, vetU2_i0p2_i2p1, vetU2_i0p2_i2p2, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY vetU_dDD211 = SIMD_fd_function_dDD11_fdorder4(vetU2, vetU2_i1m1, vetU2_i1m2, vetU2_i1p1, vetU2_i1p2, invdxx1);
        const REAL_SIMD_ARRAY vetU_dDD212 =
            SIMD_fd_function_dDD12_fdorder4(vetU2_i1m1_i2m1, vetU2_i1m1_i2m2, vetU2_i1m1_i2p1, vetU2_i1m1_i2p2, vetU2_i1m2_i2m1, vetU2_i1m2_i2m2,
                                            vetU2_i1m2_i2p1, vetU2_i1m2_i2p2, vetU2_i1p1_i2m1, vetU2_i1p1_i2m2, vetU2_i1p1_i2p1, vetU2_i1p1_i2p2,
                                            vetU2_i1p2_i2m1, vetU2_i1p2_i2m2, vetU2_i1p2_i2p1, vetU2_i1p2_i2p2, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY vetU_dDD222 = SIMD_fd_function_dDD22_fdorder4(vetU2, vetU2_i2m1, vetU2_i2m2, vetU2_i2p1, vetU2_i2p2, invdxx2);
        const REAL_SIMD_ARRAY vetU_dKOD00 =
            SIMD_fd_function_dKOD0_fdorder4(vetU0, vetU0_i0m1, vetU0_i0m2, vetU0_i0m3, vetU0_i0p1, vetU0_i0p2, vetU0_i0p3, invdxx0);
        const REAL_SIMD_ARRAY vetU_dKOD01 =
            SIMD_fd_function_dKOD1_fdorder4(vetU0, vetU0_i1m1, vetU0_i1m2, vetU0_i1m3, vetU0_i1p1, vetU0_i1p2, vetU0_i1p3, invdxx1);
        const REAL_SIMD_ARRAY vetU_dKOD02 =
            SIMD_fd_function_dKOD2_fdorder4(vetU0, vetU0_i2m1, vetU0_i2m2, vetU0_i2m3, vetU0_i2p1, vetU0_i2p2, vetU0_i2p3, invdxx2);
        const REAL_SIMD_ARRAY vetU_dKOD10 =
            SIMD_fd_function_dKOD0_fdorder4(vetU1, vetU1_i0m1, vetU1_i0m2, vetU1_i0m3, vetU1_i0p1, vetU1_i0p2, vetU1_i0p3, invdxx0);
        const REAL_SIMD_ARRAY vetU_dKOD11 =
            SIMD_fd_function_dKOD1_fdorder4(vetU1, vetU1_i1m1, vetU1_i1m2, vetU1_i1m3, vetU1_i1p1, vetU1_i1p2, vetU1_i1p3, invdxx1);
        const REAL_SIMD_ARRAY vetU_dKOD12 =
            SIMD_fd_function_dKOD2_fdorder4(vetU1, vetU1_i2m1, vetU1_i2m2, vetU1_i2m3, vetU1_i2p1, vetU1_i2p2, vetU1_i2p3, invdxx2);
        const REAL_SIMD_ARRAY vetU_dKOD20 =
            SIMD_fd_function_dKOD0_fdorder4(vetU2, vetU2_i0m1, vetU2_i0m2, vetU2_i0m3, vetU2_i0p1, vetU2_i0p2, vetU2_i0p3, invdxx0);
        const REAL_SIMD_ARRAY vetU_dKOD21 =
            SIMD_fd_function_dKOD1_fdorder4(vetU2, vetU2_i1m1, vetU2_i1m2, vetU2_i1m3, vetU2_i1p1, vetU2_i1p2, vetU2_i1p3, invdxx1);
        const REAL_SIMD_ARRAY vetU_dKOD22 =
            SIMD_fd_function_dKOD2_fdorder4(vetU2, vetU2_i2m1, vetU2_i2m2, vetU2_i2m3, vetU2_i2p1, vetU2_i2p2, vetU2_i2p3, invdxx2);
        const REAL_SIMD_ARRAY UpwindControlVectorU0 = vetU0;
        const REAL_SIMD_ARRAY UpwindControlVectorU1 = vetU1;
        const REAL_SIMD_ARRAY UpwindControlVectorU2 = vetU2;

        /*
         * NRPy+-Generated GF Access/FD Code, Step 2 of 3:
         * Implement upwinding algorithm.
         */
        const double tmp_upwind_Integer_0 = 0.000000000000000000000000000000000;

        const REAL_SIMD_ARRAY upwind_Integer_0 = ConstSIMD(tmp_upwind_Integer_0);
        const double tmp_upwind_Integer_1 = 1.000000000000000000000000000000000;

        const REAL_SIMD_ARRAY upwind_Integer_1 = ConstSIMD(tmp_upwind_Integer_1);
        const REAL_SIMD_ARRAY Upwind0 = UPWIND_ALG(UpwindControlVectorU0);
        const REAL_SIMD_ARRAY Upwind1 = UPWIND_ALG(UpwindControlVectorU1);
        const REAL_SIMD_ARRAY Upwind2 = UPWIND_ALG(UpwindControlVectorU2);
        const double dblFDPart2_NegativeOne_ = -1.0;
        const REAL_SIMD_ARRAY FDPart2_NegativeOne_ = ConstSIMD(dblFDPart2_NegativeOne_);

        const REAL_SIMD_ARRAY aDD_dupD000 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputaDD_dupD000, UpwindAlgInputaDD_ddnD000), UpwindAlgInputaDD_ddnD000);
        const REAL_SIMD_ARRAY aDD_dupD001 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputaDD_dupD001, UpwindAlgInputaDD_ddnD001), UpwindAlgInputaDD_ddnD001);
        const REAL_SIMD_ARRAY aDD_dupD002 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputaDD_dupD002, UpwindAlgInputaDD_ddnD002), UpwindAlgInputaDD_ddnD002);
        const REAL_SIMD_ARRAY aDD_dupD010 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputaDD_dupD010, UpwindAlgInputaDD_ddnD010), UpwindAlgInputaDD_ddnD010);
        const REAL_SIMD_ARRAY aDD_dupD011 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputaDD_dupD011, UpwindAlgInputaDD_ddnD011), UpwindAlgInputaDD_ddnD011);
        const REAL_SIMD_ARRAY aDD_dupD012 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputaDD_dupD012, UpwindAlgInputaDD_ddnD012), UpwindAlgInputaDD_ddnD012);
        const REAL_SIMD_ARRAY aDD_dupD020 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputaDD_dupD020, UpwindAlgInputaDD_ddnD020), UpwindAlgInputaDD_ddnD020);
        const REAL_SIMD_ARRAY aDD_dupD021 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputaDD_dupD021, UpwindAlgInputaDD_ddnD021), UpwindAlgInputaDD_ddnD021);
        const REAL_SIMD_ARRAY aDD_dupD022 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputaDD_dupD022, UpwindAlgInputaDD_ddnD022), UpwindAlgInputaDD_ddnD022);
        const REAL_SIMD_ARRAY aDD_dupD110 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputaDD_dupD110, UpwindAlgInputaDD_ddnD110), UpwindAlgInputaDD_ddnD110);
        const REAL_SIMD_ARRAY aDD_dupD111 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputaDD_dupD111, UpwindAlgInputaDD_ddnD111), UpwindAlgInputaDD_ddnD111);
        const REAL_SIMD_ARRAY aDD_dupD112 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputaDD_dupD112, UpwindAlgInputaDD_ddnD112), UpwindAlgInputaDD_ddnD112);
        const REAL_SIMD_ARRAY aDD_dupD120 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputaDD_dupD120, UpwindAlgInputaDD_ddnD120), UpwindAlgInputaDD_ddnD120);
        const REAL_SIMD_ARRAY aDD_dupD121 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputaDD_dupD121, UpwindAlgInputaDD_ddnD121), UpwindAlgInputaDD_ddnD121);
        const REAL_SIMD_ARRAY aDD_dupD122 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputaDD_dupD122, UpwindAlgInputaDD_ddnD122), UpwindAlgInputaDD_ddnD122);
        const REAL_SIMD_ARRAY aDD_dupD220 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputaDD_dupD220, UpwindAlgInputaDD_ddnD220), UpwindAlgInputaDD_ddnD220);
        const REAL_SIMD_ARRAY aDD_dupD221 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputaDD_dupD221, UpwindAlgInputaDD_ddnD221), UpwindAlgInputaDD_ddnD221);
        const REAL_SIMD_ARRAY aDD_dupD222 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputaDD_dupD222, UpwindAlgInputaDD_ddnD222), UpwindAlgInputaDD_ddnD222);
        const REAL_SIMD_ARRAY alpha_dupD0 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputalpha_dupD0, UpwindAlgInputalpha_ddnD0), UpwindAlgInputalpha_ddnD0);
        const REAL_SIMD_ARRAY alpha_dupD1 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputalpha_dupD1, UpwindAlgInputalpha_ddnD1), UpwindAlgInputalpha_ddnD1);
        const REAL_SIMD_ARRAY alpha_dupD2 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputalpha_dupD2, UpwindAlgInputalpha_ddnD2), UpwindAlgInputalpha_ddnD2);
        const REAL_SIMD_ARRAY betU_dupD00 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputbetU_dupD00, UpwindAlgInputbetU_ddnD00), UpwindAlgInputbetU_ddnD00);
        const REAL_SIMD_ARRAY betU_dupD01 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputbetU_dupD01, UpwindAlgInputbetU_ddnD01), UpwindAlgInputbetU_ddnD01);
        const REAL_SIMD_ARRAY betU_dupD02 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputbetU_dupD02, UpwindAlgInputbetU_ddnD02), UpwindAlgInputbetU_ddnD02);
        const REAL_SIMD_ARRAY betU_dupD10 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputbetU_dupD10, UpwindAlgInputbetU_ddnD10), UpwindAlgInputbetU_ddnD10);
        const REAL_SIMD_ARRAY betU_dupD11 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputbetU_dupD11, UpwindAlgInputbetU_ddnD11), UpwindAlgInputbetU_ddnD11);
        const REAL_SIMD_ARRAY betU_dupD12 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputbetU_dupD12, UpwindAlgInputbetU_ddnD12), UpwindAlgInputbetU_ddnD12);
        const REAL_SIMD_ARRAY betU_dupD20 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputbetU_dupD20, UpwindAlgInputbetU_ddnD20), UpwindAlgInputbetU_ddnD20);
        const REAL_SIMD_ARRAY betU_dupD21 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputbetU_dupD21, UpwindAlgInputbetU_ddnD21), UpwindAlgInputbetU_ddnD21);
        const REAL_SIMD_ARRAY betU_dupD22 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputbetU_dupD22, UpwindAlgInputbetU_ddnD22), UpwindAlgInputbetU_ddnD22);
        const REAL_SIMD_ARRAY cf_dupD0 = FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputcf_dupD0, UpwindAlgInputcf_ddnD0), UpwindAlgInputcf_ddnD0);
        const REAL_SIMD_ARRAY cf_dupD1 = FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputcf_dupD1, UpwindAlgInputcf_ddnD1), UpwindAlgInputcf_ddnD1);
        const REAL_SIMD_ARRAY cf_dupD2 = FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputcf_dupD2, UpwindAlgInputcf_ddnD2), UpwindAlgInputcf_ddnD2);
        const REAL_SIMD_ARRAY hDD_dupD000 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputhDD_dupD000, UpwindAlgInputhDD_ddnD000), UpwindAlgInputhDD_ddnD000);
        const REAL_SIMD_ARRAY hDD_dupD001 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputhDD_dupD001, UpwindAlgInputhDD_ddnD001), UpwindAlgInputhDD_ddnD001);
        const REAL_SIMD_ARRAY hDD_dupD002 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputhDD_dupD002, UpwindAlgInputhDD_ddnD002), UpwindAlgInputhDD_ddnD002);
        const REAL_SIMD_ARRAY hDD_dupD010 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputhDD_dupD010, UpwindAlgInputhDD_ddnD010), UpwindAlgInputhDD_ddnD010);
        const REAL_SIMD_ARRAY hDD_dupD011 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputhDD_dupD011, UpwindAlgInputhDD_ddnD011), UpwindAlgInputhDD_ddnD011);
        const REAL_SIMD_ARRAY hDD_dupD012 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputhDD_dupD012, UpwindAlgInputhDD_ddnD012), UpwindAlgInputhDD_ddnD012);
        const REAL_SIMD_ARRAY hDD_dupD020 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputhDD_dupD020, UpwindAlgInputhDD_ddnD020), UpwindAlgInputhDD_ddnD020);
        const REAL_SIMD_ARRAY hDD_dupD021 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputhDD_dupD021, UpwindAlgInputhDD_ddnD021), UpwindAlgInputhDD_ddnD021);
        const REAL_SIMD_ARRAY hDD_dupD022 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputhDD_dupD022, UpwindAlgInputhDD_ddnD022), UpwindAlgInputhDD_ddnD022);
        const REAL_SIMD_ARRAY hDD_dupD110 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputhDD_dupD110, UpwindAlgInputhDD_ddnD110), UpwindAlgInputhDD_ddnD110);
        const REAL_SIMD_ARRAY hDD_dupD111 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputhDD_dupD111, UpwindAlgInputhDD_ddnD111), UpwindAlgInputhDD_ddnD111);
        const REAL_SIMD_ARRAY hDD_dupD112 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputhDD_dupD112, UpwindAlgInputhDD_ddnD112), UpwindAlgInputhDD_ddnD112);
        const REAL_SIMD_ARRAY hDD_dupD120 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputhDD_dupD120, UpwindAlgInputhDD_ddnD120), UpwindAlgInputhDD_ddnD120);
        const REAL_SIMD_ARRAY hDD_dupD121 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputhDD_dupD121, UpwindAlgInputhDD_ddnD121), UpwindAlgInputhDD_ddnD121);
        const REAL_SIMD_ARRAY hDD_dupD122 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputhDD_dupD122, UpwindAlgInputhDD_ddnD122), UpwindAlgInputhDD_ddnD122);
        const REAL_SIMD_ARRAY hDD_dupD220 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputhDD_dupD220, UpwindAlgInputhDD_ddnD220), UpwindAlgInputhDD_ddnD220);
        const REAL_SIMD_ARRAY hDD_dupD221 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputhDD_dupD221, UpwindAlgInputhDD_ddnD221), UpwindAlgInputhDD_ddnD221);
        const REAL_SIMD_ARRAY hDD_dupD222 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputhDD_dupD222, UpwindAlgInputhDD_ddnD222), UpwindAlgInputhDD_ddnD222);
        const REAL_SIMD_ARRAY lambdaU_dupD00 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputlambdaU_dupD00, UpwindAlgInputlambdaU_ddnD00), UpwindAlgInputlambdaU_ddnD00);
        const REAL_SIMD_ARRAY lambdaU_dupD01 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputlambdaU_dupD01, UpwindAlgInputlambdaU_ddnD01), UpwindAlgInputlambdaU_ddnD01);
        const REAL_SIMD_ARRAY lambdaU_dupD02 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputlambdaU_dupD02, UpwindAlgInputlambdaU_ddnD02), UpwindAlgInputlambdaU_ddnD02);
        const REAL_SIMD_ARRAY lambdaU_dupD10 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputlambdaU_dupD10, UpwindAlgInputlambdaU_ddnD10), UpwindAlgInputlambdaU_ddnD10);
        const REAL_SIMD_ARRAY lambdaU_dupD11 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputlambdaU_dupD11, UpwindAlgInputlambdaU_ddnD11), UpwindAlgInputlambdaU_ddnD11);
        const REAL_SIMD_ARRAY lambdaU_dupD12 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputlambdaU_dupD12, UpwindAlgInputlambdaU_ddnD12), UpwindAlgInputlambdaU_ddnD12);
        const REAL_SIMD_ARRAY lambdaU_dupD20 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputlambdaU_dupD20, UpwindAlgInputlambdaU_ddnD20), UpwindAlgInputlambdaU_ddnD20);
        const REAL_SIMD_ARRAY lambdaU_dupD21 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputlambdaU_dupD21, UpwindAlgInputlambdaU_ddnD21), UpwindAlgInputlambdaU_ddnD21);
        const REAL_SIMD_ARRAY lambdaU_dupD22 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputlambdaU_dupD22, UpwindAlgInputlambdaU_ddnD22), UpwindAlgInputlambdaU_ddnD22);
        const REAL_SIMD_ARRAY trK_dupD0 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputtrK_dupD0, UpwindAlgInputtrK_ddnD0), UpwindAlgInputtrK_ddnD0);
        const REAL_SIMD_ARRAY trK_dupD1 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputtrK_dupD1, UpwindAlgInputtrK_ddnD1), UpwindAlgInputtrK_ddnD1);
        const REAL_SIMD_ARRAY trK_dupD2 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputtrK_dupD2, UpwindAlgInputtrK_ddnD2), UpwindAlgInputtrK_ddnD2);
        const REAL_SIMD_ARRAY vetU_dupD00 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputvetU_dupD00, UpwindAlgInputvetU_ddnD00), UpwindAlgInputvetU_ddnD00);
        const REAL_SIMD_ARRAY vetU_dupD01 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputvetU_dupD01, UpwindAlgInputvetU_ddnD01), UpwindAlgInputvetU_ddnD01);
        const REAL_SIMD_ARRAY vetU_dupD02 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputvetU_dupD02, UpwindAlgInputvetU_ddnD02), UpwindAlgInputvetU_ddnD02);
        const REAL_SIMD_ARRAY vetU_dupD10 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputvetU_dupD10, UpwindAlgInputvetU_ddnD10), UpwindAlgInputvetU_ddnD10);
        const REAL_SIMD_ARRAY vetU_dupD11 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputvetU_dupD11, UpwindAlgInputvetU_ddnD11), UpwindAlgInputvetU_ddnD11);
        const REAL_SIMD_ARRAY vetU_dupD12 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputvetU_dupD12, UpwindAlgInputvetU_ddnD12), UpwindAlgInputvetU_ddnD12);
        const REAL_SIMD_ARRAY vetU_dupD20 =
            FusedMulAddSIMD(Upwind0, SubSIMD(UpwindAlgInputvetU_dupD20, UpwindAlgInputvetU_ddnD20), UpwindAlgInputvetU_ddnD20);
        const REAL_SIMD_ARRAY vetU_dupD21 =
            FusedMulAddSIMD(Upwind1, SubSIMD(UpwindAlgInputvetU_dupD21, UpwindAlgInputvetU_ddnD21), UpwindAlgInputvetU_ddnD21);
        const REAL_SIMD_ARRAY vetU_dupD22 =
            FusedMulAddSIMD(Upwind2, SubSIMD(UpwindAlgInputvetU_dupD22, UpwindAlgInputvetU_ddnD22), UpwindAlgInputvetU_ddnD22);

        /*
         * NRPy+-Generated GF Access/FD Code, Step 3 of 3:
         * Evaluate SymPy expressions and write to main memory.
         */
        const double dblFDPart3_Integer_1 = 1.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_1 = ConstSIMD(dblFDPart3_Integer_1);

        const double dblFDPart3_Integer_2 = 2.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_2 = ConstSIMD(dblFDPart3_Integer_2);

        const double dblFDPart3_Integer_3 = 3.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_3 = ConstSIMD(dblFDPart3_Integer_3);

        const double dblFDPart3_Integer_4 = 4.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_4 = ConstSIMD(dblFDPart3_Integer_4);

        const double dblFDPart3_NegativeOne_ = -1.0;
        const REAL_SIMD_ARRAY FDPart3_NegativeOne_ = ConstSIMD(dblFDPart3_NegativeOne_);

        const double dblFDPart3_Rational_1_2 = 1.0 / 2.0;
        const REAL_SIMD_ARRAY FDPart3_Rational_1_2 = ConstSIMD(dblFDPart3_Rational_1_2);

        const double dblFDPart3_Rational_1_3 = 1.0 / 3.0;
        const REAL_SIMD_ARRAY FDPart3_Rational_1_3 = ConstSIMD(dblFDPart3_Rational_1_3);

        const double dblFDPart3_Rational_1_4 = 1.0 / 4.0;
        const REAL_SIMD_ARRAY FDPart3_Rational_1_4 = ConstSIMD(dblFDPart3_Rational_1_4);

        const double dblFDPart3_Rational_1_6 = 1.0 / 6.0;
        const REAL_SIMD_ARRAY FDPart3_Rational_1_6 = ConstSIMD(dblFDPart3_Rational_1_6);

        const double dblFDPart3_Rational_2_3 = 2.0 / 3.0;
        const REAL_SIMD_ARRAY FDPart3_Rational_2_3 = ConstSIMD(dblFDPart3_Rational_2_3);

        const double dblFDPart3_Rational_3_2 = 3.0 / 2.0;
        const REAL_SIMD_ARRAY FDPart3_Rational_3_2 = ConstSIMD(dblFDPart3_Rational_3_2);

        const double dblFDPart3_Rational_3_4 = 3.0 / 4.0;
        const REAL_SIMD_ARRAY FDPart3_Rational_3_4 = ConstSIMD(dblFDPart3_Rational_3_4);

        const double dblFDPart3_Rational_4_3 = 4.0 / 3.0;
        const REAL_SIMD_ARRAY FDPart3_Rational_4_3 = ConstSIMD(dblFDPart3_Rational_4_3);

        const REAL_SIMD_ARRAY FDPart3tmp0 = MulSIMD(aDD00, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp1 = AddSIMD(vetU_dD00, AddSIMD(vetU_dD11, vetU_dD22));
        const REAL_SIMD_ARRAY FDPart3tmp5 = AddSIMD(FDPart3_Integer_1, hDD11);
        const REAL_SIMD_ARRAY FDPart3tmp6 = AddSIMD(FDPart3_Integer_1, hDD22);
        const REAL_SIMD_ARRAY FDPart3tmp10 = AddSIMD(FDPart3_Integer_1, hDD00);
        const REAL_SIMD_ARRAY FDPart3tmp26 = MulSIMD(aDD01, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp28 = MulSIMD(aDD02, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp29 = MulSIMD(cf, cf);
        const REAL_SIMD_ARRAY FDPart3tmp30 = DivSIMD(FDPart3_Integer_1, cf);
        const REAL_SIMD_ARRAY FDPart3tmp36 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD122, hDD_dD221);
        const REAL_SIMD_ARRAY FDPart3tmp38 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD022, hDD_dD220);
        const REAL_SIMD_ARRAY FDPart3tmp48 = MulSIMD(FDPart3_Integer_1, MulSIMD(FDPart3_Rational_1_2, FDPart3_Rational_1_2));
        const REAL_SIMD_ARRAY FDPart3tmp55 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD121, hDD_dD112);
        const REAL_SIMD_ARRAY FDPart3tmp56 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD011, hDD_dD110);
        const REAL_SIMD_ARRAY FDPart3tmp78 = AddSIMD(hDD_dD120, SubSIMD(hDD_dD021, hDD_dD012));
        const REAL_SIMD_ARRAY FDPart3tmp82 = MulSIMD(FDPart3_Integer_2, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp90 = AddSIMD(hDD_dD120, SubSIMD(hDD_dD012, hDD_dD021));
        const REAL_SIMD_ARRAY FDPart3tmp121 = MulSIMD(aDD11, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp122 = MulSIMD(aDD12, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp123 = MulSIMD(aDD22, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp126 = AddSIMD(vetU_dDD001, AddSIMD(vetU_dDD111, vetU_dDD212));
        const REAL_SIMD_ARRAY FDPart3tmp127 = AddSIMD(vetU_dDD002, AddSIMD(vetU_dDD112, vetU_dDD222));
        const REAL_SIMD_ARRAY FDPart3tmp128 = AddSIMD(vetU_dDD000, AddSIMD(vetU_dDD101, vetU_dDD202));
        const REAL_SIMD_ARRAY FDPart3tmp130 = MulSIMD(FDPart3_Integer_3, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp145 =
            FusedMulAddSIMD(lambdaU1, vetU_dD01, FusedMulAddSIMD(lambdaU2, vetU_dD02, MulSIMD(lambdaU0, vetU_dD00)));
        const REAL_SIMD_ARRAY FDPart3tmp147 = MulSIMD(alpha, trK_dD1);
        const REAL_SIMD_ARRAY FDPart3tmp148 = MulSIMD(alpha, trK_dD2);
        const REAL_SIMD_ARRAY FDPart3tmp149 = MulSIMD(alpha, trK_dD0);
        const REAL_SIMD_ARRAY FDPart3tmp164 = MulSIMD(FDPart3_Rational_3_2, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp197 =
            FusedMulAddSIMD(lambdaU1, vetU_dD11, FusedMulAddSIMD(lambdaU2, vetU_dD12, MulSIMD(lambdaU0, vetU_dD10)));
        const REAL_SIMD_ARRAY FDPart3tmp204 =
            FusedMulAddSIMD(lambdaU1, vetU_dD21, FusedMulAddSIMD(lambdaU2, vetU_dD22, MulSIMD(lambdaU0, vetU_dD20)));
        const REAL_SIMD_ARRAY FDPart3tmp208 = MulSIMD(FDPart3_Integer_3, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp211 = MulSIMD(trK_dD1, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp212 = MulSIMD(trK_dD2, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp213 = MulSIMD(trK_dD0, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp217 = MulSIMD(FDPart3_Integer_4, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp17 = FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp6, hDD01));
        const REAL_SIMD_ARRAY FDPart3tmp20 = FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp5, hDD02));
        const REAL_SIMD_ARRAY FDPart3tmp24 = FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp10, hDD12));
        const REAL_SIMD_ARRAY FDPart3tmp25 = FusedMulSubSIMD(FDPart3tmp10, FDPart3tmp6, MulSIMD(hDD02, hDD02));
        const REAL_SIMD_ARRAY FDPart3tmp27 = FusedMulSubSIMD(FDPart3tmp10, FDPart3tmp5, MulSIMD(hDD01, hDD01));
        const REAL_SIMD_ARRAY FDPart3tmp31 = MulSIMD(FDPart3tmp30, cf_dD2);
        const REAL_SIMD_ARRAY FDPart3tmp34 = MulSIMD(FDPart3_Rational_1_2, FDPart3tmp30);
        const REAL_SIMD_ARRAY FDPart3tmp41 = MulSIMD(FDPart3tmp30, cf_dD0);
        const REAL_SIMD_ARRAY FDPart3tmp45 = MulSIMD(FDPart3tmp30, cf_dD1);
        const REAL_SIMD_ARRAY FDPart3tmp51 = DivSIMD(alpha, FDPart3tmp29);
        const REAL_SIMD_ARRAY FDPart3tmp111 = MulSIMD(FDPart3_Rational_2_3, FDPart3tmp1);
        const REAL_SIMD_ARRAY FDPart3tmp3 = MulSIMD(FDPart3_NegativeOne_, MulSIMD(FDPart3_Rational_2_3, FDPart3tmp1));
        const REAL_SIMD_ARRAY FDPart3tmp8 = FusedMulSubSIMD(FDPart3tmp5, FDPart3tmp6, MulSIMD(hDD12, hDD12));
        const REAL_SIMD_ARRAY FDPart3tmp13 = FusedMulAddSIMD(
            MulSIMD(FDPart3_Integer_2, hDD01), MulSIMD(hDD02, hDD12),
            FusedMulSubSIMD(FDPart3tmp10, MulSIMD(FDPart3tmp5, FDPart3tmp6),
                            FusedMulAddSIMD(FDPart3tmp5, MulSIMD(hDD02, hDD02),
                                            FusedMulAddSIMD(FDPart3tmp6, MulSIMD(hDD01, hDD01), MulSIMD(FDPart3tmp10, MulSIMD(hDD12, hDD12))))));
        const REAL_SIMD_ARRAY FDPart3tmp132 = FusedMulAddSIMD(FDPart3tmp130, DivSIMD(cf_dD0, cf), alpha_dD0);
        const REAL_SIMD_ARRAY FDPart3tmp150 = FusedMulAddSIMD(FDPart3tmp130, DivSIMD(cf_dD1, cf), alpha_dD1);
        const REAL_SIMD_ARRAY FDPart3tmp162 = FusedMulAddSIMD(FDPart3tmp130, DivSIMD(cf_dD2, cf), alpha_dD2);
        const REAL_SIMD_ARRAY FDPart3tmp209 = FusedMulAddSIMD(FDPart3tmp208, FDPart3tmp45, alpha_dD1);
        const REAL_SIMD_ARRAY FDPart3tmp210 = FusedMulAddSIMD(FDPart3tmp208, FDPart3tmp31, alpha_dD2);
        const REAL_SIMD_ARRAY FDPart3tmp214 = FusedMulAddSIMD(FDPart3tmp208, FDPart3tmp41, alpha_dD0);
        const REAL_SIMD_ARRAY FDPart3tmp14 = DivSIMD(FDPart3_Integer_1, FDPart3tmp13);
        const REAL_SIMD_ARRAY FDPart3tmp133 = DivSIMD(FDPart3_Integer_1, MulSIMD(FDPart3tmp13, FDPart3tmp13));
        const REAL_SIMD_ARRAY FDPart3tmp15 = MulSIMD(FDPart3tmp14, aDD00);
        const REAL_SIMD_ARRAY FDPart3tmp18 = MulSIMD(FDPart3tmp14, aDD01);
        const REAL_SIMD_ARRAY FDPart3tmp21 = MulSIMD(FDPart3tmp14, aDD02);
        const REAL_SIMD_ARRAY FDPart3tmp35 = MulSIMD(FDPart3tmp14, FDPart3tmp20);
        const REAL_SIMD_ARRAY FDPart3tmp37 = MulSIMD(FDPart3tmp14, FDPart3tmp17);
        const REAL_SIMD_ARRAY FDPart3tmp39 = MulSIMD(FDPart3tmp14, FDPart3tmp8);
        const REAL_SIMD_ARRAY FDPart3tmp42 = MulSIMD(FDPart3tmp14, FDPart3tmp24);
        const REAL_SIMD_ARRAY FDPart3tmp43 = MulSIMD(FDPart3tmp14, FDPart3tmp25);
        const REAL_SIMD_ARRAY FDPart3tmp46 = MulSIMD(FDPart3tmp14, FDPart3tmp27);
        const REAL_SIMD_ARRAY FDPart3tmp65 = MulSIMD(FDPart3tmp14, hDD_dD000);
        const REAL_SIMD_ARRAY FDPart3tmp89 = MulSIMD(FDPart3tmp14, hDD_dD002);
        const REAL_SIMD_ARRAY FDPart3tmp101 = MulSIMD(FDPart3tmp14, hDD_dD112);
        const REAL_SIMD_ARRAY FDPart3tmp102 = MulSIMD(FDPart3tmp14, hDD_dD221);
        const REAL_SIMD_ARRAY FDPart3tmp134 = MulSIMD(FDPart3tmp133, MulSIMD(FDPart3tmp17, FDPart3tmp17));
        const REAL_SIMD_ARRAY FDPart3tmp135 = MulSIMD(FDPart3tmp133, MulSIMD(FDPart3tmp20, FDPart3tmp20));
        const REAL_SIMD_ARRAY FDPart3tmp136 = MulSIMD(FDPart3tmp133, aDD00);
        const REAL_SIMD_ARRAY FDPart3tmp138 = MulSIMD(FDPart3tmp133, FDPart3tmp20);
        const REAL_SIMD_ARRAY FDPart3tmp151 = MulSIMD(FDPart3tmp133, FDPart3tmp17);
        const REAL_SIMD_ARRAY FDPart3tmp153 = MulSIMD(FDPart3tmp133, aDD22);
        const REAL_SIMD_ARRAY FDPart3tmp156 = MulSIMD(FDPart3tmp133, aDD11);
        const REAL_SIMD_ARRAY FDPart3tmp172 = MulSIMD(FDPart3tmp133, MulSIMD(FDPart3tmp24, FDPart3tmp24));
        const REAL_SIMD_ARRAY FDPart3tmp40 =
            FusedMulAddSIMD(FDPart3tmp36, FDPart3tmp37, FusedMulAddSIMD(FDPart3tmp38, FDPart3tmp39, MulSIMD(FDPart3tmp35, hDD_dD222)));
        const REAL_SIMD_ARRAY FDPart3tmp44 =
            FusedMulAddSIMD(FDPart3tmp37, FDPart3tmp38, FusedMulAddSIMD(FDPart3tmp42, hDD_dD222, MulSIMD(FDPart3tmp36, FDPart3tmp43)));
        const REAL_SIMD_ARRAY FDPart3tmp47 =
            FusedMulAddSIMD(FDPart3tmp36, FDPart3tmp42, FusedMulAddSIMD(FDPart3tmp46, hDD_dD222, MulSIMD(FDPart3tmp35, FDPart3tmp38)));
        const REAL_SIMD_ARRAY FDPart3tmp57 =
            FusedMulAddSIMD(FDPart3tmp37, hDD_dD111, FusedMulAddSIMD(FDPart3tmp39, FDPart3tmp56, MulSIMD(FDPart3tmp35, FDPart3tmp55)));
        const REAL_SIMD_ARRAY FDPart3tmp58 =
            FusedMulAddSIMD(FDPart3tmp42, FDPart3tmp55, FusedMulAddSIMD(FDPart3tmp43, hDD_dD111, MulSIMD(FDPart3tmp37, FDPart3tmp56)));
        const REAL_SIMD_ARRAY FDPart3tmp59 =
            FusedMulAddSIMD(FDPart3tmp42, hDD_dD111, FusedMulAddSIMD(FDPart3tmp46, FDPart3tmp55, MulSIMD(FDPart3tmp35, FDPart3tmp56)));
        const REAL_SIMD_ARRAY FDPart3tmp68 =
            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp17, FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD010, hDD_dD001)),
                            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp20, FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD020, hDD_dD002)),
                                            MulSIMD(FDPart3tmp65, FDPart3tmp8)));
        const REAL_SIMD_ARRAY FDPart3tmp69 =
            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp24, FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD020, hDD_dD002)),
                            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp25, FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD010, hDD_dD001)),
                                            MulSIMD(FDPart3tmp17, FDPart3tmp65)));
        const REAL_SIMD_ARRAY FDPart3tmp70 =
            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp24, FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD010, hDD_dD001)),
                            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp27, FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD020, hDD_dD002)),
                                            MulSIMD(FDPart3tmp20, FDPart3tmp65)));
        const REAL_SIMD_ARRAY FDPart3tmp79 =
            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp17, hDD_dD110),
                            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp8, hDD_dD001), MulSIMD(FDPart3tmp35, FDPart3tmp78)));
        const REAL_SIMD_ARRAY FDPart3tmp80 =
            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp17, hDD_dD001),
                            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp25, hDD_dD110), MulSIMD(FDPart3tmp42, FDPart3tmp78)));
        const REAL_SIMD_ARRAY FDPart3tmp81 =
            FusedMulAddSIMD(FDPart3tmp42, hDD_dD110, FusedMulAddSIMD(FDPart3tmp46, FDPart3tmp78, MulSIMD(FDPart3tmp35, hDD_dD001)));
        const REAL_SIMD_ARRAY FDPart3tmp92 =
            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp17, FDPart3tmp90),
                            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp20, hDD_dD220), MulSIMD(FDPart3tmp8, FDPart3tmp89)));
        const REAL_SIMD_ARRAY FDPart3tmp93 = FusedMulAddSIMD(
            FDPart3tmp42, hDD_dD220, FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp25, FDPart3tmp90), MulSIMD(FDPart3tmp17, FDPart3tmp89)));
        const REAL_SIMD_ARRAY FDPart3tmp94 = FusedMulAddSIMD(
            FDPart3tmp42, FDPart3tmp90, FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp27, hDD_dD220), MulSIMD(FDPart3tmp20, FDPart3tmp89)));
        const REAL_SIMD_ARRAY FDPart3tmp104 =
            FusedMulAddSIMD(FDPart3tmp102, FDPart3tmp20,
                            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp8, AddSIMD(hDD_dD021, SubSIMD(hDD_dD012, hDD_dD120))),
                                            MulSIMD(FDPart3tmp101, FDPart3tmp17)));
        const REAL_SIMD_ARRAY FDPart3tmp105 =
            FusedMulAddSIMD(FDPart3tmp102, FDPart3tmp24,
                            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp17, AddSIMD(hDD_dD021, SubSIMD(hDD_dD012, hDD_dD120))),
                                            MulSIMD(FDPart3tmp101, FDPart3tmp25)));
        const REAL_SIMD_ARRAY FDPart3tmp106 =
            FusedMulAddSIMD(FDPart3tmp102, FDPart3tmp27,
                            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp20, AddSIMD(hDD_dD021, SubSIMD(hDD_dD012, hDD_dD120))),
                                            MulSIMD(FDPart3tmp101, FDPart3tmp24)));
        const REAL_SIMD_ARRAY FDPart3tmp112 =
            FusedMulAddSIMD(FDPart3tmp35, aDD12, FusedMulAddSIMD(FDPart3tmp37, aDD11, MulSIMD(FDPart3tmp18, FDPart3tmp8)));
        const REAL_SIMD_ARRAY FDPart3tmp113 = MulSIMD(FDPart3tmp42, aDD12);
        const REAL_SIMD_ARRAY FDPart3tmp116 =
            FusedMulAddSIMD(FDPart3tmp42, aDD11, FusedMulAddSIMD(FDPart3tmp46, aDD12, MulSIMD(FDPart3tmp18, FDPart3tmp20)));
        const REAL_SIMD_ARRAY FDPart3tmp117 =
            FusedMulAddSIMD(FDPart3tmp35, aDD22, FusedMulAddSIMD(FDPart3tmp37, aDD12, MulSIMD(FDPart3tmp21, FDPart3tmp8)));
        const REAL_SIMD_ARRAY FDPart3tmp118 =
            FusedMulAddSIMD(FDPart3tmp42, aDD22, FusedMulAddSIMD(FDPart3tmp43, aDD12, MulSIMD(FDPart3tmp17, FDPart3tmp21)));
        const REAL_SIMD_ARRAY FDPart3tmp125 =
            FusedMulAddSIMD(FDPart3tmp37, vetU_dDD001, FusedMulAddSIMD(FDPart3tmp42, vetU_dDD012, MulSIMD(FDPart3tmp35, vetU_dDD002)));
        const REAL_SIMD_ARRAY FDPart3tmp129 =
            FusedMulAddSIMD(FDPart3tmp127, FDPart3tmp35, FusedMulAddSIMD(FDPart3tmp128, FDPart3tmp39, MulSIMD(FDPart3tmp126, FDPart3tmp37)));
        const REAL_SIMD_ARRAY FDPart3tmp137 =
            FusedMulAddSIMD(FDPart3tmp135, aDD22, FusedMulAddSIMD(FDPart3tmp136, MulSIMD(FDPart3tmp8, FDPart3tmp8), MulSIMD(FDPart3tmp134, aDD11)));
        const REAL_SIMD_ARRAY FDPart3tmp139 = MulSIMD(FDPart3tmp138, FDPart3tmp17);
        const REAL_SIMD_ARRAY FDPart3tmp141 = MulSIMD(FDPart3tmp133, MulSIMD(FDPart3tmp8, aDD01));
        const REAL_SIMD_ARRAY FDPart3tmp142 = MulSIMD(FDPart3tmp133, MulSIMD(FDPart3tmp8, aDD02));
        const REAL_SIMD_ARRAY FDPart3tmp146 =
            FusedMulAddSIMD(FDPart3tmp43, vetU_dDD011, FusedMulAddSIMD(FDPart3tmp46, vetU_dDD022, MulSIMD(FDPart3tmp39, vetU_dDD000)));
        const REAL_SIMD_ARRAY FDPart3tmp173 =
            FusedMulAddSIMD(FDPart3tmp156, MulSIMD(FDPart3tmp25, FDPart3tmp25), FusedMulAddSIMD(FDPart3tmp172, aDD22, MulSIMD(FDPart3tmp134, aDD00)));
        const REAL_SIMD_ARRAY FDPart3tmp178 = MulSIMD(FDPart3tmp133, MulSIMD(FDPart3tmp27, aDD12));
        const REAL_SIMD_ARRAY FDPart3tmp180 =
            FusedMulAddSIMD(FDPart3tmp153, MulSIMD(FDPart3tmp27, FDPart3tmp27), FusedMulAddSIMD(FDPart3tmp172, aDD11, MulSIMD(FDPart3tmp135, aDD00)));
        const REAL_SIMD_ARRAY FDPart3tmp187 = MulSIMD(FDPart3_Rational_4_3, FDPart3tmp37);
        const REAL_SIMD_ARRAY FDPart3tmp188 = MulSIMD(FDPart3_Rational_4_3, FDPart3tmp35);
        const REAL_SIMD_ARRAY FDPart3tmp189 = MulSIMD(FDPart3_Rational_4_3, FDPart3tmp42);
        const REAL_SIMD_ARRAY FDPart3tmp190 = MulSIMD(FDPart3_Rational_2_3, FDPart3tmp46);
        const REAL_SIMD_ARRAY FDPart3tmp191 = MulSIMD(FDPart3_Rational_2_3, FDPart3tmp43);
        const REAL_SIMD_ARRAY FDPart3tmp192 = MulSIMD(FDPart3_Rational_2_3, FDPart3tmp39);
        const REAL_SIMD_ARRAY FDPart3tmp194 =
            FusedMulAddSIMD(FDPart3tmp37, vetU_dDD101, FusedMulAddSIMD(FDPart3tmp42, vetU_dDD112, MulSIMD(FDPart3tmp35, vetU_dDD102)));
        const REAL_SIMD_ARRAY FDPart3tmp195 =
            FusedMulAddSIMD(FDPart3tmp127, FDPart3tmp42, FusedMulAddSIMD(FDPart3tmp128, FDPart3tmp37, MulSIMD(FDPart3tmp126, FDPart3tmp43)));
        const REAL_SIMD_ARRAY FDPart3tmp198 =
            FusedMulAddSIMD(FDPart3tmp43, vetU_dDD111, FusedMulAddSIMD(FDPart3tmp46, vetU_dDD122, MulSIMD(FDPart3tmp39, vetU_dDD100)));
        const REAL_SIMD_ARRAY FDPart3tmp201 =
            FusedMulAddSIMD(FDPart3tmp37, vetU_dDD201, FusedMulAddSIMD(FDPart3tmp42, vetU_dDD212, MulSIMD(FDPart3tmp35, vetU_dDD202)));
        const REAL_SIMD_ARRAY FDPart3tmp202 =
            FusedMulAddSIMD(FDPart3tmp127, FDPart3tmp46, FusedMulAddSIMD(FDPart3tmp128, FDPart3tmp35, MulSIMD(FDPart3tmp126, FDPart3tmp42)));
        const REAL_SIMD_ARRAY FDPart3tmp205 =
            FusedMulAddSIMD(FDPart3tmp43, vetU_dDD211, FusedMulAddSIMD(FDPart3tmp46, vetU_dDD222, MulSIMD(FDPart3tmp39, vetU_dDD200)));
        const REAL_SIMD_ARRAY FDPart3tmp23 = FusedMulAddSIMD(FDPart3tmp17, FDPart3tmp18, MulSIMD(FDPart3tmp20, FDPart3tmp21));
        const REAL_SIMD_ARRAY FDPart3tmp49 = FusedMulAddSIMD(
            FDPart3_Integer_2,
            FusedMulAddSIMD(
                FDPart3tmp31, alpha_dD2,
                MulSIMD(alpha, FusedMulAddSIMD(FDPart3tmp34, FusedMulSubSIMD(FDPart3tmp30, MulSIMD(cf_dD2, cf_dD2), cf_dDD22),
                                               MulSIMD(FDPart3tmp48, FusedMulAddSIMD(FDPart3tmp40, FDPart3tmp41,
                                                                                     FusedMulAddSIMD(FDPart3tmp44, FDPart3tmp45,
                                                                                                     MulSIMD(FDPart3tmp31, FDPart3tmp47))))))),
            alpha_dDD22);
        const REAL_SIMD_ARRAY FDPart3tmp50 =
            FusedMulAddSIMD(FDPart3tmp44, alpha_dD1, FusedMulAddSIMD(FDPart3tmp47, alpha_dD2, MulSIMD(FDPart3tmp40, alpha_dD0)));
        const REAL_SIMD_ARRAY FDPart3tmp60 = FusedMulAddSIMD(
            FDPart3_Integer_2,
            FusedMulAddSIMD(
                FDPart3tmp45, alpha_dD1,
                MulSIMD(alpha, FusedMulAddSIMD(FDPart3tmp34, FusedMulSubSIMD(FDPart3tmp30, MulSIMD(cf_dD1, cf_dD1), cf_dDD11),
                                               MulSIMD(FDPart3tmp48, FusedMulAddSIMD(FDPart3tmp41, FDPart3tmp57,
                                                                                     FusedMulAddSIMD(FDPart3tmp45, FDPart3tmp58,
                                                                                                     MulSIMD(FDPart3tmp31, FDPart3tmp59))))))),
            alpha_dDD11);
        const REAL_SIMD_ARRAY FDPart3tmp61 =
            FusedMulAddSIMD(FDPart3tmp58, alpha_dD1, FusedMulAddSIMD(FDPart3tmp59, alpha_dD2, MulSIMD(FDPart3tmp57, alpha_dD0)));
        const REAL_SIMD_ARRAY FDPart3tmp71 = FusedMulAddSIMD(
            FDPart3_Integer_2,
            FusedMulAddSIMD(
                FDPart3tmp41, alpha_dD0,
                MulSIMD(alpha, FusedMulAddSIMD(FDPart3tmp34, FusedMulSubSIMD(FDPart3tmp30, MulSIMD(cf_dD0, cf_dD0), cf_dDD00),
                                               MulSIMD(FDPart3tmp48, FusedMulAddSIMD(FDPart3tmp41, FDPart3tmp68,
                                                                                     FusedMulAddSIMD(FDPart3tmp45, FDPart3tmp69,
                                                                                                     MulSIMD(FDPart3tmp31, FDPart3tmp70))))))),
            alpha_dDD00);
        const REAL_SIMD_ARRAY FDPart3tmp72 =
            FusedMulAddSIMD(FDPart3tmp69, alpha_dD1, FusedMulAddSIMD(FDPart3tmp70, alpha_dD2, MulSIMD(FDPart3tmp68, alpha_dD0)));
        const REAL_SIMD_ARRAY FDPart3tmp83 = FusedMulAddSIMD(
            FDPart3tmp45, alpha_dD0,
            FusedMulAddSIMD(FDPart3tmp82,
                            FusedMulAddSIMD(FDPart3tmp34, FusedMulSubSIMD(FDPart3tmp41, cf_dD1, cf_dDD01),
                                            MulSIMD(FDPart3tmp48, FusedMulAddSIMD(FDPart3tmp41, FDPart3tmp79,
                                                                                  FusedMulAddSIMD(FDPart3tmp45, FDPart3tmp80,
                                                                                                  MulSIMD(FDPart3tmp31, FDPart3tmp81))))),
                            FusedMulAddSIMD(FDPart3tmp41, alpha_dD1, alpha_dDD01)));
        const REAL_SIMD_ARRAY FDPart3tmp84 =
            FusedMulAddSIMD(FDPart3tmp80, alpha_dD1, FusedMulAddSIMD(FDPart3tmp81, alpha_dD2, MulSIMD(FDPart3tmp79, alpha_dD0)));
        const REAL_SIMD_ARRAY FDPart3tmp95 = FusedMulAddSIMD(
            FDPart3tmp41, alpha_dD2,
            FusedMulAddSIMD(FDPart3tmp82,
                            FusedMulAddSIMD(FDPart3tmp34, FusedMulSubSIMD(FDPart3tmp41, cf_dD2, cf_dDD02),
                                            MulSIMD(FDPart3tmp48, FusedMulAddSIMD(FDPart3tmp41, FDPart3tmp92,
                                                                                  FusedMulAddSIMD(FDPart3tmp45, FDPart3tmp93,
                                                                                                  MulSIMD(FDPart3tmp31, FDPart3tmp94))))),
                            FusedMulAddSIMD(FDPart3tmp31, alpha_dD0, alpha_dDD02)));
        const REAL_SIMD_ARRAY FDPart3tmp96 =
            FusedMulAddSIMD(FDPart3tmp93, alpha_dD1, FusedMulAddSIMD(FDPart3tmp94, alpha_dD2, MulSIMD(FDPart3tmp92, alpha_dD0)));
        const REAL_SIMD_ARRAY FDPart3tmp107 = FusedMulAddSIMD(
            FDPart3tmp45, alpha_dD2,
            FusedMulAddSIMD(FDPart3tmp82,
                            FusedMulAddSIMD(FDPart3tmp34, FusedMulSubSIMD(FDPart3tmp45, cf_dD2, cf_dDD12),
                                            MulSIMD(FDPart3tmp48, FusedMulAddSIMD(FDPart3tmp105, FDPart3tmp45,
                                                                                  FusedMulAddSIMD(FDPart3tmp106, FDPart3tmp31,
                                                                                                  MulSIMD(FDPart3tmp104, FDPart3tmp41))))),
                            FusedMulAddSIMD(FDPart3tmp31, alpha_dD1, alpha_dDD12)));
        const REAL_SIMD_ARRAY FDPart3tmp108 =
            FusedMulAddSIMD(FDPart3tmp105, alpha_dD1, FusedMulAddSIMD(FDPart3tmp106, alpha_dD2, MulSIMD(FDPart3tmp104, alpha_dD0)));
        const REAL_SIMD_ARRAY FDPart3tmp115 = FusedMulAddSIMD(FDPart3tmp17, FDPart3tmp18, FusedMulAddSIMD(FDPart3tmp43, aDD11, FDPart3tmp113));
        const REAL_SIMD_ARRAY FDPart3tmp120 = FusedMulAddSIMD(FDPart3tmp20, FDPart3tmp21, FusedMulAddSIMD(FDPart3tmp46, aDD22, FDPart3tmp113));
        const REAL_SIMD_ARRAY FDPart3tmp143 =
            FusedMulAddSIMD(FDPart3tmp141, FDPart3tmp17, FusedMulAddSIMD(FDPart3tmp142, FDPart3tmp20, MulSIMD(FDPart3tmp139, aDD12)));
        const REAL_SIMD_ARRAY FDPart3tmp159 = FusedMulAddSIMD(
            FDPart3tmp151, MulSIMD(FDPart3tmp24, aDD12),
            FusedMulAddSIMD(
                FDPart3tmp136, MulSIMD(FDPart3tmp17, FDPart3tmp8),
                FusedMulAddSIMD(FDPart3tmp138, MulSIMD(FDPart3tmp25, aDD12),
                                FusedMulAddSIMD(FDPart3tmp141, FDPart3tmp25,
                                                FusedMulAddSIMD(FDPart3tmp142, FDPart3tmp24,
                                                                FusedMulAddSIMD(FDPart3tmp153, MulSIMD(FDPart3tmp20, FDPart3tmp24),
                                                                                FusedMulAddSIMD(FDPart3tmp156, MulSIMD(FDPart3tmp17, FDPart3tmp25),
                                                                                                FusedMulAddSIMD(FDPart3tmp134, aDD01,
                                                                                                                MulSIMD(FDPart3tmp139, aDD02)))))))));
        const REAL_SIMD_ARRAY FDPart3tmp161 = FusedMulAddSIMD(
            FDPart3tmp151, MulSIMD(FDPart3tmp27, aDD12),
            FusedMulAddSIMD(
                FDPart3tmp136, MulSIMD(FDPart3tmp20, FDPart3tmp8),
                FusedMulAddSIMD(FDPart3tmp138, MulSIMD(FDPart3tmp24, aDD12),
                                FusedMulAddSIMD(FDPart3tmp141, FDPart3tmp24,
                                                FusedMulAddSIMD(FDPart3tmp142, FDPart3tmp27,
                                                                FusedMulAddSIMD(FDPart3tmp153, MulSIMD(FDPart3tmp20, FDPart3tmp27),
                                                                                FusedMulAddSIMD(FDPart3tmp156, MulSIMD(FDPart3tmp17, FDPart3tmp24),
                                                                                                FusedMulAddSIMD(FDPart3tmp135, aDD02,
                                                                                                                MulSIMD(FDPart3tmp139, aDD01)))))))));
        const REAL_SIMD_ARRAY FDPart3tmp171 = FusedMulAddSIMD(FDPart3tmp151, MulSIMD(FDPart3tmp25, aDD01),
                                                              FusedMulAddSIMD(MulSIMD(FDPart3tmp133, FDPart3tmp24), MulSIMD(FDPart3tmp25, aDD12),
                                                                              MulSIMD(FDPart3tmp151, MulSIMD(FDPart3tmp24, aDD02))));
        const REAL_SIMD_ARRAY FDPart3tmp179 =
            FusedMulAddSIMD(FDPart3tmp138, MulSIMD(FDPart3tmp24, aDD01),
                            FusedMulAddSIMD(FDPart3tmp138, MulSIMD(FDPart3tmp27, aDD02), MulSIMD(FDPart3tmp178, FDPart3tmp24)));
        const REAL_SIMD_ARRAY FDPart3tmp185 = FusedMulAddSIMD(
            FDPart3tmp151, MulSIMD(FDPart3tmp27, aDD02),
            FusedMulAddSIMD(
                FDPart3tmp138, MulSIMD(FDPart3tmp25, aDD01),
                FusedMulAddSIMD(
                    FDPart3tmp151, MulSIMD(FDPart3tmp24, aDD01),
                    FusedMulAddSIMD(FDPart3tmp136, MulSIMD(FDPart3tmp17, FDPart3tmp20),
                                    FusedMulAddSIMD(FDPart3tmp138, MulSIMD(FDPart3tmp24, aDD02),
                                                    FusedMulAddSIMD(FDPart3tmp153, MulSIMD(FDPart3tmp24, FDPart3tmp27),
                                                                    FusedMulAddSIMD(FDPart3tmp156, MulSIMD(FDPart3tmp24, FDPart3tmp25),
                                                                                    FusedMulAddSIMD(FDPart3tmp172, aDD12,
                                                                                                    MulSIMD(FDPart3tmp178, FDPart3tmp25)))))))));
        const REAL_SIMD_ARRAY FDPart3tmp193 = MulSIMD(
            FDPart3tmp1, FusedMulAddSIMD(FDPart3tmp188, FDPart3tmp92,
                                         FusedMulAddSIMD(FDPart3tmp190, FDPart3tmp40,
                                                         FusedMulAddSIMD(FDPart3tmp191, FDPart3tmp57,
                                                                         FusedMulAddSIMD(FDPart3tmp192, FDPart3tmp68,
                                                                                         FusedMulAddSIMD(FDPart3tmp104, FDPart3tmp189,
                                                                                                         MulSIMD(FDPart3tmp187, FDPart3tmp79)))))));
        const REAL_SIMD_ARRAY FDPart3tmp200 = MulSIMD(
            FDPart3tmp1, FusedMulAddSIMD(FDPart3tmp188, FDPart3tmp93,
                                         FusedMulAddSIMD(FDPart3tmp190, FDPart3tmp44,
                                                         FusedMulAddSIMD(FDPart3tmp191, FDPart3tmp58,
                                                                         FusedMulAddSIMD(FDPart3tmp192, FDPart3tmp69,
                                                                                         FusedMulAddSIMD(FDPart3tmp105, FDPart3tmp189,
                                                                                                         MulSIMD(FDPart3tmp187, FDPart3tmp80)))))));
        const REAL_SIMD_ARRAY FDPart3tmp206 = MulSIMD(
            FDPart3tmp1, FusedMulAddSIMD(FDPart3tmp188, FDPart3tmp94,
                                         FusedMulAddSIMD(FDPart3tmp190, FDPart3tmp47,
                                                         FusedMulAddSIMD(FDPart3tmp191, FDPart3tmp59,
                                                                         FusedMulAddSIMD(FDPart3tmp192, FDPart3tmp70,
                                                                                         FusedMulAddSIMD(FDPart3tmp106, FDPart3tmp189,
                                                                                                         MulSIMD(FDPart3tmp187, FDPart3tmp81)))))));
        const REAL_SIMD_ARRAY FDPart3tmp52 = FusedMulAddSIMD(
            RbarDD22, alpha,
            FusedMulAddSIMD(FDPart3_Integer_1, MulSIMD(FDPart3_Rational_1_2, FDPart3tmp50), MulSIMD(FDPart3tmp51, MulSIMD(cf_dD2, cf_dD2))));
        const REAL_SIMD_ARRAY FDPart3tmp62 = FusedMulAddSIMD(
            RbarDD11, alpha,
            FusedMulAddSIMD(FDPart3_Integer_1, MulSIMD(FDPart3_Rational_1_2, FDPart3tmp61), MulSIMD(FDPart3tmp51, MulSIMD(cf_dD1, cf_dD1))));
        const REAL_SIMD_ARRAY FDPart3tmp73 = FusedMulAddSIMD(
            RbarDD00, alpha,
            FusedMulAddSIMD(FDPart3_Integer_1, MulSIMD(FDPart3_Rational_1_2, FDPart3tmp72), MulSIMD(FDPart3tmp51, MulSIMD(cf_dD0, cf_dD0))));
        const REAL_SIMD_ARRAY FDPart3tmp85 = FusedMulAddSIMD(FDPart3_Integer_1, MulSIMD(FDPart3_Rational_1_2, FDPart3tmp84),
                                                             FusedMulAddSIMD(FDPart3tmp51, MulSIMD(cf_dD0, cf_dD1), MulSIMD(RbarDD01, alpha)));
        const REAL_SIMD_ARRAY FDPart3tmp98 = FusedMulAddSIMD(FDPart3_Integer_1, MulSIMD(FDPart3_Rational_1_2, FDPart3tmp96),
                                                             FusedMulAddSIMD(FDPart3tmp51, MulSIMD(cf_dD0, cf_dD2), MulSIMD(RbarDD02, alpha)));
        const REAL_SIMD_ARRAY FDPart3tmp109 = FusedMulAddSIMD(FDPart3_Integer_1, MulSIMD(FDPart3_Rational_1_2, FDPart3tmp108),
                                                              FusedMulAddSIMD(FDPart3tmp51, MulSIMD(cf_dD1, cf_dD2), MulSIMD(RbarDD12, alpha)));
        const REAL_SIMD_ARRAY FDPart3tmp144 = FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp137, MulSIMD(FDPart3_Integer_4, FDPart3tmp143));
        const REAL_SIMD_ARRAY FDPart3tmp165 = FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp143, FDPart3tmp137);
        const REAL_SIMD_ARRAY FDPart3tmp174 = FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp171, FDPart3tmp173);
        const REAL_SIMD_ARRAY FDPart3tmp181 = FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp179, FDPart3tmp180);
        const REAL_SIMD_ARRAY FDPart3tmp183 = MulSIMD(FDPart3tmp130, FDPart3tmp159);
        const REAL_SIMD_ARRAY FDPart3tmp184 = MulSIMD(FDPart3tmp130, FDPart3tmp161);
        const REAL_SIMD_ARRAY FDPart3tmp186 = MulSIMD(FDPart3tmp130, FDPart3tmp185);
        const REAL_SIMD_ARRAY FDPart3tmp196 = FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp173, MulSIMD(FDPart3_Integer_4, FDPart3tmp171));
        const REAL_SIMD_ARRAY FDPart3tmp203 = FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp180, MulSIMD(FDPart3_Integer_4, FDPart3tmp179));
        const REAL_SIMD_ARRAY FDPart3tmp218 = MulSIMD(FDPart3tmp159, FDPart3tmp217);
        const REAL_SIMD_ARRAY FDPart3tmp219 = MulSIMD(FDPart3tmp161, FDPart3tmp217);
        const REAL_SIMD_ARRAY FDPart3tmp220 = MulSIMD(FDPart3tmp185, FDPart3tmp217);
        const REAL_SIMD_ARRAY FDPart3tmp175 = MulSIMD(FDPart3tmp164, FDPart3tmp174);
        const REAL_SIMD_ARRAY FDPart3tmp182 = MulSIMD(FDPart3tmp164, FDPart3tmp181);
        const REAL_SIMD_ARRAY FDPart3tmp215 = MulSIMD(FDPart3tmp174, FDPart3tmp82);
        const REAL_SIMD_ARRAY FDPart3tmp216 = MulSIMD(FDPart3tmp181, FDPart3tmp82);
        const REAL_SIMD_ARRAY __RHS_exp_0 = FusedMulAddSIMD(
            aDD_dKOD002, diss_strength,
            FusedMulAddSIMD(
                aDD_dupD000, vetU0,
                FusedMulAddSIMD(
                    aDD_dKOD000, diss_strength,
                    FusedMulAddSIMD(
                        aDD_dKOD001, diss_strength,
                        FusedMulAddSIMD(
                            FDPart3tmp29,
                            SubSIMD(
                                FDPart3tmp73,
                                FusedMulAddSIMD(
                                    FDPart3_Rational_1_3,
                                    MulSIMD(FDPart3tmp10,
                                            FusedMulAddSIMD(
                                                FDPart3tmp43, SubSIMD(FDPart3tmp62, FDPart3tmp60),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp46, SubSIMD(FDPart3tmp52, FDPart3tmp49),
                                                    FusedMulAddSIMD(
                                                        FDPart3_Integer_2,
                                                        FusedMulAddSIMD(FDPart3tmp37, SubSIMD(FDPart3tmp85, FDPart3tmp83),
                                                                        FusedMulAddSIMD(FDPart3tmp42, SubSIMD(FDPart3tmp109, FDPart3tmp107),
                                                                                        MulSIMD(FDPart3tmp35, SubSIMD(FDPart3tmp98, FDPart3tmp95)))),
                                                        MulSIMD(FDPart3tmp39, SubSIMD(FDPart3tmp73, FDPart3tmp71)))))),
                                    FDPart3tmp71)),
                            FusedMulAddSIMD(
                                FDPart3tmp3, aDD00,
                                FusedMulAddSIMD(
                                    aDD_dupD001, vetU1,
                                    FusedMulAddSIMD(
                                        aDD_dupD002, vetU2,
                                        FusedMulAddSIMD(
                                            FDPart3_Integer_2,
                                            FusedMulAddSIMD(
                                                aDD01, vetU_dD10,
                                                FusedMulAddSIMD(
                                                    aDD02, vetU_dD20,
                                                    FusedMulSubSIMD(
                                                        aDD00, vetU_dD00,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp26,
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp18, FDPart3tmp25,
                                                                FusedMulAddSIMD(FDPart3tmp21, FDPart3tmp24, MulSIMD(FDPart3tmp15, FDPart3tmp17))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp28,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp18, FDPart3tmp24,
                                                                    FusedMulAddSIMD(FDPart3tmp21, FDPart3tmp27, MulSIMD(FDPart3tmp15, FDPart3tmp20))),
                                                                MulSIMD(FDPart3tmp0, FusedMulAddSIMD(FDPart3tmp15, FDPart3tmp8, FDPart3tmp23))))))),
                                            MulSIMD(FDPart3tmp0, trK))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_1 = FusedMulAddSIMD(
            aDD_dupD010, vetU0,
            FusedMulAddSIMD(
                aDD_dupD011, vetU1,
                FusedMulAddSIMD(
                    aDD_dKOD011, diss_strength,
                    FusedMulAddSIMD(
                        aDD_dKOD012, diss_strength,
                        FusedMulAddSIMD(
                            aDD12, vetU_dD20,
                            FusedMulAddSIMD(
                                aDD_dKOD010, diss_strength,
                                FusedMulAddSIMD(
                                    aDD02, vetU_dD21,
                                    FusedMulAddSIMD(
                                        aDD11, vetU_dD10,
                                        FusedMulAddSIMD(
                                            aDD01, vetU_dD00,
                                            FusedMulAddSIMD(
                                                aDD01, vetU_dD11,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp29,
                                                    SubSIMD(
                                                        FDPart3tmp85,
                                                        FusedMulAddSIMD(
                                                            FDPart3_Rational_1_3,
                                                            MulSIMD(hDD01,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp43, SubSIMD(FDPart3tmp62, FDPart3tmp60),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp46, SubSIMD(FDPart3tmp52, FDPart3tmp49),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3_Integer_2,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp37, SubSIMD(FDPart3tmp85, FDPart3tmp83),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp42, SubSIMD(FDPart3tmp109, FDPart3tmp107),
                                                                                        MulSIMD(FDPart3tmp35, SubSIMD(FDPart3tmp98, FDPart3tmp95)))),
                                                                                MulSIMD(FDPart3tmp39, SubSIMD(FDPart3tmp73, FDPart3tmp71)))))),
                                                            FDPart3tmp83)),
                                                    FusedMulAddSIMD(
                                                        aDD00, vetU_dD01,
                                                        FusedMulAddSIMD(
                                                            aDD_dupD012, vetU2,
                                                            FusedMulSubSIMD(
                                                                FDPart3tmp26, trK,
                                                                FusedMulAddSIMD(FDPart3_Integer_2,
                                                                                FusedMulAddSIMD(FDPart3tmp115, FDPart3tmp26,
                                                                                                FusedMulAddSIMD(FDPart3tmp116, FDPart3tmp28,
                                                                                                                MulSIMD(FDPart3tmp0, FDPart3tmp112))),
                                                                                MulSIMD(FDPart3tmp111, aDD01))))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_2 = FusedMulAddSIMD(
            aDD_dupD020, vetU0,
            FusedMulAddSIMD(
                aDD_dupD021, vetU1,
                FusedMulAddSIMD(
                    aDD_dKOD021, diss_strength,
                    FusedMulAddSIMD(
                        aDD_dKOD022, diss_strength,
                        FusedMulAddSIMD(
                            aDD22, vetU_dD20,
                            FusedMulAddSIMD(
                                aDD_dKOD020, diss_strength,
                                FusedMulAddSIMD(
                                    aDD02, vetU_dD22,
                                    FusedMulAddSIMD(
                                        aDD12, vetU_dD10,
                                        FusedMulAddSIMD(
                                            aDD01, vetU_dD12,
                                            FusedMulAddSIMD(
                                                aDD02, vetU_dD00,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp29,
                                                    SubSIMD(
                                                        FDPart3tmp98,
                                                        FusedMulAddSIMD(
                                                            FDPart3_Rational_1_3,
                                                            MulSIMD(hDD02,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp43, SubSIMD(FDPart3tmp62, FDPart3tmp60),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp46, SubSIMD(FDPart3tmp52, FDPart3tmp49),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3_Integer_2,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp37, SubSIMD(FDPart3tmp85, FDPart3tmp83),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp42, SubSIMD(FDPart3tmp109, FDPart3tmp107),
                                                                                        MulSIMD(FDPart3tmp35, SubSIMD(FDPart3tmp98, FDPart3tmp95)))),
                                                                                MulSIMD(FDPart3tmp39, SubSIMD(FDPart3tmp73, FDPart3tmp71)))))),
                                                            FDPart3tmp95)),
                                                    FusedMulAddSIMD(
                                                        aDD00, vetU_dD02,
                                                        FusedMulAddSIMD(
                                                            aDD_dupD022, vetU2,
                                                            FusedMulSubSIMD(
                                                                FDPart3tmp28, trK,
                                                                FusedMulAddSIMD(FDPart3_Integer_2,
                                                                                FusedMulAddSIMD(FDPart3tmp118, FDPart3tmp26,
                                                                                                FusedMulAddSIMD(FDPart3tmp120, FDPart3tmp28,
                                                                                                                MulSIMD(FDPart3tmp0, FDPart3tmp117))),
                                                                                MulSIMD(FDPart3tmp111, aDD02))))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_3 = FusedMulAddSIMD(
            aDD_dKOD112, diss_strength,
            FusedMulAddSIMD(
                aDD_dupD110, vetU0,
                FusedMulAddSIMD(
                    aDD_dKOD110, diss_strength,
                    FusedMulAddSIMD(
                        aDD_dKOD111, diss_strength,
                        FusedMulAddSIMD(
                            FDPart3tmp29,
                            SubSIMD(
                                FDPart3tmp62,
                                FusedMulAddSIMD(
                                    FDPart3_Rational_1_3,
                                    MulSIMD(FDPart3tmp5,
                                            FusedMulAddSIMD(
                                                FDPart3tmp43, SubSIMD(FDPart3tmp62, FDPart3tmp60),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp46, SubSIMD(FDPart3tmp52, FDPart3tmp49),
                                                    FusedMulAddSIMD(
                                                        FDPart3_Integer_2,
                                                        FusedMulAddSIMD(FDPart3tmp37, SubSIMD(FDPart3tmp85, FDPart3tmp83),
                                                                        FusedMulAddSIMD(FDPart3tmp42, SubSIMD(FDPart3tmp109, FDPart3tmp107),
                                                                                        MulSIMD(FDPart3tmp35, SubSIMD(FDPart3tmp98, FDPart3tmp95)))),
                                                        MulSIMD(FDPart3tmp39, SubSIMD(FDPart3tmp73, FDPart3tmp71)))))),
                                    FDPart3tmp60)),
                            FusedMulAddSIMD(
                                FDPart3tmp3, aDD11,
                                FusedMulAddSIMD(
                                    aDD_dupD111, vetU1,
                                    FusedMulAddSIMD(
                                        aDD_dupD112, vetU2,
                                        FusedMulAddSIMD(
                                            FDPart3_Integer_2,
                                            FusedMulAddSIMD(
                                                aDD11, vetU_dD11,
                                                FusedMulAddSIMD(
                                                    aDD12, vetU_dD21,
                                                    FusedMulSubSIMD(aDD01, vetU_dD01,
                                                                    FusedMulAddSIMD(FDPart3tmp115, FDPart3tmp121,
                                                                                    FusedMulAddSIMD(FDPart3tmp116, FDPart3tmp122,
                                                                                                    MulSIMD(FDPart3tmp112, FDPart3tmp26)))))),
                                            MulSIMD(FDPart3tmp121, trK))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_4 = FusedMulAddSIMD(
            aDD_dupD120, vetU0,
            FusedMulAddSIMD(
                aDD_dupD121, vetU1,
                FusedMulAddSIMD(
                    aDD_dKOD121, diss_strength,
                    FusedMulAddSIMD(
                        aDD_dKOD122, diss_strength,
                        FusedMulAddSIMD(
                            aDD22, vetU_dD21,
                            FusedMulAddSIMD(
                                aDD_dKOD120, diss_strength,
                                FusedMulAddSIMD(
                                    aDD12, vetU_dD11,
                                    FusedMulAddSIMD(
                                        aDD12, vetU_dD22,
                                        FusedMulAddSIMD(
                                            aDD02, vetU_dD01,
                                            FusedMulAddSIMD(
                                                aDD11, vetU_dD12,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp29,
                                                    SubSIMD(
                                                        FDPart3tmp109,
                                                        FusedMulAddSIMD(
                                                            FDPart3_Rational_1_3,
                                                            MulSIMD(hDD12,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp43, SubSIMD(FDPart3tmp62, FDPart3tmp60),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp46, SubSIMD(FDPart3tmp52, FDPart3tmp49),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3_Integer_2,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp37, SubSIMD(FDPart3tmp85, FDPart3tmp83),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp42, SubSIMD(FDPart3tmp109, FDPart3tmp107),
                                                                                        MulSIMD(FDPart3tmp35, SubSIMD(FDPart3tmp98, FDPart3tmp95)))),
                                                                                MulSIMD(FDPart3tmp39, SubSIMD(FDPart3tmp73, FDPart3tmp71)))))),
                                                            FDPart3tmp107)),
                                                    FusedMulAddSIMD(
                                                        aDD01, vetU_dD02,
                                                        FusedMulAddSIMD(
                                                            aDD_dupD122, vetU2,
                                                            FusedMulSubSIMD(
                                                                FDPart3tmp122, trK,
                                                                FusedMulAddSIMD(
                                                                    FDPart3_Integer_2,
                                                                    FusedMulAddSIMD(FDPart3tmp118, FDPart3tmp121,
                                                                                    FusedMulAddSIMD(FDPart3tmp120, FDPart3tmp122,
                                                                                                    MulSIMD(FDPart3tmp117, FDPart3tmp26))),
                                                                    MulSIMD(FDPart3tmp111, aDD12))))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_5 = FusedMulAddSIMD(
            aDD_dKOD222, diss_strength,
            FusedMulAddSIMD(
                aDD_dupD220, vetU0,
                FusedMulAddSIMD(
                    aDD_dKOD220, diss_strength,
                    FusedMulAddSIMD(
                        aDD_dKOD221, diss_strength,
                        FusedMulAddSIMD(
                            FDPart3tmp29,
                            SubSIMD(
                                FDPart3tmp52,
                                FusedMulAddSIMD(
                                    FDPart3_Rational_1_3,
                                    MulSIMD(FDPart3tmp6,
                                            FusedMulAddSIMD(
                                                FDPart3tmp43, SubSIMD(FDPart3tmp62, FDPart3tmp60),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp46, SubSIMD(FDPart3tmp52, FDPart3tmp49),
                                                    FusedMulAddSIMD(
                                                        FDPart3_Integer_2,
                                                        FusedMulAddSIMD(FDPart3tmp37, SubSIMD(FDPart3tmp85, FDPart3tmp83),
                                                                        FusedMulAddSIMD(FDPart3tmp42, SubSIMD(FDPart3tmp109, FDPart3tmp107),
                                                                                        MulSIMD(FDPart3tmp35, SubSIMD(FDPart3tmp98, FDPart3tmp95)))),
                                                        MulSIMD(FDPart3tmp39, SubSIMD(FDPart3tmp73, FDPart3tmp71)))))),
                                    FDPart3tmp49)),
                            FusedMulAddSIMD(
                                FDPart3tmp3, aDD22,
                                FusedMulAddSIMD(
                                    aDD_dupD221, vetU1,
                                    FusedMulAddSIMD(
                                        aDD_dupD222, vetU2,
                                        FusedMulAddSIMD(
                                            FDPart3_Integer_2,
                                            FusedMulAddSIMD(
                                                aDD12, vetU_dD12,
                                                FusedMulAddSIMD(
                                                    aDD22, vetU_dD22,
                                                    FusedMulSubSIMD(aDD02, vetU_dD02,
                                                                    FusedMulAddSIMD(FDPart3tmp118, FDPart3tmp122,
                                                                                    FusedMulAddSIMD(FDPart3tmp120, FDPart3tmp123,
                                                                                                    MulSIMD(FDPart3tmp117, FDPart3tmp28)))))),
                                            MulSIMD(FDPart3tmp123, trK))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_6 = FusedMulAddSIMD(
            alpha_dupD0, vetU0,
            FusedMulAddSIMD(alpha_dupD1, vetU1,
                            FusedMulAddSIMD(alpha_dKOD1, diss_strength,
                                            FusedMulAddSIMD(alpha_dKOD2, diss_strength,
                                                            FusedMulAddSIMD(alpha_dupD2, vetU2,
                                                                            FusedMulSubSIMD(alpha_dKOD0, diss_strength,
                                                                                            MulSIMD(FDPart3_Integer_2, MulSIMD(alpha, trK))))))));
        const REAL_SIMD_ARRAY __RHS_exp_7 = FusedMulAddSIMD(
            betU_dupD00, vetU0,
            FusedMulAddSIMD(
                betU_dupD01, vetU1,
                FusedMulAddSIMD(
                    betU_dKOD01, diss_strength,
                    FusedMulAddSIMD(
                        betU_dKOD02, diss_strength,
                        FusedMulAddSIMD(
                            FDPart3_Rational_3_4, SubSIMD(FDPart3tmp146, FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp144, FDPart3tmp145)),
                            FusedMulAddSIMD(
                                betU_dKOD00, diss_strength,
                                FusedMulAddSIMD(
                                    FDPart3_Rational_1_4, FDPart3tmp129,
                                    FusedMulAddSIMD(
                                        FDPart3_Rational_3_2, FDPart3tmp125,
                                        FusedMulAddSIMD(
                                            betU_dupD02, vetU2,
                                            FusedMulSubSIMD(
                                                FDPart3_Rational_1_2,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp183, FDPart3tmp79,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp175, FDPart3tmp57,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp182, FDPart3tmp40,
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp184, FDPart3tmp92,
                                                                FusedMulAddSIMD(FDPart3tmp164, MulSIMD(FDPart3tmp165, FDPart3tmp68),
                                                                                FusedMulAddSIMD(FDPart3_Rational_3_4, FDPart3tmp193,
                                                                                                MulSIMD(FDPart3tmp104, FDPart3tmp186))))))),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp148, FDPart3tmp35,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp149, FDPart3tmp39,
                                                        FusedMulAddSIMD(
                                                            betU0, eta,
                                                            FusedMulAddSIMD(
                                                                FDPart3_Integer_2,
                                                                FusedMulAddSIMD(FDPart3_Rational_3_4, MulSIMD(FDPart3tmp150, FDPart3tmp159),
                                                                                MulSIMD(FDPart3_Rational_3_4, MulSIMD(FDPart3tmp161, FDPart3tmp162))),
                                                                MulSIMD(FDPart3tmp147, FDPart3tmp37)))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_8 = FusedMulAddSIMD(
            betU_dupD10, vetU0,
            FusedMulAddSIMD(
                betU_dupD11, vetU1,
                FusedMulAddSIMD(
                    betU_dKOD11, diss_strength,
                    FusedMulAddSIMD(
                        betU_dKOD12, diss_strength,
                        FusedMulAddSIMD(
                            FDPart3_Rational_3_4, SubSIMD(FDPart3tmp198, FusedMulAddSIMD(FDPart3tmp150, FDPart3tmp196, FDPart3tmp197)),
                            FusedMulAddSIMD(
                                betU_dKOD10, diss_strength,
                                FusedMulAddSIMD(
                                    FDPart3_Rational_1_4, FDPart3tmp195,
                                    FusedMulAddSIMD(
                                        FDPart3_Rational_3_2, FDPart3tmp194,
                                        FusedMulAddSIMD(
                                            betU_dupD12, vetU2,
                                            FusedMulSubSIMD(
                                                FDPart3_Rational_1_2,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp183, FDPart3tmp80,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp175, FDPart3tmp58,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp182, FDPart3tmp44,
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp184, FDPart3tmp93,
                                                                FusedMulAddSIMD(FDPart3tmp164, MulSIMD(FDPart3tmp165, FDPart3tmp69),
                                                                                FusedMulAddSIMD(FDPart3_Rational_3_4, FDPart3tmp200,
                                                                                                MulSIMD(FDPart3tmp105, FDPart3tmp186))))))),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp148, FDPart3tmp42,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp149, FDPart3tmp37,
                                                        FusedMulAddSIMD(
                                                            betU1, eta,
                                                            FusedMulAddSIMD(
                                                                FDPart3_Integer_2,
                                                                FusedMulAddSIMD(FDPart3_Rational_3_4, MulSIMD(FDPart3tmp132, FDPart3tmp159),
                                                                                MulSIMD(FDPart3_Rational_3_4, MulSIMD(FDPart3tmp162, FDPart3tmp185))),
                                                                MulSIMD(FDPart3tmp147, FDPart3tmp43)))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_9 = FusedMulAddSIMD(
            betU_dupD20, vetU0,
            FusedMulAddSIMD(
                betU_dupD21, vetU1,
                FusedMulAddSIMD(
                    betU_dKOD21, diss_strength,
                    FusedMulAddSIMD(
                        betU_dKOD22, diss_strength,
                        FusedMulAddSIMD(
                            FDPart3_Rational_3_4, SubSIMD(FDPart3tmp205, FusedMulAddSIMD(FDPart3tmp162, FDPart3tmp203, FDPart3tmp204)),
                            FusedMulAddSIMD(
                                betU_dKOD20, diss_strength,
                                FusedMulAddSIMD(
                                    FDPart3_Rational_1_4, FDPart3tmp202,
                                    FusedMulAddSIMD(
                                        FDPart3_Rational_3_2, FDPart3tmp201,
                                        FusedMulAddSIMD(
                                            betU_dupD22, vetU2,
                                            FusedMulSubSIMD(
                                                FDPart3_Rational_1_2,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp183, FDPart3tmp81,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp175, FDPart3tmp59,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp182, FDPart3tmp47,
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp184, FDPart3tmp94,
                                                                FusedMulAddSIMD(FDPart3tmp164, MulSIMD(FDPart3tmp165, FDPart3tmp70),
                                                                                FusedMulAddSIMD(FDPart3_Rational_3_4, FDPart3tmp206,
                                                                                                MulSIMD(FDPart3tmp106, FDPart3tmp186))))))),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp148, FDPart3tmp46,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp149, FDPart3tmp35,
                                                        FusedMulAddSIMD(
                                                            betU2, eta,
                                                            FusedMulAddSIMD(
                                                                FDPart3_Integer_2,
                                                                FusedMulAddSIMD(FDPart3_Rational_3_4, MulSIMD(FDPart3tmp132, FDPart3tmp161),
                                                                                MulSIMD(FDPart3_Rational_3_4, MulSIMD(FDPart3tmp150, FDPart3tmp185))),
                                                                MulSIMD(FDPart3tmp147, FDPart3tmp42)))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_10 = FusedMulAddSIMD(
            cf_dKOD1, diss_strength,
            FusedMulAddSIMD(
                cf_dKOD2, diss_strength,
                FusedMulSubSIMD(
                    cf_dKOD0, diss_strength,
                    MulSIMD(
                        FDPart3_Integer_2,
                        MulSIMD(cf, FusedMulSubSIMD(FDPart3_Rational_1_6, NegFusedMulAddSIMD(alpha, trK, FDPart3tmp1),
                                                    MulSIMD(FDPart3_Rational_1_2,
                                                            FusedMulAddSIMD(FDPart3tmp30, MulSIMD(cf_dupD1, vetU1),
                                                                            FusedMulAddSIMD(FDPart3tmp30, MulSIMD(cf_dupD2, vetU2),
                                                                                            MulSIMD(FDPart3tmp30, MulSIMD(cf_dupD0, vetU0)))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_11 = FusedMulAddSIMD(
            hDD_dupD000, vetU0,
            FusedMulAddSIMD(
                hDD_dupD001, vetU1,
                FusedMulAddSIMD(
                    hDD_dKOD001, diss_strength,
                    FusedMulAddSIMD(
                        hDD_dKOD002, diss_strength,
                        FusedMulAddSIMD(
                            hDD_dupD002, vetU2,
                            FusedMulAddSIMD(
                                FDPart3_Rational_2_3,
                                MulSIMD(FDPart3tmp10,
                                        FusedMulSubSIMD(
                                            alpha,
                                            FusedMulAddSIMD(FDPart3tmp43, aDD11,
                                                            FusedMulAddSIMD(FDPart3tmp46, aDD22,
                                                                            FusedMulAddSIMD(FDPart3_Integer_2, AddSIMD(FDPart3tmp113, FDPart3tmp23),
                                                                                            MulSIMD(FDPart3tmp15, FDPart3tmp8)))),
                                            FDPart3tmp1)),
                                FusedMulAddSIMD(
                                    FDPart3_Integer_2,
                                    FusedMulAddSIMD(hDD01, vetU_dD10,
                                                    FusedMulAddSIMD(hDD02, vetU_dD20, FusedMulSubSIMD(FDPart3tmp10, vetU_dD00, FDPart3tmp0))),
                                    MulSIMD(hDD_dKOD000, diss_strength))))))));
        const REAL_SIMD_ARRAY __RHS_exp_12 = FusedMulAddSIMD(
            hDD_dupD012, vetU2,
            FusedMulAddSIMD(
                hDD_dupD010, vetU0,
                FusedMulAddSIMD(
                    hDD_dupD011, vetU1,
                    FusedMulAddSIMD(
                        hDD_dKOD011, diss_strength,
                        FusedMulAddSIMD(
                            hDD_dKOD012, diss_strength,
                            FusedMulAddSIMD(
                                hDD12, vetU_dD20,
                                FusedMulAddSIMD(
                                    hDD_dKOD010, diss_strength,
                                    FusedMulAddSIMD(
                                        hDD01, vetU_dD11,
                                        FusedMulAddSIMD(
                                            hDD02, vetU_dD21,
                                            FusedMulAddSIMD(
                                                FDPart3tmp5, vetU_dD10,
                                                FusedMulAddSIMD(
                                                    hDD01, vetU_dD00,
                                                    FusedMulAddSIMD(
                                                        FDPart3_Rational_2_3,
                                                        MulSIMD(hDD01, FusedMulSubSIMD(alpha,
                                                                                       FusedMulAddSIMD(
                                                                                           FDPart3tmp43, aDD11,
                                                                                           FusedMulAddSIMD(
                                                                                               FDPart3tmp46, aDD22,
                                                                                               FusedMulAddSIMD(FDPart3_Integer_2,
                                                                                                               AddSIMD(FDPart3tmp113, FDPart3tmp23),
                                                                                                               MulSIMD(FDPart3tmp15, FDPart3tmp8)))),
                                                                                       FDPart3tmp1)),
                                                        FusedMulSubSIMD(FDPart3tmp10, vetU_dD01,
                                                                        MulSIMD(FDPart3_Integer_2, FDPart3tmp26))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_13 = FusedMulAddSIMD(
            hDD_dupD022, vetU2,
            FusedMulAddSIMD(
                hDD_dupD020, vetU0,
                FusedMulAddSIMD(
                    hDD_dupD021, vetU1,
                    FusedMulAddSIMD(
                        hDD_dKOD021, diss_strength,
                        FusedMulAddSIMD(
                            hDD_dKOD022, diss_strength,
                            FusedMulAddSIMD(
                                hDD12, vetU_dD10,
                                FusedMulAddSIMD(
                                    hDD_dKOD020, diss_strength,
                                    FusedMulAddSIMD(
                                        hDD02, vetU_dD00,
                                        FusedMulAddSIMD(
                                            hDD02, vetU_dD22,
                                            FusedMulAddSIMD(
                                                FDPart3tmp6, vetU_dD20,
                                                FusedMulAddSIMD(
                                                    hDD01, vetU_dD12,
                                                    FusedMulAddSIMD(
                                                        FDPart3_Rational_2_3,
                                                        MulSIMD(hDD02, FusedMulSubSIMD(alpha,
                                                                                       FusedMulAddSIMD(
                                                                                           FDPart3tmp43, aDD11,
                                                                                           FusedMulAddSIMD(
                                                                                               FDPart3tmp46, aDD22,
                                                                                               FusedMulAddSIMD(FDPart3_Integer_2,
                                                                                                               AddSIMD(FDPart3tmp113, FDPart3tmp23),
                                                                                                               MulSIMD(FDPart3tmp15, FDPart3tmp8)))),
                                                                                       FDPart3tmp1)),
                                                        FusedMulSubSIMD(FDPart3tmp10, vetU_dD02,
                                                                        MulSIMD(FDPart3_Integer_2, FDPart3tmp28))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_14 = FusedMulAddSIMD(
            hDD_dupD110, vetU0,
            FusedMulAddSIMD(
                hDD_dupD111, vetU1,
                FusedMulAddSIMD(
                    hDD_dKOD111, diss_strength,
                    FusedMulAddSIMD(
                        hDD_dKOD112, diss_strength,
                        FusedMulAddSIMD(
                            hDD_dupD112, vetU2,
                            FusedMulAddSIMD(
                                FDPart3_Rational_2_3,
                                MulSIMD(FDPart3tmp5,
                                        FusedMulSubSIMD(
                                            alpha,
                                            FusedMulAddSIMD(FDPart3tmp43, aDD11,
                                                            FusedMulAddSIMD(FDPart3tmp46, aDD22,
                                                                            FusedMulAddSIMD(FDPart3_Integer_2, AddSIMD(FDPart3tmp113, FDPart3tmp23),
                                                                                            MulSIMD(FDPart3tmp15, FDPart3tmp8)))),
                                            FDPart3tmp1)),
                                FusedMulAddSIMD(
                                    FDPart3_Integer_2,
                                    FusedMulAddSIMD(hDD01, vetU_dD01,
                                                    FusedMulAddSIMD(hDD12, vetU_dD21, FusedMulSubSIMD(FDPart3tmp5, vetU_dD11, FDPart3tmp121))),
                                    MulSIMD(hDD_dKOD110, diss_strength))))))));
        const REAL_SIMD_ARRAY __RHS_exp_15 = FusedMulAddSIMD(
            hDD_dupD122, vetU2,
            FusedMulAddSIMD(
                hDD_dupD120, vetU0,
                FusedMulAddSIMD(
                    hDD_dupD121, vetU1,
                    FusedMulAddSIMD(
                        hDD_dKOD121, diss_strength,
                        FusedMulAddSIMD(
                            hDD_dKOD122, diss_strength,
                            FusedMulAddSIMD(
                                hDD12, vetU_dD22,
                                FusedMulAddSIMD(
                                    hDD_dKOD120, diss_strength,
                                    FusedMulAddSIMD(
                                        hDD02, vetU_dD01,
                                        FusedMulAddSIMD(
                                            hDD12, vetU_dD11,
                                            FusedMulAddSIMD(
                                                FDPart3tmp6, vetU_dD21,
                                                FusedMulAddSIMD(
                                                    hDD01, vetU_dD02,
                                                    FusedMulAddSIMD(
                                                        FDPart3_Rational_2_3,
                                                        MulSIMD(hDD12, FusedMulSubSIMD(alpha,
                                                                                       FusedMulAddSIMD(
                                                                                           FDPart3tmp43, aDD11,
                                                                                           FusedMulAddSIMD(
                                                                                               FDPart3tmp46, aDD22,
                                                                                               FusedMulAddSIMD(FDPart3_Integer_2,
                                                                                                               AddSIMD(FDPart3tmp113, FDPart3tmp23),
                                                                                                               MulSIMD(FDPart3tmp15, FDPart3tmp8)))),
                                                                                       FDPart3tmp1)),
                                                        FusedMulSubSIMD(FDPart3tmp5, vetU_dD12, MulSIMD(FDPart3tmp82, aDD12))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_16 = FusedMulAddSIMD(
            hDD_dupD220, vetU0,
            FusedMulAddSIMD(
                hDD_dupD221, vetU1,
                FusedMulAddSIMD(
                    hDD_dKOD221, diss_strength,
                    FusedMulAddSIMD(
                        hDD_dKOD222, diss_strength,
                        FusedMulAddSIMD(
                            hDD_dupD222, vetU2,
                            FusedMulAddSIMD(
                                FDPart3_Rational_2_3,
                                MulSIMD(FDPart3tmp6,
                                        FusedMulSubSIMD(
                                            alpha,
                                            FusedMulAddSIMD(FDPart3tmp43, aDD11,
                                                            FusedMulAddSIMD(FDPart3tmp46, aDD22,
                                                                            FusedMulAddSIMD(FDPart3_Integer_2, AddSIMD(FDPart3tmp113, FDPart3tmp23),
                                                                                            MulSIMD(FDPart3tmp15, FDPart3tmp8)))),
                                            FDPart3tmp1)),
                                FusedMulAddSIMD(
                                    FDPart3_Integer_2,
                                    FusedMulAddSIMD(hDD02, vetU_dD02,
                                                    FusedMulAddSIMD(hDD12, vetU_dD12, FusedMulSubSIMD(FDPart3tmp6, vetU_dD22, FDPart3tmp123))),
                                    MulSIMD(hDD_dKOD220, diss_strength))))))));
        const REAL_SIMD_ARRAY __RHS_exp_17 = FusedMulAddSIMD(
            lambdaU_dupD00, vetU0,
            FusedMulAddSIMD(
                lambdaU_dupD01, vetU1,
                FusedMulAddSIMD(
                    lambdaU_dKOD01, diss_strength,
                    FusedMulAddSIMD(
                        lambdaU_dKOD02, diss_strength,
                        FusedMulAddSIMD(
                            FDPart3_Rational_1_3, FDPart3tmp129,
                            FusedMulAddSIMD(
                                lambdaU_dKOD00, diss_strength,
                                FusedMulAddSIMD(
                                    FDPart3_Integer_2,
                                    SubSIMD(FDPart3tmp125, FusedMulAddSIMD(FDPart3tmp159, FDPart3tmp209, MulSIMD(FDPart3tmp161, FDPart3tmp210))),
                                    FusedMulAddSIMD(
                                        FDPart3_Rational_1_2,
                                        FusedMulAddSIMD(
                                            FDPart3tmp218, FDPart3tmp79,
                                            FusedMulAddSIMD(
                                                FDPart3tmp215, FDPart3tmp57,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp216, FDPart3tmp40,
                                                    FusedMulAddSIMD(FDPart3tmp219, FDPart3tmp92,
                                                                    FusedMulAddSIMD(FDPart3tmp165, MulSIMD(FDPart3tmp68, FDPart3tmp82),
                                                                                    FusedMulAddSIMD(FDPart3tmp104, FDPart3tmp220, FDPart3tmp193)))))),
                                        FusedMulAddSIMD(
                                            lambdaU_dupD02, vetU2,
                                            SubSIMD(FDPart3tmp146,
                                                    FusedMulAddSIMD(FDPart3_Rational_4_3,
                                                                    FusedMulAddSIMD(FDPart3tmp212, FDPart3tmp35,
                                                                                    FusedMulAddSIMD(FDPart3tmp213, FDPart3tmp39,
                                                                                                    MulSIMD(FDPart3tmp211, FDPart3tmp37))),
                                                                    FusedMulAddSIMD(FDPart3tmp144, FDPart3tmp214, FDPart3tmp145))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_18 = FusedMulAddSIMD(
            lambdaU_dupD10, vetU0,
            FusedMulAddSIMD(
                lambdaU_dupD11, vetU1,
                FusedMulAddSIMD(
                    lambdaU_dKOD11, diss_strength,
                    FusedMulAddSIMD(
                        lambdaU_dKOD12, diss_strength,
                        FusedMulAddSIMD(
                            FDPart3_Rational_1_3, FDPart3tmp195,
                            FusedMulAddSIMD(
                                lambdaU_dKOD10, diss_strength,
                                FusedMulAddSIMD(
                                    FDPart3_Integer_2,
                                    SubSIMD(FDPart3tmp194, FusedMulAddSIMD(FDPart3tmp159, FDPart3tmp214, MulSIMD(FDPart3tmp185, FDPart3tmp210))),
                                    FusedMulAddSIMD(
                                        FDPart3_Rational_1_2,
                                        FusedMulAddSIMD(
                                            FDPart3tmp218, FDPart3tmp80,
                                            FusedMulAddSIMD(
                                                FDPart3tmp215, FDPart3tmp58,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp216, FDPart3tmp44,
                                                    FusedMulAddSIMD(FDPart3tmp219, FDPart3tmp93,
                                                                    FusedMulAddSIMD(FDPart3tmp165, MulSIMD(FDPart3tmp69, FDPart3tmp82),
                                                                                    FusedMulAddSIMD(FDPart3tmp105, FDPart3tmp220, FDPart3tmp200)))))),
                                        FusedMulAddSIMD(
                                            lambdaU_dupD12, vetU2,
                                            SubSIMD(FDPart3tmp198,
                                                    FusedMulAddSIMD(FDPart3_Rational_4_3,
                                                                    FusedMulAddSIMD(FDPart3tmp212, FDPart3tmp42,
                                                                                    FusedMulAddSIMD(FDPart3tmp213, FDPart3tmp37,
                                                                                                    MulSIMD(FDPart3tmp211, FDPart3tmp43))),
                                                                    FusedMulAddSIMD(FDPart3tmp196, FDPart3tmp209, FDPart3tmp197))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_19 = FusedMulAddSIMD(
            lambdaU_dupD20, vetU0,
            FusedMulAddSIMD(
                lambdaU_dupD21, vetU1,
                FusedMulAddSIMD(
                    lambdaU_dKOD21, diss_strength,
                    FusedMulAddSIMD(
                        lambdaU_dKOD22, diss_strength,
                        FusedMulAddSIMD(
                            FDPart3_Rational_1_3, FDPart3tmp202,
                            FusedMulAddSIMD(
                                lambdaU_dKOD20, diss_strength,
                                FusedMulAddSIMD(
                                    FDPart3_Integer_2,
                                    SubSIMD(FDPart3tmp201, FusedMulAddSIMD(FDPart3tmp161, FDPart3tmp214, MulSIMD(FDPart3tmp185, FDPart3tmp209))),
                                    FusedMulAddSIMD(
                                        FDPart3_Rational_1_2,
                                        FusedMulAddSIMD(
                                            FDPart3tmp218, FDPart3tmp81,
                                            FusedMulAddSIMD(
                                                FDPart3tmp215, FDPart3tmp59,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp216, FDPart3tmp47,
                                                    FusedMulAddSIMD(FDPart3tmp219, FDPart3tmp94,
                                                                    FusedMulAddSIMD(FDPart3tmp165, MulSIMD(FDPart3tmp70, FDPart3tmp82),
                                                                                    FusedMulAddSIMD(FDPart3tmp106, FDPart3tmp220, FDPart3tmp206)))))),
                                        FusedMulAddSIMD(
                                            lambdaU_dupD22, vetU2,
                                            SubSIMD(FDPart3tmp205,
                                                    FusedMulAddSIMD(FDPart3_Rational_4_3,
                                                                    FusedMulAddSIMD(FDPart3tmp212, FDPart3tmp46,
                                                                                    FusedMulAddSIMD(FDPart3tmp213, FDPart3tmp35,
                                                                                                    MulSIMD(FDPart3tmp211, FDPart3tmp42))),
                                                                    FusedMulAddSIMD(FDPart3tmp203, FDPart3tmp210, FDPart3tmp204))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_20 = FusedMulAddSIMD(
            trK_dupD2, vetU2,
            FusedMulAddSIMD(
                trK_dupD0, vetU0,
                FusedMulAddSIMD(
                    trK_dupD1, vetU1,
                    FusedMulAddSIMD(
                        trK_dKOD1, diss_strength,
                        FusedMulAddSIMD(
                            trK_dKOD2, diss_strength,
                            FusedMulAddSIMD(
                                FDPart3tmp123, FDPart3tmp181,
                                FusedMulAddSIMD(
                                    trK_dKOD0, diss_strength,
                                    FusedMulAddSIMD(
                                        FDPart3tmp0, FDPart3tmp165,
                                        FusedMulAddSIMD(
                                            FDPart3tmp121, FDPart3tmp174,
                                            FusedMulAddSIMD(
                                                FDPart3_Rational_1_3, MulSIMD(alpha, MulSIMD(trK, trK)),
                                                FusedMulSubSIMD(
                                                    FDPart3_Integer_2,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp159, FDPart3tmp26,
                                                        FusedMulAddSIMD(FDPart3tmp161, FDPart3tmp28, MulSIMD(FDPart3tmp122, FDPart3tmp185))),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp29,
                                                        MulSIMD(FDPart3tmp42,
                                                                SubSIMD(alpha_dDD12, FusedMulAddSIMD(FDPart3_Rational_1_2, FDPart3tmp108,
                                                                                                     MulSIMD(FDPart3tmp45, alpha_dD2)))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp29,
                                                            MulSIMD(FDPart3tmp39,
                                                                    SubSIMD(alpha_dDD00, FusedMulAddSIMD(FDPart3_Rational_1_2, FDPart3tmp72,
                                                                                                         MulSIMD(FDPart3tmp41, alpha_dD0)))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp29,
                                                                MulSIMD(FDPart3tmp42,
                                                                        SubSIMD(alpha_dDD12, FusedMulAddSIMD(FDPart3_Rational_1_2, FDPart3tmp108,
                                                                                                             MulSIMD(FDPart3tmp31, alpha_dD1)))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp29,
                                                                    MulSIMD(FDPart3tmp37,
                                                                            SubSIMD(alpha_dDD01, FusedMulAddSIMD(FDPart3_Rational_1_2, FDPart3tmp84,
                                                                                                                 MulSIMD(FDPart3tmp41, alpha_dD1)))),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp29,
                                                                        MulSIMD(
                                                                            FDPart3tmp37,
                                                                            SubSIMD(alpha_dDD01, FusedMulAddSIMD(FDPart3_Rational_1_2, FDPart3tmp84,
                                                                                                                 MulSIMD(FDPart3tmp45, alpha_dD0)))),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp29,
                                                                            MulSIMD(FDPart3tmp43,
                                                                                    SubSIMD(alpha_dDD11,
                                                                                            FusedMulAddSIMD(FDPart3_Rational_1_2, FDPart3tmp61,
                                                                                                            MulSIMD(FDPart3tmp45, alpha_dD1)))),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp29,
                                                                                MulSIMD(FDPart3tmp46,
                                                                                        SubSIMD(alpha_dDD22,
                                                                                                FusedMulAddSIMD(FDPart3_Rational_1_2, FDPart3tmp50,
                                                                                                                MulSIMD(FDPart3tmp31, alpha_dD2)))),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp29,
                                                                                    MulSIMD(
                                                                                        FDPart3tmp35,
                                                                                        SubSIMD(alpha_dDD02,
                                                                                                FusedMulAddSIMD(FDPart3_Rational_1_2, FDPart3tmp96,
                                                                                                                MulSIMD(FDPart3tmp31, alpha_dD0)))),
                                                                                    MulSIMD(
                                                                                        FDPart3tmp29,
                                                                                        MulSIMD(FDPart3tmp35,
                                                                                                SubSIMD(alpha_dDD02,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3_Rational_1_2, FDPart3tmp96,
                                                                                                            MulSIMD(FDPart3tmp41,
                                                                                                                    alpha_dD2))))))))))))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_21 = FusedMulAddSIMD(
            vetU_dKOD00, diss_strength,
            FusedMulAddSIMD(
                vetU1, vetU_dupD01,
                FusedMulAddSIMD(vetU2, vetU_dupD02,
                                FusedMulAddSIMD(vetU_dKOD01, diss_strength,
                                                FusedMulAddSIMD(vetU_dKOD02, diss_strength, FusedMulAddSIMD(vetU0, vetU_dupD00, betU0))))));
        const REAL_SIMD_ARRAY __RHS_exp_22 = FusedMulAddSIMD(
            vetU_dKOD10, diss_strength,
            FusedMulAddSIMD(
                vetU1, vetU_dupD11,
                FusedMulAddSIMD(vetU2, vetU_dupD12,
                                FusedMulAddSIMD(vetU_dKOD11, diss_strength,
                                                FusedMulAddSIMD(vetU_dKOD12, diss_strength, FusedMulAddSIMD(vetU0, vetU_dupD10, betU1))))));
        const REAL_SIMD_ARRAY __RHS_exp_23 = FusedMulAddSIMD(
            vetU_dKOD20, diss_strength,
            FusedMulAddSIMD(
                vetU1, vetU_dupD21,
                FusedMulAddSIMD(vetU2, vetU_dupD22,
                                FusedMulAddSIMD(vetU_dKOD21, diss_strength,
                                                FusedMulAddSIMD(vetU_dKOD22, diss_strength, FusedMulAddSIMD(vetU0, vetU_dupD20, betU2))))));

        WriteSIMD(&aDD00_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_0);
        WriteSIMD(&aDD01_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_1);
        WriteSIMD(&aDD02_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_2);
        WriteSIMD(&aDD11_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_3);
        WriteSIMD(&aDD12_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_4);
        WriteSIMD(&aDD22_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_5);
        WriteSIMD(&alpha_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_6);
        WriteSIMD(&betU0_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_7);
        WriteSIMD(&betU1_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_8);
        WriteSIMD(&betU2_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_9);
        WriteSIMD(&cf_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_10);
        WriteSIMD(&hDD00_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_11);
        WriteSIMD(&hDD01_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_12);
        WriteSIMD(&hDD02_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_13);
        WriteSIMD(&hDD11_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_14);
        WriteSIMD(&hDD12_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_15);
        WriteSIMD(&hDD22_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_16);
        WriteSIMD(&lambdaU0_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_17);
        WriteSIMD(&lambdaU1_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_18);
        WriteSIMD(&lambdaU2_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_19);
        WriteSIMD(&trK_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_20);
        WriteSIMD(&vetU0_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_21);
        WriteSIMD(&vetU1_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_22);
        WriteSIMD(&vetU2_rhsGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_23);

      } // END LOOP: for (int i0 = cctk_nghostzones[0]; i0 < cctk_lsh[0]-cctk_nghostzones[0]; i0 += simd_width)
    } // END LOOP: for (int i1 = cctk_nghostzones[1]; i1 < cctk_lsh[1]-cctk_nghostzones[1]; i1++)
  } // END LOOP: for (int i2 = cctk_nghostzones[2]; i2 < cctk_lsh[2]-cctk_nghostzones[2]; i2++)
}
