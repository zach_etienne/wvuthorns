#include "Slicing.h"
#include "cctk.h"
/**
 * Register slicing condition for NRPy+-generated thorn BaikalVacuum.
 */
int BaikalVacuum_RegisterSlicing() {

  Einstein_RegisterSlicing("BaikalVacuum");
  return 0;
}
