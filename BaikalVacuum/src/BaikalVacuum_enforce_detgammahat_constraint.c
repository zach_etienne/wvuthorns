#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "math.h"
/**
 * Enforce det(gammabar) = det(gammahat) constraint. Required for strong hyperbolicity.
 */
void BaikalVacuum_enforce_detgammahat_constraint(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS_BaikalVacuum_enforce_detgammahat_constraint;
  DECLARE_CCTK_PARAMETERS;

#pragma omp parallel for
  for (int i2 = 0; i2 < cctk_lsh[2]; i2++) {
    for (int i1 = 0; i1 < cctk_lsh[1]; i1++) {
      for (int i0 = 0; i0 < cctk_lsh[0]; i0++) {
        /*
         * NRPy+-Generated GF Access/FD Code, Step 1 of 2:
         * Read gridfunction(s) from main memory and compute FD stencils as needed.
         */
        const CCTK_REAL hDD00 = hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD01 = hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD02 = hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD11 = hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD12 = hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD22 = hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];

        /*
         * NRPy+-Generated GF Access/FD Code, Step 2 of 2:
         * Evaluate SymPy expressions and write to main memory.
         */
        const CCTK_REAL FDPart3tmp0 = hDD00 + 1;
        const CCTK_REAL FDPart3tmp1 = hDD22 + 1;
        const CCTK_REAL FDPart3tmp2 = hDD11 + 1;
        const CCTK_REAL FDPart3tmp3 =
            cbrt(fabs(1) / (FDPart3tmp0 * FDPart3tmp1 * FDPart3tmp2 - FDPart3tmp0 * ((hDD12) * (hDD12)) - FDPart3tmp1 * ((hDD01) * (hDD01)) -
                            FDPart3tmp2 * ((hDD02) * (hDD02)) + 2 * hDD01 * hDD02 * hDD12));
        hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = FDPart3tmp0 * FDPart3tmp3 - 1;
        hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = FDPart3tmp3 * hDD01;
        hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = FDPart3tmp3 * hDD02;
        hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = FDPart3tmp2 * FDPart3tmp3 - 1;
        hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = FDPart3tmp3 * hDD12;
        hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = FDPart3tmp1 * FDPart3tmp3 - 1;

      } // END LOOP: for (int i0 = 0; i0 < cctk_lsh[0]; i0++)
    } // END LOOP: for (int i1 = 0; i1 < cctk_lsh[1]; i1++)
  } // END LOOP: for (int i2 = 0; i2 < cctk_lsh[2]; i2++)
}
