#include "./simd/simd_intrinsics.h"
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "math.h"
/**
 * Finite difference function for operator dD0, with FD accuracy order 8.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dD0_fdorder8(const REAL_SIMD_ARRAY FDPROTO_i0m1, const REAL_SIMD_ARRAY FDPROTO_i0m2,
                                                                             const REAL_SIMD_ARRAY FDPROTO_i0m3, const REAL_SIMD_ARRAY FDPROTO_i0m4,
                                                                             const REAL_SIMD_ARRAY FDPROTO_i0p1, const REAL_SIMD_ARRAY FDPROTO_i0p2,
                                                                             const REAL_SIMD_ARRAY FDPROTO_i0p3, const REAL_SIMD_ARRAY FDPROTO_i0p4,
                                                                             const REAL_SIMD_ARRAY invdxx0) {
  const double dblFDPart1_Rational_1_280 = 1.0 / 280.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_280 = ConstSIMD(dblFDPart1_Rational_1_280);

  const double dblFDPart1_Rational_1_5 = 1.0 / 5.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_5 = ConstSIMD(dblFDPart1_Rational_1_5);

  const double dblFDPart1_Rational_4_105 = 4.0 / 105.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_105 = ConstSIMD(dblFDPart1_Rational_4_105);

  const double dblFDPart1_Rational_4_5 = 4.0 / 5.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_5 = ConstSIMD(dblFDPart1_Rational_4_5);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(invdxx0, FusedMulAddSIMD(FDPart1_Rational_4_105, SubSIMD(FDPROTO_i0p3, FDPROTO_i0m3),
                                       FusedMulAddSIMD(FDPart1_Rational_4_5, SubSIMD(FDPROTO_i0p1, FDPROTO_i0m1),
                                                       FusedMulAddSIMD(FDPart1_Rational_1_280, SubSIMD(FDPROTO_i0m4, FDPROTO_i0p4),
                                                                       MulSIMD(FDPart1_Rational_1_5, SubSIMD(FDPROTO_i0m2, FDPROTO_i0p2))))));

  return FD_result;
}
/**
 * Finite difference function for operator dD1, with FD accuracy order 8.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dD1_fdorder8(const REAL_SIMD_ARRAY FDPROTO_i1m1, const REAL_SIMD_ARRAY FDPROTO_i1m2,
                                                                             const REAL_SIMD_ARRAY FDPROTO_i1m3, const REAL_SIMD_ARRAY FDPROTO_i1m4,
                                                                             const REAL_SIMD_ARRAY FDPROTO_i1p1, const REAL_SIMD_ARRAY FDPROTO_i1p2,
                                                                             const REAL_SIMD_ARRAY FDPROTO_i1p3, const REAL_SIMD_ARRAY FDPROTO_i1p4,
                                                                             const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Rational_1_280 = 1.0 / 280.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_280 = ConstSIMD(dblFDPart1_Rational_1_280);

  const double dblFDPart1_Rational_1_5 = 1.0 / 5.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_5 = ConstSIMD(dblFDPart1_Rational_1_5);

  const double dblFDPart1_Rational_4_105 = 4.0 / 105.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_105 = ConstSIMD(dblFDPart1_Rational_4_105);

  const double dblFDPart1_Rational_4_5 = 4.0 / 5.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_5 = ConstSIMD(dblFDPart1_Rational_4_5);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(invdxx1, FusedMulAddSIMD(FDPart1_Rational_4_105, SubSIMD(FDPROTO_i1p3, FDPROTO_i1m3),
                                       FusedMulAddSIMD(FDPart1_Rational_4_5, SubSIMD(FDPROTO_i1p1, FDPROTO_i1m1),
                                                       FusedMulAddSIMD(FDPart1_Rational_1_280, SubSIMD(FDPROTO_i1m4, FDPROTO_i1p4),
                                                                       MulSIMD(FDPart1_Rational_1_5, SubSIMD(FDPROTO_i1m2, FDPROTO_i1p2))))));

  return FD_result;
}
/**
 * Finite difference function for operator dD2, with FD accuracy order 8.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dD2_fdorder8(const REAL_SIMD_ARRAY FDPROTO_i2m1, const REAL_SIMD_ARRAY FDPROTO_i2m2,
                                                                             const REAL_SIMD_ARRAY FDPROTO_i2m3, const REAL_SIMD_ARRAY FDPROTO_i2m4,
                                                                             const REAL_SIMD_ARRAY FDPROTO_i2p1, const REAL_SIMD_ARRAY FDPROTO_i2p2,
                                                                             const REAL_SIMD_ARRAY FDPROTO_i2p3, const REAL_SIMD_ARRAY FDPROTO_i2p4,
                                                                             const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_280 = 1.0 / 280.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_280 = ConstSIMD(dblFDPart1_Rational_1_280);

  const double dblFDPart1_Rational_1_5 = 1.0 / 5.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_5 = ConstSIMD(dblFDPart1_Rational_1_5);

  const double dblFDPart1_Rational_4_105 = 4.0 / 105.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_105 = ConstSIMD(dblFDPart1_Rational_4_105);

  const double dblFDPart1_Rational_4_5 = 4.0 / 5.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_5 = ConstSIMD(dblFDPart1_Rational_4_5);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(invdxx2, FusedMulAddSIMD(FDPart1_Rational_4_105, SubSIMD(FDPROTO_i2p3, FDPROTO_i2m3),
                                       FusedMulAddSIMD(FDPart1_Rational_4_5, SubSIMD(FDPROTO_i2p1, FDPROTO_i2m1),
                                                       FusedMulAddSIMD(FDPart1_Rational_1_280, SubSIMD(FDPROTO_i2m4, FDPROTO_i2p4),
                                                                       MulSIMD(FDPart1_Rational_1_5, SubSIMD(FDPROTO_i2m2, FDPROTO_i2p2))))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD00, with FD accuracy order 8.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD00_fdorder8(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i0m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0m2, const REAL_SIMD_ARRAY FDPROTO_i0m3,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0m4, const REAL_SIMD_ARRAY FDPROTO_i0p1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p2, const REAL_SIMD_ARRAY FDPROTO_i0p3,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p4, const REAL_SIMD_ARRAY invdxx0) {
  const double dblFDPart1_Rational_1_5 = 1.0 / 5.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_5 = ConstSIMD(dblFDPart1_Rational_1_5);

  const double dblFDPart1_Rational_1_560 = 1.0 / 560.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_560 = ConstSIMD(dblFDPart1_Rational_1_560);

  const double dblFDPart1_Rational_205_72 = 205.0 / 72.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_205_72 = ConstSIMD(dblFDPart1_Rational_205_72);

  const double dblFDPart1_Rational_8_315 = 8.0 / 315.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_8_315 = ConstSIMD(dblFDPart1_Rational_8_315);

  const double dblFDPart1_Rational_8_5 = 8.0 / 5.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_8_5 = ConstSIMD(dblFDPart1_Rational_8_5);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(invdxx0, invdxx0),
              FusedMulAddSIMD(FDPart1_Rational_8_5, AddSIMD(FDPROTO_i0m1, FDPROTO_i0p1),
                              FusedMulSubSIMD(FDPart1_Rational_8_315, AddSIMD(FDPROTO_i0m3, FDPROTO_i0p3),
                                              FusedMulAddSIMD(FDPart1_Rational_1_5, AddSIMD(FDPROTO_i0m2, FDPROTO_i0p2),
                                                              FusedMulAddSIMD(FDPart1_Rational_1_560, AddSIMD(FDPROTO_i0m4, FDPROTO_i0p4),
                                                                              MulSIMD(FDPROTO, FDPart1_Rational_205_72))))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD01, with FD accuracy order 8.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD01_fdorder8(
    const REAL_SIMD_ARRAY FDPROTO_i0m1_i1m1, const REAL_SIMD_ARRAY FDPROTO_i0m1_i1m2, const REAL_SIMD_ARRAY FDPROTO_i0m1_i1m3,
    const REAL_SIMD_ARRAY FDPROTO_i0m1_i1m4, const REAL_SIMD_ARRAY FDPROTO_i0m1_i1p1, const REAL_SIMD_ARRAY FDPROTO_i0m1_i1p2,
    const REAL_SIMD_ARRAY FDPROTO_i0m1_i1p3, const REAL_SIMD_ARRAY FDPROTO_i0m1_i1p4, const REAL_SIMD_ARRAY FDPROTO_i0m2_i1m1,
    const REAL_SIMD_ARRAY FDPROTO_i0m2_i1m2, const REAL_SIMD_ARRAY FDPROTO_i0m2_i1m3, const REAL_SIMD_ARRAY FDPROTO_i0m2_i1m4,
    const REAL_SIMD_ARRAY FDPROTO_i0m2_i1p1, const REAL_SIMD_ARRAY FDPROTO_i0m2_i1p2, const REAL_SIMD_ARRAY FDPROTO_i0m2_i1p3,
    const REAL_SIMD_ARRAY FDPROTO_i0m2_i1p4, const REAL_SIMD_ARRAY FDPROTO_i0m3_i1m1, const REAL_SIMD_ARRAY FDPROTO_i0m3_i1m2,
    const REAL_SIMD_ARRAY FDPROTO_i0m3_i1m3, const REAL_SIMD_ARRAY FDPROTO_i0m3_i1m4, const REAL_SIMD_ARRAY FDPROTO_i0m3_i1p1,
    const REAL_SIMD_ARRAY FDPROTO_i0m3_i1p2, const REAL_SIMD_ARRAY FDPROTO_i0m3_i1p3, const REAL_SIMD_ARRAY FDPROTO_i0m3_i1p4,
    const REAL_SIMD_ARRAY FDPROTO_i0m4_i1m1, const REAL_SIMD_ARRAY FDPROTO_i0m4_i1m2, const REAL_SIMD_ARRAY FDPROTO_i0m4_i1m3,
    const REAL_SIMD_ARRAY FDPROTO_i0m4_i1m4, const REAL_SIMD_ARRAY FDPROTO_i0m4_i1p1, const REAL_SIMD_ARRAY FDPROTO_i0m4_i1p2,
    const REAL_SIMD_ARRAY FDPROTO_i0m4_i1p3, const REAL_SIMD_ARRAY FDPROTO_i0m4_i1p4, const REAL_SIMD_ARRAY FDPROTO_i0p1_i1m1,
    const REAL_SIMD_ARRAY FDPROTO_i0p1_i1m2, const REAL_SIMD_ARRAY FDPROTO_i0p1_i1m3, const REAL_SIMD_ARRAY FDPROTO_i0p1_i1m4,
    const REAL_SIMD_ARRAY FDPROTO_i0p1_i1p1, const REAL_SIMD_ARRAY FDPROTO_i0p1_i1p2, const REAL_SIMD_ARRAY FDPROTO_i0p1_i1p3,
    const REAL_SIMD_ARRAY FDPROTO_i0p1_i1p4, const REAL_SIMD_ARRAY FDPROTO_i0p2_i1m1, const REAL_SIMD_ARRAY FDPROTO_i0p2_i1m2,
    const REAL_SIMD_ARRAY FDPROTO_i0p2_i1m3, const REAL_SIMD_ARRAY FDPROTO_i0p2_i1m4, const REAL_SIMD_ARRAY FDPROTO_i0p2_i1p1,
    const REAL_SIMD_ARRAY FDPROTO_i0p2_i1p2, const REAL_SIMD_ARRAY FDPROTO_i0p2_i1p3, const REAL_SIMD_ARRAY FDPROTO_i0p2_i1p4,
    const REAL_SIMD_ARRAY FDPROTO_i0p3_i1m1, const REAL_SIMD_ARRAY FDPROTO_i0p3_i1m2, const REAL_SIMD_ARRAY FDPROTO_i0p3_i1m3,
    const REAL_SIMD_ARRAY FDPROTO_i0p3_i1m4, const REAL_SIMD_ARRAY FDPROTO_i0p3_i1p1, const REAL_SIMD_ARRAY FDPROTO_i0p3_i1p2,
    const REAL_SIMD_ARRAY FDPROTO_i0p3_i1p3, const REAL_SIMD_ARRAY FDPROTO_i0p3_i1p4, const REAL_SIMD_ARRAY FDPROTO_i0p4_i1m1,
    const REAL_SIMD_ARRAY FDPROTO_i0p4_i1m2, const REAL_SIMD_ARRAY FDPROTO_i0p4_i1m3, const REAL_SIMD_ARRAY FDPROTO_i0p4_i1m4,
    const REAL_SIMD_ARRAY FDPROTO_i0p4_i1p1, const REAL_SIMD_ARRAY FDPROTO_i0p4_i1p2, const REAL_SIMD_ARRAY FDPROTO_i0p4_i1p3,
    const REAL_SIMD_ARRAY FDPROTO_i0p4_i1p4, const REAL_SIMD_ARRAY invdxx0, const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Rational_16_11025 = 16.0 / 11025.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_16_11025 = ConstSIMD(dblFDPart1_Rational_16_11025);

  const double dblFDPart1_Rational_16_25 = 16.0 / 25.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_16_25 = ConstSIMD(dblFDPart1_Rational_16_25);

  const double dblFDPart1_Rational_16_525 = 16.0 / 525.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_16_525 = ConstSIMD(dblFDPart1_Rational_16_525);

  const double dblFDPart1_Rational_1_1400 = 1.0 / 1400.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_1400 = ConstSIMD(dblFDPart1_Rational_1_1400);

  const double dblFDPart1_Rational_1_25 = 1.0 / 25.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_25 = ConstSIMD(dblFDPart1_Rational_1_25);

  const double dblFDPart1_Rational_1_350 = 1.0 / 350.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_350 = ConstSIMD(dblFDPart1_Rational_1_350);

  const double dblFDPart1_Rational_1_7350 = 1.0 / 7350.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_7350 = ConstSIMD(dblFDPart1_Rational_1_7350);

  const double dblFDPart1_Rational_1_78400 = 1.0 / 78400.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_78400 = ConstSIMD(dblFDPart1_Rational_1_78400);

  const double dblFDPart1_Rational_4_25 = 4.0 / 25.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_25 = ConstSIMD(dblFDPart1_Rational_4_25);

  const double dblFDPart1_Rational_4_525 = 4.0 / 525.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_525 = ConstSIMD(dblFDPart1_Rational_4_525);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(
      invdxx0,
      MulSIMD(
          invdxx1,
          FusedMulAddSIMD(
              FDPart1_Rational_1_7350,
              AddSIMD(AddSIMD(FDPROTO_i0m4_i1p3, FDPROTO_i0p3_i1m4),
                      AddSIMD(FDPROTO_i0p4_i1m3, SubSIMD(FDPROTO_i0m3_i1p4, AddSIMD(AddSIMD(FDPROTO_i0m3_i1m4, FDPROTO_i0m4_i1m3),
                                                                                    AddSIMD(FDPROTO_i0p3_i1p4, FDPROTO_i0p4_i1p3))))),
              FusedMulAddSIMD(
                  FDPart1_Rational_1_78400, AddSIMD(FDPROTO_i0p4_i1p4, SubSIMD(FDPROTO_i0m4_i1m4, AddSIMD(FDPROTO_i0m4_i1p4, FDPROTO_i0p4_i1m4))),
                  FusedMulAddSIMD(
                      FDPart1_Rational_1_25, AddSIMD(FDPROTO_i0p2_i1p2, SubSIMD(FDPROTO_i0m2_i1m2, AddSIMD(FDPROTO_i0m2_i1p2, FDPROTO_i0p2_i1m2))),
                      FusedMulAddSIMD(
                          FDPart1_Rational_1_350,
                          AddSIMD(AddSIMD(FDPROTO_i0m4_i1p1, FDPROTO_i0p1_i1m4),
                                  AddSIMD(FDPROTO_i0p4_i1m1, SubSIMD(FDPROTO_i0m1_i1p4, AddSIMD(AddSIMD(FDPROTO_i0m1_i1m4, FDPROTO_i0m4_i1m1),
                                                                                                AddSIMD(FDPROTO_i0p1_i1p4, FDPROTO_i0p4_i1p1))))),
                          FusedMulAddSIMD(
                              FDPart1_Rational_16_525,
                              AddSIMD(AddSIMD(FDPROTO_i0m3_i1m1, FDPROTO_i0p1_i1p3),
                                      AddSIMD(FDPROTO_i0p3_i1p1, SubSIMD(FDPROTO_i0m1_i1m3, AddSIMD(AddSIMD(FDPROTO_i0m1_i1p3, FDPROTO_i0m3_i1p1),
                                                                                                    AddSIMD(FDPROTO_i0p1_i1m3, FDPROTO_i0p3_i1m1))))),
                              FusedMulAddSIMD(
                                  FDPart1_Rational_1_1400,
                                  AddSIMD(
                                      AddSIMD(FDPROTO_i0m4_i1m2, FDPROTO_i0p2_i1p4),
                                      AddSIMD(FDPROTO_i0p4_i1p2, SubSIMD(FDPROTO_i0m2_i1m4, AddSIMD(AddSIMD(FDPROTO_i0m2_i1p4, FDPROTO_i0m4_i1p2),
                                                                                                    AddSIMD(FDPROTO_i0p2_i1m4, FDPROTO_i0p4_i1m2))))),
                                  FusedMulAddSIMD(
                                      FDPart1_Rational_4_25,
                                      AddSIMD(AddSIMD(FDPROTO_i0m2_i1p1, FDPROTO_i0p1_i1m2),
                                              AddSIMD(FDPROTO_i0p2_i1m1,
                                                      SubSIMD(FDPROTO_i0m1_i1p2, AddSIMD(AddSIMD(FDPROTO_i0m1_i1m2, FDPROTO_i0m2_i1m1),
                                                                                         AddSIMD(FDPROTO_i0p1_i1p2, FDPROTO_i0p2_i1p1))))),
                                      FusedMulAddSIMD(
                                          FDPart1_Rational_4_525,
                                          AddSIMD(AddSIMD(FDPROTO_i0m3_i1p2, FDPROTO_i0p2_i1m3),
                                                  AddSIMD(FDPROTO_i0p3_i1m2,
                                                          SubSIMD(FDPROTO_i0m2_i1p3, AddSIMD(AddSIMD(FDPROTO_i0m2_i1m3, FDPROTO_i0m3_i1m2),
                                                                                             AddSIMD(FDPROTO_i0p2_i1p3, FDPROTO_i0p3_i1p2))))),
                                          FusedMulAddSIMD(
                                              FDPart1_Rational_16_11025,
                                              AddSIMD(FDPROTO_i0p3_i1p3, SubSIMD(FDPROTO_i0m3_i1m3, AddSIMD(FDPROTO_i0m3_i1p3, FDPROTO_i0p3_i1m3))),
                                              MulSIMD(FDPart1_Rational_16_25,
                                                      AddSIMD(FDPROTO_i0p1_i1p1,
                                                              SubSIMD(FDPROTO_i0m1_i1m1, AddSIMD(FDPROTO_i0m1_i1p1, FDPROTO_i0p1_i1m1)))))))))))))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD02, with FD accuracy order 8.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD02_fdorder8(
    const REAL_SIMD_ARRAY FDPROTO_i0m1_i2m1, const REAL_SIMD_ARRAY FDPROTO_i0m1_i2m2, const REAL_SIMD_ARRAY FDPROTO_i0m1_i2m3,
    const REAL_SIMD_ARRAY FDPROTO_i0m1_i2m4, const REAL_SIMD_ARRAY FDPROTO_i0m1_i2p1, const REAL_SIMD_ARRAY FDPROTO_i0m1_i2p2,
    const REAL_SIMD_ARRAY FDPROTO_i0m1_i2p3, const REAL_SIMD_ARRAY FDPROTO_i0m1_i2p4, const REAL_SIMD_ARRAY FDPROTO_i0m2_i2m1,
    const REAL_SIMD_ARRAY FDPROTO_i0m2_i2m2, const REAL_SIMD_ARRAY FDPROTO_i0m2_i2m3, const REAL_SIMD_ARRAY FDPROTO_i0m2_i2m4,
    const REAL_SIMD_ARRAY FDPROTO_i0m2_i2p1, const REAL_SIMD_ARRAY FDPROTO_i0m2_i2p2, const REAL_SIMD_ARRAY FDPROTO_i0m2_i2p3,
    const REAL_SIMD_ARRAY FDPROTO_i0m2_i2p4, const REAL_SIMD_ARRAY FDPROTO_i0m3_i2m1, const REAL_SIMD_ARRAY FDPROTO_i0m3_i2m2,
    const REAL_SIMD_ARRAY FDPROTO_i0m3_i2m3, const REAL_SIMD_ARRAY FDPROTO_i0m3_i2m4, const REAL_SIMD_ARRAY FDPROTO_i0m3_i2p1,
    const REAL_SIMD_ARRAY FDPROTO_i0m3_i2p2, const REAL_SIMD_ARRAY FDPROTO_i0m3_i2p3, const REAL_SIMD_ARRAY FDPROTO_i0m3_i2p4,
    const REAL_SIMD_ARRAY FDPROTO_i0m4_i2m1, const REAL_SIMD_ARRAY FDPROTO_i0m4_i2m2, const REAL_SIMD_ARRAY FDPROTO_i0m4_i2m3,
    const REAL_SIMD_ARRAY FDPROTO_i0m4_i2m4, const REAL_SIMD_ARRAY FDPROTO_i0m4_i2p1, const REAL_SIMD_ARRAY FDPROTO_i0m4_i2p2,
    const REAL_SIMD_ARRAY FDPROTO_i0m4_i2p3, const REAL_SIMD_ARRAY FDPROTO_i0m4_i2p4, const REAL_SIMD_ARRAY FDPROTO_i0p1_i2m1,
    const REAL_SIMD_ARRAY FDPROTO_i0p1_i2m2, const REAL_SIMD_ARRAY FDPROTO_i0p1_i2m3, const REAL_SIMD_ARRAY FDPROTO_i0p1_i2m4,
    const REAL_SIMD_ARRAY FDPROTO_i0p1_i2p1, const REAL_SIMD_ARRAY FDPROTO_i0p1_i2p2, const REAL_SIMD_ARRAY FDPROTO_i0p1_i2p3,
    const REAL_SIMD_ARRAY FDPROTO_i0p1_i2p4, const REAL_SIMD_ARRAY FDPROTO_i0p2_i2m1, const REAL_SIMD_ARRAY FDPROTO_i0p2_i2m2,
    const REAL_SIMD_ARRAY FDPROTO_i0p2_i2m3, const REAL_SIMD_ARRAY FDPROTO_i0p2_i2m4, const REAL_SIMD_ARRAY FDPROTO_i0p2_i2p1,
    const REAL_SIMD_ARRAY FDPROTO_i0p2_i2p2, const REAL_SIMD_ARRAY FDPROTO_i0p2_i2p3, const REAL_SIMD_ARRAY FDPROTO_i0p2_i2p4,
    const REAL_SIMD_ARRAY FDPROTO_i0p3_i2m1, const REAL_SIMD_ARRAY FDPROTO_i0p3_i2m2, const REAL_SIMD_ARRAY FDPROTO_i0p3_i2m3,
    const REAL_SIMD_ARRAY FDPROTO_i0p3_i2m4, const REAL_SIMD_ARRAY FDPROTO_i0p3_i2p1, const REAL_SIMD_ARRAY FDPROTO_i0p3_i2p2,
    const REAL_SIMD_ARRAY FDPROTO_i0p3_i2p3, const REAL_SIMD_ARRAY FDPROTO_i0p3_i2p4, const REAL_SIMD_ARRAY FDPROTO_i0p4_i2m1,
    const REAL_SIMD_ARRAY FDPROTO_i0p4_i2m2, const REAL_SIMD_ARRAY FDPROTO_i0p4_i2m3, const REAL_SIMD_ARRAY FDPROTO_i0p4_i2m4,
    const REAL_SIMD_ARRAY FDPROTO_i0p4_i2p1, const REAL_SIMD_ARRAY FDPROTO_i0p4_i2p2, const REAL_SIMD_ARRAY FDPROTO_i0p4_i2p3,
    const REAL_SIMD_ARRAY FDPROTO_i0p4_i2p4, const REAL_SIMD_ARRAY invdxx0, const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_16_11025 = 16.0 / 11025.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_16_11025 = ConstSIMD(dblFDPart1_Rational_16_11025);

  const double dblFDPart1_Rational_16_25 = 16.0 / 25.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_16_25 = ConstSIMD(dblFDPart1_Rational_16_25);

  const double dblFDPart1_Rational_16_525 = 16.0 / 525.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_16_525 = ConstSIMD(dblFDPart1_Rational_16_525);

  const double dblFDPart1_Rational_1_1400 = 1.0 / 1400.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_1400 = ConstSIMD(dblFDPart1_Rational_1_1400);

  const double dblFDPart1_Rational_1_25 = 1.0 / 25.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_25 = ConstSIMD(dblFDPart1_Rational_1_25);

  const double dblFDPart1_Rational_1_350 = 1.0 / 350.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_350 = ConstSIMD(dblFDPart1_Rational_1_350);

  const double dblFDPart1_Rational_1_7350 = 1.0 / 7350.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_7350 = ConstSIMD(dblFDPart1_Rational_1_7350);

  const double dblFDPart1_Rational_1_78400 = 1.0 / 78400.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_78400 = ConstSIMD(dblFDPart1_Rational_1_78400);

  const double dblFDPart1_Rational_4_25 = 4.0 / 25.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_25 = ConstSIMD(dblFDPart1_Rational_4_25);

  const double dblFDPart1_Rational_4_525 = 4.0 / 525.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_525 = ConstSIMD(dblFDPart1_Rational_4_525);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(
      invdxx0,
      MulSIMD(
          invdxx2,
          FusedMulAddSIMD(
              FDPart1_Rational_1_7350,
              AddSIMD(AddSIMD(FDPROTO_i0m4_i2p3, FDPROTO_i0p3_i2m4),
                      AddSIMD(FDPROTO_i0p4_i2m3, SubSIMD(FDPROTO_i0m3_i2p4, AddSIMD(AddSIMD(FDPROTO_i0m3_i2m4, FDPROTO_i0m4_i2m3),
                                                                                    AddSIMD(FDPROTO_i0p3_i2p4, FDPROTO_i0p4_i2p3))))),
              FusedMulAddSIMD(
                  FDPart1_Rational_1_78400, AddSIMD(FDPROTO_i0p4_i2p4, SubSIMD(FDPROTO_i0m4_i2m4, AddSIMD(FDPROTO_i0m4_i2p4, FDPROTO_i0p4_i2m4))),
                  FusedMulAddSIMD(
                      FDPart1_Rational_1_25, AddSIMD(FDPROTO_i0p2_i2p2, SubSIMD(FDPROTO_i0m2_i2m2, AddSIMD(FDPROTO_i0m2_i2p2, FDPROTO_i0p2_i2m2))),
                      FusedMulAddSIMD(
                          FDPart1_Rational_1_350,
                          AddSIMD(AddSIMD(FDPROTO_i0m4_i2p1, FDPROTO_i0p1_i2m4),
                                  AddSIMD(FDPROTO_i0p4_i2m1, SubSIMD(FDPROTO_i0m1_i2p4, AddSIMD(AddSIMD(FDPROTO_i0m1_i2m4, FDPROTO_i0m4_i2m1),
                                                                                                AddSIMD(FDPROTO_i0p1_i2p4, FDPROTO_i0p4_i2p1))))),
                          FusedMulAddSIMD(
                              FDPart1_Rational_16_525,
                              AddSIMD(AddSIMD(FDPROTO_i0m3_i2m1, FDPROTO_i0p1_i2p3),
                                      AddSIMD(FDPROTO_i0p3_i2p1, SubSIMD(FDPROTO_i0m1_i2m3, AddSIMD(AddSIMD(FDPROTO_i0m1_i2p3, FDPROTO_i0m3_i2p1),
                                                                                                    AddSIMD(FDPROTO_i0p1_i2m3, FDPROTO_i0p3_i2m1))))),
                              FusedMulAddSIMD(
                                  FDPart1_Rational_1_1400,
                                  AddSIMD(
                                      AddSIMD(FDPROTO_i0m4_i2m2, FDPROTO_i0p2_i2p4),
                                      AddSIMD(FDPROTO_i0p4_i2p2, SubSIMD(FDPROTO_i0m2_i2m4, AddSIMD(AddSIMD(FDPROTO_i0m2_i2p4, FDPROTO_i0m4_i2p2),
                                                                                                    AddSIMD(FDPROTO_i0p2_i2m4, FDPROTO_i0p4_i2m2))))),
                                  FusedMulAddSIMD(
                                      FDPart1_Rational_4_25,
                                      AddSIMD(AddSIMD(FDPROTO_i0m2_i2p1, FDPROTO_i0p1_i2m2),
                                              AddSIMD(FDPROTO_i0p2_i2m1,
                                                      SubSIMD(FDPROTO_i0m1_i2p2, AddSIMD(AddSIMD(FDPROTO_i0m1_i2m2, FDPROTO_i0m2_i2m1),
                                                                                         AddSIMD(FDPROTO_i0p1_i2p2, FDPROTO_i0p2_i2p1))))),
                                      FusedMulAddSIMD(
                                          FDPart1_Rational_4_525,
                                          AddSIMD(AddSIMD(FDPROTO_i0m3_i2p2, FDPROTO_i0p2_i2m3),
                                                  AddSIMD(FDPROTO_i0p3_i2m2,
                                                          SubSIMD(FDPROTO_i0m2_i2p3, AddSIMD(AddSIMD(FDPROTO_i0m2_i2m3, FDPROTO_i0m3_i2m2),
                                                                                             AddSIMD(FDPROTO_i0p2_i2p3, FDPROTO_i0p3_i2p2))))),
                                          FusedMulAddSIMD(
                                              FDPart1_Rational_16_11025,
                                              AddSIMD(FDPROTO_i0p3_i2p3, SubSIMD(FDPROTO_i0m3_i2m3, AddSIMD(FDPROTO_i0m3_i2p3, FDPROTO_i0p3_i2m3))),
                                              MulSIMD(FDPart1_Rational_16_25,
                                                      AddSIMD(FDPROTO_i0p1_i2p1,
                                                              SubSIMD(FDPROTO_i0m1_i2m1, AddSIMD(FDPROTO_i0m1_i2p1, FDPROTO_i0p1_i2m1)))))))))))))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD11, with FD accuracy order 8.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD11_fdorder8(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i1m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1m2, const REAL_SIMD_ARRAY FDPROTO_i1m3,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1m4, const REAL_SIMD_ARRAY FDPROTO_i1p1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p2, const REAL_SIMD_ARRAY FDPROTO_i1p3,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p4, const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Rational_1_5 = 1.0 / 5.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_5 = ConstSIMD(dblFDPart1_Rational_1_5);

  const double dblFDPart1_Rational_1_560 = 1.0 / 560.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_560 = ConstSIMD(dblFDPart1_Rational_1_560);

  const double dblFDPart1_Rational_205_72 = 205.0 / 72.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_205_72 = ConstSIMD(dblFDPart1_Rational_205_72);

  const double dblFDPart1_Rational_8_315 = 8.0 / 315.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_8_315 = ConstSIMD(dblFDPart1_Rational_8_315);

  const double dblFDPart1_Rational_8_5 = 8.0 / 5.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_8_5 = ConstSIMD(dblFDPart1_Rational_8_5);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(invdxx1, invdxx1),
              FusedMulAddSIMD(FDPart1_Rational_8_5, AddSIMD(FDPROTO_i1m1, FDPROTO_i1p1),
                              FusedMulSubSIMD(FDPart1_Rational_8_315, AddSIMD(FDPROTO_i1m3, FDPROTO_i1p3),
                                              FusedMulAddSIMD(FDPart1_Rational_1_5, AddSIMD(FDPROTO_i1m2, FDPROTO_i1p2),
                                                              FusedMulAddSIMD(FDPart1_Rational_1_560, AddSIMD(FDPROTO_i1m4, FDPROTO_i1p4),
                                                                              MulSIMD(FDPROTO, FDPart1_Rational_205_72))))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD12, with FD accuracy order 8.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD12_fdorder8(
    const REAL_SIMD_ARRAY FDPROTO_i1m1_i2m1, const REAL_SIMD_ARRAY FDPROTO_i1m1_i2m2, const REAL_SIMD_ARRAY FDPROTO_i1m1_i2m3,
    const REAL_SIMD_ARRAY FDPROTO_i1m1_i2m4, const REAL_SIMD_ARRAY FDPROTO_i1m1_i2p1, const REAL_SIMD_ARRAY FDPROTO_i1m1_i2p2,
    const REAL_SIMD_ARRAY FDPROTO_i1m1_i2p3, const REAL_SIMD_ARRAY FDPROTO_i1m1_i2p4, const REAL_SIMD_ARRAY FDPROTO_i1m2_i2m1,
    const REAL_SIMD_ARRAY FDPROTO_i1m2_i2m2, const REAL_SIMD_ARRAY FDPROTO_i1m2_i2m3, const REAL_SIMD_ARRAY FDPROTO_i1m2_i2m4,
    const REAL_SIMD_ARRAY FDPROTO_i1m2_i2p1, const REAL_SIMD_ARRAY FDPROTO_i1m2_i2p2, const REAL_SIMD_ARRAY FDPROTO_i1m2_i2p3,
    const REAL_SIMD_ARRAY FDPROTO_i1m2_i2p4, const REAL_SIMD_ARRAY FDPROTO_i1m3_i2m1, const REAL_SIMD_ARRAY FDPROTO_i1m3_i2m2,
    const REAL_SIMD_ARRAY FDPROTO_i1m3_i2m3, const REAL_SIMD_ARRAY FDPROTO_i1m3_i2m4, const REAL_SIMD_ARRAY FDPROTO_i1m3_i2p1,
    const REAL_SIMD_ARRAY FDPROTO_i1m3_i2p2, const REAL_SIMD_ARRAY FDPROTO_i1m3_i2p3, const REAL_SIMD_ARRAY FDPROTO_i1m3_i2p4,
    const REAL_SIMD_ARRAY FDPROTO_i1m4_i2m1, const REAL_SIMD_ARRAY FDPROTO_i1m4_i2m2, const REAL_SIMD_ARRAY FDPROTO_i1m4_i2m3,
    const REAL_SIMD_ARRAY FDPROTO_i1m4_i2m4, const REAL_SIMD_ARRAY FDPROTO_i1m4_i2p1, const REAL_SIMD_ARRAY FDPROTO_i1m4_i2p2,
    const REAL_SIMD_ARRAY FDPROTO_i1m4_i2p3, const REAL_SIMD_ARRAY FDPROTO_i1m4_i2p4, const REAL_SIMD_ARRAY FDPROTO_i1p1_i2m1,
    const REAL_SIMD_ARRAY FDPROTO_i1p1_i2m2, const REAL_SIMD_ARRAY FDPROTO_i1p1_i2m3, const REAL_SIMD_ARRAY FDPROTO_i1p1_i2m4,
    const REAL_SIMD_ARRAY FDPROTO_i1p1_i2p1, const REAL_SIMD_ARRAY FDPROTO_i1p1_i2p2, const REAL_SIMD_ARRAY FDPROTO_i1p1_i2p3,
    const REAL_SIMD_ARRAY FDPROTO_i1p1_i2p4, const REAL_SIMD_ARRAY FDPROTO_i1p2_i2m1, const REAL_SIMD_ARRAY FDPROTO_i1p2_i2m2,
    const REAL_SIMD_ARRAY FDPROTO_i1p2_i2m3, const REAL_SIMD_ARRAY FDPROTO_i1p2_i2m4, const REAL_SIMD_ARRAY FDPROTO_i1p2_i2p1,
    const REAL_SIMD_ARRAY FDPROTO_i1p2_i2p2, const REAL_SIMD_ARRAY FDPROTO_i1p2_i2p3, const REAL_SIMD_ARRAY FDPROTO_i1p2_i2p4,
    const REAL_SIMD_ARRAY FDPROTO_i1p3_i2m1, const REAL_SIMD_ARRAY FDPROTO_i1p3_i2m2, const REAL_SIMD_ARRAY FDPROTO_i1p3_i2m3,
    const REAL_SIMD_ARRAY FDPROTO_i1p3_i2m4, const REAL_SIMD_ARRAY FDPROTO_i1p3_i2p1, const REAL_SIMD_ARRAY FDPROTO_i1p3_i2p2,
    const REAL_SIMD_ARRAY FDPROTO_i1p3_i2p3, const REAL_SIMD_ARRAY FDPROTO_i1p3_i2p4, const REAL_SIMD_ARRAY FDPROTO_i1p4_i2m1,
    const REAL_SIMD_ARRAY FDPROTO_i1p4_i2m2, const REAL_SIMD_ARRAY FDPROTO_i1p4_i2m3, const REAL_SIMD_ARRAY FDPROTO_i1p4_i2m4,
    const REAL_SIMD_ARRAY FDPROTO_i1p4_i2p1, const REAL_SIMD_ARRAY FDPROTO_i1p4_i2p2, const REAL_SIMD_ARRAY FDPROTO_i1p4_i2p3,
    const REAL_SIMD_ARRAY FDPROTO_i1p4_i2p4, const REAL_SIMD_ARRAY invdxx1, const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_16_11025 = 16.0 / 11025.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_16_11025 = ConstSIMD(dblFDPart1_Rational_16_11025);

  const double dblFDPart1_Rational_16_25 = 16.0 / 25.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_16_25 = ConstSIMD(dblFDPart1_Rational_16_25);

  const double dblFDPart1_Rational_16_525 = 16.0 / 525.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_16_525 = ConstSIMD(dblFDPart1_Rational_16_525);

  const double dblFDPart1_Rational_1_1400 = 1.0 / 1400.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_1400 = ConstSIMD(dblFDPart1_Rational_1_1400);

  const double dblFDPart1_Rational_1_25 = 1.0 / 25.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_25 = ConstSIMD(dblFDPart1_Rational_1_25);

  const double dblFDPart1_Rational_1_350 = 1.0 / 350.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_350 = ConstSIMD(dblFDPart1_Rational_1_350);

  const double dblFDPart1_Rational_1_7350 = 1.0 / 7350.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_7350 = ConstSIMD(dblFDPart1_Rational_1_7350);

  const double dblFDPart1_Rational_1_78400 = 1.0 / 78400.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_78400 = ConstSIMD(dblFDPart1_Rational_1_78400);

  const double dblFDPart1_Rational_4_25 = 4.0 / 25.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_25 = ConstSIMD(dblFDPart1_Rational_4_25);

  const double dblFDPart1_Rational_4_525 = 4.0 / 525.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_4_525 = ConstSIMD(dblFDPart1_Rational_4_525);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(
      invdxx1,
      MulSIMD(
          invdxx2,
          FusedMulAddSIMD(
              FDPart1_Rational_1_7350,
              AddSIMD(AddSIMD(FDPROTO_i1m4_i2p3, FDPROTO_i1p3_i2m4),
                      AddSIMD(FDPROTO_i1p4_i2m3, SubSIMD(FDPROTO_i1m3_i2p4, AddSIMD(AddSIMD(FDPROTO_i1m3_i2m4, FDPROTO_i1m4_i2m3),
                                                                                    AddSIMD(FDPROTO_i1p3_i2p4, FDPROTO_i1p4_i2p3))))),
              FusedMulAddSIMD(
                  FDPart1_Rational_1_78400, AddSIMD(FDPROTO_i1p4_i2p4, SubSIMD(FDPROTO_i1m4_i2m4, AddSIMD(FDPROTO_i1m4_i2p4, FDPROTO_i1p4_i2m4))),
                  FusedMulAddSIMD(
                      FDPart1_Rational_1_25, AddSIMD(FDPROTO_i1p2_i2p2, SubSIMD(FDPROTO_i1m2_i2m2, AddSIMD(FDPROTO_i1m2_i2p2, FDPROTO_i1p2_i2m2))),
                      FusedMulAddSIMD(
                          FDPart1_Rational_1_350,
                          AddSIMD(AddSIMD(FDPROTO_i1m4_i2p1, FDPROTO_i1p1_i2m4),
                                  AddSIMD(FDPROTO_i1p4_i2m1, SubSIMD(FDPROTO_i1m1_i2p4, AddSIMD(AddSIMD(FDPROTO_i1m1_i2m4, FDPROTO_i1m4_i2m1),
                                                                                                AddSIMD(FDPROTO_i1p1_i2p4, FDPROTO_i1p4_i2p1))))),
                          FusedMulAddSIMD(
                              FDPart1_Rational_16_525,
                              AddSIMD(AddSIMD(FDPROTO_i1m3_i2m1, FDPROTO_i1p1_i2p3),
                                      AddSIMD(FDPROTO_i1p3_i2p1, SubSIMD(FDPROTO_i1m1_i2m3, AddSIMD(AddSIMD(FDPROTO_i1m1_i2p3, FDPROTO_i1m3_i2p1),
                                                                                                    AddSIMD(FDPROTO_i1p1_i2m3, FDPROTO_i1p3_i2m1))))),
                              FusedMulAddSIMD(
                                  FDPart1_Rational_1_1400,
                                  AddSIMD(
                                      AddSIMD(FDPROTO_i1m4_i2m2, FDPROTO_i1p2_i2p4),
                                      AddSIMD(FDPROTO_i1p4_i2p2, SubSIMD(FDPROTO_i1m2_i2m4, AddSIMD(AddSIMD(FDPROTO_i1m2_i2p4, FDPROTO_i1m4_i2p2),
                                                                                                    AddSIMD(FDPROTO_i1p2_i2m4, FDPROTO_i1p4_i2m2))))),
                                  FusedMulAddSIMD(
                                      FDPart1_Rational_4_25,
                                      AddSIMD(AddSIMD(FDPROTO_i1m2_i2p1, FDPROTO_i1p1_i2m2),
                                              AddSIMD(FDPROTO_i1p2_i2m1,
                                                      SubSIMD(FDPROTO_i1m1_i2p2, AddSIMD(AddSIMD(FDPROTO_i1m1_i2m2, FDPROTO_i1m2_i2m1),
                                                                                         AddSIMD(FDPROTO_i1p1_i2p2, FDPROTO_i1p2_i2p1))))),
                                      FusedMulAddSIMD(
                                          FDPart1_Rational_4_525,
                                          AddSIMD(AddSIMD(FDPROTO_i1m3_i2p2, FDPROTO_i1p2_i2m3),
                                                  AddSIMD(FDPROTO_i1p3_i2m2,
                                                          SubSIMD(FDPROTO_i1m2_i2p3, AddSIMD(AddSIMD(FDPROTO_i1m2_i2m3, FDPROTO_i1m3_i2m2),
                                                                                             AddSIMD(FDPROTO_i1p2_i2p3, FDPROTO_i1p3_i2p2))))),
                                          FusedMulAddSIMD(
                                              FDPart1_Rational_16_11025,
                                              AddSIMD(FDPROTO_i1p3_i2p3, SubSIMD(FDPROTO_i1m3_i2m3, AddSIMD(FDPROTO_i1m3_i2p3, FDPROTO_i1p3_i2m3))),
                                              MulSIMD(FDPart1_Rational_16_25,
                                                      AddSIMD(FDPROTO_i1p1_i2p1,
                                                              SubSIMD(FDPROTO_i1m1_i2m1, AddSIMD(FDPROTO_i1m1_i2p1, FDPROTO_i1p1_i2m1)))))))))))))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD22, with FD accuracy order 8.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD22_fdorder8(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2m2, const REAL_SIMD_ARRAY FDPROTO_i2m3,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2m4, const REAL_SIMD_ARRAY FDPROTO_i2p1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2p2, const REAL_SIMD_ARRAY FDPROTO_i2p3,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2p4, const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_5 = 1.0 / 5.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_5 = ConstSIMD(dblFDPart1_Rational_1_5);

  const double dblFDPart1_Rational_1_560 = 1.0 / 560.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_560 = ConstSIMD(dblFDPart1_Rational_1_560);

  const double dblFDPart1_Rational_205_72 = 205.0 / 72.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_205_72 = ConstSIMD(dblFDPart1_Rational_205_72);

  const double dblFDPart1_Rational_8_315 = 8.0 / 315.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_8_315 = ConstSIMD(dblFDPart1_Rational_8_315);

  const double dblFDPart1_Rational_8_5 = 8.0 / 5.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_8_5 = ConstSIMD(dblFDPart1_Rational_8_5);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(invdxx2, invdxx2),
              FusedMulAddSIMD(FDPart1_Rational_8_5, AddSIMD(FDPROTO_i2m1, FDPROTO_i2p1),
                              FusedMulSubSIMD(FDPart1_Rational_8_315, AddSIMD(FDPROTO_i2m3, FDPROTO_i2p3),
                                              FusedMulAddSIMD(FDPart1_Rational_1_5, AddSIMD(FDPROTO_i2m2, FDPROTO_i2p2),
                                                              FusedMulAddSIMD(FDPart1_Rational_1_560, AddSIMD(FDPROTO_i2m4, FDPROTO_i2p4),
                                                                              MulSIMD(FDPROTO, FDPart1_Rational_205_72))))));

  return FD_result;
}

/**
 * Evaluate BSSN constraints.
 */
void BaikalVacuum_BSSN_constraints_order_8(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS_BaikalVacuum_BSSN_constraints_order_8;

  const REAL_SIMD_ARRAY invdxx0 CCTK_ATTRIBUTE_UNUSED = ConstSIMD(1.0 / CCTK_DELTA_SPACE(0));
  const REAL_SIMD_ARRAY invdxx1 CCTK_ATTRIBUTE_UNUSED = ConstSIMD(1.0 / CCTK_DELTA_SPACE(1));
  const REAL_SIMD_ARRAY invdxx2 CCTK_ATTRIBUTE_UNUSED = ConstSIMD(1.0 / CCTK_DELTA_SPACE(2));
  const CCTK_REAL *param_PI CCTK_ATTRIBUTE_UNUSED = CCTK_ParameterGet("PI", "BaikalVacuum", NULL);
  const REAL_SIMD_ARRAY PI CCTK_ATTRIBUTE_UNUSED = ConstSIMD(*param_PI);

#pragma omp parallel for
  for (int i2 = cctk_nghostzones[2]; i2 < cctk_lsh[2] - cctk_nghostzones[2]; i2++) {
    for (int i1 = cctk_nghostzones[1]; i1 < cctk_lsh[1] - cctk_nghostzones[1]; i1++) {
      for (int i0 = cctk_nghostzones[0]; i0 < cctk_lsh[0] - cctk_nghostzones[0]; i0 += simd_width) {
        /*
         * NRPy+-Generated GF Access/FD Code, Step 1 of 2:
         * Read gridfunction(s) from main memory and compute FD stencils as needed.
         */
        const REAL_SIMD_ARRAY aDD00_i2m4 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY aDD00_i2m3 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY aDD00_i2m2 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY aDD00_i2m1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD00_i1m4 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY aDD00_i1m3 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY aDD00_i1m2 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY aDD00_i1m1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0m4 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0m3 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0m2 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0m1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0p1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0p2 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0p3 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0p4 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i1p1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i1p2 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY aDD00_i1p3 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY aDD00_i1p4 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY aDD00_i2p1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD00_i2p2 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY aDD00_i2p3 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY aDD00_i2p4 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY aDD01_i2m4 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY aDD01_i2m3 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY aDD01_i2m2 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY aDD01_i2m1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD01_i1m4 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY aDD01_i1m3 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY aDD01_i1m2 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY aDD01_i1m1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0m4 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0m3 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0m2 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0m1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0p1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0p2 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0p3 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0p4 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i1p1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i1p2 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY aDD01_i1p3 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY aDD01_i1p4 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY aDD01_i2p1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD01_i2p2 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY aDD01_i2p3 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY aDD01_i2p4 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY aDD02_i2m4 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY aDD02_i2m3 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY aDD02_i2m2 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY aDD02_i2m1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD02_i1m4 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY aDD02_i1m3 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY aDD02_i1m2 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY aDD02_i1m1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0m4 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0m3 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0m2 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0m1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0p1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0p2 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0p3 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0p4 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i1p1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i1p2 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY aDD02_i1p3 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY aDD02_i1p4 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY aDD02_i2p1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD02_i2p2 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY aDD02_i2p3 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY aDD02_i2p4 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY aDD11_i2m4 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY aDD11_i2m3 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY aDD11_i2m2 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY aDD11_i2m1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD11_i1m4 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY aDD11_i1m3 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY aDD11_i1m2 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY aDD11_i1m1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0m4 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0m3 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0m2 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0m1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0p1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0p2 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0p3 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0p4 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i1p1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i1p2 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY aDD11_i1p3 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY aDD11_i1p4 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY aDD11_i2p1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD11_i2p2 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY aDD11_i2p3 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY aDD11_i2p4 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY aDD12_i2m4 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY aDD12_i2m3 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY aDD12_i2m2 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY aDD12_i2m1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD12_i1m4 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY aDD12_i1m3 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY aDD12_i1m2 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY aDD12_i1m1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0m4 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0m3 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0m2 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0m1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0p1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0p2 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0p3 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0p4 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i1p1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i1p2 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY aDD12_i1p3 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY aDD12_i1p4 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY aDD12_i2p1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD12_i2p2 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY aDD12_i2p3 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY aDD12_i2p4 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY aDD22_i2m4 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY aDD22_i2m3 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY aDD22_i2m2 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY aDD22_i2m1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD22_i1m4 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY aDD22_i1m3 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY aDD22_i1m2 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY aDD22_i1m1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0m4 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0m3 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0m2 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0m1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0p1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0p2 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0p3 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0p4 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i1p1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i1p2 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY aDD22_i1p3 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY aDD22_i1p4 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY aDD22_i2p1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD22_i2p2 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY aDD22_i2p3 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY aDD22_i2p4 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i1m4_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i1m3_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i1m2_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i0m4_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i0m3_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i0m2_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i0p2_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i0p3_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i0p4_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i1p2_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i1p3_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i1p4_i2m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 4)]);
        const REAL_SIMD_ARRAY cf_i1m4_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i1m3_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i1m2_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i0m4_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i0m3_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i0m2_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i0p2_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i0p3_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i0p4_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i1p2_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i1p3_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i1p4_i2m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 3)]);
        const REAL_SIMD_ARRAY cf_i1m4_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i1m3_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i1m2_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i0m4_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i0m3_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i0p3_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i0p4_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i1p2_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i1p3_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i1p4_i2m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 2)]);
        const REAL_SIMD_ARRAY cf_i1m4_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i1m3_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i1m2_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0m4_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0m3_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0m2_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0p2_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0p3_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0p4_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i1p2_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i1p3_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i1p4_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0m4_i1m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0m3_i1m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i1m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY cf_i1m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i1m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0p3_i1m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0p4_i1m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0m4_i1m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0m3_i1m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i1m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY cf_i1m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i1m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0p3_i1m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0p4_i1m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0m4_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0m3_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0p3_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0p4_i1m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0m4_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m3_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p3_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p4_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY cf = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m4_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m3_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p3_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p4_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m4_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0m3_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0p3_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0p4_i1p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY cf_i0m4_i1p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0m3_i1p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i1p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY cf_i1p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i1p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0p3_i1p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0p4_i1p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY cf_i0m4_i1p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0m3_i1p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i1p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY cf_i1p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i1p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0p3_i1p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY cf_i0p4_i1p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY cf_i1m4_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i1m3_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i1m2_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0m4_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0m3_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0m2_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0p2_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0p3_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0p4_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i1p2_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i1p3_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i1p4_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i1m4_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i1m3_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i1m2_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i0m4_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i0m3_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i0m2_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i0p2_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i0p3_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i0p4_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i1p2_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i1p3_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i1p4_i2p2 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 2)]);
        const REAL_SIMD_ARRAY cf_i1m4_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i1m3_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i1m2_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i0m4_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i0m3_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i0m2_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i0p2_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i0p3_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i0p4_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i1p2_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i1p3_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i1p4_i2p3 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 3)]);
        const REAL_SIMD_ARRAY cf_i1m4_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i1m3_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i1m2_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i0m4_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i0m3_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i0m2_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i0p2_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i0p3_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i0p4_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i1p2_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i1p3_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 4)]);
        const REAL_SIMD_ARRAY cf_i1p4_i2p4 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i1m4_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i1m3_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i1m2_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i1m1_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i1p1_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i1p2_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i1p3_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i1p4_i2m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD00_i1m4_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i1m3_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i1m2_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i1m1_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i1p1_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i1p2_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i1p3_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i1p4_i2m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD00_i1m4_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i1m3_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i1m2_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i1m1_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i1p1_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i1p2_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i1p3_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i1p4_i2m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD00_i1m4_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i1m3_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i1m2_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i1m1_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i1p1_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i1p2_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i1p3_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i1p4_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i1m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i1m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i1m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i1m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i1m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i1m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i1m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i1m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i1m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i1m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i1m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i1m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i1m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i1m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i1m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i1m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i1m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i1m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i1m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i1m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i1m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i1m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i1m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i1m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i1p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i1p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i1p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i1p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i1p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i1p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i1p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i1p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i1p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i1p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i1p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i1p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i1p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i1p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i1p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i1p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i1p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i1p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i1p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i1p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i1p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i1p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i1p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i1p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1m4_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i1m3_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i1m2_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i1m1_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i1p1_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i1p2_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i1p3_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i1p4_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i1m4_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i1m3_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i1m2_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i1m1_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i1p1_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i1p2_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i1p3_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i1p4_i2p2 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD00_i1m4_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i1m3_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i1m2_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i1m1_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i1p1_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i1p2_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i1p3_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i1p4_i2p3 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD00_i1m4_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i1m3_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i1m2_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i1m1_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i0m4_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i0m3_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i0m2_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i0p2_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i0p3_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i0p4_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i1p1_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i1p2_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i1p3_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD00_i1p4_i2p4 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i1m4_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i1m3_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i1m2_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i1m1_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i1p1_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i1p2_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i1p3_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i1p4_i2m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD01_i1m4_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i1m3_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i1m2_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i1m1_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i1p1_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i1p2_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i1p3_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i1p4_i2m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD01_i1m4_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i1m3_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i1m2_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i1m1_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i1p1_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i1p2_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i1p3_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i1p4_i2m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD01_i1m4_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i1m3_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i1m2_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i1m1_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i1p1_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i1p2_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i1p3_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i1p4_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i1m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i1m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i1m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i1m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i1m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i1m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i1m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i1m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i1m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i1m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i1m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i1m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i1m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i1m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i1m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i1m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i1m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i1m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i1m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i1m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i1m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i1m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i1m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i1m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i1p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i1p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i1p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i1p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i1p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i1p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i1p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i1p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i1p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i1p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i1p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i1p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i1p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i1p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i1p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i1p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i1p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i1p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i1p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i1p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i1p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i1p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i1p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i1p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1m4_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i1m3_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i1m2_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i1m1_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i1p1_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i1p2_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i1p3_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i1p4_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i1m4_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i1m3_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i1m2_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i1m1_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i1p1_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i1p2_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i1p3_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i1p4_i2p2 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD01_i1m4_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i1m3_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i1m2_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i1m1_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i1p1_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i1p2_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i1p3_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i1p4_i2p3 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD01_i1m4_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i1m3_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i1m2_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i1m1_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i0m4_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i0m3_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i0m2_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i0p2_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i0p3_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i0p4_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i1p1_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i1p2_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i1p3_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD01_i1p4_i2p4 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i1m4_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i1m3_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i1m2_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i1m1_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i1p1_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i1p2_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i1p3_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i1p4_i2m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD02_i1m4_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i1m3_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i1m2_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i1m1_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i1p1_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i1p2_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i1p3_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i1p4_i2m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD02_i1m4_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i1m3_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i1m2_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i1m1_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i1p1_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i1p2_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i1p3_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i1p4_i2m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD02_i1m4_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i1m3_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i1m2_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i1m1_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i1p1_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i1p2_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i1p3_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i1p4_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i1m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i1m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i1m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i1m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i1m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i1m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i1m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i1m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i1m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i1m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i1m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i1m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i1m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i1m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i1m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i1m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i1m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i1m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i1m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i1m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i1m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i1m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i1m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i1m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i1p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i1p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i1p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i1p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i1p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i1p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i1p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i1p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i1p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i1p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i1p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i1p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i1p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i1p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i1p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i1p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i1p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i1p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i1p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i1p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i1p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i1p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i1p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i1p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1m4_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i1m3_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i1m2_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i1m1_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i1p1_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i1p2_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i1p3_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i1p4_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i1m4_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i1m3_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i1m2_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i1m1_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i1p1_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i1p2_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i1p3_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i1p4_i2p2 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD02_i1m4_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i1m3_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i1m2_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i1m1_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i1p1_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i1p2_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i1p3_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i1p4_i2p3 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD02_i1m4_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i1m3_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i1m2_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i1m1_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i0m4_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i0m3_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i0m2_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i0p2_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i0p3_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i0p4_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i1p1_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i1p2_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i1p3_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD02_i1p4_i2p4 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i1m4_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i1m3_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i1m2_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i1m1_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i1p1_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i1p2_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i1p3_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i1p4_i2m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD11_i1m4_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i1m3_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i1m2_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i1m1_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i1p1_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i1p2_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i1p3_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i1p4_i2m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD11_i1m4_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i1m3_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i1m2_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i1m1_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i1p1_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i1p2_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i1p3_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i1p4_i2m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD11_i1m4_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i1m3_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i1m2_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i1m1_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i1p1_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i1p2_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i1p3_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i1p4_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i1m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i1m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i1m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i1m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i1m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i1m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i1m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i1m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i1m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i1m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i1m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i1m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i1m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i1m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i1m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i1m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i1m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i1m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i1m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i1m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i1m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i1m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i1m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i1m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i1p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i1p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i1p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i1p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i1p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i1p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i1p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i1p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i1p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i1p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i1p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i1p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i1p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i1p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i1p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i1p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i1p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i1p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i1p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i1p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i1p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i1p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i1p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i1p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1m4_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i1m3_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i1m2_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i1m1_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i1p1_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i1p2_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i1p3_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i1p4_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i1m4_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i1m3_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i1m2_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i1m1_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i1p1_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i1p2_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i1p3_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i1p4_i2p2 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD11_i1m4_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i1m3_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i1m2_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i1m1_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i1p1_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i1p2_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i1p3_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i1p4_i2p3 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD11_i1m4_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i1m3_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i1m2_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i1m1_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i0m4_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i0m3_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i0m2_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i0p2_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i0p3_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i0p4_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i1p1_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i1p2_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i1p3_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD11_i1p4_i2p4 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i1m4_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i1m3_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i1m2_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i1m1_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i1p1_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i1p2_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i1p3_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i1p4_i2m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD12_i1m4_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i1m3_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i1m2_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i1m1_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i1p1_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i1p2_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i1p3_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i1p4_i2m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD12_i1m4_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i1m3_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i1m2_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i1m1_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i1p1_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i1p2_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i1p3_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i1p4_i2m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD12_i1m4_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i1m3_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i1m2_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i1m1_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i1p1_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i1p2_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i1p3_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i1p4_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i1m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i1m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i1m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i1m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i1m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i1m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i1m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i1m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i1m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i1m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i1m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i1m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i1m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i1m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i1m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i1m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i1m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i1m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i1m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i1m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i1m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i1m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i1m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i1m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i1p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i1p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i1p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i1p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i1p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i1p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i1p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i1p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i1p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i1p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i1p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i1p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i1p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i1p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i1p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i1p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i1p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i1p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i1p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i1p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i1p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i1p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i1p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i1p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1m4_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i1m3_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i1m2_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i1m1_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i1p1_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i1p2_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i1p3_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i1p4_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i1m4_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i1m3_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i1m2_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i1m1_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i1p1_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i1p2_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i1p3_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i1p4_i2p2 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD12_i1m4_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i1m3_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i1m2_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i1m1_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i1p1_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i1p2_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i1p3_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i1p4_i2p3 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD12_i1m4_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i1m3_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i1m2_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i1m1_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i0m4_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i0m3_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i0m2_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i0p2_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i0p3_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i0p4_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i1p1_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i1p2_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i1p3_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD12_i1p4_i2p4 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i1m4_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i1m3_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i1m2_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i1m1_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i1p1_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i1p2_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i1p3_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i1p4_i2m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 4)]);
        const REAL_SIMD_ARRAY hDD22_i1m4_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i1m3_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i1m2_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i1m1_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i1p1_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i1p2_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i1p3_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i1p4_i2m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 3)]);
        const REAL_SIMD_ARRAY hDD22_i1m4_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i1m3_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i1m2_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i1m1_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i1p1_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i1p2_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i1p3_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i1p4_i2m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 2)]);
        const REAL_SIMD_ARRAY hDD22_i1m4_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i1m3_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i1m2_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i1m1_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i1p1_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i1p2_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i1p3_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i1p4_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i1m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i1m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i1m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i1m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i1m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i1m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i1m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i1m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i1m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i1m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i1m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i1m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i1m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i1m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i1m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i1m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i1m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i1m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i1m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i1m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i1m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i1m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i1m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i1m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i1p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i1p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i1p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i1p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i1p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i1p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i1p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i1p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i1p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i1p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i1p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i1p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i1p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i1p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i1p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i1p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i1p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i1p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i1p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i1p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i1p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i1p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i1p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i1p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1m4_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i1m3_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i1m2_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i1m1_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i1p1_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i1p2_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i1p3_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i1p4_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i1m4_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i1m3_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i1m2_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i1m1_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i1p1_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i1p2_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i1p3_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i1p4_i2p2 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 2)]);
        const REAL_SIMD_ARRAY hDD22_i1m4_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i1m3_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i1m2_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i1m1_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i1p1_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i1p2_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i1p3_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i1p4_i2p3 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 3)]);
        const REAL_SIMD_ARRAY hDD22_i1m4_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i1m3_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i1m2_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i1m1_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i0m4_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i0m3_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i0m2_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i0p2_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i0p3_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i0p4_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i1p1_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i1p2_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i1p3_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2 + 4)]);
        const REAL_SIMD_ARRAY hDD22_i1p4_i2p4 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2 + 4)]);
        const REAL_SIMD_ARRAY lambdaU0_i2m4 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY lambdaU0_i2m3 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY lambdaU0_i2m2 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY lambdaU0_i2m1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY lambdaU0_i1m4 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1m3 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1m2 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1m1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0m4 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0m3 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0m2 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0m1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0p1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0p2 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0p3 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0p4 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1p1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1p2 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1p3 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1p4 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i2p1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY lambdaU0_i2p2 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY lambdaU0_i2p3 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY lambdaU0_i2p4 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY lambdaU1_i2m4 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY lambdaU1_i2m3 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY lambdaU1_i2m2 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY lambdaU1_i2m1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY lambdaU1_i1m4 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1m3 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1m2 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1m1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0m4 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0m3 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0m2 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0m1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0p1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0p2 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0p3 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0p4 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1p1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1p2 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1p3 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1p4 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i2p1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY lambdaU1_i2p2 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY lambdaU1_i2p3 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY lambdaU1_i2p4 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY lambdaU2_i2m4 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY lambdaU2_i2m3 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY lambdaU2_i2m2 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY lambdaU2_i2m1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY lambdaU2_i1m4 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1m3 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1m2 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1m1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0m4 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0m3 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0m2 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0m1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0p1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0p2 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0p3 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0p4 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1p1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1p2 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1p3 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1p4 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i2p1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY lambdaU2_i2p2 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY lambdaU2_i2p3 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY lambdaU2_i2p4 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY trK_i2m4 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 4)]);
        const REAL_SIMD_ARRAY trK_i2m3 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 3)]);
        const REAL_SIMD_ARRAY trK_i2m2 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 2)]);
        const REAL_SIMD_ARRAY trK_i2m1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY trK_i1m4 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 4, i2)]);
        const REAL_SIMD_ARRAY trK_i1m3 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 3, i2)]);
        const REAL_SIMD_ARRAY trK_i1m2 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 2, i2)]);
        const REAL_SIMD_ARRAY trK_i1m1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY trK_i0m4 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 - 4, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i0m3 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 - 3, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i0m2 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 - 2, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i0m1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY trK = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i0p1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i0p2 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 + 2, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i0p3 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 + 3, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i0p4 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 + 4, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i1p1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY trK_i1p2 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 2, i2)]);
        const REAL_SIMD_ARRAY trK_i1p3 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 3, i2)]);
        const REAL_SIMD_ARRAY trK_i1p4 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 4, i2)]);
        const REAL_SIMD_ARRAY trK_i2p1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY trK_i2p2 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 2)]);
        const REAL_SIMD_ARRAY trK_i2p3 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 3)]);
        const REAL_SIMD_ARRAY trK_i2p4 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 4)]);
        const REAL_SIMD_ARRAY aDD_dD000 =
            SIMD_fd_function_dD0_fdorder8(aDD00_i0m1, aDD00_i0m2, aDD00_i0m3, aDD00_i0m4, aDD00_i0p1, aDD00_i0p2, aDD00_i0p3, aDD00_i0p4, invdxx0);
        const REAL_SIMD_ARRAY aDD_dD001 =
            SIMD_fd_function_dD1_fdorder8(aDD00_i1m1, aDD00_i1m2, aDD00_i1m3, aDD00_i1m4, aDD00_i1p1, aDD00_i1p2, aDD00_i1p3, aDD00_i1p4, invdxx1);
        const REAL_SIMD_ARRAY aDD_dD002 =
            SIMD_fd_function_dD2_fdorder8(aDD00_i2m1, aDD00_i2m2, aDD00_i2m3, aDD00_i2m4, aDD00_i2p1, aDD00_i2p2, aDD00_i2p3, aDD00_i2p4, invdxx2);
        const REAL_SIMD_ARRAY aDD_dD010 =
            SIMD_fd_function_dD0_fdorder8(aDD01_i0m1, aDD01_i0m2, aDD01_i0m3, aDD01_i0m4, aDD01_i0p1, aDD01_i0p2, aDD01_i0p3, aDD01_i0p4, invdxx0);
        const REAL_SIMD_ARRAY aDD_dD011 =
            SIMD_fd_function_dD1_fdorder8(aDD01_i1m1, aDD01_i1m2, aDD01_i1m3, aDD01_i1m4, aDD01_i1p1, aDD01_i1p2, aDD01_i1p3, aDD01_i1p4, invdxx1);
        const REAL_SIMD_ARRAY aDD_dD012 =
            SIMD_fd_function_dD2_fdorder8(aDD01_i2m1, aDD01_i2m2, aDD01_i2m3, aDD01_i2m4, aDD01_i2p1, aDD01_i2p2, aDD01_i2p3, aDD01_i2p4, invdxx2);
        const REAL_SIMD_ARRAY aDD_dD020 =
            SIMD_fd_function_dD0_fdorder8(aDD02_i0m1, aDD02_i0m2, aDD02_i0m3, aDD02_i0m4, aDD02_i0p1, aDD02_i0p2, aDD02_i0p3, aDD02_i0p4, invdxx0);
        const REAL_SIMD_ARRAY aDD_dD021 =
            SIMD_fd_function_dD1_fdorder8(aDD02_i1m1, aDD02_i1m2, aDD02_i1m3, aDD02_i1m4, aDD02_i1p1, aDD02_i1p2, aDD02_i1p3, aDD02_i1p4, invdxx1);
        const REAL_SIMD_ARRAY aDD_dD022 =
            SIMD_fd_function_dD2_fdorder8(aDD02_i2m1, aDD02_i2m2, aDD02_i2m3, aDD02_i2m4, aDD02_i2p1, aDD02_i2p2, aDD02_i2p3, aDD02_i2p4, invdxx2);
        const REAL_SIMD_ARRAY aDD_dD110 =
            SIMD_fd_function_dD0_fdorder8(aDD11_i0m1, aDD11_i0m2, aDD11_i0m3, aDD11_i0m4, aDD11_i0p1, aDD11_i0p2, aDD11_i0p3, aDD11_i0p4, invdxx0);
        const REAL_SIMD_ARRAY aDD_dD111 =
            SIMD_fd_function_dD1_fdorder8(aDD11_i1m1, aDD11_i1m2, aDD11_i1m3, aDD11_i1m4, aDD11_i1p1, aDD11_i1p2, aDD11_i1p3, aDD11_i1p4, invdxx1);
        const REAL_SIMD_ARRAY aDD_dD112 =
            SIMD_fd_function_dD2_fdorder8(aDD11_i2m1, aDD11_i2m2, aDD11_i2m3, aDD11_i2m4, aDD11_i2p1, aDD11_i2p2, aDD11_i2p3, aDD11_i2p4, invdxx2);
        const REAL_SIMD_ARRAY aDD_dD120 =
            SIMD_fd_function_dD0_fdorder8(aDD12_i0m1, aDD12_i0m2, aDD12_i0m3, aDD12_i0m4, aDD12_i0p1, aDD12_i0p2, aDD12_i0p3, aDD12_i0p4, invdxx0);
        const REAL_SIMD_ARRAY aDD_dD121 =
            SIMD_fd_function_dD1_fdorder8(aDD12_i1m1, aDD12_i1m2, aDD12_i1m3, aDD12_i1m4, aDD12_i1p1, aDD12_i1p2, aDD12_i1p3, aDD12_i1p4, invdxx1);
        const REAL_SIMD_ARRAY aDD_dD122 =
            SIMD_fd_function_dD2_fdorder8(aDD12_i2m1, aDD12_i2m2, aDD12_i2m3, aDD12_i2m4, aDD12_i2p1, aDD12_i2p2, aDD12_i2p3, aDD12_i2p4, invdxx2);
        const REAL_SIMD_ARRAY aDD_dD220 =
            SIMD_fd_function_dD0_fdorder8(aDD22_i0m1, aDD22_i0m2, aDD22_i0m3, aDD22_i0m4, aDD22_i0p1, aDD22_i0p2, aDD22_i0p3, aDD22_i0p4, invdxx0);
        const REAL_SIMD_ARRAY aDD_dD221 =
            SIMD_fd_function_dD1_fdorder8(aDD22_i1m1, aDD22_i1m2, aDD22_i1m3, aDD22_i1m4, aDD22_i1p1, aDD22_i1p2, aDD22_i1p3, aDD22_i1p4, invdxx1);
        const REAL_SIMD_ARRAY aDD_dD222 =
            SIMD_fd_function_dD2_fdorder8(aDD22_i2m1, aDD22_i2m2, aDD22_i2m3, aDD22_i2m4, aDD22_i2p1, aDD22_i2p2, aDD22_i2p3, aDD22_i2p4, invdxx2);
        const REAL_SIMD_ARRAY cf_dD0 = SIMD_fd_function_dD0_fdorder8(cf_i0m1, cf_i0m2, cf_i0m3, cf_i0m4, cf_i0p1, cf_i0p2, cf_i0p3, cf_i0p4, invdxx0);
        const REAL_SIMD_ARRAY cf_dD1 = SIMD_fd_function_dD1_fdorder8(cf_i1m1, cf_i1m2, cf_i1m3, cf_i1m4, cf_i1p1, cf_i1p2, cf_i1p3, cf_i1p4, invdxx1);
        const REAL_SIMD_ARRAY cf_dD2 = SIMD_fd_function_dD2_fdorder8(cf_i2m1, cf_i2m2, cf_i2m3, cf_i2m4, cf_i2p1, cf_i2p2, cf_i2p3, cf_i2p4, invdxx2);
        const REAL_SIMD_ARRAY cf_dDD00 =
            SIMD_fd_function_dDD00_fdorder8(cf, cf_i0m1, cf_i0m2, cf_i0m3, cf_i0m4, cf_i0p1, cf_i0p2, cf_i0p3, cf_i0p4, invdxx0);
        const REAL_SIMD_ARRAY cf_dDD01 = SIMD_fd_function_dDD01_fdorder8(
            cf_i0m1_i1m1, cf_i0m1_i1m2, cf_i0m1_i1m3, cf_i0m1_i1m4, cf_i0m1_i1p1, cf_i0m1_i1p2, cf_i0m1_i1p3, cf_i0m1_i1p4, cf_i0m2_i1m1,
            cf_i0m2_i1m2, cf_i0m2_i1m3, cf_i0m2_i1m4, cf_i0m2_i1p1, cf_i0m2_i1p2, cf_i0m2_i1p3, cf_i0m2_i1p4, cf_i0m3_i1m1, cf_i0m3_i1m2,
            cf_i0m3_i1m3, cf_i0m3_i1m4, cf_i0m3_i1p1, cf_i0m3_i1p2, cf_i0m3_i1p3, cf_i0m3_i1p4, cf_i0m4_i1m1, cf_i0m4_i1m2, cf_i0m4_i1m3,
            cf_i0m4_i1m4, cf_i0m4_i1p1, cf_i0m4_i1p2, cf_i0m4_i1p3, cf_i0m4_i1p4, cf_i0p1_i1m1, cf_i0p1_i1m2, cf_i0p1_i1m3, cf_i0p1_i1m4,
            cf_i0p1_i1p1, cf_i0p1_i1p2, cf_i0p1_i1p3, cf_i0p1_i1p4, cf_i0p2_i1m1, cf_i0p2_i1m2, cf_i0p2_i1m3, cf_i0p2_i1m4, cf_i0p2_i1p1,
            cf_i0p2_i1p2, cf_i0p2_i1p3, cf_i0p2_i1p4, cf_i0p3_i1m1, cf_i0p3_i1m2, cf_i0p3_i1m3, cf_i0p3_i1m4, cf_i0p3_i1p1, cf_i0p3_i1p2,
            cf_i0p3_i1p3, cf_i0p3_i1p4, cf_i0p4_i1m1, cf_i0p4_i1m2, cf_i0p4_i1m3, cf_i0p4_i1m4, cf_i0p4_i1p1, cf_i0p4_i1p2, cf_i0p4_i1p3,
            cf_i0p4_i1p4, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY cf_dDD02 = SIMD_fd_function_dDD02_fdorder8(
            cf_i0m1_i2m1, cf_i0m1_i2m2, cf_i0m1_i2m3, cf_i0m1_i2m4, cf_i0m1_i2p1, cf_i0m1_i2p2, cf_i0m1_i2p3, cf_i0m1_i2p4, cf_i0m2_i2m1,
            cf_i0m2_i2m2, cf_i0m2_i2m3, cf_i0m2_i2m4, cf_i0m2_i2p1, cf_i0m2_i2p2, cf_i0m2_i2p3, cf_i0m2_i2p4, cf_i0m3_i2m1, cf_i0m3_i2m2,
            cf_i0m3_i2m3, cf_i0m3_i2m4, cf_i0m3_i2p1, cf_i0m3_i2p2, cf_i0m3_i2p3, cf_i0m3_i2p4, cf_i0m4_i2m1, cf_i0m4_i2m2, cf_i0m4_i2m3,
            cf_i0m4_i2m4, cf_i0m4_i2p1, cf_i0m4_i2p2, cf_i0m4_i2p3, cf_i0m4_i2p4, cf_i0p1_i2m1, cf_i0p1_i2m2, cf_i0p1_i2m3, cf_i0p1_i2m4,
            cf_i0p1_i2p1, cf_i0p1_i2p2, cf_i0p1_i2p3, cf_i0p1_i2p4, cf_i0p2_i2m1, cf_i0p2_i2m2, cf_i0p2_i2m3, cf_i0p2_i2m4, cf_i0p2_i2p1,
            cf_i0p2_i2p2, cf_i0p2_i2p3, cf_i0p2_i2p4, cf_i0p3_i2m1, cf_i0p3_i2m2, cf_i0p3_i2m3, cf_i0p3_i2m4, cf_i0p3_i2p1, cf_i0p3_i2p2,
            cf_i0p3_i2p3, cf_i0p3_i2p4, cf_i0p4_i2m1, cf_i0p4_i2m2, cf_i0p4_i2m3, cf_i0p4_i2m4, cf_i0p4_i2p1, cf_i0p4_i2p2, cf_i0p4_i2p3,
            cf_i0p4_i2p4, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY cf_dDD11 =
            SIMD_fd_function_dDD11_fdorder8(cf, cf_i1m1, cf_i1m2, cf_i1m3, cf_i1m4, cf_i1p1, cf_i1p2, cf_i1p3, cf_i1p4, invdxx1);
        const REAL_SIMD_ARRAY cf_dDD12 = SIMD_fd_function_dDD12_fdorder8(
            cf_i1m1_i2m1, cf_i1m1_i2m2, cf_i1m1_i2m3, cf_i1m1_i2m4, cf_i1m1_i2p1, cf_i1m1_i2p2, cf_i1m1_i2p3, cf_i1m1_i2p4, cf_i1m2_i2m1,
            cf_i1m2_i2m2, cf_i1m2_i2m3, cf_i1m2_i2m4, cf_i1m2_i2p1, cf_i1m2_i2p2, cf_i1m2_i2p3, cf_i1m2_i2p4, cf_i1m3_i2m1, cf_i1m3_i2m2,
            cf_i1m3_i2m3, cf_i1m3_i2m4, cf_i1m3_i2p1, cf_i1m3_i2p2, cf_i1m3_i2p3, cf_i1m3_i2p4, cf_i1m4_i2m1, cf_i1m4_i2m2, cf_i1m4_i2m3,
            cf_i1m4_i2m4, cf_i1m4_i2p1, cf_i1m4_i2p2, cf_i1m4_i2p3, cf_i1m4_i2p4, cf_i1p1_i2m1, cf_i1p1_i2m2, cf_i1p1_i2m3, cf_i1p1_i2m4,
            cf_i1p1_i2p1, cf_i1p1_i2p2, cf_i1p1_i2p3, cf_i1p1_i2p4, cf_i1p2_i2m1, cf_i1p2_i2m2, cf_i1p2_i2m3, cf_i1p2_i2m4, cf_i1p2_i2p1,
            cf_i1p2_i2p2, cf_i1p2_i2p3, cf_i1p2_i2p4, cf_i1p3_i2m1, cf_i1p3_i2m2, cf_i1p3_i2m3, cf_i1p3_i2m4, cf_i1p3_i2p1, cf_i1p3_i2p2,
            cf_i1p3_i2p3, cf_i1p3_i2p4, cf_i1p4_i2m1, cf_i1p4_i2m2, cf_i1p4_i2m3, cf_i1p4_i2m4, cf_i1p4_i2p1, cf_i1p4_i2p2, cf_i1p4_i2p3,
            cf_i1p4_i2p4, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY cf_dDD22 =
            SIMD_fd_function_dDD22_fdorder8(cf, cf_i2m1, cf_i2m2, cf_i2m3, cf_i2m4, cf_i2p1, cf_i2p2, cf_i2p3, cf_i2p4, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD000 =
            SIMD_fd_function_dD0_fdorder8(hDD00_i0m1, hDD00_i0m2, hDD00_i0m3, hDD00_i0m4, hDD00_i0p1, hDD00_i0p2, hDD00_i0p3, hDD00_i0p4, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD001 =
            SIMD_fd_function_dD1_fdorder8(hDD00_i1m1, hDD00_i1m2, hDD00_i1m3, hDD00_i1m4, hDD00_i1p1, hDD00_i1p2, hDD00_i1p3, hDD00_i1p4, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD002 =
            SIMD_fd_function_dD2_fdorder8(hDD00_i2m1, hDD00_i2m2, hDD00_i2m3, hDD00_i2m4, hDD00_i2p1, hDD00_i2p2, hDD00_i2p3, hDD00_i2p4, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD010 =
            SIMD_fd_function_dD0_fdorder8(hDD01_i0m1, hDD01_i0m2, hDD01_i0m3, hDD01_i0m4, hDD01_i0p1, hDD01_i0p2, hDD01_i0p3, hDD01_i0p4, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD011 =
            SIMD_fd_function_dD1_fdorder8(hDD01_i1m1, hDD01_i1m2, hDD01_i1m3, hDD01_i1m4, hDD01_i1p1, hDD01_i1p2, hDD01_i1p3, hDD01_i1p4, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD012 =
            SIMD_fd_function_dD2_fdorder8(hDD01_i2m1, hDD01_i2m2, hDD01_i2m3, hDD01_i2m4, hDD01_i2p1, hDD01_i2p2, hDD01_i2p3, hDD01_i2p4, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD020 =
            SIMD_fd_function_dD0_fdorder8(hDD02_i0m1, hDD02_i0m2, hDD02_i0m3, hDD02_i0m4, hDD02_i0p1, hDD02_i0p2, hDD02_i0p3, hDD02_i0p4, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD021 =
            SIMD_fd_function_dD1_fdorder8(hDD02_i1m1, hDD02_i1m2, hDD02_i1m3, hDD02_i1m4, hDD02_i1p1, hDD02_i1p2, hDD02_i1p3, hDD02_i1p4, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD022 =
            SIMD_fd_function_dD2_fdorder8(hDD02_i2m1, hDD02_i2m2, hDD02_i2m3, hDD02_i2m4, hDD02_i2p1, hDD02_i2p2, hDD02_i2p3, hDD02_i2p4, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD110 =
            SIMD_fd_function_dD0_fdorder8(hDD11_i0m1, hDD11_i0m2, hDD11_i0m3, hDD11_i0m4, hDD11_i0p1, hDD11_i0p2, hDD11_i0p3, hDD11_i0p4, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD111 =
            SIMD_fd_function_dD1_fdorder8(hDD11_i1m1, hDD11_i1m2, hDD11_i1m3, hDD11_i1m4, hDD11_i1p1, hDD11_i1p2, hDD11_i1p3, hDD11_i1p4, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD112 =
            SIMD_fd_function_dD2_fdorder8(hDD11_i2m1, hDD11_i2m2, hDD11_i2m3, hDD11_i2m4, hDD11_i2p1, hDD11_i2p2, hDD11_i2p3, hDD11_i2p4, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD120 =
            SIMD_fd_function_dD0_fdorder8(hDD12_i0m1, hDD12_i0m2, hDD12_i0m3, hDD12_i0m4, hDD12_i0p1, hDD12_i0p2, hDD12_i0p3, hDD12_i0p4, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD121 =
            SIMD_fd_function_dD1_fdorder8(hDD12_i1m1, hDD12_i1m2, hDD12_i1m3, hDD12_i1m4, hDD12_i1p1, hDD12_i1p2, hDD12_i1p3, hDD12_i1p4, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD122 =
            SIMD_fd_function_dD2_fdorder8(hDD12_i2m1, hDD12_i2m2, hDD12_i2m3, hDD12_i2m4, hDD12_i2p1, hDD12_i2p2, hDD12_i2p3, hDD12_i2p4, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD220 =
            SIMD_fd_function_dD0_fdorder8(hDD22_i0m1, hDD22_i0m2, hDD22_i0m3, hDD22_i0m4, hDD22_i0p1, hDD22_i0p2, hDD22_i0p3, hDD22_i0p4, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD221 =
            SIMD_fd_function_dD1_fdorder8(hDD22_i1m1, hDD22_i1m2, hDD22_i1m3, hDD22_i1m4, hDD22_i1p1, hDD22_i1p2, hDD22_i1p3, hDD22_i1p4, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD222 =
            SIMD_fd_function_dD2_fdorder8(hDD22_i2m1, hDD22_i2m2, hDD22_i2m3, hDD22_i2m4, hDD22_i2p1, hDD22_i2p2, hDD22_i2p3, hDD22_i2p4, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0000 = SIMD_fd_function_dDD00_fdorder8(hDD00, hDD00_i0m1, hDD00_i0m2, hDD00_i0m3, hDD00_i0m4, hDD00_i0p1,
                                                                            hDD00_i0p2, hDD00_i0p3, hDD00_i0p4, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD0001 = SIMD_fd_function_dDD01_fdorder8(
            hDD00_i0m1_i1m1, hDD00_i0m1_i1m2, hDD00_i0m1_i1m3, hDD00_i0m1_i1m4, hDD00_i0m1_i1p1, hDD00_i0m1_i1p2, hDD00_i0m1_i1p3, hDD00_i0m1_i1p4,
            hDD00_i0m2_i1m1, hDD00_i0m2_i1m2, hDD00_i0m2_i1m3, hDD00_i0m2_i1m4, hDD00_i0m2_i1p1, hDD00_i0m2_i1p2, hDD00_i0m2_i1p3, hDD00_i0m2_i1p4,
            hDD00_i0m3_i1m1, hDD00_i0m3_i1m2, hDD00_i0m3_i1m3, hDD00_i0m3_i1m4, hDD00_i0m3_i1p1, hDD00_i0m3_i1p2, hDD00_i0m3_i1p3, hDD00_i0m3_i1p4,
            hDD00_i0m4_i1m1, hDD00_i0m4_i1m2, hDD00_i0m4_i1m3, hDD00_i0m4_i1m4, hDD00_i0m4_i1p1, hDD00_i0m4_i1p2, hDD00_i0m4_i1p3, hDD00_i0m4_i1p4,
            hDD00_i0p1_i1m1, hDD00_i0p1_i1m2, hDD00_i0p1_i1m3, hDD00_i0p1_i1m4, hDD00_i0p1_i1p1, hDD00_i0p1_i1p2, hDD00_i0p1_i1p3, hDD00_i0p1_i1p4,
            hDD00_i0p2_i1m1, hDD00_i0p2_i1m2, hDD00_i0p2_i1m3, hDD00_i0p2_i1m4, hDD00_i0p2_i1p1, hDD00_i0p2_i1p2, hDD00_i0p2_i1p3, hDD00_i0p2_i1p4,
            hDD00_i0p3_i1m1, hDD00_i0p3_i1m2, hDD00_i0p3_i1m3, hDD00_i0p3_i1m4, hDD00_i0p3_i1p1, hDD00_i0p3_i1p2, hDD00_i0p3_i1p3, hDD00_i0p3_i1p4,
            hDD00_i0p4_i1m1, hDD00_i0p4_i1m2, hDD00_i0p4_i1m3, hDD00_i0p4_i1m4, hDD00_i0p4_i1p1, hDD00_i0p4_i1p2, hDD00_i0p4_i1p3, hDD00_i0p4_i1p4,
            invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0002 = SIMD_fd_function_dDD02_fdorder8(
            hDD00_i0m1_i2m1, hDD00_i0m1_i2m2, hDD00_i0m1_i2m3, hDD00_i0m1_i2m4, hDD00_i0m1_i2p1, hDD00_i0m1_i2p2, hDD00_i0m1_i2p3, hDD00_i0m1_i2p4,
            hDD00_i0m2_i2m1, hDD00_i0m2_i2m2, hDD00_i0m2_i2m3, hDD00_i0m2_i2m4, hDD00_i0m2_i2p1, hDD00_i0m2_i2p2, hDD00_i0m2_i2p3, hDD00_i0m2_i2p4,
            hDD00_i0m3_i2m1, hDD00_i0m3_i2m2, hDD00_i0m3_i2m3, hDD00_i0m3_i2m4, hDD00_i0m3_i2p1, hDD00_i0m3_i2p2, hDD00_i0m3_i2p3, hDD00_i0m3_i2p4,
            hDD00_i0m4_i2m1, hDD00_i0m4_i2m2, hDD00_i0m4_i2m3, hDD00_i0m4_i2m4, hDD00_i0m4_i2p1, hDD00_i0m4_i2p2, hDD00_i0m4_i2p3, hDD00_i0m4_i2p4,
            hDD00_i0p1_i2m1, hDD00_i0p1_i2m2, hDD00_i0p1_i2m3, hDD00_i0p1_i2m4, hDD00_i0p1_i2p1, hDD00_i0p1_i2p2, hDD00_i0p1_i2p3, hDD00_i0p1_i2p4,
            hDD00_i0p2_i2m1, hDD00_i0p2_i2m2, hDD00_i0p2_i2m3, hDD00_i0p2_i2m4, hDD00_i0p2_i2p1, hDD00_i0p2_i2p2, hDD00_i0p2_i2p3, hDD00_i0p2_i2p4,
            hDD00_i0p3_i2m1, hDD00_i0p3_i2m2, hDD00_i0p3_i2m3, hDD00_i0p3_i2m4, hDD00_i0p3_i2p1, hDD00_i0p3_i2p2, hDD00_i0p3_i2p3, hDD00_i0p3_i2p4,
            hDD00_i0p4_i2m1, hDD00_i0p4_i2m2, hDD00_i0p4_i2m3, hDD00_i0p4_i2m4, hDD00_i0p4_i2p1, hDD00_i0p4_i2p2, hDD00_i0p4_i2p3, hDD00_i0p4_i2p4,
            invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0011 = SIMD_fd_function_dDD11_fdorder8(hDD00, hDD00_i1m1, hDD00_i1m2, hDD00_i1m3, hDD00_i1m4, hDD00_i1p1,
                                                                            hDD00_i1p2, hDD00_i1p3, hDD00_i1p4, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0012 = SIMD_fd_function_dDD12_fdorder8(
            hDD00_i1m1_i2m1, hDD00_i1m1_i2m2, hDD00_i1m1_i2m3, hDD00_i1m1_i2m4, hDD00_i1m1_i2p1, hDD00_i1m1_i2p2, hDD00_i1m1_i2p3, hDD00_i1m1_i2p4,
            hDD00_i1m2_i2m1, hDD00_i1m2_i2m2, hDD00_i1m2_i2m3, hDD00_i1m2_i2m4, hDD00_i1m2_i2p1, hDD00_i1m2_i2p2, hDD00_i1m2_i2p3, hDD00_i1m2_i2p4,
            hDD00_i1m3_i2m1, hDD00_i1m3_i2m2, hDD00_i1m3_i2m3, hDD00_i1m3_i2m4, hDD00_i1m3_i2p1, hDD00_i1m3_i2p2, hDD00_i1m3_i2p3, hDD00_i1m3_i2p4,
            hDD00_i1m4_i2m1, hDD00_i1m4_i2m2, hDD00_i1m4_i2m3, hDD00_i1m4_i2m4, hDD00_i1m4_i2p1, hDD00_i1m4_i2p2, hDD00_i1m4_i2p3, hDD00_i1m4_i2p4,
            hDD00_i1p1_i2m1, hDD00_i1p1_i2m2, hDD00_i1p1_i2m3, hDD00_i1p1_i2m4, hDD00_i1p1_i2p1, hDD00_i1p1_i2p2, hDD00_i1p1_i2p3, hDD00_i1p1_i2p4,
            hDD00_i1p2_i2m1, hDD00_i1p2_i2m2, hDD00_i1p2_i2m3, hDD00_i1p2_i2m4, hDD00_i1p2_i2p1, hDD00_i1p2_i2p2, hDD00_i1p2_i2p3, hDD00_i1p2_i2p4,
            hDD00_i1p3_i2m1, hDD00_i1p3_i2m2, hDD00_i1p3_i2m3, hDD00_i1p3_i2m4, hDD00_i1p3_i2p1, hDD00_i1p3_i2p2, hDD00_i1p3_i2p3, hDD00_i1p3_i2p4,
            hDD00_i1p4_i2m1, hDD00_i1p4_i2m2, hDD00_i1p4_i2m3, hDD00_i1p4_i2m4, hDD00_i1p4_i2p1, hDD00_i1p4_i2p2, hDD00_i1p4_i2p3, hDD00_i1p4_i2p4,
            invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0022 = SIMD_fd_function_dDD22_fdorder8(hDD00, hDD00_i2m1, hDD00_i2m2, hDD00_i2m3, hDD00_i2m4, hDD00_i2p1,
                                                                            hDD00_i2p2, hDD00_i2p3, hDD00_i2p4, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0100 = SIMD_fd_function_dDD00_fdorder8(hDD01, hDD01_i0m1, hDD01_i0m2, hDD01_i0m3, hDD01_i0m4, hDD01_i0p1,
                                                                            hDD01_i0p2, hDD01_i0p3, hDD01_i0p4, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD0101 = SIMD_fd_function_dDD01_fdorder8(
            hDD01_i0m1_i1m1, hDD01_i0m1_i1m2, hDD01_i0m1_i1m3, hDD01_i0m1_i1m4, hDD01_i0m1_i1p1, hDD01_i0m1_i1p2, hDD01_i0m1_i1p3, hDD01_i0m1_i1p4,
            hDD01_i0m2_i1m1, hDD01_i0m2_i1m2, hDD01_i0m2_i1m3, hDD01_i0m2_i1m4, hDD01_i0m2_i1p1, hDD01_i0m2_i1p2, hDD01_i0m2_i1p3, hDD01_i0m2_i1p4,
            hDD01_i0m3_i1m1, hDD01_i0m3_i1m2, hDD01_i0m3_i1m3, hDD01_i0m3_i1m4, hDD01_i0m3_i1p1, hDD01_i0m3_i1p2, hDD01_i0m3_i1p3, hDD01_i0m3_i1p4,
            hDD01_i0m4_i1m1, hDD01_i0m4_i1m2, hDD01_i0m4_i1m3, hDD01_i0m4_i1m4, hDD01_i0m4_i1p1, hDD01_i0m4_i1p2, hDD01_i0m4_i1p3, hDD01_i0m4_i1p4,
            hDD01_i0p1_i1m1, hDD01_i0p1_i1m2, hDD01_i0p1_i1m3, hDD01_i0p1_i1m4, hDD01_i0p1_i1p1, hDD01_i0p1_i1p2, hDD01_i0p1_i1p3, hDD01_i0p1_i1p4,
            hDD01_i0p2_i1m1, hDD01_i0p2_i1m2, hDD01_i0p2_i1m3, hDD01_i0p2_i1m4, hDD01_i0p2_i1p1, hDD01_i0p2_i1p2, hDD01_i0p2_i1p3, hDD01_i0p2_i1p4,
            hDD01_i0p3_i1m1, hDD01_i0p3_i1m2, hDD01_i0p3_i1m3, hDD01_i0p3_i1m4, hDD01_i0p3_i1p1, hDD01_i0p3_i1p2, hDD01_i0p3_i1p3, hDD01_i0p3_i1p4,
            hDD01_i0p4_i1m1, hDD01_i0p4_i1m2, hDD01_i0p4_i1m3, hDD01_i0p4_i1m4, hDD01_i0p4_i1p1, hDD01_i0p4_i1p2, hDD01_i0p4_i1p3, hDD01_i0p4_i1p4,
            invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0102 = SIMD_fd_function_dDD02_fdorder8(
            hDD01_i0m1_i2m1, hDD01_i0m1_i2m2, hDD01_i0m1_i2m3, hDD01_i0m1_i2m4, hDD01_i0m1_i2p1, hDD01_i0m1_i2p2, hDD01_i0m1_i2p3, hDD01_i0m1_i2p4,
            hDD01_i0m2_i2m1, hDD01_i0m2_i2m2, hDD01_i0m2_i2m3, hDD01_i0m2_i2m4, hDD01_i0m2_i2p1, hDD01_i0m2_i2p2, hDD01_i0m2_i2p3, hDD01_i0m2_i2p4,
            hDD01_i0m3_i2m1, hDD01_i0m3_i2m2, hDD01_i0m3_i2m3, hDD01_i0m3_i2m4, hDD01_i0m3_i2p1, hDD01_i0m3_i2p2, hDD01_i0m3_i2p3, hDD01_i0m3_i2p4,
            hDD01_i0m4_i2m1, hDD01_i0m4_i2m2, hDD01_i0m4_i2m3, hDD01_i0m4_i2m4, hDD01_i0m4_i2p1, hDD01_i0m4_i2p2, hDD01_i0m4_i2p3, hDD01_i0m4_i2p4,
            hDD01_i0p1_i2m1, hDD01_i0p1_i2m2, hDD01_i0p1_i2m3, hDD01_i0p1_i2m4, hDD01_i0p1_i2p1, hDD01_i0p1_i2p2, hDD01_i0p1_i2p3, hDD01_i0p1_i2p4,
            hDD01_i0p2_i2m1, hDD01_i0p2_i2m2, hDD01_i0p2_i2m3, hDD01_i0p2_i2m4, hDD01_i0p2_i2p1, hDD01_i0p2_i2p2, hDD01_i0p2_i2p3, hDD01_i0p2_i2p4,
            hDD01_i0p3_i2m1, hDD01_i0p3_i2m2, hDD01_i0p3_i2m3, hDD01_i0p3_i2m4, hDD01_i0p3_i2p1, hDD01_i0p3_i2p2, hDD01_i0p3_i2p3, hDD01_i0p3_i2p4,
            hDD01_i0p4_i2m1, hDD01_i0p4_i2m2, hDD01_i0p4_i2m3, hDD01_i0p4_i2m4, hDD01_i0p4_i2p1, hDD01_i0p4_i2p2, hDD01_i0p4_i2p3, hDD01_i0p4_i2p4,
            invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0111 = SIMD_fd_function_dDD11_fdorder8(hDD01, hDD01_i1m1, hDD01_i1m2, hDD01_i1m3, hDD01_i1m4, hDD01_i1p1,
                                                                            hDD01_i1p2, hDD01_i1p3, hDD01_i1p4, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0112 = SIMD_fd_function_dDD12_fdorder8(
            hDD01_i1m1_i2m1, hDD01_i1m1_i2m2, hDD01_i1m1_i2m3, hDD01_i1m1_i2m4, hDD01_i1m1_i2p1, hDD01_i1m1_i2p2, hDD01_i1m1_i2p3, hDD01_i1m1_i2p4,
            hDD01_i1m2_i2m1, hDD01_i1m2_i2m2, hDD01_i1m2_i2m3, hDD01_i1m2_i2m4, hDD01_i1m2_i2p1, hDD01_i1m2_i2p2, hDD01_i1m2_i2p3, hDD01_i1m2_i2p4,
            hDD01_i1m3_i2m1, hDD01_i1m3_i2m2, hDD01_i1m3_i2m3, hDD01_i1m3_i2m4, hDD01_i1m3_i2p1, hDD01_i1m3_i2p2, hDD01_i1m3_i2p3, hDD01_i1m3_i2p4,
            hDD01_i1m4_i2m1, hDD01_i1m4_i2m2, hDD01_i1m4_i2m3, hDD01_i1m4_i2m4, hDD01_i1m4_i2p1, hDD01_i1m4_i2p2, hDD01_i1m4_i2p3, hDD01_i1m4_i2p4,
            hDD01_i1p1_i2m1, hDD01_i1p1_i2m2, hDD01_i1p1_i2m3, hDD01_i1p1_i2m4, hDD01_i1p1_i2p1, hDD01_i1p1_i2p2, hDD01_i1p1_i2p3, hDD01_i1p1_i2p4,
            hDD01_i1p2_i2m1, hDD01_i1p2_i2m2, hDD01_i1p2_i2m3, hDD01_i1p2_i2m4, hDD01_i1p2_i2p1, hDD01_i1p2_i2p2, hDD01_i1p2_i2p3, hDD01_i1p2_i2p4,
            hDD01_i1p3_i2m1, hDD01_i1p3_i2m2, hDD01_i1p3_i2m3, hDD01_i1p3_i2m4, hDD01_i1p3_i2p1, hDD01_i1p3_i2p2, hDD01_i1p3_i2p3, hDD01_i1p3_i2p4,
            hDD01_i1p4_i2m1, hDD01_i1p4_i2m2, hDD01_i1p4_i2m3, hDD01_i1p4_i2m4, hDD01_i1p4_i2p1, hDD01_i1p4_i2p2, hDD01_i1p4_i2p3, hDD01_i1p4_i2p4,
            invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0122 = SIMD_fd_function_dDD22_fdorder8(hDD01, hDD01_i2m1, hDD01_i2m2, hDD01_i2m3, hDD01_i2m4, hDD01_i2p1,
                                                                            hDD01_i2p2, hDD01_i2p3, hDD01_i2p4, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0200 = SIMD_fd_function_dDD00_fdorder8(hDD02, hDD02_i0m1, hDD02_i0m2, hDD02_i0m3, hDD02_i0m4, hDD02_i0p1,
                                                                            hDD02_i0p2, hDD02_i0p3, hDD02_i0p4, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD0201 = SIMD_fd_function_dDD01_fdorder8(
            hDD02_i0m1_i1m1, hDD02_i0m1_i1m2, hDD02_i0m1_i1m3, hDD02_i0m1_i1m4, hDD02_i0m1_i1p1, hDD02_i0m1_i1p2, hDD02_i0m1_i1p3, hDD02_i0m1_i1p4,
            hDD02_i0m2_i1m1, hDD02_i0m2_i1m2, hDD02_i0m2_i1m3, hDD02_i0m2_i1m4, hDD02_i0m2_i1p1, hDD02_i0m2_i1p2, hDD02_i0m2_i1p3, hDD02_i0m2_i1p4,
            hDD02_i0m3_i1m1, hDD02_i0m3_i1m2, hDD02_i0m3_i1m3, hDD02_i0m3_i1m4, hDD02_i0m3_i1p1, hDD02_i0m3_i1p2, hDD02_i0m3_i1p3, hDD02_i0m3_i1p4,
            hDD02_i0m4_i1m1, hDD02_i0m4_i1m2, hDD02_i0m4_i1m3, hDD02_i0m4_i1m4, hDD02_i0m4_i1p1, hDD02_i0m4_i1p2, hDD02_i0m4_i1p3, hDD02_i0m4_i1p4,
            hDD02_i0p1_i1m1, hDD02_i0p1_i1m2, hDD02_i0p1_i1m3, hDD02_i0p1_i1m4, hDD02_i0p1_i1p1, hDD02_i0p1_i1p2, hDD02_i0p1_i1p3, hDD02_i0p1_i1p4,
            hDD02_i0p2_i1m1, hDD02_i0p2_i1m2, hDD02_i0p2_i1m3, hDD02_i0p2_i1m4, hDD02_i0p2_i1p1, hDD02_i0p2_i1p2, hDD02_i0p2_i1p3, hDD02_i0p2_i1p4,
            hDD02_i0p3_i1m1, hDD02_i0p3_i1m2, hDD02_i0p3_i1m3, hDD02_i0p3_i1m4, hDD02_i0p3_i1p1, hDD02_i0p3_i1p2, hDD02_i0p3_i1p3, hDD02_i0p3_i1p4,
            hDD02_i0p4_i1m1, hDD02_i0p4_i1m2, hDD02_i0p4_i1m3, hDD02_i0p4_i1m4, hDD02_i0p4_i1p1, hDD02_i0p4_i1p2, hDD02_i0p4_i1p3, hDD02_i0p4_i1p4,
            invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0202 = SIMD_fd_function_dDD02_fdorder8(
            hDD02_i0m1_i2m1, hDD02_i0m1_i2m2, hDD02_i0m1_i2m3, hDD02_i0m1_i2m4, hDD02_i0m1_i2p1, hDD02_i0m1_i2p2, hDD02_i0m1_i2p3, hDD02_i0m1_i2p4,
            hDD02_i0m2_i2m1, hDD02_i0m2_i2m2, hDD02_i0m2_i2m3, hDD02_i0m2_i2m4, hDD02_i0m2_i2p1, hDD02_i0m2_i2p2, hDD02_i0m2_i2p3, hDD02_i0m2_i2p4,
            hDD02_i0m3_i2m1, hDD02_i0m3_i2m2, hDD02_i0m3_i2m3, hDD02_i0m3_i2m4, hDD02_i0m3_i2p1, hDD02_i0m3_i2p2, hDD02_i0m3_i2p3, hDD02_i0m3_i2p4,
            hDD02_i0m4_i2m1, hDD02_i0m4_i2m2, hDD02_i0m4_i2m3, hDD02_i0m4_i2m4, hDD02_i0m4_i2p1, hDD02_i0m4_i2p2, hDD02_i0m4_i2p3, hDD02_i0m4_i2p4,
            hDD02_i0p1_i2m1, hDD02_i0p1_i2m2, hDD02_i0p1_i2m3, hDD02_i0p1_i2m4, hDD02_i0p1_i2p1, hDD02_i0p1_i2p2, hDD02_i0p1_i2p3, hDD02_i0p1_i2p4,
            hDD02_i0p2_i2m1, hDD02_i0p2_i2m2, hDD02_i0p2_i2m3, hDD02_i0p2_i2m4, hDD02_i0p2_i2p1, hDD02_i0p2_i2p2, hDD02_i0p2_i2p3, hDD02_i0p2_i2p4,
            hDD02_i0p3_i2m1, hDD02_i0p3_i2m2, hDD02_i0p3_i2m3, hDD02_i0p3_i2m4, hDD02_i0p3_i2p1, hDD02_i0p3_i2p2, hDD02_i0p3_i2p3, hDD02_i0p3_i2p4,
            hDD02_i0p4_i2m1, hDD02_i0p4_i2m2, hDD02_i0p4_i2m3, hDD02_i0p4_i2m4, hDD02_i0p4_i2p1, hDD02_i0p4_i2p2, hDD02_i0p4_i2p3, hDD02_i0p4_i2p4,
            invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0211 = SIMD_fd_function_dDD11_fdorder8(hDD02, hDD02_i1m1, hDD02_i1m2, hDD02_i1m3, hDD02_i1m4, hDD02_i1p1,
                                                                            hDD02_i1p2, hDD02_i1p3, hDD02_i1p4, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0212 = SIMD_fd_function_dDD12_fdorder8(
            hDD02_i1m1_i2m1, hDD02_i1m1_i2m2, hDD02_i1m1_i2m3, hDD02_i1m1_i2m4, hDD02_i1m1_i2p1, hDD02_i1m1_i2p2, hDD02_i1m1_i2p3, hDD02_i1m1_i2p4,
            hDD02_i1m2_i2m1, hDD02_i1m2_i2m2, hDD02_i1m2_i2m3, hDD02_i1m2_i2m4, hDD02_i1m2_i2p1, hDD02_i1m2_i2p2, hDD02_i1m2_i2p3, hDD02_i1m2_i2p4,
            hDD02_i1m3_i2m1, hDD02_i1m3_i2m2, hDD02_i1m3_i2m3, hDD02_i1m3_i2m4, hDD02_i1m3_i2p1, hDD02_i1m3_i2p2, hDD02_i1m3_i2p3, hDD02_i1m3_i2p4,
            hDD02_i1m4_i2m1, hDD02_i1m4_i2m2, hDD02_i1m4_i2m3, hDD02_i1m4_i2m4, hDD02_i1m4_i2p1, hDD02_i1m4_i2p2, hDD02_i1m4_i2p3, hDD02_i1m4_i2p4,
            hDD02_i1p1_i2m1, hDD02_i1p1_i2m2, hDD02_i1p1_i2m3, hDD02_i1p1_i2m4, hDD02_i1p1_i2p1, hDD02_i1p1_i2p2, hDD02_i1p1_i2p3, hDD02_i1p1_i2p4,
            hDD02_i1p2_i2m1, hDD02_i1p2_i2m2, hDD02_i1p2_i2m3, hDD02_i1p2_i2m4, hDD02_i1p2_i2p1, hDD02_i1p2_i2p2, hDD02_i1p2_i2p3, hDD02_i1p2_i2p4,
            hDD02_i1p3_i2m1, hDD02_i1p3_i2m2, hDD02_i1p3_i2m3, hDD02_i1p3_i2m4, hDD02_i1p3_i2p1, hDD02_i1p3_i2p2, hDD02_i1p3_i2p3, hDD02_i1p3_i2p4,
            hDD02_i1p4_i2m1, hDD02_i1p4_i2m2, hDD02_i1p4_i2m3, hDD02_i1p4_i2m4, hDD02_i1p4_i2p1, hDD02_i1p4_i2p2, hDD02_i1p4_i2p3, hDD02_i1p4_i2p4,
            invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0222 = SIMD_fd_function_dDD22_fdorder8(hDD02, hDD02_i2m1, hDD02_i2m2, hDD02_i2m3, hDD02_i2m4, hDD02_i2p1,
                                                                            hDD02_i2p2, hDD02_i2p3, hDD02_i2p4, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1100 = SIMD_fd_function_dDD00_fdorder8(hDD11, hDD11_i0m1, hDD11_i0m2, hDD11_i0m3, hDD11_i0m4, hDD11_i0p1,
                                                                            hDD11_i0p2, hDD11_i0p3, hDD11_i0p4, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD1101 = SIMD_fd_function_dDD01_fdorder8(
            hDD11_i0m1_i1m1, hDD11_i0m1_i1m2, hDD11_i0m1_i1m3, hDD11_i0m1_i1m4, hDD11_i0m1_i1p1, hDD11_i0m1_i1p2, hDD11_i0m1_i1p3, hDD11_i0m1_i1p4,
            hDD11_i0m2_i1m1, hDD11_i0m2_i1m2, hDD11_i0m2_i1m3, hDD11_i0m2_i1m4, hDD11_i0m2_i1p1, hDD11_i0m2_i1p2, hDD11_i0m2_i1p3, hDD11_i0m2_i1p4,
            hDD11_i0m3_i1m1, hDD11_i0m3_i1m2, hDD11_i0m3_i1m3, hDD11_i0m3_i1m4, hDD11_i0m3_i1p1, hDD11_i0m3_i1p2, hDD11_i0m3_i1p3, hDD11_i0m3_i1p4,
            hDD11_i0m4_i1m1, hDD11_i0m4_i1m2, hDD11_i0m4_i1m3, hDD11_i0m4_i1m4, hDD11_i0m4_i1p1, hDD11_i0m4_i1p2, hDD11_i0m4_i1p3, hDD11_i0m4_i1p4,
            hDD11_i0p1_i1m1, hDD11_i0p1_i1m2, hDD11_i0p1_i1m3, hDD11_i0p1_i1m4, hDD11_i0p1_i1p1, hDD11_i0p1_i1p2, hDD11_i0p1_i1p3, hDD11_i0p1_i1p4,
            hDD11_i0p2_i1m1, hDD11_i0p2_i1m2, hDD11_i0p2_i1m3, hDD11_i0p2_i1m4, hDD11_i0p2_i1p1, hDD11_i0p2_i1p2, hDD11_i0p2_i1p3, hDD11_i0p2_i1p4,
            hDD11_i0p3_i1m1, hDD11_i0p3_i1m2, hDD11_i0p3_i1m3, hDD11_i0p3_i1m4, hDD11_i0p3_i1p1, hDD11_i0p3_i1p2, hDD11_i0p3_i1p3, hDD11_i0p3_i1p4,
            hDD11_i0p4_i1m1, hDD11_i0p4_i1m2, hDD11_i0p4_i1m3, hDD11_i0p4_i1m4, hDD11_i0p4_i1p1, hDD11_i0p4_i1p2, hDD11_i0p4_i1p3, hDD11_i0p4_i1p4,
            invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD1102 = SIMD_fd_function_dDD02_fdorder8(
            hDD11_i0m1_i2m1, hDD11_i0m1_i2m2, hDD11_i0m1_i2m3, hDD11_i0m1_i2m4, hDD11_i0m1_i2p1, hDD11_i0m1_i2p2, hDD11_i0m1_i2p3, hDD11_i0m1_i2p4,
            hDD11_i0m2_i2m1, hDD11_i0m2_i2m2, hDD11_i0m2_i2m3, hDD11_i0m2_i2m4, hDD11_i0m2_i2p1, hDD11_i0m2_i2p2, hDD11_i0m2_i2p3, hDD11_i0m2_i2p4,
            hDD11_i0m3_i2m1, hDD11_i0m3_i2m2, hDD11_i0m3_i2m3, hDD11_i0m3_i2m4, hDD11_i0m3_i2p1, hDD11_i0m3_i2p2, hDD11_i0m3_i2p3, hDD11_i0m3_i2p4,
            hDD11_i0m4_i2m1, hDD11_i0m4_i2m2, hDD11_i0m4_i2m3, hDD11_i0m4_i2m4, hDD11_i0m4_i2p1, hDD11_i0m4_i2p2, hDD11_i0m4_i2p3, hDD11_i0m4_i2p4,
            hDD11_i0p1_i2m1, hDD11_i0p1_i2m2, hDD11_i0p1_i2m3, hDD11_i0p1_i2m4, hDD11_i0p1_i2p1, hDD11_i0p1_i2p2, hDD11_i0p1_i2p3, hDD11_i0p1_i2p4,
            hDD11_i0p2_i2m1, hDD11_i0p2_i2m2, hDD11_i0p2_i2m3, hDD11_i0p2_i2m4, hDD11_i0p2_i2p1, hDD11_i0p2_i2p2, hDD11_i0p2_i2p3, hDD11_i0p2_i2p4,
            hDD11_i0p3_i2m1, hDD11_i0p3_i2m2, hDD11_i0p3_i2m3, hDD11_i0p3_i2m4, hDD11_i0p3_i2p1, hDD11_i0p3_i2p2, hDD11_i0p3_i2p3, hDD11_i0p3_i2p4,
            hDD11_i0p4_i2m1, hDD11_i0p4_i2m2, hDD11_i0p4_i2m3, hDD11_i0p4_i2m4, hDD11_i0p4_i2p1, hDD11_i0p4_i2p2, hDD11_i0p4_i2p3, hDD11_i0p4_i2p4,
            invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1111 = SIMD_fd_function_dDD11_fdorder8(hDD11, hDD11_i1m1, hDD11_i1m2, hDD11_i1m3, hDD11_i1m4, hDD11_i1p1,
                                                                            hDD11_i1p2, hDD11_i1p3, hDD11_i1p4, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD1112 = SIMD_fd_function_dDD12_fdorder8(
            hDD11_i1m1_i2m1, hDD11_i1m1_i2m2, hDD11_i1m1_i2m3, hDD11_i1m1_i2m4, hDD11_i1m1_i2p1, hDD11_i1m1_i2p2, hDD11_i1m1_i2p3, hDD11_i1m1_i2p4,
            hDD11_i1m2_i2m1, hDD11_i1m2_i2m2, hDD11_i1m2_i2m3, hDD11_i1m2_i2m4, hDD11_i1m2_i2p1, hDD11_i1m2_i2p2, hDD11_i1m2_i2p3, hDD11_i1m2_i2p4,
            hDD11_i1m3_i2m1, hDD11_i1m3_i2m2, hDD11_i1m3_i2m3, hDD11_i1m3_i2m4, hDD11_i1m3_i2p1, hDD11_i1m3_i2p2, hDD11_i1m3_i2p3, hDD11_i1m3_i2p4,
            hDD11_i1m4_i2m1, hDD11_i1m4_i2m2, hDD11_i1m4_i2m3, hDD11_i1m4_i2m4, hDD11_i1m4_i2p1, hDD11_i1m4_i2p2, hDD11_i1m4_i2p3, hDD11_i1m4_i2p4,
            hDD11_i1p1_i2m1, hDD11_i1p1_i2m2, hDD11_i1p1_i2m3, hDD11_i1p1_i2m4, hDD11_i1p1_i2p1, hDD11_i1p1_i2p2, hDD11_i1p1_i2p3, hDD11_i1p1_i2p4,
            hDD11_i1p2_i2m1, hDD11_i1p2_i2m2, hDD11_i1p2_i2m3, hDD11_i1p2_i2m4, hDD11_i1p2_i2p1, hDD11_i1p2_i2p2, hDD11_i1p2_i2p3, hDD11_i1p2_i2p4,
            hDD11_i1p3_i2m1, hDD11_i1p3_i2m2, hDD11_i1p3_i2m3, hDD11_i1p3_i2m4, hDD11_i1p3_i2p1, hDD11_i1p3_i2p2, hDD11_i1p3_i2p3, hDD11_i1p3_i2p4,
            hDD11_i1p4_i2m1, hDD11_i1p4_i2m2, hDD11_i1p4_i2m3, hDD11_i1p4_i2m4, hDD11_i1p4_i2p1, hDD11_i1p4_i2p2, hDD11_i1p4_i2p3, hDD11_i1p4_i2p4,
            invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1122 = SIMD_fd_function_dDD22_fdorder8(hDD11, hDD11_i2m1, hDD11_i2m2, hDD11_i2m3, hDD11_i2m4, hDD11_i2p1,
                                                                            hDD11_i2p2, hDD11_i2p3, hDD11_i2p4, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1200 = SIMD_fd_function_dDD00_fdorder8(hDD12, hDD12_i0m1, hDD12_i0m2, hDD12_i0m3, hDD12_i0m4, hDD12_i0p1,
                                                                            hDD12_i0p2, hDD12_i0p3, hDD12_i0p4, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD1201 = SIMD_fd_function_dDD01_fdorder8(
            hDD12_i0m1_i1m1, hDD12_i0m1_i1m2, hDD12_i0m1_i1m3, hDD12_i0m1_i1m4, hDD12_i0m1_i1p1, hDD12_i0m1_i1p2, hDD12_i0m1_i1p3, hDD12_i0m1_i1p4,
            hDD12_i0m2_i1m1, hDD12_i0m2_i1m2, hDD12_i0m2_i1m3, hDD12_i0m2_i1m4, hDD12_i0m2_i1p1, hDD12_i0m2_i1p2, hDD12_i0m2_i1p3, hDD12_i0m2_i1p4,
            hDD12_i0m3_i1m1, hDD12_i0m3_i1m2, hDD12_i0m3_i1m3, hDD12_i0m3_i1m4, hDD12_i0m3_i1p1, hDD12_i0m3_i1p2, hDD12_i0m3_i1p3, hDD12_i0m3_i1p4,
            hDD12_i0m4_i1m1, hDD12_i0m4_i1m2, hDD12_i0m4_i1m3, hDD12_i0m4_i1m4, hDD12_i0m4_i1p1, hDD12_i0m4_i1p2, hDD12_i0m4_i1p3, hDD12_i0m4_i1p4,
            hDD12_i0p1_i1m1, hDD12_i0p1_i1m2, hDD12_i0p1_i1m3, hDD12_i0p1_i1m4, hDD12_i0p1_i1p1, hDD12_i0p1_i1p2, hDD12_i0p1_i1p3, hDD12_i0p1_i1p4,
            hDD12_i0p2_i1m1, hDD12_i0p2_i1m2, hDD12_i0p2_i1m3, hDD12_i0p2_i1m4, hDD12_i0p2_i1p1, hDD12_i0p2_i1p2, hDD12_i0p2_i1p3, hDD12_i0p2_i1p4,
            hDD12_i0p3_i1m1, hDD12_i0p3_i1m2, hDD12_i0p3_i1m3, hDD12_i0p3_i1m4, hDD12_i0p3_i1p1, hDD12_i0p3_i1p2, hDD12_i0p3_i1p3, hDD12_i0p3_i1p4,
            hDD12_i0p4_i1m1, hDD12_i0p4_i1m2, hDD12_i0p4_i1m3, hDD12_i0p4_i1m4, hDD12_i0p4_i1p1, hDD12_i0p4_i1p2, hDD12_i0p4_i1p3, hDD12_i0p4_i1p4,
            invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD1202 = SIMD_fd_function_dDD02_fdorder8(
            hDD12_i0m1_i2m1, hDD12_i0m1_i2m2, hDD12_i0m1_i2m3, hDD12_i0m1_i2m4, hDD12_i0m1_i2p1, hDD12_i0m1_i2p2, hDD12_i0m1_i2p3, hDD12_i0m1_i2p4,
            hDD12_i0m2_i2m1, hDD12_i0m2_i2m2, hDD12_i0m2_i2m3, hDD12_i0m2_i2m4, hDD12_i0m2_i2p1, hDD12_i0m2_i2p2, hDD12_i0m2_i2p3, hDD12_i0m2_i2p4,
            hDD12_i0m3_i2m1, hDD12_i0m3_i2m2, hDD12_i0m3_i2m3, hDD12_i0m3_i2m4, hDD12_i0m3_i2p1, hDD12_i0m3_i2p2, hDD12_i0m3_i2p3, hDD12_i0m3_i2p4,
            hDD12_i0m4_i2m1, hDD12_i0m4_i2m2, hDD12_i0m4_i2m3, hDD12_i0m4_i2m4, hDD12_i0m4_i2p1, hDD12_i0m4_i2p2, hDD12_i0m4_i2p3, hDD12_i0m4_i2p4,
            hDD12_i0p1_i2m1, hDD12_i0p1_i2m2, hDD12_i0p1_i2m3, hDD12_i0p1_i2m4, hDD12_i0p1_i2p1, hDD12_i0p1_i2p2, hDD12_i0p1_i2p3, hDD12_i0p1_i2p4,
            hDD12_i0p2_i2m1, hDD12_i0p2_i2m2, hDD12_i0p2_i2m3, hDD12_i0p2_i2m4, hDD12_i0p2_i2p1, hDD12_i0p2_i2p2, hDD12_i0p2_i2p3, hDD12_i0p2_i2p4,
            hDD12_i0p3_i2m1, hDD12_i0p3_i2m2, hDD12_i0p3_i2m3, hDD12_i0p3_i2m4, hDD12_i0p3_i2p1, hDD12_i0p3_i2p2, hDD12_i0p3_i2p3, hDD12_i0p3_i2p4,
            hDD12_i0p4_i2m1, hDD12_i0p4_i2m2, hDD12_i0p4_i2m3, hDD12_i0p4_i2m4, hDD12_i0p4_i2p1, hDD12_i0p4_i2p2, hDD12_i0p4_i2p3, hDD12_i0p4_i2p4,
            invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1211 = SIMD_fd_function_dDD11_fdorder8(hDD12, hDD12_i1m1, hDD12_i1m2, hDD12_i1m3, hDD12_i1m4, hDD12_i1p1,
                                                                            hDD12_i1p2, hDD12_i1p3, hDD12_i1p4, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD1212 = SIMD_fd_function_dDD12_fdorder8(
            hDD12_i1m1_i2m1, hDD12_i1m1_i2m2, hDD12_i1m1_i2m3, hDD12_i1m1_i2m4, hDD12_i1m1_i2p1, hDD12_i1m1_i2p2, hDD12_i1m1_i2p3, hDD12_i1m1_i2p4,
            hDD12_i1m2_i2m1, hDD12_i1m2_i2m2, hDD12_i1m2_i2m3, hDD12_i1m2_i2m4, hDD12_i1m2_i2p1, hDD12_i1m2_i2p2, hDD12_i1m2_i2p3, hDD12_i1m2_i2p4,
            hDD12_i1m3_i2m1, hDD12_i1m3_i2m2, hDD12_i1m3_i2m3, hDD12_i1m3_i2m4, hDD12_i1m3_i2p1, hDD12_i1m3_i2p2, hDD12_i1m3_i2p3, hDD12_i1m3_i2p4,
            hDD12_i1m4_i2m1, hDD12_i1m4_i2m2, hDD12_i1m4_i2m3, hDD12_i1m4_i2m4, hDD12_i1m4_i2p1, hDD12_i1m4_i2p2, hDD12_i1m4_i2p3, hDD12_i1m4_i2p4,
            hDD12_i1p1_i2m1, hDD12_i1p1_i2m2, hDD12_i1p1_i2m3, hDD12_i1p1_i2m4, hDD12_i1p1_i2p1, hDD12_i1p1_i2p2, hDD12_i1p1_i2p3, hDD12_i1p1_i2p4,
            hDD12_i1p2_i2m1, hDD12_i1p2_i2m2, hDD12_i1p2_i2m3, hDD12_i1p2_i2m4, hDD12_i1p2_i2p1, hDD12_i1p2_i2p2, hDD12_i1p2_i2p3, hDD12_i1p2_i2p4,
            hDD12_i1p3_i2m1, hDD12_i1p3_i2m2, hDD12_i1p3_i2m3, hDD12_i1p3_i2m4, hDD12_i1p3_i2p1, hDD12_i1p3_i2p2, hDD12_i1p3_i2p3, hDD12_i1p3_i2p4,
            hDD12_i1p4_i2m1, hDD12_i1p4_i2m2, hDD12_i1p4_i2m3, hDD12_i1p4_i2m4, hDD12_i1p4_i2p1, hDD12_i1p4_i2p2, hDD12_i1p4_i2p3, hDD12_i1p4_i2p4,
            invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1222 = SIMD_fd_function_dDD22_fdorder8(hDD12, hDD12_i2m1, hDD12_i2m2, hDD12_i2m3, hDD12_i2m4, hDD12_i2p1,
                                                                            hDD12_i2p2, hDD12_i2p3, hDD12_i2p4, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD2200 = SIMD_fd_function_dDD00_fdorder8(hDD22, hDD22_i0m1, hDD22_i0m2, hDD22_i0m3, hDD22_i0m4, hDD22_i0p1,
                                                                            hDD22_i0p2, hDD22_i0p3, hDD22_i0p4, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD2201 = SIMD_fd_function_dDD01_fdorder8(
            hDD22_i0m1_i1m1, hDD22_i0m1_i1m2, hDD22_i0m1_i1m3, hDD22_i0m1_i1m4, hDD22_i0m1_i1p1, hDD22_i0m1_i1p2, hDD22_i0m1_i1p3, hDD22_i0m1_i1p4,
            hDD22_i0m2_i1m1, hDD22_i0m2_i1m2, hDD22_i0m2_i1m3, hDD22_i0m2_i1m4, hDD22_i0m2_i1p1, hDD22_i0m2_i1p2, hDD22_i0m2_i1p3, hDD22_i0m2_i1p4,
            hDD22_i0m3_i1m1, hDD22_i0m3_i1m2, hDD22_i0m3_i1m3, hDD22_i0m3_i1m4, hDD22_i0m3_i1p1, hDD22_i0m3_i1p2, hDD22_i0m3_i1p3, hDD22_i0m3_i1p4,
            hDD22_i0m4_i1m1, hDD22_i0m4_i1m2, hDD22_i0m4_i1m3, hDD22_i0m4_i1m4, hDD22_i0m4_i1p1, hDD22_i0m4_i1p2, hDD22_i0m4_i1p3, hDD22_i0m4_i1p4,
            hDD22_i0p1_i1m1, hDD22_i0p1_i1m2, hDD22_i0p1_i1m3, hDD22_i0p1_i1m4, hDD22_i0p1_i1p1, hDD22_i0p1_i1p2, hDD22_i0p1_i1p3, hDD22_i0p1_i1p4,
            hDD22_i0p2_i1m1, hDD22_i0p2_i1m2, hDD22_i0p2_i1m3, hDD22_i0p2_i1m4, hDD22_i0p2_i1p1, hDD22_i0p2_i1p2, hDD22_i0p2_i1p3, hDD22_i0p2_i1p4,
            hDD22_i0p3_i1m1, hDD22_i0p3_i1m2, hDD22_i0p3_i1m3, hDD22_i0p3_i1m4, hDD22_i0p3_i1p1, hDD22_i0p3_i1p2, hDD22_i0p3_i1p3, hDD22_i0p3_i1p4,
            hDD22_i0p4_i1m1, hDD22_i0p4_i1m2, hDD22_i0p4_i1m3, hDD22_i0p4_i1m4, hDD22_i0p4_i1p1, hDD22_i0p4_i1p2, hDD22_i0p4_i1p3, hDD22_i0p4_i1p4,
            invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD2202 = SIMD_fd_function_dDD02_fdorder8(
            hDD22_i0m1_i2m1, hDD22_i0m1_i2m2, hDD22_i0m1_i2m3, hDD22_i0m1_i2m4, hDD22_i0m1_i2p1, hDD22_i0m1_i2p2, hDD22_i0m1_i2p3, hDD22_i0m1_i2p4,
            hDD22_i0m2_i2m1, hDD22_i0m2_i2m2, hDD22_i0m2_i2m3, hDD22_i0m2_i2m4, hDD22_i0m2_i2p1, hDD22_i0m2_i2p2, hDD22_i0m2_i2p3, hDD22_i0m2_i2p4,
            hDD22_i0m3_i2m1, hDD22_i0m3_i2m2, hDD22_i0m3_i2m3, hDD22_i0m3_i2m4, hDD22_i0m3_i2p1, hDD22_i0m3_i2p2, hDD22_i0m3_i2p3, hDD22_i0m3_i2p4,
            hDD22_i0m4_i2m1, hDD22_i0m4_i2m2, hDD22_i0m4_i2m3, hDD22_i0m4_i2m4, hDD22_i0m4_i2p1, hDD22_i0m4_i2p2, hDD22_i0m4_i2p3, hDD22_i0m4_i2p4,
            hDD22_i0p1_i2m1, hDD22_i0p1_i2m2, hDD22_i0p1_i2m3, hDD22_i0p1_i2m4, hDD22_i0p1_i2p1, hDD22_i0p1_i2p2, hDD22_i0p1_i2p3, hDD22_i0p1_i2p4,
            hDD22_i0p2_i2m1, hDD22_i0p2_i2m2, hDD22_i0p2_i2m3, hDD22_i0p2_i2m4, hDD22_i0p2_i2p1, hDD22_i0p2_i2p2, hDD22_i0p2_i2p3, hDD22_i0p2_i2p4,
            hDD22_i0p3_i2m1, hDD22_i0p3_i2m2, hDD22_i0p3_i2m3, hDD22_i0p3_i2m4, hDD22_i0p3_i2p1, hDD22_i0p3_i2p2, hDD22_i0p3_i2p3, hDD22_i0p3_i2p4,
            hDD22_i0p4_i2m1, hDD22_i0p4_i2m2, hDD22_i0p4_i2m3, hDD22_i0p4_i2m4, hDD22_i0p4_i2p1, hDD22_i0p4_i2p2, hDD22_i0p4_i2p3, hDD22_i0p4_i2p4,
            invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD2211 = SIMD_fd_function_dDD11_fdorder8(hDD22, hDD22_i1m1, hDD22_i1m2, hDD22_i1m3, hDD22_i1m4, hDD22_i1p1,
                                                                            hDD22_i1p2, hDD22_i1p3, hDD22_i1p4, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD2212 = SIMD_fd_function_dDD12_fdorder8(
            hDD22_i1m1_i2m1, hDD22_i1m1_i2m2, hDD22_i1m1_i2m3, hDD22_i1m1_i2m4, hDD22_i1m1_i2p1, hDD22_i1m1_i2p2, hDD22_i1m1_i2p3, hDD22_i1m1_i2p4,
            hDD22_i1m2_i2m1, hDD22_i1m2_i2m2, hDD22_i1m2_i2m3, hDD22_i1m2_i2m4, hDD22_i1m2_i2p1, hDD22_i1m2_i2p2, hDD22_i1m2_i2p3, hDD22_i1m2_i2p4,
            hDD22_i1m3_i2m1, hDD22_i1m3_i2m2, hDD22_i1m3_i2m3, hDD22_i1m3_i2m4, hDD22_i1m3_i2p1, hDD22_i1m3_i2p2, hDD22_i1m3_i2p3, hDD22_i1m3_i2p4,
            hDD22_i1m4_i2m1, hDD22_i1m4_i2m2, hDD22_i1m4_i2m3, hDD22_i1m4_i2m4, hDD22_i1m4_i2p1, hDD22_i1m4_i2p2, hDD22_i1m4_i2p3, hDD22_i1m4_i2p4,
            hDD22_i1p1_i2m1, hDD22_i1p1_i2m2, hDD22_i1p1_i2m3, hDD22_i1p1_i2m4, hDD22_i1p1_i2p1, hDD22_i1p1_i2p2, hDD22_i1p1_i2p3, hDD22_i1p1_i2p4,
            hDD22_i1p2_i2m1, hDD22_i1p2_i2m2, hDD22_i1p2_i2m3, hDD22_i1p2_i2m4, hDD22_i1p2_i2p1, hDD22_i1p2_i2p2, hDD22_i1p2_i2p3, hDD22_i1p2_i2p4,
            hDD22_i1p3_i2m1, hDD22_i1p3_i2m2, hDD22_i1p3_i2m3, hDD22_i1p3_i2m4, hDD22_i1p3_i2p1, hDD22_i1p3_i2p2, hDD22_i1p3_i2p3, hDD22_i1p3_i2p4,
            hDD22_i1p4_i2m1, hDD22_i1p4_i2m2, hDD22_i1p4_i2m3, hDD22_i1p4_i2m4, hDD22_i1p4_i2p1, hDD22_i1p4_i2p2, hDD22_i1p4_i2p3, hDD22_i1p4_i2p4,
            invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD2222 = SIMD_fd_function_dDD22_fdorder8(hDD22, hDD22_i2m1, hDD22_i2m2, hDD22_i2m3, hDD22_i2m4, hDD22_i2p1,
                                                                            hDD22_i2p2, hDD22_i2p3, hDD22_i2p4, invdxx2);
        const REAL_SIMD_ARRAY lambdaU_dD00 = SIMD_fd_function_dD0_fdorder8(lambdaU0_i0m1, lambdaU0_i0m2, lambdaU0_i0m3, lambdaU0_i0m4, lambdaU0_i0p1,
                                                                           lambdaU0_i0p2, lambdaU0_i0p3, lambdaU0_i0p4, invdxx0);
        const REAL_SIMD_ARRAY lambdaU_dD01 = SIMD_fd_function_dD1_fdorder8(lambdaU0_i1m1, lambdaU0_i1m2, lambdaU0_i1m3, lambdaU0_i1m4, lambdaU0_i1p1,
                                                                           lambdaU0_i1p2, lambdaU0_i1p3, lambdaU0_i1p4, invdxx1);
        const REAL_SIMD_ARRAY lambdaU_dD02 = SIMD_fd_function_dD2_fdorder8(lambdaU0_i2m1, lambdaU0_i2m2, lambdaU0_i2m3, lambdaU0_i2m4, lambdaU0_i2p1,
                                                                           lambdaU0_i2p2, lambdaU0_i2p3, lambdaU0_i2p4, invdxx2);
        const REAL_SIMD_ARRAY lambdaU_dD10 = SIMD_fd_function_dD0_fdorder8(lambdaU1_i0m1, lambdaU1_i0m2, lambdaU1_i0m3, lambdaU1_i0m4, lambdaU1_i0p1,
                                                                           lambdaU1_i0p2, lambdaU1_i0p3, lambdaU1_i0p4, invdxx0);
        const REAL_SIMD_ARRAY lambdaU_dD11 = SIMD_fd_function_dD1_fdorder8(lambdaU1_i1m1, lambdaU1_i1m2, lambdaU1_i1m3, lambdaU1_i1m4, lambdaU1_i1p1,
                                                                           lambdaU1_i1p2, lambdaU1_i1p3, lambdaU1_i1p4, invdxx1);
        const REAL_SIMD_ARRAY lambdaU_dD12 = SIMD_fd_function_dD2_fdorder8(lambdaU1_i2m1, lambdaU1_i2m2, lambdaU1_i2m3, lambdaU1_i2m4, lambdaU1_i2p1,
                                                                           lambdaU1_i2p2, lambdaU1_i2p3, lambdaU1_i2p4, invdxx2);
        const REAL_SIMD_ARRAY lambdaU_dD20 = SIMD_fd_function_dD0_fdorder8(lambdaU2_i0m1, lambdaU2_i0m2, lambdaU2_i0m3, lambdaU2_i0m4, lambdaU2_i0p1,
                                                                           lambdaU2_i0p2, lambdaU2_i0p3, lambdaU2_i0p4, invdxx0);
        const REAL_SIMD_ARRAY lambdaU_dD21 = SIMD_fd_function_dD1_fdorder8(lambdaU2_i1m1, lambdaU2_i1m2, lambdaU2_i1m3, lambdaU2_i1m4, lambdaU2_i1p1,
                                                                           lambdaU2_i1p2, lambdaU2_i1p3, lambdaU2_i1p4, invdxx1);
        const REAL_SIMD_ARRAY lambdaU_dD22 = SIMD_fd_function_dD2_fdorder8(lambdaU2_i2m1, lambdaU2_i2m2, lambdaU2_i2m3, lambdaU2_i2m4, lambdaU2_i2p1,
                                                                           lambdaU2_i2p2, lambdaU2_i2p3, lambdaU2_i2p4, invdxx2);
        const REAL_SIMD_ARRAY trK_dD0 =
            SIMD_fd_function_dD0_fdorder8(trK_i0m1, trK_i0m2, trK_i0m3, trK_i0m4, trK_i0p1, trK_i0p2, trK_i0p3, trK_i0p4, invdxx0);
        const REAL_SIMD_ARRAY trK_dD1 =
            SIMD_fd_function_dD1_fdorder8(trK_i1m1, trK_i1m2, trK_i1m3, trK_i1m4, trK_i1p1, trK_i1p2, trK_i1p3, trK_i1p4, invdxx1);
        const REAL_SIMD_ARRAY trK_dD2 =
            SIMD_fd_function_dD2_fdorder8(trK_i2m1, trK_i2m2, trK_i2m3, trK_i2m4, trK_i2p1, trK_i2p2, trK_i2p3, trK_i2p4, invdxx2);

        /*
         * NRPy+-Generated GF Access/FD Code, Step 2 of 2:
         * Evaluate SymPy expressions and write to main memory.
         */
        const double dblFDPart3_Integer_1 = 1.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_1 = ConstSIMD(dblFDPart3_Integer_1);

        const double dblFDPart3_Integer_12 = 12.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_12 = ConstSIMD(dblFDPart3_Integer_12);

        const double dblFDPart3_Integer_16 = 16.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_16 = ConstSIMD(dblFDPart3_Integer_16);

        const double dblFDPart3_Integer_2 = 2.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_2 = ConstSIMD(dblFDPart3_Integer_2);

        const double dblFDPart3_Integer_3 = 3.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_3 = ConstSIMD(dblFDPart3_Integer_3);

        const double dblFDPart3_Integer_4 = 4.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_4 = ConstSIMD(dblFDPart3_Integer_4);

        const double dblFDPart3_Integer_6 = 6.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_6 = ConstSIMD(dblFDPart3_Integer_6);

        const double dblFDPart3_Integer_8 = 8.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_8 = ConstSIMD(dblFDPart3_Integer_8);

        const double dblFDPart3_NegativeOne_ = -1.0;
        const REAL_SIMD_ARRAY FDPart3_NegativeOne_ = ConstSIMD(dblFDPart3_NegativeOne_);

        const double dblFDPart3_Rational_1_2 = 1.0 / 2.0;
        const REAL_SIMD_ARRAY FDPart3_Rational_1_2 = ConstSIMD(dblFDPart3_Rational_1_2);

        const double dblFDPart3_Rational_2_3 = 2.0 / 3.0;
        const REAL_SIMD_ARRAY FDPart3_Rational_2_3 = ConstSIMD(dblFDPart3_Rational_2_3);

        const REAL_SIMD_ARRAY FDPart3tmp0 = AddSIMD(FDPart3_Integer_1, hDD11);
        const REAL_SIMD_ARRAY FDPart3tmp3 = AddSIMD(FDPart3_Integer_1, hDD00);
        const REAL_SIMD_ARRAY FDPart3tmp4 = AddSIMD(FDPart3_Integer_1, hDD22);
        const REAL_SIMD_ARRAY FDPart3tmp51 = MulSIMD(cf, cf);
        const REAL_SIMD_ARRAY FDPart3tmp64 = DivSIMD(FDPart3_Integer_1, cf);
        const REAL_SIMD_ARRAY FDPart3tmp69 = AddSIMD(hDD_dD120, SubSIMD(hDD_dD021, hDD_dD012));
        const REAL_SIMD_ARRAY FDPart3tmp75 = MulSIMD(FDPart3_Rational_1_2, FDPart3_Rational_1_2);
        const REAL_SIMD_ARRAY FDPart3tmp79 = AddSIMD(hDD_dD120, SubSIMD(hDD_dD012, hDD_dD021));
        const REAL_SIMD_ARRAY FDPart3tmp90 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD122, hDD_dD221);
        const REAL_SIMD_ARRAY FDPart3tmp91 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD022, hDD_dD220);
        const REAL_SIMD_ARRAY FDPart3tmp95 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD121, hDD_dD112);
        const REAL_SIMD_ARRAY FDPart3tmp96 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD011, hDD_dD110);
        const REAL_SIMD_ARRAY FDPart3tmp100 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD010, hDD_dD001);
        const REAL_SIMD_ARRAY FDPart3tmp101 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD020, hDD_dD002);
        const REAL_SIMD_ARRAY FDPart3tmp473 = MulSIMD(MulSIMD(MulSIMD(cf, cf), cf), cf);
        const REAL_SIMD_ARRAY FDPart3tmp1 = FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp0, hDD02));
        const REAL_SIMD_ARRAY FDPart3tmp11 = FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp4, hDD01));
        const REAL_SIMD_ARRAY FDPart3tmp23 = FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp3, hDD12));
        const REAL_SIMD_ARRAY FDPart3tmp25 = FusedMulSubSIMD(FDPart3tmp3, FDPart3tmp4, MulSIMD(hDD02, hDD02));
        const REAL_SIMD_ARRAY FDPart3tmp35 = FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD01, hDD01));
        const REAL_SIMD_ARRAY FDPart3tmp54 = DivSIMD(FDPart3_Integer_1, FDPart3tmp51);
        const REAL_SIMD_ARRAY FDPart3tmp65 = MulSIMD(FDPart3tmp64, cf_dD1);
        const REAL_SIMD_ARRAY FDPart3tmp66 = MulSIMD(FDPart3_Rational_1_2, FDPart3tmp64);
        const REAL_SIMD_ARRAY FDPart3tmp71 = MulSIMD(FDPart3tmp64, cf_dD0);
        const REAL_SIMD_ARRAY FDPart3tmp74 = MulSIMD(FDPart3tmp64, cf_dD2);
        const REAL_SIMD_ARRAY FDPart3tmp76 = MulSIMD(FDPart3_Integer_1, FDPart3tmp75);
        const REAL_SIMD_ARRAY FDPart3tmp117 = MulSIMD(FDPart3_Integer_2, FDPart3tmp75);
        const REAL_SIMD_ARRAY FDPart3tmp9 = FusedMulAddSIMD(
            MulSIMD(FDPart3_Integer_2, hDD01), MulSIMD(hDD02, hDD12),
            FusedMulSubSIMD(FDPart3tmp0, MulSIMD(FDPart3tmp3, FDPart3tmp4),
                            FusedMulAddSIMD(FDPart3tmp3, MulSIMD(hDD12, hDD12),
                                            FusedMulAddSIMD(FDPart3tmp4, MulSIMD(hDD01, hDD01), MulSIMD(FDPart3tmp0, MulSIMD(hDD02, hDD02))))));
        const REAL_SIMD_ARRAY FDPart3tmp14 = FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp4, MulSIMD(hDD12, hDD12));
        const REAL_SIMD_ARRAY FDPart3tmp10 = DivSIMD(FDPart3_Integer_1, MulSIMD(FDPart3tmp9, FDPart3tmp9));
        const REAL_SIMD_ARRAY FDPart3tmp52 = DivSIMD(FDPart3_Integer_1, FDPart3tmp9);
        const REAL_SIMD_ARRAY FDPart3tmp12 = MulSIMD(FDPart3tmp10, FDPart3tmp11);
        const REAL_SIMD_ARRAY FDPart3tmp15 = MulSIMD(FDPart3tmp10, FDPart3tmp14);
        const REAL_SIMD_ARRAY FDPart3tmp19 = MulSIMD(FDPart3tmp10, MulSIMD(FDPart3tmp11, FDPart3tmp11));
        const REAL_SIMD_ARRAY FDPart3tmp20 = MulSIMD(FDPart3tmp10, MulSIMD(FDPart3tmp1, FDPart3tmp1));
        const REAL_SIMD_ARRAY FDPart3tmp21 = MulSIMD(FDPart3tmp10, MulSIMD(FDPart3tmp14, FDPart3tmp14));
        const REAL_SIMD_ARRAY FDPart3tmp27 = MulSIMD(FDPart3tmp10, FDPart3tmp25);
        const REAL_SIMD_ARRAY FDPart3tmp30 = MulSIMD(FDPart3tmp10, MulSIMD(FDPart3tmp23, FDPart3tmp23));
        const REAL_SIMD_ARRAY FDPart3tmp31 = MulSIMD(FDPart3tmp10, MulSIMD(FDPart3tmp25, FDPart3tmp25));
        const REAL_SIMD_ARRAY FDPart3tmp33 = MulSIMD(FDPart3tmp1, FDPart3tmp10);
        const REAL_SIMD_ARRAY FDPart3tmp40 = MulSIMD(FDPart3tmp10, MulSIMD(FDPart3tmp35, FDPart3tmp35));
        const REAL_SIMD_ARRAY FDPart3tmp53 = MulSIMD(FDPart3tmp11, FDPart3tmp52);
        const REAL_SIMD_ARRAY FDPart3tmp56 = MulSIMD(FDPart3tmp1, FDPart3tmp52);
        const REAL_SIMD_ARRAY FDPart3tmp57 = MulSIMD(FDPart3tmp23, FDPart3tmp52);
        const REAL_SIMD_ARRAY FDPart3tmp59 = MulSIMD(FDPart3tmp14, FDPart3tmp52);
        const REAL_SIMD_ARRAY FDPart3tmp61 = MulSIMD(FDPart3tmp25, FDPart3tmp52);
        const REAL_SIMD_ARRAY FDPart3tmp63 = MulSIMD(FDPart3tmp35, FDPart3tmp52);
        const REAL_SIMD_ARRAY FDPart3tmp78 = MulSIMD(FDPart3tmp52, hDD_dD002);
        const REAL_SIMD_ARRAY FDPart3tmp84 = MulSIMD(FDPart3tmp52, hDD_dD112);
        const REAL_SIMD_ARRAY FDPart3tmp85 = MulSIMD(FDPart3tmp52, hDD_dD221);
        const REAL_SIMD_ARRAY FDPart3tmp13 = MulSIMD(FDPart3tmp1, FDPart3tmp12);
        const REAL_SIMD_ARRAY FDPart3tmp16 = MulSIMD(FDPart3tmp15, aDD01);
        const REAL_SIMD_ARRAY FDPart3tmp17 = MulSIMD(FDPart3tmp15, aDD02);
        const REAL_SIMD_ARRAY FDPart3tmp22 = FusedMulAddSIMD(FDPart3tmp20, aDD22, FusedMulAddSIMD(FDPart3tmp21, aDD00, MulSIMD(FDPart3tmp19, aDD11)));
        const REAL_SIMD_ARRAY FDPart3tmp24 = MulSIMD(FDPart3tmp12, FDPart3tmp23);
        const REAL_SIMD_ARRAY FDPart3tmp28 = MulSIMD(FDPart3tmp23, FDPart3tmp27);
        const REAL_SIMD_ARRAY FDPart3tmp32 = FusedMulAddSIMD(FDPart3tmp30, aDD22, FusedMulAddSIMD(FDPart3tmp31, aDD11, MulSIMD(FDPart3tmp19, aDD00)));
        const REAL_SIMD_ARRAY FDPart3tmp34 = MulSIMD(FDPart3tmp23, FDPart3tmp33);
        const REAL_SIMD_ARRAY FDPart3tmp41 = FusedMulAddSIMD(FDPart3tmp30, aDD11, FusedMulAddSIMD(FDPart3tmp40, aDD22, MulSIMD(FDPart3tmp20, aDD00)));
        const REAL_SIMD_ARRAY FDPart3tmp49 = MulSIMD(FDPart3tmp10, MulSIMD(FDPart3tmp23, FDPart3tmp35));
        const REAL_SIMD_ARRAY FDPart3tmp70 =
            FusedMulAddSIMD(FDPart3tmp11, MulSIMD(FDPart3tmp52, hDD_dD110),
                            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp52, hDD_dD001), MulSIMD(FDPart3tmp56, FDPart3tmp69)));
        const REAL_SIMD_ARRAY FDPart3tmp72 =
            FusedMulAddSIMD(FDPart3tmp11, MulSIMD(FDPart3tmp52, hDD_dD001),
                            FusedMulAddSIMD(FDPart3tmp25, MulSIMD(FDPart3tmp52, hDD_dD110), MulSIMD(FDPart3tmp57, FDPart3tmp69)));
        const REAL_SIMD_ARRAY FDPart3tmp73 =
            FusedMulAddSIMD(FDPart3tmp57, hDD_dD110, FusedMulAddSIMD(FDPart3tmp63, FDPart3tmp69, MulSIMD(FDPart3tmp56, hDD_dD001)));
        const REAL_SIMD_ARRAY FDPart3tmp81 =
            FusedMulAddSIMD(FDPart3tmp1, MulSIMD(FDPart3tmp52, hDD_dD220),
                            FusedMulAddSIMD(FDPart3tmp11, MulSIMD(FDPart3tmp52, FDPart3tmp79), MulSIMD(FDPart3tmp14, FDPart3tmp78)));
        const REAL_SIMD_ARRAY FDPart3tmp82 = FusedMulAddSIMD(
            FDPart3tmp57, hDD_dD220, FusedMulAddSIMD(FDPart3tmp25, MulSIMD(FDPart3tmp52, FDPart3tmp79), MulSIMD(FDPart3tmp11, FDPart3tmp78)));
        const REAL_SIMD_ARRAY FDPart3tmp83 = FusedMulAddSIMD(
            FDPart3tmp57, FDPart3tmp79, FusedMulAddSIMD(FDPart3tmp35, MulSIMD(FDPart3tmp52, hDD_dD220), MulSIMD(FDPart3tmp1, FDPart3tmp78)));
        const REAL_SIMD_ARRAY FDPart3tmp87 =
            FusedMulAddSIMD(FDPart3tmp11, FDPart3tmp84,
                            FusedMulAddSIMD(FDPart3tmp14, MulSIMD(FDPart3tmp52, AddSIMD(hDD_dD021, SubSIMD(hDD_dD012, hDD_dD120))),
                                            MulSIMD(FDPart3tmp1, FDPart3tmp85)));
        const REAL_SIMD_ARRAY FDPart3tmp88 =
            FusedMulAddSIMD(FDPart3tmp25, FDPart3tmp84,
                            FusedMulAddSIMD(FDPart3tmp11, MulSIMD(FDPart3tmp52, AddSIMD(hDD_dD021, SubSIMD(hDD_dD012, hDD_dD120))),
                                            MulSIMD(FDPart3tmp23, FDPart3tmp85)));
        const REAL_SIMD_ARRAY FDPart3tmp89 =
            FusedMulAddSIMD(FDPart3tmp35, FDPart3tmp85,
                            FusedMulAddSIMD(FDPart3tmp1, MulSIMD(FDPart3tmp52, AddSIMD(hDD_dD021, SubSIMD(hDD_dD012, hDD_dD120))),
                                            MulSIMD(FDPart3tmp23, FDPart3tmp84)));
        const REAL_SIMD_ARRAY FDPart3tmp92 =
            FusedMulAddSIMD(FDPart3tmp56, hDD_dD222, FusedMulAddSIMD(FDPart3tmp59, FDPart3tmp91, MulSIMD(FDPart3tmp53, FDPart3tmp90)));
        const REAL_SIMD_ARRAY FDPart3tmp93 =
            FusedMulAddSIMD(FDPart3tmp57, hDD_dD222, FusedMulAddSIMD(FDPart3tmp61, FDPart3tmp90, MulSIMD(FDPart3tmp53, FDPart3tmp91)));
        const REAL_SIMD_ARRAY FDPart3tmp94 =
            FusedMulAddSIMD(FDPart3tmp57, FDPart3tmp90, FusedMulAddSIMD(FDPart3tmp63, hDD_dD222, MulSIMD(FDPart3tmp56, FDPart3tmp91)));
        const REAL_SIMD_ARRAY FDPart3tmp97 =
            FusedMulAddSIMD(FDPart3tmp56, FDPart3tmp95, FusedMulAddSIMD(FDPart3tmp59, FDPart3tmp96, MulSIMD(FDPart3tmp53, hDD_dD111)));
        const REAL_SIMD_ARRAY FDPart3tmp98 =
            FusedMulAddSIMD(FDPart3tmp57, FDPart3tmp95, FusedMulAddSIMD(FDPart3tmp61, hDD_dD111, MulSIMD(FDPart3tmp53, FDPart3tmp96)));
        const REAL_SIMD_ARRAY FDPart3tmp99 =
            FusedMulAddSIMD(FDPart3tmp57, hDD_dD111, FusedMulAddSIMD(FDPart3tmp63, FDPart3tmp95, MulSIMD(FDPart3tmp56, FDPart3tmp96)));
        const REAL_SIMD_ARRAY FDPart3tmp102 =
            FusedMulAddSIMD(FDPart3tmp101, FDPart3tmp56, FusedMulAddSIMD(FDPart3tmp59, hDD_dD000, MulSIMD(FDPart3tmp100, FDPart3tmp53)));
        const REAL_SIMD_ARRAY FDPart3tmp103 =
            FusedMulAddSIMD(FDPart3tmp101, FDPart3tmp57, FusedMulAddSIMD(FDPart3tmp53, hDD_dD000, MulSIMD(FDPart3tmp100, FDPart3tmp61)));
        const REAL_SIMD_ARRAY FDPart3tmp104 =
            FusedMulAddSIMD(FDPart3tmp101, FDPart3tmp63, FusedMulAddSIMD(FDPart3tmp56, hDD_dD000, MulSIMD(FDPart3tmp100, FDPart3tmp57)));
        const REAL_SIMD_ARRAY FDPart3tmp185 = MulSIMD(FDPart3_Integer_3, FDPart3tmp53);
        const REAL_SIMD_ARRAY FDPart3tmp192 = MulSIMD(FDPart3_Integer_3, FDPart3tmp56);
        const REAL_SIMD_ARRAY FDPart3tmp194 = MulSIMD(FDPart3_Integer_3, FDPart3tmp57);
        const REAL_SIMD_ARRAY FDPart3tmp435 = MulSIMD(FDPart3tmp11, FDPart3tmp15);
        const REAL_SIMD_ARRAY FDPart3tmp440 = MulSIMD(FDPart3tmp1, FDPart3tmp15);
        const REAL_SIMD_ARRAY FDPart3tmp455 = MulSIMD(FDPart3tmp15, FDPart3tmp23);
        const REAL_SIMD_ARRAY FDPart3tmp461 = MulSIMD(FDPart3tmp25, FDPart3tmp33);
        const REAL_SIMD_ARRAY FDPart3tmp18 =
            FusedMulAddSIMD(FDPart3tmp11, FDPart3tmp16, FusedMulAddSIMD(FDPart3tmp13, aDD12, MulSIMD(FDPart3tmp1, FDPart3tmp17)));
        const REAL_SIMD_ARRAY FDPart3tmp29 =
            FusedMulAddSIMD(FDPart3tmp28, aDD12, FusedMulAddSIMD(FDPart3tmp12, MulSIMD(FDPart3tmp25, aDD01), MulSIMD(FDPart3tmp24, aDD02)));
        const REAL_SIMD_ARRAY FDPart3tmp39 =
            FusedMulAddSIMD(FDPart3tmp33, MulSIMD(FDPart3tmp35, aDD02),
                            FusedMulAddSIMD(MulSIMD(FDPart3tmp10, FDPart3tmp23), MulSIMD(FDPart3tmp35, aDD12), MulSIMD(FDPart3tmp34, aDD01)));
        const REAL_SIMD_ARRAY FDPart3tmp47 = FusedMulAddSIMD(
            FDPart3tmp12, MulSIMD(FDPart3tmp25, aDD11),
            FusedMulAddSIMD(
                FDPart3tmp11, MulSIMD(FDPart3tmp15, aDD00),
                FusedMulAddSIMD(
                    FDPart3tmp12, MulSIMD(FDPart3tmp23, aDD12),
                    FusedMulAddSIMD(FDPart3tmp17, FDPart3tmp23,
                                    FusedMulAddSIMD(FDPart3tmp19, aDD01,
                                                    FusedMulAddSIMD(FDPart3tmp23, MulSIMD(FDPart3tmp33, aDD22),
                                                                    FusedMulAddSIMD(FDPart3tmp25, MulSIMD(FDPart3tmp33, aDD12),
                                                                                    FusedMulAddSIMD(FDPart3tmp13, aDD02,
                                                                                                    MulSIMD(FDPart3tmp16, FDPart3tmp25)))))))));
        const REAL_SIMD_ARRAY FDPart3tmp48 = FusedMulAddSIMD(
            FDPart3tmp12, MulSIMD(FDPart3tmp35, aDD12),
            FusedMulAddSIMD(
                FDPart3tmp1, MulSIMD(FDPart3tmp15, aDD00),
                FusedMulAddSIMD(
                    FDPart3tmp12, MulSIMD(FDPart3tmp23, aDD11),
                    FusedMulAddSIMD(FDPart3tmp17, FDPart3tmp35,
                                    FusedMulAddSIMD(FDPart3tmp20, aDD02,
                                                    FusedMulAddSIMD(FDPart3tmp23, MulSIMD(FDPart3tmp33, aDD12),
                                                                    FusedMulAddSIMD(FDPart3tmp33, MulSIMD(FDPart3tmp35, aDD22),
                                                                                    FusedMulAddSIMD(FDPart3tmp13, aDD01,
                                                                                                    MulSIMD(FDPart3tmp16, FDPart3tmp23)))))))));
        const REAL_SIMD_ARRAY FDPart3tmp50 = FusedMulAddSIMD(
            FDPart3tmp12, MulSIMD(FDPart3tmp35, aDD02),
            FusedMulAddSIMD(
                FDPart3tmp34, aDD02,
                FusedMulAddSIMD(FDPart3tmp49, aDD22,
                                FusedMulAddSIMD(FDPart3tmp28, aDD11,
                                                FusedMulAddSIMD(FDPart3tmp30, aDD12,
                                                                FusedMulAddSIMD(FDPart3tmp25, MulSIMD(FDPart3tmp33, aDD01),
                                                                                FusedMulAddSIMD(FDPart3tmp27, MulSIMD(FDPart3tmp35, aDD12),
                                                                                                FusedMulAddSIMD(FDPart3tmp13, aDD00,
                                                                                                                MulSIMD(FDPart3tmp24, aDD01)))))))));
        const REAL_SIMD_ARRAY FDPart3tmp105 =
            FusedMulAddSIMD(FDPart3tmp88, hDD01, FusedMulAddSIMD(FDPart3tmp89, hDD02, MulSIMD(FDPart3tmp3, FDPart3tmp87)));
        const REAL_SIMD_ARRAY FDPart3tmp107 =
            FusedMulAddSIMD(FDPart3tmp70, hDD02, FusedMulAddSIMD(FDPart3tmp72, hDD12, MulSIMD(FDPart3tmp4, FDPart3tmp73)));
        const REAL_SIMD_ARRAY FDPart3tmp110 =
            FusedMulAddSIMD(FDPart3tmp81, hDD01, FusedMulAddSIMD(FDPart3tmp83, hDD12, MulSIMD(FDPart3tmp0, FDPart3tmp82)));
        const REAL_SIMD_ARRAY FDPart3tmp133 =
            FusedMulAddSIMD(FDPart3tmp103, hDD12, FusedMulAddSIMD(FDPart3tmp104, FDPart3tmp4, MulSIMD(FDPart3tmp102, hDD02)));
        const REAL_SIMD_ARRAY FDPart3tmp141 =
            FusedMulAddSIMD(FDPart3tmp97, hDD02, FusedMulAddSIMD(FDPart3tmp98, hDD12, MulSIMD(FDPart3tmp4, FDPart3tmp99)));
        const REAL_SIMD_ARRAY FDPart3tmp149 =
            FusedMulAddSIMD(FDPart3tmp93, hDD01, FusedMulAddSIMD(FDPart3tmp94, hDD02, MulSIMD(FDPart3tmp3, FDPart3tmp92)));
        const REAL_SIMD_ARRAY FDPart3tmp156 =
            FusedMulAddSIMD(FDPart3tmp92, hDD01, FusedMulAddSIMD(FDPart3tmp94, hDD12, MulSIMD(FDPart3tmp0, FDPart3tmp93)));
        const REAL_SIMD_ARRAY FDPart3tmp220 =
            FusedMulAddSIMD(FDPart3tmp98, hDD01, FusedMulAddSIMD(FDPart3tmp99, hDD02, MulSIMD(FDPart3tmp3, FDPart3tmp97)));
        const REAL_SIMD_ARRAY FDPart3tmp230 =
            FusedMulAddSIMD(FDPart3tmp102, hDD01, FusedMulAddSIMD(FDPart3tmp104, hDD12, MulSIMD(FDPart3tmp0, FDPart3tmp103)));
        const REAL_SIMD_ARRAY FDPart3tmp443 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp103, aDD01),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp104, aDD02),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp102, aDD00)))),
                               aDD_dD000);
        const REAL_SIMD_ARRAY FDPart3tmp445 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp93, aDD12),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp94, aDD22),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp92, aDD02)))),
                               aDD_dD222);
        const REAL_SIMD_ARRAY FDPart3tmp447 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp98, aDD11),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp99, aDD12),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp97, aDD01)))),
                               aDD_dD111);
        const REAL_SIMD_ARRAY FDPart3tmp449 =
            FusedMulAddSIMD(FDPart3tmp82, aDD11, FusedMulAddSIMD(FDPart3tmp83, aDD12, MulSIMD(FDPart3tmp81, aDD01)));
        const REAL_SIMD_ARRAY FDPart3tmp450 =
            FusedMulAddSIMD(FDPart3tmp88, aDD01, FusedMulAddSIMD(FDPart3tmp89, aDD02, MulSIMD(FDPart3tmp87, aDD00)));
        const REAL_SIMD_ARRAY FDPart3tmp453 =
            FusedMulAddSIMD(FDPart3tmp72, aDD12, FusedMulAddSIMD(FDPart3tmp73, aDD22, MulSIMD(FDPart3tmp70, aDD02)));
        const REAL_SIMD_ARRAY FDPart3tmp106 = MulSIMD(FDPart3tmp105, FDPart3tmp81);
        const REAL_SIMD_ARRAY FDPart3tmp111 = MulSIMD(FDPart3tmp110, FDPart3tmp88);
        const REAL_SIMD_ARRAY FDPart3tmp114 = MulSIMD(FDPart3tmp105, FDPart3tmp87);
        const REAL_SIMD_ARRAY FDPart3tmp118 = MulSIMD(FDPart3tmp110, FDPart3tmp82);
        const REAL_SIMD_ARRAY FDPart3tmp123 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp75, FDPart3tmp93));
        const REAL_SIMD_ARRAY FDPart3tmp126 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp75, FDPart3tmp92));
        const REAL_SIMD_ARRAY FDPart3tmp130 =
            FusedMulAddSIMD(FDPart3tmp82, hDD01, FusedMulAddSIMD(FDPart3tmp83, hDD02, MulSIMD(FDPart3tmp3, FDPart3tmp81)));
        const REAL_SIMD_ARRAY FDPart3tmp138 =
            FusedMulAddSIMD(FDPart3tmp87, hDD01, FusedMulAddSIMD(FDPart3tmp89, hDD12, MulSIMD(FDPart3tmp0, FDPart3tmp88)));
        const REAL_SIMD_ARRAY FDPart3tmp146 =
            FusedMulAddSIMD(FDPart3tmp81, hDD02, FusedMulAddSIMD(FDPart3tmp82, hDD12, MulSIMD(FDPart3tmp4, FDPart3tmp83)));
        const REAL_SIMD_ARRAY FDPart3tmp154 =
            FusedMulAddSIMD(FDPart3tmp87, hDD02, FusedMulAddSIMD(FDPart3tmp88, hDD12, MulSIMD(FDPart3tmp4, FDPart3tmp89)));
        const REAL_SIMD_ARRAY FDPart3tmp176 = FusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp56, FDPart3tmp81, FusedMulAddSIMD(FDPart3tmp57, FDPart3tmp87, MulSIMD(FDPart3tmp53, FDPart3tmp70))),
            MulSIMD(FDPart3tmp75,
                    FusedMulAddSIMD(FDPart3tmp61, FDPart3tmp97, FusedMulAddSIMD(FDPart3tmp63, FDPart3tmp92, MulSIMD(FDPart3tmp102, FDPart3tmp59)))));
        const REAL_SIMD_ARRAY FDPart3tmp178 = FusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp56, FDPart3tmp82, FusedMulAddSIMD(FDPart3tmp57, FDPart3tmp88, MulSIMD(FDPart3tmp53, FDPart3tmp72))),
            MulSIMD(FDPart3tmp75,
                    FusedMulAddSIMD(FDPart3tmp61, FDPart3tmp98, FusedMulAddSIMD(FDPart3tmp63, FDPart3tmp93, MulSIMD(FDPart3tmp103, FDPart3tmp59)))));
        const REAL_SIMD_ARRAY FDPart3tmp183 = FusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp56, FDPart3tmp83, FusedMulAddSIMD(FDPart3tmp57, FDPart3tmp89, MulSIMD(FDPart3tmp53, FDPart3tmp73))),
            MulSIMD(FDPart3tmp75,
                    FusedMulAddSIMD(FDPart3tmp61, FDPart3tmp99, FusedMulAddSIMD(FDPart3tmp63, FDPart3tmp94, MulSIMD(FDPart3tmp104, FDPart3tmp59)))));
        const REAL_SIMD_ARRAY FDPart3tmp195 =
            FusedMulAddSIMD(FDPart3tmp92, hDD02, FusedMulAddSIMD(FDPart3tmp93, hDD12, MulSIMD(FDPart3tmp4, FDPart3tmp94)));
        const REAL_SIMD_ARRAY FDPart3tmp198 = MulSIMD(FDPart3tmp105, FDPart3tmp70);
        const REAL_SIMD_ARRAY FDPart3tmp201 = MulSIMD(FDPart3tmp107, FDPart3tmp89);
        const REAL_SIMD_ARRAY FDPart3tmp205 = MulSIMD(FDPart3tmp107, FDPart3tmp73);
        const REAL_SIMD_ARRAY FDPart3tmp210 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp75, FDPart3tmp99));
        const REAL_SIMD_ARRAY FDPart3tmp213 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp75, FDPart3tmp97));
        const REAL_SIMD_ARRAY FDPart3tmp217 =
            FusedMulAddSIMD(FDPart3tmp70, hDD01, FusedMulAddSIMD(FDPart3tmp73, hDD12, MulSIMD(FDPart3tmp0, FDPart3tmp72)));
        const REAL_SIMD_ARRAY FDPart3tmp227 =
            FusedMulAddSIMD(FDPart3tmp72, hDD01, FusedMulAddSIMD(FDPart3tmp73, hDD02, MulSIMD(FDPart3tmp3, FDPart3tmp70)));
        const REAL_SIMD_ARRAY FDPart3tmp260 =
            FusedMulAddSIMD(FDPart3tmp97, hDD01, FusedMulAddSIMD(FDPart3tmp99, hDD12, MulSIMD(FDPart3tmp0, FDPart3tmp98)));
        const REAL_SIMD_ARRAY FDPart3tmp263 = MulSIMD(FDPart3tmp110, FDPart3tmp72);
        const REAL_SIMD_ARRAY FDPart3tmp266 = MulSIMD(FDPart3tmp107, FDPart3tmp83);
        const REAL_SIMD_ARRAY FDPart3tmp273 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp104, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp276 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp103, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp310 =
            FusedMulAddSIMD(FDPart3tmp103, hDD01, FusedMulAddSIMD(FDPart3tmp104, hDD02, MulSIMD(FDPart3tmp102, FDPart3tmp3)));
        const REAL_SIMD_ARRAY FDPart3tmp314 = MulSIMD(FDPart3tmp110, FDPart3tmp83);
        const REAL_SIMD_ARRAY FDPart3tmp316 = MulSIMD(FDPart3tmp110, FDPart3tmp81);
        const REAL_SIMD_ARRAY FDPart3tmp324 = MulSIMD(FDPart3tmp105, FDPart3tmp89);
        const REAL_SIMD_ARRAY FDPart3tmp330 = MulSIMD(FDPart3tmp230, FDPart3tmp81);
        const REAL_SIMD_ARRAY FDPart3tmp332 = MulSIMD(FDPart3tmp149, FDPart3tmp89);
        const REAL_SIMD_ARRAY FDPart3tmp333 = MulSIMD(FDPart3tmp156, FDPart3tmp83);
        const REAL_SIMD_ARRAY FDPart3tmp340 = MulSIMD(FDPart3tmp105, FDPart3tmp88);
        const REAL_SIMD_ARRAY FDPart3tmp343 = MulSIMD(FDPart3tmp220, FDPart3tmp88);
        const REAL_SIMD_ARRAY FDPart3tmp369 = MulSIMD(FDPart3tmp107, FDPart3tmp72);
        const REAL_SIMD_ARRAY FDPart3tmp374 = MulSIMD(FDPart3tmp141, FDPart3tmp72);
        const REAL_SIMD_ARRAY FDPart3tmp380 = MulSIMD(FDPart3tmp107, FDPart3tmp70);
        const REAL_SIMD_ARRAY FDPart3tmp386 = MulSIMD(FDPart3tmp133, FDPart3tmp70);
        const REAL_SIMD_ARRAY FDPart3tmp418 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp72, aDD11),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp73, aDD12),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp70, aDD01)))),
                               aDD_dD110);
        const REAL_SIMD_ARRAY FDPart3tmp422 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp82, aDD12),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp83, aDD22),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp81, aDD02)))),
                               aDD_dD220);
        const REAL_SIMD_ARRAY FDPart3tmp426 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp88, aDD11),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp89, aDD12),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp87, aDD01)))),
                               aDD_dD112);
        const REAL_SIMD_ARRAY FDPart3tmp430 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp88, aDD12),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp89, aDD22),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp87, aDD02)))),
                               aDD_dD221);
        const REAL_SIMD_ARRAY FDPart3tmp434 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp72, aDD01),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp73, aDD02),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp70, aDD00)))),
                               aDD_dD001);
        const REAL_SIMD_ARRAY FDPart3tmp439 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp82, aDD01),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp83, aDD02),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp81, aDD00)))),
                               aDD_dD002);
        const REAL_SIMD_ARRAY FDPart3tmp452 = NegFusedMulAddSIMD(FDPart3_Rational_1_2, AddSIMD(FDPart3tmp449, FDPart3tmp450), aDD_dD012);
        const REAL_SIMD_ARRAY FDPart3tmp454 = NegFusedMulAddSIMD(FDPart3_Rational_1_2, AddSIMD(FDPart3tmp450, FDPart3tmp453), aDD_dD021);
        const REAL_SIMD_ARRAY FDPart3tmp456 = NegFusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp73, aDD12,
                            FusedMulAddSIMD(FDPart3tmp97, aDD00,
                                            FusedMulAddSIMD(FDPart3tmp98, aDD01,
                                                            FusedMulAddSIMD(FDPart3tmp99, aDD02,
                                                                            FusedMulAddSIMD(FDPart3tmp70, aDD01, MulSIMD(FDPart3tmp72, aDD11)))))),
            aDD_dD011);
        const REAL_SIMD_ARRAY FDPart3tmp457 = NegFusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp83, aDD22,
                            FusedMulAddSIMD(FDPart3tmp92, aDD00,
                                            FusedMulAddSIMD(FDPart3tmp93, aDD01,
                                                            FusedMulAddSIMD(FDPart3tmp94, aDD02,
                                                                            FusedMulAddSIMD(FDPart3tmp81, aDD02, MulSIMD(FDPart3tmp82, aDD12)))))),
            aDD_dD022);
        const REAL_SIMD_ARRAY FDPart3tmp458 = NegFusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp89, aDD12,
                            FusedMulAddSIMD(FDPart3tmp97, aDD02,
                                            FusedMulAddSIMD(FDPart3tmp98, aDD12,
                                                            FusedMulAddSIMD(FDPart3tmp99, aDD22,
                                                                            FusedMulAddSIMD(FDPart3tmp87, aDD01, MulSIMD(FDPart3tmp88, aDD11)))))),
            aDD_dD121);
        const REAL_SIMD_ARRAY FDPart3tmp459 = NegFusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp89, aDD22,
                            FusedMulAddSIMD(FDPart3tmp92, aDD01,
                                            FusedMulAddSIMD(FDPart3tmp93, aDD11,
                                                            FusedMulAddSIMD(FDPart3tmp94, aDD12,
                                                                            FusedMulAddSIMD(FDPart3tmp87, aDD02, MulSIMD(FDPart3tmp88, aDD12)))))),
            aDD_dD122);
        const REAL_SIMD_ARRAY FDPart3tmp466 = NegFusedMulAddSIMD(FDPart3_Rational_1_2, AddSIMD(FDPart3tmp449, FDPart3tmp453), aDD_dD120);
        const REAL_SIMD_ARRAY FDPart3tmp467 = NegFusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp104, aDD12,
                            FusedMulAddSIMD(FDPart3tmp70, aDD00,
                                            FusedMulAddSIMD(FDPart3tmp72, aDD01,
                                                            FusedMulAddSIMD(FDPart3tmp73, aDD02,
                                                                            FusedMulAddSIMD(FDPart3tmp102, aDD01, MulSIMD(FDPart3tmp103, aDD11)))))),
            aDD_dD010);
        const REAL_SIMD_ARRAY FDPart3tmp468 = NegFusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp104, aDD22,
                            FusedMulAddSIMD(FDPart3tmp81, aDD00,
                                            FusedMulAddSIMD(FDPart3tmp82, aDD01,
                                                            FusedMulAddSIMD(FDPart3tmp83, aDD02,
                                                                            FusedMulAddSIMD(FDPart3tmp102, aDD02, MulSIMD(FDPart3tmp103, aDD12)))))),
            aDD_dD020);
        const REAL_SIMD_ARRAY FDPart3tmp109 = MulSIMD(FDPart3tmp107, MulSIMD(FDPart3tmp75, FDPart3tmp81));
        const REAL_SIMD_ARRAY FDPart3tmp113 = MulSIMD(FDPart3tmp107, MulSIMD(FDPart3tmp75, FDPart3tmp88));
        const REAL_SIMD_ARRAY FDPart3tmp131 = MulSIMD(FDPart3tmp130, FDPart3tmp87);
        const REAL_SIMD_ARRAY FDPart3tmp139 = MulSIMD(FDPart3tmp138, FDPart3tmp82);
        const REAL_SIMD_ARRAY FDPart3tmp163 = MulSIMD(FDPart3tmp138, FDPart3tmp88);
        const REAL_SIMD_ARRAY FDPart3tmp166 = MulSIMD(FDPart3tmp141, MulSIMD(FDPart3tmp75, FDPart3tmp88));
        const REAL_SIMD_ARRAY FDPart3tmp167 = MulSIMD(FDPart3tmp130, FDPart3tmp81);
        const REAL_SIMD_ARRAY FDPart3tmp170 = MulSIMD(FDPart3tmp133, MulSIMD(FDPart3tmp75, FDPart3tmp81));
        const REAL_SIMD_ARRAY FDPart3tmp184 = MulSIMD(FDPart3tmp146, FDPart3tmp89);
        const REAL_SIMD_ARRAY FDPart3tmp186 = MulSIMD(FDPart3tmp154, FDPart3tmp83);
        const REAL_SIMD_ARRAY FDPart3tmp187 = MulSIMD(FDPart3tmp154, FDPart3tmp89);
        const REAL_SIMD_ARRAY FDPart3tmp189 = MulSIMD(FDPart3tmp146, FDPart3tmp83);
        const REAL_SIMD_ARRAY FDPart3tmp196 = MulSIMD(FDPart3tmp195, FDPart3tmp83);
        const REAL_SIMD_ARRAY FDPart3tmp197 = MulSIMD(FDPart3tmp195, FDPart3tmp89);
        const REAL_SIMD_ARRAY FDPart3tmp200 = MulSIMD(FDPart3tmp110, MulSIMD(FDPart3tmp70, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp203 = MulSIMD(FDPart3tmp110, MulSIMD(FDPart3tmp75, FDPart3tmp89));
        const REAL_SIMD_ARRAY FDPart3tmp228 = MulSIMD(FDPart3tmp227, FDPart3tmp87);
        const REAL_SIMD_ARRAY FDPart3tmp232 = MulSIMD(FDPart3tmp154, FDPart3tmp73);
        const REAL_SIMD_ARRAY FDPart3tmp242 = MulSIMD(FDPart3tmp156, MulSIMD(FDPart3tmp75, FDPart3tmp89));
        const REAL_SIMD_ARRAY FDPart3tmp243 = MulSIMD(FDPart3tmp227, FDPart3tmp70);
        const REAL_SIMD_ARRAY FDPart3tmp246 = MulSIMD(FDPart3tmp230, MulSIMD(FDPart3tmp70, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp254 = MulSIMD(FDPart3tmp217, FDPart3tmp88);
        const REAL_SIMD_ARRAY FDPart3tmp255 = MulSIMD(FDPart3tmp138, FDPart3tmp72);
        const REAL_SIMD_ARRAY FDPart3tmp257 = MulSIMD(FDPart3tmp217, FDPart3tmp72);
        const REAL_SIMD_ARRAY FDPart3tmp261 = MulSIMD(FDPart3tmp260, FDPart3tmp72);
        const REAL_SIMD_ARRAY FDPart3tmp262 = MulSIMD(FDPart3tmp260, FDPart3tmp88);
        const REAL_SIMD_ARRAY FDPart3tmp265 = MulSIMD(FDPart3tmp105, MulSIMD(FDPart3tmp72, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp268 = MulSIMD(FDPart3tmp105, MulSIMD(FDPart3tmp75, FDPart3tmp83));
        const REAL_SIMD_ARRAY FDPart3tmp287 = MulSIMD(FDPart3tmp217, FDPart3tmp82);
        const REAL_SIMD_ARRAY FDPart3tmp290 = MulSIMD(FDPart3tmp146, FDPart3tmp73);
        const REAL_SIMD_ARRAY FDPart3tmp295 = MulSIMD(FDPart3tmp149, MulSIMD(FDPart3tmp75, FDPart3tmp83));
        const REAL_SIMD_ARRAY FDPart3tmp298 = MulSIMD(FDPart3tmp220, MulSIMD(FDPart3tmp72, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp306 = MulSIMD(FDPart3tmp227, FDPart3tmp81);
        const REAL_SIMD_ARRAY FDPart3tmp307 = MulSIMD(FDPart3tmp130, FDPart3tmp70);
        const REAL_SIMD_ARRAY FDPart3tmp311 = MulSIMD(FDPart3tmp310, FDPart3tmp70);
        const REAL_SIMD_ARRAY FDPart3tmp312 = MulSIMD(FDPart3tmp310, FDPart3tmp81);
        const REAL_SIMD_ARRAY FDPart3tmp313 = MulSIMD(FDPart3tmp130, FDPart3tmp89);
        const REAL_SIMD_ARRAY FDPart3tmp317 = FusedMulAddSIMD(FDPart3tmp110, FDPart3tmp73, MulSIMD(FDPart3tmp130, FDPart3tmp99));
        const REAL_SIMD_ARRAY FDPart3tmp318 = MulSIMD(FDPart3tmp130, FDPart3tmp97);
        const REAL_SIMD_ARRAY FDPart3tmp322 = MulSIMD(FDPart3tmp217, FDPart3tmp81);
        const REAL_SIMD_ARRAY FDPart3tmp325 = MulSIMD(FDPart3tmp138, FDPart3tmp83);
        const REAL_SIMD_ARRAY FDPart3tmp329 = FusedMulAddSIMD(FDPart3tmp104, FDPart3tmp138, MulSIMD(FDPart3tmp105, FDPart3tmp73));
        const REAL_SIMD_ARRAY FDPart3tmp331 = MulSIMD(FDPart3tmp310, FDPart3tmp87);
        const REAL_SIMD_ARRAY FDPart3tmp336 = FusedMulAddSIMD(FDPart3tmp149, FDPart3tmp99, MulSIMD(FDPart3tmp156, FDPart3tmp73));
        const REAL_SIMD_ARRAY FDPart3tmp337 = MulSIMD(FDPart3tmp104, FDPart3tmp154);
        const REAL_SIMD_ARRAY FDPart3tmp338 = FusedMulAddSIMD(FDPart3tmp104, FDPart3tmp156, MulSIMD(FDPart3tmp149, FDPart3tmp73));
        const REAL_SIMD_ARRAY FDPart3tmp344 = MulSIMD(FDPart3tmp260, FDPart3tmp82);
        const REAL_SIMD_ARRAY FDPart3tmp347 = MulSIMD(FDPart3tmp227, FDPart3tmp88);
        const REAL_SIMD_ARRAY FDPart3tmp349 = MulSIMD(FDPart3tmp103, FDPart3tmp138);
        const REAL_SIMD_ARRAY FDPart3tmp352 = FusedMulSubSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(
                hDD02, lambdaU_dD21,
                FusedMulAddSIMD(
                    hDD01, lambdaU_dD00,
                    FusedMulAddSIMD(
                        hDD01, lambdaU_dD11,
                        FusedMulAddSIMD(
                            FDPart3tmp183, AddSIMD(FDPart3tmp105, FDPart3tmp110),
                            FusedMulAddSIMD(
                                FDPart3tmp3, lambdaU_dD01,
                                FusedMulAddSIMD(
                                    FDPart3tmp176, AddSIMD(FDPart3tmp227, FDPart3tmp230),
                                    FusedMulAddSIMD(
                                        FDPart3tmp178, AddSIMD(FDPart3tmp217, FDPart3tmp220),
                                        FusedMulAddSIMD(hDD12, lambdaU_dD20,
                                                        FusedMulSubSIMD(FDPart3tmp0, lambdaU_dD10,
                                                                        FusedMulAddSIMD(FDPart3tmp61, hDD_dDD0111,
                                                                                        FusedMulAddSIMD(FDPart3tmp63, hDD_dDD0122,
                                                                                                        MulSIMD(FDPart3tmp59, hDD_dDD0100)))))))))))),
            FusedMulAddSIMD(FDPart3tmp56, hDD_dDD0102, FusedMulAddSIMD(FDPart3tmp57, hDD_dDD0112, MulSIMD(FDPart3tmp53, hDD_dDD0101))));
        const REAL_SIMD_ARRAY FDPart3tmp360 = MulSIMD(FDPart3tmp146, FDPart3tmp99);
        const REAL_SIMD_ARRAY FDPart3tmp364 = MulSIMD(FDPart3tmp154, FDPart3tmp72);
        const REAL_SIMD_ARRAY FDPart3tmp365 = FusedMulAddSIMD(FDPart3tmp103, FDPart3tmp154, MulSIMD(FDPart3tmp105, FDPart3tmp82));
        const REAL_SIMD_ARRAY FDPart3tmp371 = FusedMulAddSIMD(FDPart3tmp107, FDPart3tmp82, MulSIMD(FDPart3tmp227, FDPart3tmp93));
        const REAL_SIMD_ARRAY FDPart3tmp373 = MulSIMD(FDPart3tmp195, FDPart3tmp73);
        const REAL_SIMD_ARRAY FDPart3tmp375 = FusedMulAddSIMD(FDPart3tmp103, FDPart3tmp141, MulSIMD(FDPart3tmp220, FDPart3tmp82));
        const REAL_SIMD_ARRAY FDPart3tmp377 = MulSIMD(FDPart3tmp217, FDPart3tmp93);
        const REAL_SIMD_ARRAY FDPart3tmp378 = FusedMulAddSIMD(FDPart3tmp141, FDPart3tmp82, MulSIMD(FDPart3tmp220, FDPart3tmp93));
        const REAL_SIMD_ARRAY FDPart3tmp382 = MulSIMD(FDPart3tmp227, FDPart3tmp92);
        const REAL_SIMD_ARRAY FDPart3tmp383 = MulSIMD(FDPart3tmp146, FDPart3tmp70);
        const REAL_SIMD_ARRAY FDPart3tmp388 = FusedMulSubSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(
                hDD02, lambdaU_dD22,
                FusedMulAddSIMD(
                    hDD01, lambdaU_dD12,
                    FusedMulAddSIMD(
                        hDD02, lambdaU_dD00,
                        FusedMulAddSIMD(
                            FDPart3tmp3, lambdaU_dD02,
                            FusedMulAddSIMD(
                                FDPart3tmp4, lambdaU_dD20,
                                FusedMulAddSIMD(
                                    FDPart3tmp178, AddSIMD(FDPart3tmp105, FDPart3tmp107),
                                    FusedMulAddSIMD(
                                        FDPart3tmp183, AddSIMD(FDPart3tmp146, FDPart3tmp149),
                                        FusedMulAddSIMD(hDD12, lambdaU_dD10,
                                                        FusedMulSubSIMD(FDPart3tmp176, AddSIMD(FDPart3tmp130, FDPart3tmp133),
                                                                        FusedMulAddSIMD(FDPart3tmp61, hDD_dDD0211,
                                                                                        FusedMulAddSIMD(FDPart3tmp63, hDD_dDD0222,
                                                                                                        MulSIMD(FDPart3tmp59, hDD_dDD0200)))))))))))),
            FusedMulAddSIMD(FDPart3tmp56, hDD_dDD0202, FusedMulAddSIMD(FDPart3tmp57, hDD_dDD0212, MulSIMD(FDPart3tmp53, hDD_dDD0201))));
        const REAL_SIMD_ARRAY FDPart3tmp397 = FusedMulAddSIMD(FDPart3tmp110, FDPart3tmp87, MulSIMD(FDPart3tmp146, FDPart3tmp97));
        const REAL_SIMD_ARRAY FDPart3tmp402 = FusedMulAddSIMD(FDPart3tmp107, FDPart3tmp87, MulSIMD(FDPart3tmp217, FDPart3tmp92));
        const REAL_SIMD_ARRAY FDPart3tmp405 = FusedMulAddSIMD(FDPart3tmp133, FDPart3tmp97, MulSIMD(FDPart3tmp230, FDPart3tmp87));
        const REAL_SIMD_ARRAY FDPart3tmp407 = FusedMulAddSIMD(FDPart3tmp133, FDPart3tmp87, MulSIMD(FDPart3tmp230, FDPart3tmp92));
        const REAL_SIMD_ARRAY FDPart3tmp413 = FusedMulSubSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(
                hDD12, lambdaU_dD11,
                FusedMulAddSIMD(
                    hDD01, lambdaU_dD02,
                    FusedMulAddSIMD(
                        hDD02, lambdaU_dD01,
                        FusedMulAddSIMD(
                            FDPart3tmp183, AddSIMD(FDPart3tmp154, FDPart3tmp156),
                            FusedMulAddSIMD(
                                FDPart3tmp4, lambdaU_dD21,
                                FusedMulAddSIMD(
                                    FDPart3tmp176, AddSIMD(FDPart3tmp107, FDPart3tmp110),
                                    FusedMulAddSIMD(
                                        FDPart3tmp178, AddSIMD(FDPart3tmp138, FDPart3tmp141),
                                        FusedMulAddSIMD(hDD12, lambdaU_dD22,
                                                        FusedMulSubSIMD(FDPart3tmp0, lambdaU_dD12,
                                                                        FusedMulAddSIMD(FDPart3tmp61, hDD_dDD1211,
                                                                                        FusedMulAddSIMD(FDPart3tmp63, hDD_dDD1222,
                                                                                                        MulSIMD(FDPart3tmp59, hDD_dDD1200)))))))))))),
            FusedMulAddSIMD(FDPart3tmp56, hDD_dDD1202, FusedMulAddSIMD(FDPart3tmp57, hDD_dDD1212, MulSIMD(FDPart3tmp53, hDD_dDD1201))));
        const REAL_SIMD_ARRAY FDPart3tmp462 = MulSIMD(FDPart3tmp35, FDPart3tmp457);
        const REAL_SIMD_ARRAY FDPart3tmp463 = MulSIMD(FDPart3tmp25, FDPart3tmp456);
        const REAL_SIMD_ARRAY FDPart3tmp148 = MulSIMD(FDPart3tmp146, MulSIMD(FDPart3tmp75, FDPart3tmp81));
        const REAL_SIMD_ARRAY FDPart3tmp161 = MulSIMD(FDPart3tmp154, MulSIMD(FDPart3tmp75, FDPart3tmp88));
        const REAL_SIMD_ARRAY FDPart3tmp172 = MulSIMD(FDPart3tmp130, MulSIMD(FDPart3tmp75, FDPart3tmp92));
        const REAL_SIMD_ARRAY FDPart3tmp174 = MulSIMD(FDPart3tmp138, MulSIMD(FDPart3tmp75, FDPart3tmp93));
        const REAL_SIMD_ARRAY FDPart3tmp219 = MulSIMD(FDPart3tmp217, MulSIMD(FDPart3tmp70, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp238 = MulSIMD(FDPart3tmp138, MulSIMD(FDPart3tmp75, FDPart3tmp89));
        const REAL_SIMD_ARRAY FDPart3tmp248 = MulSIMD(FDPart3tmp227, MulSIMD(FDPart3tmp75, FDPart3tmp97));
        const REAL_SIMD_ARRAY FDPart3tmp250 = MulSIMD(FDPart3tmp154, MulSIMD(FDPart3tmp75, FDPart3tmp99));
        const REAL_SIMD_ARRAY FDPart3tmp278 = MulSIMD(FDPart3tmp227, MulSIMD(FDPart3tmp72, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp285 = MulSIMD(FDPart3tmp130, MulSIMD(FDPart3tmp75, FDPart3tmp83));
        const REAL_SIMD_ARRAY FDPart3tmp300 = MulSIMD(FDPart3tmp103, MulSIMD(FDPart3tmp217, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp302 = MulSIMD(FDPart3tmp104, MulSIMD(FDPart3tmp146, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp321 = FusedMulAddSIMD(FDPart3tmp104, FDPart3tmp110, MulSIMD(FDPart3tmp130, FDPart3tmp73));
        const REAL_SIMD_ARRAY FDPart3tmp326 = FusedMulAddSIMD(FDPart3tmp141, FDPart3tmp83, FDPart3tmp325);
        const REAL_SIMD_ARRAY FDPart3tmp327 = FusedMulAddSIMD(FDPart3tmp105, FDPart3tmp99, MulSIMD(FDPart3tmp138, FDPart3tmp73));
        const REAL_SIMD_ARRAY FDPart3tmp356 = FusedMulAddSIMD(FDPart3tmp230, FDPart3tmp88, FDPart3tmp347);
        const REAL_SIMD_ARRAY FDPart3tmp357 = FusedMulAddSIMD(FDPart3tmp133, FDPart3tmp89, FDPart3tmp313);
        const REAL_SIMD_ARRAY FDPart3tmp368 = FusedMulAddSIMD(FDPart3tmp105, FDPart3tmp93, MulSIMD(FDPart3tmp154, FDPart3tmp82));
        const REAL_SIMD_ARRAY FDPart3tmp370 = FusedMulAddSIMD(FDPart3tmp103, FDPart3tmp107, MulSIMD(FDPart3tmp227, FDPart3tmp82));
        const REAL_SIMD_ARRAY FDPart3tmp391 = FusedMulAddSIMD(FDPart3tmp156, FDPart3tmp72, FDPart3tmp364);
        const REAL_SIMD_ARRAY FDPart3tmp399 = FusedMulAddSIMD(FDPart3tmp110, FDPart3tmp92, MulSIMD(FDPart3tmp146, FDPart3tmp87));
        const REAL_SIMD_ARRAY FDPart3tmp401 = FusedMulAddSIMD(FDPart3tmp107, FDPart3tmp97, MulSIMD(FDPart3tmp217, FDPart3tmp87));
        const REAL_SIMD_ARRAY FDPart3tmp469 = FusedMulAddSIMD(
            FDPart3tmp12, MulSIMD(FDPart3tmp25, FDPart3tmp447),
            FusedMulAddSIMD(
                FDPart3tmp12, MulSIMD(FDPart3tmp35, FDPart3tmp459),
                FusedMulAddSIMD(
                    FDPart3tmp454, FDPart3tmp455,
                    FusedMulAddSIMD(
                        FDPart3tmp458, FDPart3tmp461,
                        FusedMulAddSIMD(
                            FDPart3tmp439, FDPart3tmp440,
                            FusedMulAddSIMD(
                                FDPart3tmp452, FDPart3tmp455,
                                FusedMulAddSIMD(
                                    FDPart3tmp34, FDPart3tmp459,
                                    FusedMulAddSIMD(
                                        FDPart3tmp434, FDPart3tmp435,
                                        FusedMulAddSIMD(
                                            FDPart3tmp24, FDPart3tmp458,
                                            FusedMulAddSIMD(
                                                FDPart3tmp34, FDPart3tmp430,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp21, FDPart3tmp443,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp24, FDPart3tmp426,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp20, FDPart3tmp422,
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp20, FDPart3tmp457,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp19, FDPart3tmp418,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp19, FDPart3tmp456,
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp15, FDPart3tmp462,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp15, FDPart3tmp463,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp13, FDPart3tmp452,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp13, FDPart3tmp454,
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp33, MulSIMD(FDPart3tmp35, FDPart3tmp445),
                                                                                            FusedMulSubSIMD(
                                                                                                FDPart3_Integer_2,
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp435, FDPart3tmp467,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp440, FDPart3tmp468,
                                                                                                        MulSIMD(FDPart3tmp13, FDPart3tmp466))),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3_Rational_1_2,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3_Integer_6,
                                                                                                        MulSIMD(FDPart3tmp47, FDPart3tmp65),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3_Integer_6,
                                                                                                            MulSIMD(FDPart3tmp48, FDPart3tmp74),
                                                                                                            MulSIMD(FDPart3tmp71,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3_Integer_12,
                                                                                                                        FDPart3tmp18,
                                                                                                                        MulSIMD(FDPart3_Integer_6,
                                                                                                                                FDPart3tmp22))))),
                                                                                                    MulSIMD(
                                                                                                        FDPart3_Rational_2_3,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp56, trK_dD2,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp59, trK_dD0,
                                                                                                                MulSIMD(
                                                                                                                    FDPart3tmp53,
                                                                                                                    trK_dD1)))))))))))))))))))))))))));
        const REAL_SIMD_ARRAY FDPart3tmp470 = FusedMulAddSIMD(
            FDPart3tmp12, MulSIMD(FDPart3tmp25, FDPart3tmp418),
            FusedMulAddSIMD(
                FDPart3tmp15, MulSIMD(FDPart3tmp25, FDPart3tmp467),
                FusedMulAddSIMD(
                    FDPart3tmp455, FDPart3tmp468,
                    FusedMulAddSIMD(
                        FDPart3tmp461, FDPart3tmp466,
                        FusedMulAddSIMD(
                            FDPart3tmp445, FDPart3tmp49,
                            FusedMulAddSIMD(
                                FDPart3tmp452, FDPart3tmp461,
                                FusedMulAddSIMD(
                                    FDPart3tmp34, FDPart3tmp457,
                                    FusedMulAddSIMD(
                                        FDPart3tmp435, FDPart3tmp443,
                                        FusedMulAddSIMD(
                                            FDPart3tmp31, FDPart3tmp447,
                                            FusedMulAddSIMD(
                                                FDPart3tmp34, FDPart3tmp422,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp30, FDPart3tmp430,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp30, FDPart3tmp459,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp24, FDPart3tmp466,
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp28, FDPart3tmp426,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp19, FDPart3tmp467,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp24, FDPart3tmp452,
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp13, FDPart3tmp468,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp19, FDPart3tmp434,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp12, FDPart3tmp462,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp13, FDPart3tmp439,
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp27, MulSIMD(FDPart3tmp35, FDPart3tmp459),
                                                                                            FusedMulSubSIMD(
                                                                                                FDPart3_Integer_2,
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp24, FDPart3tmp454,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp28, FDPart3tmp458,
                                                                                                        MulSIMD(FDPart3tmp12, FDPart3tmp463))),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3_Rational_1_2,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3_Integer_6,
                                                                                                        MulSIMD(FDPart3tmp47, FDPart3tmp71),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3_Integer_6,
                                                                                                            MulSIMD(FDPart3tmp50, FDPart3tmp74),
                                                                                                            MulSIMD(FDPart3tmp65,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3_Integer_12,
                                                                                                                        FDPart3tmp29,
                                                                                                                        MulSIMD(FDPart3_Integer_6,
                                                                                                                                FDPart3tmp32))))),
                                                                                                    MulSIMD(
                                                                                                        FDPart3_Rational_2_3,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp57, trK_dD2,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp61, trK_dD1,
                                                                                                                MulSIMD(
                                                                                                                    FDPart3tmp53,
                                                                                                                    trK_dD0)))))))))))))))))))))))))));
        const REAL_SIMD_ARRAY FDPart3tmp472 = FusedMulAddSIMD(
            FDPart3tmp15, MulSIMD(FDPart3tmp35, FDPart3tmp468),
            FusedMulAddSIMD(
                FDPart3tmp27, MulSIMD(FDPart3tmp35, FDPart3tmp458),
                FusedMulAddSIMD(
                    FDPart3tmp12, MulSIMD(FDPart3tmp35, FDPart3tmp454),
                    FusedMulAddSIMD(
                        FDPart3tmp12, MulSIMD(FDPart3tmp35, FDPart3tmp466),
                        FusedMulAddSIMD(
                            FDPart3tmp440, FDPart3tmp443,
                            FusedMulAddSIMD(
                                FDPart3tmp455, FDPart3tmp467,
                                FusedMulAddSIMD(
                                    FDPart3tmp40, FDPart3tmp445,
                                    FusedMulAddSIMD(
                                        FDPart3tmp430, FDPart3tmp49,
                                        FusedMulAddSIMD(
                                            FDPart3tmp34, FDPart3tmp454,
                                            FusedMulAddSIMD(
                                                FDPart3tmp34, FDPart3tmp466,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp30, FDPart3tmp458,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp33, FDPart3tmp463,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp28, FDPart3tmp447,
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp30, FDPart3tmp426,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp24, FDPart3tmp418,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp24, FDPart3tmp456,
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp20, FDPart3tmp439,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp20, FDPart3tmp468,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp13, FDPart3tmp434,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp13, FDPart3tmp467,
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp33, MulSIMD(FDPart3tmp35, FDPart3tmp422),
                                                                                            FusedMulSubSIMD(
                                                                                                FDPart3_Integer_2,
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp34, FDPart3tmp452,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp459, FDPart3tmp49,
                                                                                                        MulSIMD(FDPart3tmp33, FDPart3tmp462))),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3_Rational_1_2,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3_Integer_6,
                                                                                                        MulSIMD(FDPart3tmp48, FDPart3tmp71),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3_Integer_6,
                                                                                                            MulSIMD(FDPart3tmp50, FDPart3tmp65),
                                                                                                            MulSIMD(FDPart3tmp74,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3_Integer_12,
                                                                                                                        FDPart3tmp39,
                                                                                                                        MulSIMD(FDPart3_Integer_6,
                                                                                                                                FDPart3tmp41))))),
                                                                                                    MulSIMD(
                                                                                                        FDPart3_Rational_2_3,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp57, trK_dD1,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp63, trK_dD2,
                                                                                                                MulSIMD(
                                                                                                                    FDPart3tmp56,
                                                                                                                    trK_dD0)))))))))))))))))))))))))));
        const REAL_SIMD_ARRAY FDPart3tmp341 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp139, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp348 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp287, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp361 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp131, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp362 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp228, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp393 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp232, FDPart3tmp75));
        const REAL_SIMD_ARRAY FDPart3tmp395 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp290, FDPart3tmp75));
        const REAL_SIMD_ARRAY __RHS_exp_0 = FusedMulAddSIMD(
            FDPart3tmp51,
            FusedMulAddSIMD(
                FDPart3tmp61,
                FusedMulAddSIMD(
                    hDD01, lambdaU_dD01,
                    FusedMulAddSIMD(
                        FDPart3tmp63, FusedMulAddSIMD(FDPart3tmp114, FDPart3tmp75, MulSIMD(FDPart3tmp110, MulSIMD(FDPart3tmp117, FDPart3tmp87))),
                        FusedMulAddSIMD(
                            FDPart3tmp75,
                            FusedMulAddSIMD(
                                FDPart3tmp138, MulSIMD(FDPart3tmp194, FDPart3tmp98),
                                FusedMulAddSIMD(
                                    FDPart3_Integer_3, MulSIMD(FDPart3tmp163, FDPart3tmp63),
                                    FusedMulAddSIMD(
                                        FDPart3_Integer_3, MulSIMD(FDPart3tmp257, FDPart3tmp59),
                                        FusedMulAddSIMD(
                                            FDPart3tmp192, FDPart3tmp255,
                                            FusedMulAddSIMD(
                                                FDPart3tmp194, FDPart3tmp262,
                                                FusedMulAddSIMD(FDPart3tmp185, MulSIMD(FDPart3tmp217, FDPart3tmp98),
                                                                FusedMulAddSIMD(MulSIMD(FDPart3_Integer_3, FDPart3tmp260),
                                                                                MulSIMD(FDPart3tmp61, FDPart3tmp98),
                                                                                FusedMulAddSIMD(FDPart3tmp185, FDPart3tmp261,
                                                                                                MulSIMD(FDPart3tmp192, FDPart3tmp254))))))))),
                            FusedMulAddSIMD(
                                FDPart3tmp61,
                                FusedMulAddSIMD(FDPart3tmp213, FDPart3tmp217, MulSIMD(FDPart3tmp220, MulSIMD(FDPart3tmp75, FDPart3tmp97))),
                                FusedMulAddSIMD(
                                    FDPart3tmp63, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp242, MulSIMD(FDPart3tmp187, FDPart3tmp75)),
                                    FusedMulAddSIMD(
                                        FDPart3tmp59,
                                        FusedMulAddSIMD(FDPart3tmp205, FDPart3tmp75, MulSIMD(FDPart3tmp110, MulSIMD(FDPart3tmp117, FDPart3tmp73))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp61,
                                            FusedMulAddSIMD(FDPart3tmp138, FDPart3tmp210,
                                                            MulSIMD(FDPart3tmp141, MulSIMD(FDPart3tmp75, FDPart3tmp99))),
                                            FusedMulAddSIMD(
                                                FDPart3tmp57,
                                                FusedMulAddSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp217, FDPart3tmp87),
                                                                MulSIMD(FDPart3tmp220, MulSIMD(FDPart3tmp75, FDPart3tmp87))),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp59,
                                                    FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp246, MulSIMD(FDPart3tmp243, FDPart3tmp75)),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp57,
                                                        FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp238,
                                                                        MulSIMD(FDPart3tmp141, MulSIMD(FDPart3tmp75, FDPart3tmp89))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp57,
                                                            FusedMulAddSIMD(FDPart3tmp110, FDPart3tmp213,
                                                                            MulSIMD(FDPart3tmp105, MulSIMD(FDPart3tmp75, FDPart3tmp97))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp56,
                                                                FusedMulAddSIMD(FDPart3tmp232, FDPart3tmp75,
                                                                                MulSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp156, FDPart3tmp73))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp57, FusedMulAddSIMD(FDPart3tmp156, FDPart3tmp210, FDPart3tmp250),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp56,
                                                                        FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp203,
                                                                                        MulSIMD(FDPart3tmp201, FDPart3tmp75)),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp56,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp228, FDPart3tmp75,
                                                                                MulSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp230, FDPart3tmp87))),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp53,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp117, MulSIMD(FDPart3tmp138, FDPart3tmp73),
                                                                                    MulSIMD(FDPart3tmp141, MulSIMD(FDPart3tmp73, FDPart3tmp75))),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp56,
                                                                                    FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp200,
                                                                                                    MulSIMD(FDPart3tmp198, FDPart3tmp75)),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp53,
                                                                                        FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp219,
                                                                                                        MulSIMD(FDPart3tmp220,
                                                                                                                MulSIMD(FDPart3tmp70, FDPart3tmp75))),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp53,
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp110, FDPart3tmp210,
                                                                                                MulSIMD(FDPart3tmp107,
                                                                                                        MulSIMD(FDPart3tmp75, FDPart3tmp99))),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp0, lambdaU_dD11,
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp53,
                                                                                                    FusedMulAddSIMD(FDPart3tmp213, FDPart3tmp230,
                                                                                                                    FDPart3tmp248),
                                                                                                    FusedMulAddSIMD(
                                                                                                        hDD12, lambdaU_dD21,
                                                                                                        FusedMulSubSIMD(
                                                                                                            FDPart3_Integer_2,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp178,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                    MulSIMD(FDPart3tmp97, hDD01),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                        MulSIMD(FDPart3tmp99, hDD12),
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(FDPart3tmp0,
                                                                                                                                    FDPart3tmp98)))),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp183,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                        MulSIMD(FDPart3tmp87, hDD01),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(FDPart3tmp89,
                                                                                                                                    hDD12),
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3_Rational_1_2,
                                                                                                                                MulSIMD(
                                                                                                                                    FDPart3tmp0,
                                                                                                                                    FDPart3tmp88)))),
                                                                                                                    MulSIMD(
                                                                                                                        FDPart3tmp176,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(FDPart3tmp70,
                                                                                                                                    hDD01),
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3_Rational_1_2,
                                                                                                                                MulSIMD(FDPart3tmp73,
                                                                                                                                        hDD12),
                                                                                                                                MulSIMD(
                                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp0,
                                                                                                                                        FDPart3tmp72))))))),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp56, hDD_dDD1102,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp57, hDD_dDD1112,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp61, hDD_dDD1111,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp63,
                                                                                                                                hDD_dDD1122,
                                                                                                                                MulSIMD(
                                                                                                                                    FDPart3tmp59,
                                                                                                                                    hDD_dDD1100))),
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3tmp53,
                                                                                                                            hDD_dDD1101))))))))))))))))))))))))))),
                FusedMulAddSIMD(
                    FDPart3tmp57,
                    FusedMulAddSIMD(
                        FDPart3tmp61, FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp262, MulSIMD(FDPart3tmp141, MulSIMD(FDPart3tmp75, FDPart3tmp98))),
                        FusedMulAddSIMD(
                            FDPart3tmp57,
                            FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp163, FDPart3tmp75),
                                            MulSIMD(FDPart3tmp154, MulSIMD(FDPart3tmp75, FDPart3tmp98))),
                            FusedMulAddSIMD(
                                FDPart3tmp59, FusedMulAddSIMD(FDPart3tmp369, FDPart3tmp75, FDPart3tmp348),
                                FusedMulAddSIMD(
                                    FDPart3tmp56, FusedMulAddSIMD(FDPart3tmp364, FDPart3tmp75, FDPart3tmp341),
                                    FusedMulAddSIMD(
                                        FDPart3tmp57, FusedMulAddSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp260, FDPart3tmp93), FDPart3tmp166),
                                        FusedMulAddSIMD(
                                            FDPart3tmp53, FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp344, MulSIMD(FDPart3tmp374, FDPart3tmp75)),
                                            FusedMulAddSIMD(
                                                FDPart3tmp56, FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp377, FDPart3tmp113),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp63, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp174, FDPart3tmp161),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp75,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp61, FusedMulAddSIMD(FDPart3tmp220, FDPart3tmp87, FDPart3tmp401),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp61,
                                                                FusedMulAddSIMD(FDPart3tmp141, FDPart3tmp89,
                                                                                FusedMulAddSIMD(FDPart3tmp154, FDPart3tmp99,
                                                                                                MulSIMD(FDPart3tmp138, FDPart3tmp89))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp59, AddSIMD(FDPart3tmp266, AddSIMD(FDPart3tmp290, FDPart3tmp314)),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp59, AddSIMD(FDPart3tmp306, AddSIMD(FDPart3tmp330, FDPart3tmp386)),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp57,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp138, FDPart3tmp94,
                                                                                FusedMulAddSIMD(FDPart3tmp141, FDPart3tmp94, FDPart3tmp187)),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp57,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp156, FDPart3tmp89,
                                                                                    FusedMulAddSIMD(FDPart3tmp195, FDPart3tmp99, FDPart3tmp187)),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp57, AddSIMD(FDPart3tmp114, FDPart3tmp397),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp57,
                                                                                        FusedMulAddSIMD(FDPart3tmp220, FDPart3tmp92, FDPart3tmp402),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp56,
                                                                                            FusedMulAddSIMD(FDPart3tmp107, FDPart3tmp94,
                                                                                                            FusedMulAddSIMD(FDPart3tmp110,
                                                                                                                            FDPart3tmp94,
                                                                                                                            FDPart3tmp184)),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp56,
                                                                                                AddSIMD(FDPart3tmp186,
                                                                                                        AddSIMD(FDPart3tmp333, FDPart3tmp373)),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp56,
                                                                                                    AddSIMD(FDPart3tmp382, FDPart3tmp407),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp56,
                                                                                                        AddSIMD(
                                                                                                            FDPart3tmp106,
                                                                                                            AddSIMD(FDPart3tmp316, FDPart3tmp383)),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp53,
                                                                                                            AddSIMD(FDPart3tmp201,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp110,
                                                                                                                                    FDPart3tmp89,
                                                                                                                                    FDPart3tmp360)),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp53,
                                                                                                                AddSIMD(
                                                                                                                    FDPart3tmp322,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp220,
                                                                                                                                    FDPart3tmp81,
                                                                                                                                    FDPart3tmp380)),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp63,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp105,
                                                                                                                                    FDPart3tmp92,
                                                                                                                                    FDPart3tmp399),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp63,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp154,
                                                                                                                            FDPart3tmp94,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp156,
                                                                                                                                FDPart3tmp94,
                                                                                                                                FDPart3tmp197)),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp53,
                                                                                                                            AddSIMD(FDPart3tmp228,
                                                                                                                                    FDPart3tmp405),
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3tmp53,
                                                                                                                                AddSIMD(
                                                                                                                                    FDPart3tmp232,
                                                                                                                                    FDPart3tmp326))))))))))))))))))),
                                                        FusedMulAddSIMD(FDPart3tmp53,
                                                                        FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp254,
                                                                                        MulSIMD(FDPart3tmp107, MulSIMD(FDPart3tmp75, FDPart3tmp98))),
                                                                        FDPart3tmp413)))))))))),
                    FusedMulAddSIMD(
                        FDPart3tmp59,
                        FusedMulAddSIMD(
                            hDD01, lambdaU_dD10,
                            FusedMulAddSIMD(
                                FDPart3tmp63,
                                FusedMulAddSIMD(FDPart3tmp118, FDPart3tmp75, MulSIMD(FDPart3tmp105, MulSIMD(FDPart3tmp117, FDPart3tmp82))),
                                FusedMulAddSIMD(
                                    FDPart3tmp75,
                                    FusedMulAddSIMD(
                                        FDPart3tmp102, MulSIMD(FDPart3tmp130, FDPart3tmp192),
                                        FusedMulAddSIMD(
                                            FDPart3_Integer_3, MulSIMD(FDPart3tmp167, FDPart3tmp63),
                                            FusedMulAddSIMD(
                                                FDPart3_Integer_3, MulSIMD(FDPart3tmp243, FDPart3tmp61),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp194, FDPart3tmp306,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp194, FDPart3tmp307,
                                                        FusedMulAddSIMD(FDPart3tmp102, MulSIMD(FDPart3tmp185, FDPart3tmp227),
                                                                        FusedMulAddSIMD(MulSIMD(FDPart3_Integer_3, FDPart3tmp102),
                                                                                        MulSIMD(FDPart3tmp310, FDPart3tmp59),
                                                                                        FusedMulAddSIMD(FDPart3tmp185, FDPart3tmp311,
                                                                                                        MulSIMD(FDPart3tmp192, FDPart3tmp312))))))))),
                                    FusedMulAddSIMD(
                                        FDPart3tmp61,
                                        FusedMulAddSIMD(FDPart3tmp205, FDPart3tmp75, MulSIMD(FDPart3tmp105, MulSIMD(FDPart3tmp117, FDPart3tmp73))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp63, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp295, MulSIMD(FDPart3tmp189, FDPart3tmp75)),
                                            FusedMulAddSIMD(
                                                FDPart3tmp59,
                                                FusedMulAddSIMD(FDPart3tmp227, FDPart3tmp276,
                                                                MulSIMD(FDPart3tmp103, MulSIMD(FDPart3tmp230, FDPart3tmp75))),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp61,
                                                    FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp298, MulSIMD(FDPart3tmp257, FDPart3tmp75)),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp57,
                                                        FusedMulAddSIMD(FDPart3tmp290, FDPart3tmp75,
                                                                        MulSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp149, FDPart3tmp73))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp59,
                                                            FusedMulAddSIMD(FDPart3tmp130, FDPart3tmp273,
                                                                            MulSIMD(FDPart3tmp104, MulSIMD(FDPart3tmp133, FDPart3tmp75))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp57,
                                                                FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp268,
                                                                                MulSIMD(FDPart3tmp266, FDPart3tmp75)),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp57,
                                                                    FusedMulAddSIMD(FDPart3tmp287, FDPart3tmp75,
                                                                                    MulSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp220, FDPart3tmp82))),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp56,
                                                                        FusedMulAddSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp227, FDPart3tmp82),
                                                                                        MulSIMD(FDPart3tmp230, MulSIMD(FDPart3tmp75, FDPart3tmp82))),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp57,
                                                                            FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp265,
                                                                                            MulSIMD(FDPart3tmp263, FDPart3tmp75)),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp56,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3_Integer_2, FDPart3tmp285,
                                                                                    MulSIMD(FDPart3tmp133, MulSIMD(FDPart3tmp75, FDPart3tmp83))),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp56,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp105, FDPart3tmp276,
                                                                                        MulSIMD(FDPart3tmp103, MulSIMD(FDPart3tmp110, FDPart3tmp75))),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp53,
                                                                                        FusedMulAddSIMD(FDPart3tmp117,
                                                                                                        MulSIMD(FDPart3tmp130, FDPart3tmp73),
                                                                                                        MulSIMD(FDPart3tmp133,
                                                                                                                MulSIMD(FDPart3tmp73, FDPart3tmp75))),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp56,
                                                                                            FusedMulAddSIMD(FDPart3tmp149, FDPart3tmp273,
                                                                                                            FDPart3tmp302),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp53,
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3_Integer_2, FDPart3tmp278,
                                                                                                    MulSIMD(FDPart3tmp230,
                                                                                                            MulSIMD(FDPart3tmp72, FDPart3tmp75))),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp53,
                                                                                                    FusedMulAddSIMD(FDPart3tmp105, FDPart3tmp273,
                                                                                                                    MulSIMD(FDPart3tmp104,
                                                                                                                            MulSIMD(FDPart3tmp107,
                                                                                                                                    FDPart3tmp75))),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp3, lambdaU_dD00,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp53,
                                                                                                            FusedMulAddSIMD(FDPart3tmp220,
                                                                                                                            FDPart3tmp276,
                                                                                                                            FDPart3tmp300),
                                                                                                            FusedMulAddSIMD(
                                                                                                                hDD02, lambdaU_dD20,
                                                                                                                FusedMulSubSIMD(
                                                                                                                    FDPart3_Integer_2,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp178,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(FDPart3tmp72,
                                                                                                                                    hDD01),
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3_Rational_1_2,
                                                                                                                                MulSIMD(FDPart3tmp73,
                                                                                                                                        hDD02),
                                                                                                                                MulSIMD(
                                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp3,
                                                                                                                                        FDPart3tmp70)))),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp183,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3_Rational_1_2,
                                                                                                                                MulSIMD(FDPart3tmp82,
                                                                                                                                        hDD01),
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp83,
                                                                                                                                        hDD02),
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                                        MulSIMD(
                                                                                                                                            FDPart3tmp3,
                                                                                                                                            FDPart3tmp81)))),
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3tmp176,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp103,
                                                                                                                                        hDD01),
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                                        MulSIMD(
                                                                                                                                            FDPart3tmp104,
                                                                                                                                            hDD02),
                                                                                                                                        MulSIMD(
                                                                                                                                            FDPart3_Rational_1_2,
                                                                                                                                            MulSIMD(
                                                                                                                                                FDPart3tmp102,
                                                                                                                                                FDPart3tmp3))))))),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp56, hDD_dDD0002,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp57, hDD_dDD0012,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3_Rational_1_2,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp61,
                                                                                                                                    hDD_dDD0011,
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp63,
                                                                                                                                        hDD_dDD0022,
                                                                                                                                        MulSIMD(
                                                                                                                                            FDPart3tmp59,
                                                                                                                                            hDD_dDD0000))),
                                                                                                                                MulSIMD(
                                                                                                                                    FDPart3tmp53,
                                                                                                                                    hDD_dDD0001))))))))))))))))))))))))))),
                        FusedMulAddSIMD(
                            FDPart3tmp56,
                            FusedMulAddSIMD(
                                FDPart3tmp61, FusedMulAddSIMD(FDPart3tmp380, FDPart3tmp75, FDPart3tmp362),
                                FusedMulAddSIMD(
                                    FDPart3tmp57, FusedMulAddSIMD(FDPart3tmp383, FDPart3tmp75, FDPart3tmp361),
                                    FusedMulAddSIMD(
                                        FDPart3tmp59,
                                        FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp312, MulSIMD(FDPart3tmp102, MulSIMD(FDPart3tmp133, FDPart3tmp75))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp56,
                                            FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp167, FDPart3tmp75),
                                                            MulSIMD(FDPart3tmp102, MulSIMD(FDPart3tmp146, FDPart3tmp75))),
                                            FusedMulAddSIMD(
                                                FDPart3tmp57, FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp382, FDPart3tmp109),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp53, FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp331, MulSIMD(FDPart3tmp386, FDPart3tmp75)),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp56,
                                                        FusedMulAddSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp310, FDPart3tmp92), FDPart3tmp170),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp63, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp172, FDPart3tmp148),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp75,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp61, AddSIMD(FDPart3tmp201, AddSIMD(FDPart3tmp232, FDPart3tmp324)),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp61, AddSIMD(FDPart3tmp254, AddSIMD(FDPart3tmp343, FDPart3tmp374)),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp59, FusedMulAddSIMD(FDPart3tmp230, FDPart3tmp82, FDPart3tmp370),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp59,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp130, FDPart3tmp83,
                                                                                    FusedMulAddSIMD(FDPart3tmp133, FDPart3tmp83,
                                                                                                    MulSIMD(FDPart3tmp104, FDPart3tmp146))),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp57,
                                                                                    AddSIMD(FDPart3tmp184, AddSIMD(FDPart3tmp332, FDPart3tmp373)),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp57,
                                                                                        FusedMulAddSIMD(FDPart3tmp105, FDPart3tmp94,
                                                                                                        FusedMulAddSIMD(FDPart3tmp107, FDPart3tmp94,
                                                                                                                        FDPart3tmp186)),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp57, AddSIMD(FDPart3tmp377, FDPart3tmp378),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp57,
                                                                                                AddSIMD(FDPart3tmp111,
                                                                                                        AddSIMD(FDPart3tmp340, FDPart3tmp364)),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp56,
                                                                                                    FusedMulAddSIMD(FDPart3tmp104, FDPart3tmp195,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp149,
                                                                                                                                    FDPart3tmp83,
                                                                                                                                    FDPart3tmp189)),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp56,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp130, FDPart3tmp94,
                                                                                                            FusedMulAddSIMD(FDPart3tmp133,
                                                                                                                            FDPart3tmp94,
                                                                                                                            FDPart3tmp189)),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp56,
                                                                                                            AddSIMD(FDPart3tmp118, FDPart3tmp365),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp56,
                                                                                                                FusedMulAddSIMD(FDPart3tmp230,
                                                                                                                                FDPart3tmp93,
                                                                                                                                FDPart3tmp371),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp53,
                                                                                                                    AddSIMD(FDPart3tmp356,
                                                                                                                            FDPart3tmp369),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp53,
                                                                                                                        AddSIMD(FDPart3tmp266,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp105,
                                                                                                                                    FDPart3tmp83,
                                                                                                                                    FDPart3tmp337)),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp63,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp110,
                                                                                                                                FDPart3tmp93,
                                                                                                                                FDPart3tmp368),
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp63,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp146,
                                                                                                                                    FDPart3tmp94,
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp149,
                                                                                                                                        FDPart3tmp94,
                                                                                                                                        FDPart3tmp196)),
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp53,
                                                                                                                                    AddSIMD(
                                                                                                                                        FDPart3tmp287,
                                                                                                                                        FDPart3tmp375),
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp53,
                                                                                                                                        AddSIMD(
                                                                                                                                            FDPart3tmp290,
                                                                                                                                            FDPart3tmp357))))))))))))))))))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp53,
                                                                    FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp306,
                                                                                    MulSIMD(FDPart3tmp102, MulSIMD(FDPart3tmp107, FDPart3tmp75))),
                                                                    FDPart3tmp388)))))))))),
                            FusedMulAddSIMD(
                                FDPart3tmp57,
                                FusedMulAddSIMD(
                                    FDPart3tmp61, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp250, FDPart3tmp238),
                                    FusedMulAddSIMD(
                                        FDPart3tmp57,
                                        FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp187, FDPart3tmp75),
                                                        MulSIMD(FDPart3tmp138, MulSIMD(FDPart3tmp75, FDPart3tmp94))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp59, FusedMulAddSIMD(FDPart3tmp314, FDPart3tmp75, FDPart3tmp395),
                                            FusedMulAddSIMD(
                                                FDPart3tmp56, FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp373, MulSIMD(FDPart3tmp333, FDPart3tmp75)),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp57, FusedMulAddSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp195, FDPart3tmp99), FDPart3tmp242),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp53, FusedMulAddSIMD(FDPart3tmp325, FDPart3tmp75, FDPart3tmp393),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp56,
                                                            FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp184,
                                                                            MulSIMD(FDPart3tmp110, MulSIMD(FDPart3tmp75, FDPart3tmp94))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp63,
                                                                FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp197,
                                                                                MulSIMD(FDPart3tmp156, MulSIMD(FDPart3tmp75, FDPart3tmp94))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp75,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp61, FusedMulAddSIMD(FDPart3tmp105, FDPart3tmp97, FDPart3tmp401),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp61,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp138, FDPart3tmp98,
                                                                                FusedMulAddSIMD(FDPart3tmp141, FDPart3tmp98, FDPart3tmp262)),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp59,
                                                                                AddSIMD(FDPart3tmp263, AddSIMD(FDPart3tmp287, FDPart3tmp369)),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp59,
                                                                                    AddSIMD(FDPart3tmp307, AddSIMD(FDPart3tmp330, FDPart3tmp386)),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp57,
                                                                                        FusedMulAddSIMD(FDPart3tmp141, FDPart3tmp88,
                                                                                                        FusedMulAddSIMD(FDPart3tmp260, FDPart3tmp93,
                                                                                                                        FDPart3tmp163)),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp57,
                                                                                            FusedMulAddSIMD(FDPart3tmp154, FDPart3tmp98,
                                                                                                            FusedMulAddSIMD(FDPart3tmp156,
                                                                                                                            FDPart3tmp98,
                                                                                                                            FDPart3tmp163)),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp57, AddSIMD(FDPart3tmp114, FDPart3tmp402),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp57,
                                                                                                    FusedMulAddSIMD(FDPart3tmp149, FDPart3tmp97,
                                                                                                                    FDPart3tmp397),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp56,
                                                                                                        AddSIMD(FDPart3tmp111,
                                                                                                                FusedMulAddSIMD(FDPart3tmp107,
                                                                                                                                FDPart3tmp88,
                                                                                                                                FDPart3tmp377)),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp56,
                                                                                                            AddSIMD(FDPart3tmp316,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp149,
                                                                                                                                    FDPart3tmp70,
                                                                                                                                    FDPart3tmp383)),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp56,
                                                                                                                AddSIMD(FDPart3tmp131, FDPart3tmp407),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp56,
                                                                                                                    AddSIMD(FDPart3tmp139,
                                                                                                                            FDPart3tmp391),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp53,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp107,
                                                                                                                            FDPart3tmp98,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp110,
                                                                                                                                FDPart3tmp98,
                                                                                                                                FDPart3tmp254)),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp53,
                                                                                                                            AddSIMD(
                                                                                                                                FDPart3tmp255,
                                                                                                                                AddSIMD(
                                                                                                                                    FDPart3tmp344,
                                                                                                                                    FDPart3tmp374)),
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp63,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp149,
                                                                                                                                    FDPart3tmp87,
                                                                                                                                    FDPart3tmp399),
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp63,
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp154,
                                                                                                                                        FDPart3tmp88,
                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                            FDPart3tmp156,
                                                                                                                                            FDPart3tmp88,
                                                                                                                                            MulSIMD(
                                                                                                                                                FDPart3tmp138,
                                                                                                                                                FDPart3tmp93))),
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp53,
                                                                                                                                        AddSIMD(
                                                                                                                                            FDPart3tmp318,
                                                                                                                                            FDPart3tmp405),
                                                                                                                                        MulSIMD(
                                                                                                                                            FDPart3tmp53,
                                                                                                                                            AddSIMD(
                                                                                                                                                FDPart3tmp198,
                                                                                                                                                AddSIMD(
                                                                                                                                                    FDPart3tmp322,
                                                                                                                                                    FDPart3tmp380)))))))))))))))))))),
                                                                    FusedMulAddSIMD(FDPart3tmp53,
                                                                                    FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp360, FDPart3tmp203),
                                                                                    FDPart3tmp413)))))))))),
                                FusedMulAddSIMD(
                                    FDPart3tmp53,
                                    FusedMulAddSIMD(
                                        FDPart3tmp61,
                                        FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp261, MulSIMD(FDPart3tmp220, MulSIMD(FDPart3tmp75, FDPart3tmp98))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp57, FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp344, MulSIMD(FDPart3tmp343, FDPart3tmp75)),
                                            FusedMulAddSIMD(
                                                FDPart3tmp59, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp300, FDPart3tmp278),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp56, FusedMulAddSIMD(FDPart3tmp347, FDPart3tmp75, FDPart3tmp348),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp57,
                                                        FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp255,
                                                                        MulSIMD(FDPart3tmp105, MulSIMD(FDPart3tmp75, FDPart3tmp98))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp53,
                                                            FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp257, FDPart3tmp75),
                                                                            MulSIMD(FDPart3tmp227, MulSIMD(FDPart3tmp75, FDPart3tmp98))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp56, FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp349, FDPart3tmp265),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp63, FusedMulAddSIMD(FDPart3tmp340, FDPart3tmp75, FDPart3tmp341),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp75,
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp61, FusedMulAddSIMD(FDPart3tmp141, FDPart3tmp73, FDPart3tmp327),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp61,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp220, FDPart3tmp70,
                                                                                    FusedMulAddSIMD(FDPart3tmp227, FDPart3tmp97,
                                                                                                    MulSIMD(FDPart3tmp217, FDPart3tmp70))),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp59,
                                                                                    FusedMulAddSIMD(FDPart3tmp104, FDPart3tmp107, FDPart3tmp321),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp59,
                                                                                        FusedMulAddSIMD(FDPart3tmp102, FDPart3tmp227,
                                                                                                        FusedMulAddSIMD(FDPart3tmp102, FDPart3tmp230,
                                                                                                                        FDPart3tmp311)),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp57,
                                                                                            AddSIMD(FDPart3tmp198,
                                                                                                    FusedMulAddSIMD(FDPart3tmp110, FDPart3tmp70,
                                                                                                                    FDPart3tmp318)),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp57,
                                                                                                AddSIMD(FDPart3tmp228,
                                                                                                        FusedMulAddSIMD(FDPart3tmp220, FDPart3tmp81,
                                                                                                                        FDPart3tmp322)),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp57,
                                                                                                    AddSIMD(FDPart3tmp232, FDPart3tmp336),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp57,
                                                                                                        AddSIMD(FDPart3tmp324, FDPart3tmp326),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp56,
                                                                                                            AddSIMD(FDPart3tmp306,
                                                                                                                    AddSIMD(FDPart3tmp330,
                                                                                                                            FDPart3tmp331)),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp56,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp102, FDPart3tmp105,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp102,
                                                                                                                                    FDPart3tmp110,
                                                                                                                                    FDPart3tmp307)),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp56,
                                                                                                                    AddSIMD(FDPart3tmp337,
                                                                                                                            FDPart3tmp338),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp56,
                                                                                                                        AddSIMD(
                                                                                                                            FDPart3tmp266,
                                                                                                                            AddSIMD(FDPart3tmp313,
                                                                                                                                    FDPart3tmp314)),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp53,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp102,
                                                                                                                                FDPart3tmp217,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp102,
                                                                                                                                    FDPart3tmp220,
                                                                                                                                    FDPart3tmp243)),
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp53,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp230,
                                                                                                                                    FDPart3tmp70,
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp310,
                                                                                                                                        FDPart3tmp97,
                                                                                                                                        FDPart3tmp243)),
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp63,
                                                                                                                                    AddSIMD(
                                                                                                                                        FDPart3tmp106,
                                                                                                                                        AddSIMD(
                                                                                                                                            FDPart3tmp131,
                                                                                                                                            FDPart3tmp316)),
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp63,
                                                                                                                                        AddSIMD(
                                                                                                                                            FDPart3tmp186,
                                                                                                                                            AddSIMD(
                                                                                                                                                FDPart3tmp332,
                                                                                                                                                FDPart3tmp333)),
                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                            FDPart3tmp53,
                                                                                                                                            AddSIMD(
                                                                                                                                                FDPart3tmp205,
                                                                                                                                                FDPart3tmp317),
                                                                                                                                            MulSIMD(
                                                                                                                                                FDPart3tmp53,
                                                                                                                                                FusedMulAddSIMD(
                                                                                                                                                    FDPart3tmp104,
                                                                                                                                                    FDPart3tmp141,
                                                                                                                                                    FDPart3tmp329))))))))))))))))))),
                                                                        FusedMulAddSIMD(FDPart3tmp53,
                                                                                        FusedMulAddSIMD(FDPart3tmp103,
                                                                                                        MulSIMD(FDPart3tmp117, FDPart3tmp260),
                                                                                                        FDPart3tmp298),
                                                                                        FDPart3tmp352)))))))))),
                                    FusedMulAddSIMD(
                                        FDPart3tmp56,
                                        FusedMulAddSIMD(
                                            FDPart3tmp61, FusedMulAddSIMD(FDPart3tmp324, FDPart3tmp75, FDPart3tmp393),
                                            FusedMulAddSIMD(
                                                FDPart3tmp57, FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp373, MulSIMD(FDPart3tmp332, FDPart3tmp75)),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp59, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp302, FDPart3tmp285),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp56,
                                                        FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp189, FDPart3tmp75),
                                                                        MulSIMD(FDPart3tmp130, MulSIMD(FDPart3tmp75, FDPart3tmp94))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp57,
                                                            FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp186,
                                                                            MulSIMD(FDPart3tmp105, MulSIMD(FDPart3tmp75, FDPart3tmp94))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp53, FusedMulAddSIMD(FDPart3tmp313, FDPart3tmp75, FDPart3tmp395),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp56,
                                                                    FusedMulAddSIMD(FDPart3tmp104, MulSIMD(FDPart3tmp117, FDPart3tmp195),
                                                                                    FDPart3tmp295),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp63,
                                                                        FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp196,
                                                                                        MulSIMD(FDPart3tmp149, MulSIMD(FDPart3tmp75, FDPart3tmp94))),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp75,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp61,
                                                                                AddSIMD(FDPart3tmp198, AddSIMD(FDPart3tmp228, FDPart3tmp380)),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp61,
                                                                                    AddSIMD(FDPart3tmp255, AddSIMD(FDPart3tmp343, FDPart3tmp374)),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp59,
                                                                                        FusedMulAddSIMD(FDPart3tmp103, FDPart3tmp110, FDPart3tmp370),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp59,
                                                                                            FusedMulAddSIMD(FDPart3tmp102, FDPart3tmp130,
                                                                                                            FusedMulAddSIMD(FDPart3tmp102,
                                                                                                                            FDPart3tmp133,
                                                                                                                            FDPart3tmp312)),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp57,
                                                                                                AddSIMD(FDPart3tmp106,
                                                                                                        FusedMulAddSIMD(FDPart3tmp107, FDPart3tmp81,
                                                                                                                        FDPart3tmp382)),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp57,
                                                                                                    AddSIMD(FDPart3tmp131,
                                                                                                            FusedMulAddSIMD(FDPart3tmp149,
                                                                                                                            FDPart3tmp70,
                                                                                                                            FDPart3tmp383)),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp57,
                                                                                                        AddSIMD(FDPart3tmp139, FDPart3tmp378),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp57,
                                                                                                            AddSIMD(FDPart3tmp340, FDPart3tmp391),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp56,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp102, FDPart3tmp146,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp102,
                                                                                                                                    FDPart3tmp149,
                                                                                                                                    FDPart3tmp167)),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp56,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp133, FDPart3tmp81,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp310,
                                                                                                                            FDPart3tmp92,
                                                                                                                            FDPart3tmp167)),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp56,
                                                                                                                        AddSIMD(FDPart3tmp118,
                                                                                                                                FDPart3tmp371),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp56,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp103,
                                                                                                                                FDPart3tmp156,
                                                                                                                                FDPart3tmp365),
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp53,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp102,
                                                                                                                                    FDPart3tmp105,
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp102,
                                                                                                                                        FDPart3tmp107,
                                                                                                                                        FDPart3tmp306)),
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp53,
                                                                                                                                    AddSIMD(
                                                                                                                                        FDPart3tmp307,
                                                                                                                                        AddSIMD(
                                                                                                                                            FDPart3tmp331,
                                                                                                                                            FDPart3tmp386)),
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp63,
                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                            FDPart3tmp156,
                                                                                                                                            FDPart3tmp82,
                                                                                                                                            FDPart3tmp368),
                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                            FDPart3tmp63,
                                                                                                                                            FusedMulAddSIMD(
                                                                                                                                                FDPart3tmp146,
                                                                                                                                                FDPart3tmp81,
                                                                                                                                                FusedMulAddSIMD(
                                                                                                                                                    FDPart3tmp149,
                                                                                                                                                    FDPart3tmp81,
                                                                                                                                                    MulSIMD(
                                                                                                                                                        FDPart3tmp130,
                                                                                                                                                        FDPart3tmp92))),
                                                                                                                                            FusedMulAddSIMD(
                                                                                                                                                FDPart3tmp53,
                                                                                                                                                AddSIMD(
                                                                                                                                                    FDPart3tmp349,
                                                                                                                                                    FDPart3tmp375),
                                                                                                                                                MulSIMD(
                                                                                                                                                    FDPart3tmp53,
                                                                                                                                                    AddSIMD(
                                                                                                                                                        FDPart3tmp263,
                                                                                                                                                        AddSIMD(
                                                                                                                                                            FDPart3tmp347,
                                                                                                                                                            FDPart3tmp369)))))))))))))))))))),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp53,
                                                                                FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp337, FDPart3tmp268),
                                                                                FDPart3tmp388)))))))))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp63,
                                            FusedMulAddSIMD(
                                                hDD02, lambdaU_dD02,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp63,
                                                    FusedMulAddSIMD(FDPart3tmp126, FDPart3tmp146,
                                                                    MulSIMD(FDPart3tmp149, MulSIMD(FDPart3tmp75, FDPart3tmp92))),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp75,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp146, MulSIMD(FDPart3tmp192, FDPart3tmp94),
                                                            FusedMulAddSIMD(
                                                                FDPart3_Integer_3, MulSIMD(FDPart3tmp187, FDPart3tmp61),
                                                                FusedMulAddSIMD(
                                                                    FDPart3_Integer_3, MulSIMD(FDPart3tmp189, FDPart3tmp59),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp192, FDPart3tmp196,
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp194, FDPart3tmp197,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp154, MulSIMD(FDPart3tmp194, FDPart3tmp94),
                                                                                FusedMulAddSIMD(
                                                                                    MulSIMD(FDPart3_Integer_3, FDPart3tmp195),
                                                                                    MulSIMD(FDPart3tmp63, FDPart3tmp94),
                                                                                    FusedMulAddSIMD(FDPart3tmp184, FDPart3tmp185,
                                                                                                    MulSIMD(FDPart3tmp185, FDPart3tmp186))))))))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp61,
                                                            FusedMulAddSIMD(FDPart3tmp114, FDPart3tmp75,
                                                                            MulSIMD(FDPart3tmp107, MulSIMD(FDPart3tmp117, FDPart3tmp87))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp63,
                                                                FusedMulAddSIMD(FDPart3tmp123, FDPart3tmp154,
                                                                                MulSIMD(FDPart3tmp156, MulSIMD(FDPart3tmp75, FDPart3tmp93))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp59,
                                                                    FusedMulAddSIMD(FDPart3tmp118, FDPart3tmp75,
                                                                                    MulSIMD(FDPart3tmp107, MulSIMD(FDPart3tmp117, FDPart3tmp82))),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp61,
                                                                        FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp166,
                                                                                        MulSIMD(FDPart3tmp163, FDPart3tmp75)),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp57,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp117, MulSIMD(FDPart3tmp146, FDPart3tmp87),
                                                                                MulSIMD(FDPart3tmp149, MulSIMD(FDPart3tmp75, FDPart3tmp87))),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp59,
                                                                                FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp170,
                                                                                                MulSIMD(FDPart3tmp167, FDPart3tmp75)),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp57,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3_Integer_2, FDPart3tmp161,
                                                                                        MulSIMD(FDPart3tmp156, MulSIMD(FDPart3tmp75, FDPart3tmp88))),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp57,
                                                                                        FusedMulAddSIMD(FDPart3tmp107, FDPart3tmp126,
                                                                                                        MulSIMD(FDPart3tmp105,
                                                                                                                MulSIMD(FDPart3tmp75, FDPart3tmp92))),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp56,
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp117, MulSIMD(FDPart3tmp154, FDPart3tmp82),
                                                                                                MulSIMD(FDPart3tmp156,
                                                                                                        MulSIMD(FDPart3tmp75, FDPart3tmp82))),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp57,
                                                                                                FusedMulAddSIMD(FDPart3tmp123, FDPart3tmp141,
                                                                                                                FDPart3tmp174),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp56,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3_Integer_2, FDPart3tmp148,
                                                                                                        MulSIMD(FDPart3tmp149,
                                                                                                                MulSIMD(FDPart3tmp75, FDPart3tmp81))),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp56,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp107, FDPart3tmp123,
                                                                                                            MulSIMD(
                                                                                                                FDPart3tmp110,
                                                                                                                MulSIMD(FDPart3tmp75, FDPart3tmp93))),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp53,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp139, FDPart3tmp75,
                                                                                                                MulSIMD(FDPart3tmp117,
                                                                                                                        MulSIMD(FDPart3tmp141,
                                                                                                                                FDPart3tmp82))),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp56,
                                                                                                                FusedMulAddSIMD(FDPart3tmp126,
                                                                                                                                FDPart3tmp133,
                                                                                                                                FDPart3tmp172),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp53,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3_Integer_2,
                                                                                                                        FDPart3tmp113,
                                                                                                                        MulSIMD(FDPart3tmp111,
                                                                                                                                FDPart3tmp75)),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp53,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp131,
                                                                                                                            FDPart3tmp75,
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3tmp117,
                                                                                                                                MulSIMD(
                                                                                                                                    FDPart3tmp133,
                                                                                                                                    FDPart3tmp87))),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp4, lambdaU_dD22,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp53,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3_Integer_2,
                                                                                                                                    FDPart3tmp109,
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp106,
                                                                                                                                        FDPart3tmp75)),
                                                                                                                                FusedMulAddSIMD(hDD12,
                                                                                                                                                lambdaU_dD12,
                                                                                                                                                FusedMulSubSIMD(FDPart3_Integer_2,
                                                                                                                                                                FusedMulAddSIMD(FDPart3tmp178,
                                                                                                                                                                                FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp87, hDD02), FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp88, hDD12), MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp4, FDPart3tmp89)))), FusedMulAddSIMD(FDPart3tmp183, FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp92, hDD02), FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp93, hDD12), MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp4, FDPart3tmp94)))), MulSIMD(FDPart3tmp176, FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp81, hDD02), FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp82, hDD12), MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp4, FDPart3tmp83))))))),
                                                                                                                                                                FusedMulAddSIMD(
                                                                                                                                                                    FDPart3tmp56,
                                                                                                                                                                    hDD_dDD2202,
                                                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                                                        FDPart3tmp57,
                                                                                                                                                                        hDD_dDD2212, FusedMulAddSIMD(FDPart3_Rational_1_2, FusedMulAddSIMD(FDPart3tmp61, hDD_dDD2211, FusedMulAddSIMD(FDPart3tmp63, hDD_dDD2222, MulSIMD(FDPart3tmp59, hDD_dDD2200))), MulSIMD(FDPart3tmp53, hDD_dDD2201))))))))))))))))))))))))))),
                                            FusedMulSubSIMD(
                                                FDPart3tmp53,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp61, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp248, FDPart3tmp219),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp57, FusedMulAddSIMD(FDPart3tmp322, FDPart3tmp75, FDPart3tmp362),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp59,
                                                            FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp311,
                                                                            MulSIMD(FDPart3tmp102, MulSIMD(FDPart3tmp230, FDPart3tmp75))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp56,
                                                                FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp331, MulSIMD(FDPart3tmp330, FDPart3tmp75)),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp57, FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp318, FDPart3tmp200),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp53,
                                                                        FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp243, FDPart3tmp75),
                                                                                        MulSIMD(FDPart3tmp102, MulSIMD(FDPart3tmp217, FDPart3tmp75))),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp56,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp117, FDPart3tmp307,
                                                                                MulSIMD(FDPart3tmp102, MulSIMD(FDPart3tmp110, FDPart3tmp75))),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp63,
                                                                                FusedMulAddSIMD(FDPart3tmp316, FDPart3tmp75, FDPart3tmp361),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp75,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp61,
                                                                                        FusedMulAddSIMD(FDPart3tmp107, FDPart3tmp99, FDPart3tmp327),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp61,
                                                                                            FusedMulAddSIMD(FDPart3tmp217, FDPart3tmp98,
                                                                                                            FusedMulAddSIMD(FDPart3tmp220,
                                                                                                                            FDPart3tmp98,
                                                                                                                            FDPart3tmp261)),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp59,
                                                                                                FusedMulAddSIMD(FDPart3tmp133, FDPart3tmp73,
                                                                                                                FDPart3tmp321),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp59,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp227, FDPart3tmp72,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp230, FDPart3tmp72,
                                                                                                            MulSIMD(FDPart3tmp103, FDPart3tmp217))),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp57,
                                                                                                        AddSIMD(
                                                                                                            FDPart3tmp254,
                                                                                                            AddSIMD(FDPart3tmp343, FDPart3tmp344)),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp57,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp105, FDPart3tmp98,
                                                                                                                FusedMulAddSIMD(FDPart3tmp110,
                                                                                                                                FDPart3tmp98,
                                                                                                                                FDPart3tmp255)),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp57,
                                                                                                                AddSIMD(FDPart3tmp336, FDPart3tmp360),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp57,
                                                                                                                    AddSIMD(FDPart3tmp201,
                                                                                                                            AddSIMD(FDPart3tmp324,
                                                                                                                                    FDPart3tmp325)),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp56,
                                                                                                                        AddSIMD(FDPart3tmp314,
                                                                                                                                FDPart3tmp357),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp56,
                                                                                                                            AddSIMD(
                                                                                                                                FDPart3tmp263,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp105,
                                                                                                                                    FDPart3tmp72,
                                                                                                                                    FDPart3tmp349)),
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp56,
                                                                                                                                AddSIMD(
                                                                                                                                    FDPart3tmp287,
                                                                                                                                    FDPart3tmp356),
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp56,
                                                                                                                                    AddSIMD(
                                                                                                                                        FDPart3tmp290,
                                                                                                                                        FDPart3tmp338),
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp53,
                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                            FDPart3tmp103,
                                                                                                                                            FDPart3tmp260,
                                                                                                                                            FusedMulAddSIMD(
                                                                                                                                                FDPart3tmp220,
                                                                                                                                                FDPart3tmp72,
                                                                                                                                                FDPart3tmp257)),
                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                            FDPart3tmp53,
                                                                                                                                            FusedMulAddSIMD(
                                                                                                                                                FDPart3tmp227,
                                                                                                                                                FDPart3tmp98,
                                                                                                                                                FusedMulAddSIMD(
                                                                                                                                                    FDPart3tmp230,
                                                                                                                                                    FDPart3tmp98,
                                                                                                                                                    FDPart3tmp257)),
                                                                                                                                            FusedMulAddSIMD(
                                                                                                                                                FDPart3tmp63,
                                                                                                                                                AddSIMD(
                                                                                                                                                    FDPart3tmp111,
                                                                                                                                                    AddSIMD(
                                                                                                                                                        FDPart3tmp139,
                                                                                                                                                        FDPart3tmp340)),
                                                                                                                                                FusedMulAddSIMD(
                                                                                                                                                    FDPart3tmp63,
                                                                                                                                                    AddSIMD(
                                                                                                                                                        FDPart3tmp184,
                                                                                                                                                        AddSIMD(
                                                                                                                                                            FDPart3tmp332,
                                                                                                                                                            FDPart3tmp333)),
                                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                                        FDPart3tmp53,
                                                                                                                                                        AddSIMD(
                                                                                                                                                            FDPart3tmp205,
                                                                                                                                                            FDPart3tmp329),
                                                                                                                                                        MulSIMD(
                                                                                                                                                            FDPart3tmp53,
                                                                                                                                                            FusedMulAddSIMD(
                                                                                                                                                                FDPart3tmp133,
                                                                                                                                                                FDPart3tmp99,
                                                                                                                                                                FDPart3tmp317))))))))))))))))))),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp53,
                                                                                        FusedMulAddSIMD(FDPart3tmp117,
                                                                                                        MulSIMD(FDPart3tmp310, FDPart3tmp97),
                                                                                                        FDPart3tmp246),
                                                                                        FDPart3tmp352)))))))))),
                                                FusedMulAddSIMD(
                                                    FDPart3_Integer_4,
                                                    FusedMulAddSIMD(
                                                        MulSIMD(FDPart3tmp54, FDPart3tmp56), MulSIMD(cf_dD0, cf_dD2),
                                                        FusedMulAddSIMD(MulSIMD(FDPart3tmp54, FDPart3tmp57), MulSIMD(cf_dD1, cf_dD2),
                                                                        MulSIMD(MulSIMD(FDPart3tmp53, FDPart3tmp54), MulSIMD(cf_dD0, cf_dD1)))),
                                                    FusedMulAddSIMD(
                                                        FDPart3_Integer_8,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp61,
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp66, FusedMulSubSIMD(FDPart3tmp64, MulSIMD(cf_dD1, cf_dD1), cf_dDD11),
                                                                MulSIMD(FDPart3tmp76,
                                                                        FusedMulAddSIMD(FDPart3tmp71, FDPart3tmp97,
                                                                                        FusedMulAddSIMD(FDPart3tmp74, FDPart3tmp99,
                                                                                                        MulSIMD(FDPart3tmp65, FDPart3tmp98))))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp63,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp66, FusedMulSubSIMD(FDPart3tmp64, MulSIMD(cf_dD2, cf_dD2), cf_dDD22),
                                                                    MulSIMD(FDPart3tmp76,
                                                                            FusedMulAddSIMD(FDPart3tmp71, FDPart3tmp92,
                                                                                            FusedMulAddSIMD(FDPart3tmp74, FDPart3tmp94,
                                                                                                            MulSIMD(FDPart3tmp65, FDPart3tmp93))))),
                                                                MulSIMD(FDPart3tmp59,
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp66,
                                                                            FusedMulSubSIMD(FDPart3tmp64, MulSIMD(cf_dD0, cf_dD0), cf_dDD00),
                                                                            MulSIMD(FDPart3tmp76,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp103, FDPart3tmp65,
                                                                                        FusedMulAddSIMD(FDPart3tmp104, FDPart3tmp74,
                                                                                                        MulSIMD(FDPart3tmp102, FDPart3tmp71)))))))),
                                                        FusedMulAddSIMD(
                                                            FDPart3_Integer_16,
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp56,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp66, FusedMulSubSIMD(FDPart3tmp71, cf_dD2, cf_dDD02),
                                                                    MulSIMD(FDPart3tmp76,
                                                                            FusedMulAddSIMD(FDPart3tmp71, FDPart3tmp81,
                                                                                            FusedMulAddSIMD(FDPart3tmp74, FDPart3tmp83,
                                                                                                            MulSIMD(FDPart3tmp65, FDPart3tmp82))))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp57,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp66, FusedMulSubSIMD(FDPart3tmp65, cf_dD2, cf_dDD12),
                                                                        MulSIMD(
                                                                            FDPart3tmp76,
                                                                            FusedMulAddSIMD(FDPart3tmp71, FDPart3tmp87,
                                                                                            FusedMulAddSIMD(FDPart3tmp74, FDPart3tmp89,
                                                                                                            MulSIMD(FDPart3tmp65, FDPart3tmp88))))),
                                                                    MulSIMD(FDPart3tmp53,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp66, FusedMulSubSIMD(FDPart3tmp65, cf_dD0, cf_dDD01),
                                                                                MulSIMD(FDPart3tmp76,
                                                                                        FusedMulAddSIMD(FDPart3tmp70, FDPart3tmp71,
                                                                                                        FusedMulAddSIMD(FDPart3tmp73, FDPart3tmp74,
                                                                                                                        MulSIMD(FDPart3tmp65,
                                                                                                                                FDPart3tmp72)))))))),
                                                            MulSIMD(FDPart3_Integer_2,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp54, MulSIMD(FDPart3tmp61, MulSIMD(cf_dD1, cf_dD1)),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp54, MulSIMD(FDPart3tmp63, MulSIMD(cf_dD2, cf_dD2)),
                                                                            MulSIMD(FDPart3tmp54,
                                                                                    MulSIMD(FDPart3tmp59, MulSIMD(cf_dD0, cf_dD0)))))))))))))))))),
            FusedMulSubSIMD(
                FDPart3_Rational_2_3, MulSIMD(trK, trK),
                FusedMulAddSIMD(aDD11, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp29, FDPart3tmp32),
                                FusedMulAddSIMD(aDD22, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp39, FDPart3tmp41),
                                                FusedMulAddSIMD(FDPart3_Integer_2,
                                                                FusedMulAddSIMD(FDPart3tmp48, aDD02,
                                                                                FusedMulAddSIMD(FDPart3tmp50, aDD12, MulSIMD(FDPart3tmp47, aDD01))),
                                                                MulSIMD(aDD00, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp18, FDPart3tmp22)))))));
        const REAL_SIMD_ARRAY __RHS_exp_1 = MulSIMD(FDPart3tmp469, FDPart3tmp51);
        const REAL_SIMD_ARRAY __RHS_exp_2 = MulSIMD(FDPart3tmp470, FDPart3tmp51);
        const REAL_SIMD_ARRAY __RHS_exp_3 = MulSIMD(FDPart3tmp472, FDPart3tmp51);
        const REAL_SIMD_ARRAY __RHS_exp_4 = FusedMulAddSIMD(
            FDPart3tmp3, MulSIMD(FDPart3tmp473, MulSIMD(FDPart3tmp469, FDPart3tmp469)),
            FusedMulAddSIMD(
                FDPart3tmp4, MulSIMD(FDPart3tmp473, MulSIMD(FDPart3tmp472, FDPart3tmp472)),
                FusedMulAddSIMD(FDPart3_Integer_2,
                                FusedMulAddSIMD(MulSIMD(FDPart3tmp469, FDPart3tmp472), MulSIMD(FDPart3tmp473, hDD02),
                                                FusedMulAddSIMD(MulSIMD(FDPart3tmp470, FDPart3tmp472), MulSIMD(FDPart3tmp473, hDD12),
                                                                MulSIMD(MulSIMD(FDPart3tmp469, FDPart3tmp470), MulSIMD(FDPart3tmp473, hDD01)))),
                                MulSIMD(FDPart3tmp0, MulSIMD(FDPart3tmp473, MulSIMD(FDPart3tmp470, FDPart3tmp470))))));

        WriteSIMD(&HGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_0);
        WriteSIMD(&MU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_1);
        WriteSIMD(&MU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_2);
        WriteSIMD(&MU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_3);
        WriteSIMD(&MSQUAREDGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_4);

      } // END LOOP: for (int i0 = cctk_nghostzones[0]; i0 < cctk_lsh[0]-cctk_nghostzones[0]; i0 += simd_width)
    } // END LOOP: for (int i1 = cctk_nghostzones[1]; i1 < cctk_lsh[1]-cctk_nghostzones[1]; i1++)
  } // END LOOP: for (int i2 = cctk_nghostzones[2]; i2 < cctk_lsh[2]-cctk_nghostzones[2]; i2++)
}
