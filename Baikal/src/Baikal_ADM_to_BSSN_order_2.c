#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "math.h"
/**
 * Converting from ADM to BSSN quantities is required in the Einstein Toolkit,
 * as initial data are given in terms of ADM quantities, and Baikal evolves the BSSN quantities.
 */
void Baikal_ADM_to_BSSN_order_2(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS_Baikal_ADM_to_BSSN_order_2;
  DECLARE_CCTK_PARAMETERS;

  const CCTK_REAL invdxx0 = 1.0 / CCTK_DELTA_SPACE(0);
  const CCTK_REAL invdxx1 = 1.0 / CCTK_DELTA_SPACE(1);
  const CCTK_REAL invdxx2 = 1.0 / CCTK_DELTA_SPACE(2);

#pragma omp parallel for
  for (int i2 = 0; i2 < cctk_lsh[2]; i2++) {
    for (int i1 = 0; i1 < cctk_lsh[1]; i1++) {
      for (int i0 = 0; i0 < cctk_lsh[0]; i0++) {
        alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = alp[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_betaU0 = betax[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_betaU1 = betay[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_betaU2 = betaz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_BU0 = dtbetax[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_BU1 = dtbetay[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_BU2 = dtbetaz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_gDD00 = gxx[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_gDD01 = gxy[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_gDD02 = gxz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_gDD11 = gyy[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_gDD12 = gyz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_gDD22 = gzz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_kDD00 = kxx[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_kDD01 = kxy[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_kDD02 = kxz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_kDD11 = kyy[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_kDD12 = kyz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL local_kDD22 = kzz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL tmp1 = local_gDD00 * ((local_gDD12) * (local_gDD12));
        const CCTK_REAL tmp3 = ((local_gDD01) * (local_gDD01)) * local_gDD22;
        const CCTK_REAL tmp5 = ((local_gDD02) * (local_gDD02)) * local_gDD11;
        const CCTK_REAL tmp6 = local_gDD00 * local_gDD11 * local_gDD22 + 2 * local_gDD01 * local_gDD02 * local_gDD12 - tmp1 - tmp3 - tmp5;
        const CCTK_REAL tmp7 = (1.0 / (tmp6));
        const CCTK_REAL tmp8 = 2 * tmp7;
        const CCTK_REAL tmp10 = cbrt(tmp7);
        const CCTK_REAL tmp9 = local_kDD00 * tmp7 * (local_gDD11 * local_gDD22 - ((local_gDD12) * (local_gDD12))) +
                               local_kDD01 * tmp8 * (-local_gDD01 * local_gDD22 + local_gDD02 * local_gDD12) +
                               local_kDD02 * tmp8 * (local_gDD01 * local_gDD12 - local_gDD02 * local_gDD11) +
                               local_kDD11 * tmp7 * (local_gDD00 * local_gDD22 - ((local_gDD02) * (local_gDD02))) +
                               local_kDD12 * tmp8 * (-local_gDD00 * local_gDD12 + local_gDD01 * local_gDD02) +
                               local_kDD22 * tmp7 * (local_gDD00 * local_gDD11 - ((local_gDD01) * (local_gDD01)));
        const CCTK_REAL tmp11 = (1.0 / 3.0) * tmp9;
        cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] =
            (1.0 / (sqrt(cbrt(tmp6 / (local_gDD00 * local_gDD11 * local_gDD22 * tmp7 + 2 * local_gDD01 * local_gDD02 * local_gDD12 * tmp7 -
                                      tmp1 * tmp7 - tmp3 * tmp7 - tmp5 * tmp7)))));
        trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = tmp9;
        vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = local_betaU0;
        vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = local_betaU1;
        vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = local_betaU2;
        betU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = local_BU0;
        betU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = local_BU1;
        betU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = local_BU2;
        hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = local_gDD00 * tmp10 - 1;
        hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = local_gDD01 * tmp10;
        hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = local_gDD02 * tmp10;
        hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = local_gDD11 * tmp10 - 1;
        hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = local_gDD12 * tmp10;
        hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = local_gDD22 * tmp10 - 1;
        aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = tmp10 * (-local_gDD00 * tmp11 + local_kDD00);
        aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = tmp10 * (-local_gDD01 * tmp11 + local_kDD01);
        aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = tmp10 * (-local_gDD02 * tmp11 + local_kDD02);
        aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = tmp10 * (-local_gDD11 * tmp11 + local_kDD11);
        aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = tmp10 * (-local_gDD12 * tmp11 + local_kDD12);
        aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = tmp10 * (-local_gDD22 * tmp11 + local_kDD22);
      } // END LOOP: for (int i0 = 0; i0 < cctk_lsh[0]; i0++)
    } // END LOOP: for (int i1 = 0; i1 < cctk_lsh[1]; i1++)
  } // END LOOP: for (int i2 = 0; i2 < cctk_lsh[2]; i2++)

#pragma omp parallel for
  for (int i2 = cctk_nghostzones[2]; i2 < cctk_lsh[2] - cctk_nghostzones[2]; i2++) {
    for (int i1 = cctk_nghostzones[1]; i1 < cctk_lsh[1] - cctk_nghostzones[1]; i1++) {
      for (int i0 = cctk_nghostzones[0]; i0 < cctk_lsh[0] - cctk_nghostzones[0]; i0++) {
        /*
         * NRPy+-Generated GF Access/FD Code, Step 1 of 2:
         * Read gridfunction(s) from main memory and compute FD stencils as needed.
         */
        const CCTK_REAL hDD00_i2m1 = hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)];
        const CCTK_REAL hDD00_i1m1 = hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)];
        const CCTK_REAL hDD00_i0m1 = hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)];
        const CCTK_REAL hDD00 = hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD00_i0p1 = hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)];
        const CCTK_REAL hDD00_i1p1 = hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)];
        const CCTK_REAL hDD00_i2p1 = hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)];
        const CCTK_REAL hDD01_i2m1 = hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)];
        const CCTK_REAL hDD01_i1m1 = hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)];
        const CCTK_REAL hDD01_i0m1 = hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)];
        const CCTK_REAL hDD01 = hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD01_i0p1 = hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)];
        const CCTK_REAL hDD01_i1p1 = hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)];
        const CCTK_REAL hDD01_i2p1 = hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)];
        const CCTK_REAL hDD02_i2m1 = hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)];
        const CCTK_REAL hDD02_i1m1 = hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)];
        const CCTK_REAL hDD02_i0m1 = hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)];
        const CCTK_REAL hDD02 = hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD02_i0p1 = hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)];
        const CCTK_REAL hDD02_i1p1 = hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)];
        const CCTK_REAL hDD02_i2p1 = hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)];
        const CCTK_REAL hDD11_i2m1 = hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)];
        const CCTK_REAL hDD11_i1m1 = hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)];
        const CCTK_REAL hDD11_i0m1 = hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)];
        const CCTK_REAL hDD11 = hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD11_i0p1 = hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)];
        const CCTK_REAL hDD11_i1p1 = hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)];
        const CCTK_REAL hDD11_i2p1 = hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)];
        const CCTK_REAL hDD12_i2m1 = hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)];
        const CCTK_REAL hDD12_i1m1 = hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)];
        const CCTK_REAL hDD12_i0m1 = hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)];
        const CCTK_REAL hDD12 = hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD12_i0p1 = hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)];
        const CCTK_REAL hDD12_i1p1 = hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)];
        const CCTK_REAL hDD12_i2p1 = hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)];
        const CCTK_REAL hDD22_i2m1 = hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)];
        const CCTK_REAL hDD22_i1m1 = hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)];
        const CCTK_REAL hDD22_i0m1 = hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)];
        const CCTK_REAL hDD22 = hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD22_i0p1 = hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)];
        const CCTK_REAL hDD22_i1p1 = hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)];
        const CCTK_REAL hDD22_i2p1 = hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)];
        const CCTK_REAL FDPart1_Rational_1_2 = 1.0 / 2.0;
        const CCTK_REAL FDPart1tmp0 = FDPart1_Rational_1_2 * invdxx0;
        const CCTK_REAL FDPart1tmp1 = FDPart1_Rational_1_2 * invdxx1;
        const CCTK_REAL FDPart1tmp2 = FDPart1_Rational_1_2 * invdxx2;
        const CCTK_REAL hDD_dD000 = FDPart1tmp0 * (-hDD00_i0m1 + hDD00_i0p1);
        const CCTK_REAL hDD_dD001 = FDPart1tmp1 * (-hDD00_i1m1 + hDD00_i1p1);
        const CCTK_REAL hDD_dD002 = FDPart1tmp2 * (-hDD00_i2m1 + hDD00_i2p1);
        const CCTK_REAL hDD_dD010 = FDPart1tmp0 * (-hDD01_i0m1 + hDD01_i0p1);
        const CCTK_REAL hDD_dD011 = FDPart1tmp1 * (-hDD01_i1m1 + hDD01_i1p1);
        const CCTK_REAL hDD_dD012 = FDPart1tmp2 * (-hDD01_i2m1 + hDD01_i2p1);
        const CCTK_REAL hDD_dD020 = FDPart1tmp0 * (-hDD02_i0m1 + hDD02_i0p1);
        const CCTK_REAL hDD_dD021 = FDPart1tmp1 * (-hDD02_i1m1 + hDD02_i1p1);
        const CCTK_REAL hDD_dD022 = FDPart1tmp2 * (-hDD02_i2m1 + hDD02_i2p1);
        const CCTK_REAL hDD_dD110 = FDPart1tmp0 * (-hDD11_i0m1 + hDD11_i0p1);
        const CCTK_REAL hDD_dD111 = FDPart1tmp1 * (-hDD11_i1m1 + hDD11_i1p1);
        const CCTK_REAL hDD_dD112 = FDPart1tmp2 * (-hDD11_i2m1 + hDD11_i2p1);
        const CCTK_REAL hDD_dD120 = FDPart1tmp0 * (-hDD12_i0m1 + hDD12_i0p1);
        const CCTK_REAL hDD_dD121 = FDPart1tmp1 * (-hDD12_i1m1 + hDD12_i1p1);
        const CCTK_REAL hDD_dD122 = FDPart1tmp2 * (-hDD12_i2m1 + hDD12_i2p1);
        const CCTK_REAL hDD_dD220 = FDPart1tmp0 * (-hDD22_i0m1 + hDD22_i0p1);
        const CCTK_REAL hDD_dD221 = FDPart1tmp1 * (-hDD22_i1m1 + hDD22_i1p1);
        const CCTK_REAL hDD_dD222 = FDPart1tmp2 * (-hDD22_i2m1 + hDD22_i2p1);

        /*
         * NRPy+-Generated GF Access/FD Code, Step 2 of 2:
         * Evaluate SymPy expressions and write to main memory.
         */
        const CCTK_REAL FDPart3tmp0 = hDD22 + 1;
        const CCTK_REAL FDPart3tmp4 = hDD11 + 1;
        const CCTK_REAL FDPart3tmp6 = hDD00 + 1;
        const CCTK_REAL FDPart3tmp12 = hDD_dD012 + hDD_dD021 - hDD_dD120;
        const CCTK_REAL FDPart3tmp18 = hDD_dD012 - hDD_dD021 + hDD_dD120;
        const CCTK_REAL FDPart3tmp20 = -hDD_dD012 + hDD_dD021 + hDD_dD120;
        const CCTK_REAL FDPart3tmp22 = 2 * hDD_dD122 - hDD_dD221;
        const CCTK_REAL FDPart3tmp23 = 2 * hDD_dD022 - hDD_dD220;
        const CCTK_REAL FDPart3tmp25 = -hDD_dD112 + 2 * hDD_dD121;
        const CCTK_REAL FDPart3tmp26 = 2 * hDD_dD011 - hDD_dD110;
        const CCTK_REAL FDPart3tmp28 = -hDD_dD001 + 2 * hDD_dD010;
        const CCTK_REAL FDPart3tmp29 = -hDD_dD002 + 2 * hDD_dD020;
        const CCTK_REAL FDPart3tmp1 = -FDPart3tmp0 * hDD01 + hDD02 * hDD12;
        const CCTK_REAL FDPart3tmp7 = (1.0 / (FDPart3tmp0 * FDPart3tmp4 * FDPart3tmp6 - FDPart3tmp0 * ((hDD01) * (hDD01)) -
                                              FDPart3tmp4 * ((hDD02) * (hDD02)) - FDPart3tmp6 * ((hDD12) * (hDD12)) + 2 * hDD01 * hDD02 * hDD12));
        const CCTK_REAL FDPart3tmp10 = -FDPart3tmp4 * hDD02 + hDD01 * hDD12;
        const CCTK_REAL FDPart3tmp15 = -FDPart3tmp6 * hDD12 + hDD01 * hDD02;
        const CCTK_REAL FDPart3tmp8 = (1.0 / 2.0) * FDPart3tmp7;
        const CCTK_REAL FDPart3tmp13 = FDPart3tmp7 * (FDPart3tmp0 * FDPart3tmp4 - ((hDD12) * (hDD12)));
        const CCTK_REAL FDPart3tmp16 = 2 * FDPart3tmp7;
        const CCTK_REAL FDPart3tmp24 = FDPart3tmp7 * (FDPart3tmp4 * FDPart3tmp6 - ((hDD01) * (hDD01)));
        const CCTK_REAL FDPart3tmp27 = FDPart3tmp7 * (FDPart3tmp0 * FDPart3tmp6 - ((hDD02) * (hDD02)));
        const CCTK_REAL FDPart3tmp9 = FDPart3tmp1 * FDPart3tmp8;
        const CCTK_REAL FDPart3tmp11 = FDPart3tmp10 * FDPart3tmp8;
        const CCTK_REAL FDPart3tmp14 = (1.0 / 2.0) * FDPart3tmp13;
        const CCTK_REAL FDPart3tmp17 = FDPart3tmp15 * FDPart3tmp16;
        const CCTK_REAL FDPart3tmp19 = FDPart3tmp10 * FDPart3tmp16;
        const CCTK_REAL FDPart3tmp21 = FDPart3tmp1 * FDPart3tmp16;
        const CCTK_REAL FDPart3tmp30 = FDPart3tmp15 * FDPart3tmp8;
        const CCTK_REAL FDPart3tmp31 = (1.0 / 2.0) * FDPart3tmp27;
        const CCTK_REAL FDPart3tmp32 = (1.0 / 2.0) * FDPart3tmp24;
        lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] =
            FDPart3tmp13 * (FDPart3tmp11 * FDPart3tmp29 + FDPart3tmp14 * hDD_dD000 + FDPart3tmp28 * FDPart3tmp9) +
            FDPart3tmp17 * (FDPart3tmp11 * hDD_dD221 + FDPart3tmp12 * FDPart3tmp14 + FDPart3tmp9 * hDD_dD112) +
            FDPart3tmp19 * (FDPart3tmp11 * hDD_dD220 + FDPart3tmp14 * hDD_dD002 + FDPart3tmp18 * FDPart3tmp9) +
            FDPart3tmp21 * (FDPart3tmp11 * FDPart3tmp20 + FDPart3tmp14 * hDD_dD001 + FDPart3tmp9 * hDD_dD110) +
            FDPart3tmp24 * (FDPart3tmp11 * hDD_dD222 + FDPart3tmp14 * FDPart3tmp23 + FDPart3tmp22 * FDPart3tmp9) +
            FDPart3tmp27 * (FDPart3tmp11 * FDPart3tmp25 + FDPart3tmp14 * FDPart3tmp26 + FDPart3tmp9 * hDD_dD111);
        lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] =
            FDPart3tmp13 * (FDPart3tmp28 * FDPart3tmp31 + FDPart3tmp29 * FDPart3tmp30 + FDPart3tmp9 * hDD_dD000) +
            FDPart3tmp17 * (FDPart3tmp12 * FDPart3tmp9 + FDPart3tmp30 * hDD_dD221 + FDPart3tmp31 * hDD_dD112) +
            FDPart3tmp19 * (FDPart3tmp18 * FDPart3tmp31 + FDPart3tmp30 * hDD_dD220 + FDPart3tmp9 * hDD_dD002) +
            FDPart3tmp21 * (FDPart3tmp20 * FDPart3tmp30 + FDPart3tmp31 * hDD_dD110 + FDPart3tmp9 * hDD_dD001) +
            FDPart3tmp24 * (FDPart3tmp22 * FDPart3tmp31 + FDPart3tmp23 * FDPart3tmp9 + FDPart3tmp30 * hDD_dD222) +
            FDPart3tmp27 * (FDPart3tmp25 * FDPart3tmp30 + FDPart3tmp26 * FDPart3tmp9 + FDPart3tmp31 * hDD_dD111);
        lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] =
            FDPart3tmp13 * (FDPart3tmp11 * hDD_dD000 + FDPart3tmp28 * FDPart3tmp30 + FDPart3tmp29 * FDPart3tmp32) +
            FDPart3tmp17 * (FDPart3tmp11 * FDPart3tmp12 + FDPart3tmp30 * hDD_dD112 + FDPart3tmp32 * hDD_dD221) +
            FDPart3tmp19 * (FDPart3tmp11 * hDD_dD002 + FDPart3tmp18 * FDPart3tmp30 + FDPart3tmp32 * hDD_dD220) +
            FDPart3tmp21 * (FDPart3tmp11 * hDD_dD001 + FDPart3tmp20 * FDPart3tmp32 + FDPart3tmp30 * hDD_dD110) +
            FDPart3tmp24 * (FDPart3tmp11 * FDPart3tmp23 + FDPart3tmp22 * FDPart3tmp30 + FDPart3tmp32 * hDD_dD222) +
            FDPart3tmp27 * (FDPart3tmp11 * FDPart3tmp26 + FDPart3tmp25 * FDPart3tmp32 + FDPart3tmp30 * hDD_dD111);

      } // END LOOP: for (int i0 = cctk_nghostzones[0]; i0 < cctk_lsh[0]-cctk_nghostzones[0]; i0++)
    } // END LOOP: for (int i1 = cctk_nghostzones[1]; i1 < cctk_lsh[1]-cctk_nghostzones[1]; i1++)
  } // END LOOP: for (int i2 = cctk_nghostzones[2]; i2 < cctk_lsh[2]-cctk_nghostzones[2]; i2++)

  ExtrapolateGammas(cctkGH, lambdaU0GF);
  ExtrapolateGammas(cctkGH, lambdaU1GF);
  ExtrapolateGammas(cctkGH, lambdaU2GF);
}
