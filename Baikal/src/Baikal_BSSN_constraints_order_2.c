#include "./simd/simd_intrinsics.h"
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "math.h"
/**
 * Finite difference function for operator dD0, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dD0_fdorder2(const REAL_SIMD_ARRAY FDPROTO_i0m1, const REAL_SIMD_ARRAY FDPROTO_i0p1,
                                                                             const REAL_SIMD_ARRAY invdxx0) {
  const double dblFDPart1_Rational_1_2 = 1.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_2 = ConstSIMD(dblFDPart1_Rational_1_2);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(FDPart1_Rational_1_2, MulSIMD(invdxx0, SubSIMD(FDPROTO_i0p1, FDPROTO_i0m1)));

  return FD_result;
}
/**
 * Finite difference function for operator dD1, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dD1_fdorder2(const REAL_SIMD_ARRAY FDPROTO_i1m1, const REAL_SIMD_ARRAY FDPROTO_i1p1,
                                                                             const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Rational_1_2 = 1.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_2 = ConstSIMD(dblFDPart1_Rational_1_2);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(FDPart1_Rational_1_2, MulSIMD(invdxx1, SubSIMD(FDPROTO_i1p1, FDPROTO_i1m1)));

  return FD_result;
}
/**
 * Finite difference function for operator dD2, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dD2_fdorder2(const REAL_SIMD_ARRAY FDPROTO_i2m1, const REAL_SIMD_ARRAY FDPROTO_i2p1,
                                                                             const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_2 = 1.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_2 = ConstSIMD(dblFDPart1_Rational_1_2);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(FDPart1_Rational_1_2, MulSIMD(invdxx2, SubSIMD(FDPROTO_i2p1, FDPROTO_i2m1)));

  return FD_result;
}
/**
 * Finite difference function for operator dDD00, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD00_fdorder2(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i0m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p1, const REAL_SIMD_ARRAY invdxx0) {
  const double dblFDPart1_Integer_2 = 2.0;
  const REAL_SIMD_ARRAY FDPart1_Integer_2 = ConstSIMD(dblFDPart1_Integer_2);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(invdxx0, invdxx0), AddSIMD(FDPROTO_i0p1, NegFusedMulAddSIMD(FDPROTO, FDPart1_Integer_2, FDPROTO_i0m1)));

  return FD_result;
}
/**
 * Finite difference function for operator dDD01, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD01_fdorder2(const REAL_SIMD_ARRAY FDPROTO_i0m1_i1m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0m1_i1p1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p1_i1m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p1_i1p1, const REAL_SIMD_ARRAY invdxx0,
                                                                               const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Rational_1_4 = 1.0 / 4.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_4 = ConstSIMD(dblFDPart1_Rational_1_4);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(FDPart1_Rational_1_4, invdxx0),
              MulSIMD(invdxx1, AddSIMD(FDPROTO_i0p1_i1p1, SubSIMD(FDPROTO_i0m1_i1m1, AddSIMD(FDPROTO_i0m1_i1p1, FDPROTO_i0p1_i1m1)))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD02, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD02_fdorder2(const REAL_SIMD_ARRAY FDPROTO_i0m1_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0m1_i2p1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p1_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p1_i2p1, const REAL_SIMD_ARRAY invdxx0,
                                                                               const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_4 = 1.0 / 4.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_4 = ConstSIMD(dblFDPart1_Rational_1_4);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(FDPart1_Rational_1_4, invdxx0),
              MulSIMD(invdxx2, AddSIMD(FDPROTO_i0p1_i2p1, SubSIMD(FDPROTO_i0m1_i2m1, AddSIMD(FDPROTO_i0m1_i2p1, FDPROTO_i0p1_i2m1)))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD11, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD11_fdorder2(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i1m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p1, const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Integer_2 = 2.0;
  const REAL_SIMD_ARRAY FDPart1_Integer_2 = ConstSIMD(dblFDPart1_Integer_2);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(invdxx1, invdxx1), AddSIMD(FDPROTO_i1p1, NegFusedMulAddSIMD(FDPROTO, FDPart1_Integer_2, FDPROTO_i1m1)));

  return FD_result;
}
/**
 * Finite difference function for operator dDD12, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD12_fdorder2(const REAL_SIMD_ARRAY FDPROTO_i1m1_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1m1_i2p1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p1_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p1_i2p1, const REAL_SIMD_ARRAY invdxx1,
                                                                               const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_4 = 1.0 / 4.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_4 = ConstSIMD(dblFDPart1_Rational_1_4);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(FDPart1_Rational_1_4, invdxx1),
              MulSIMD(invdxx2, AddSIMD(FDPROTO_i1p1_i2p1, SubSIMD(FDPROTO_i1m1_i2m1, AddSIMD(FDPROTO_i1m1_i2p1, FDPROTO_i1p1_i2m1)))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD22, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD22_fdorder2(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2p1, const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Integer_2 = 2.0;
  const REAL_SIMD_ARRAY FDPart1_Integer_2 = ConstSIMD(dblFDPart1_Integer_2);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(invdxx2, invdxx2), AddSIMD(FDPROTO_i2p1, NegFusedMulAddSIMD(FDPROTO, FDPart1_Integer_2, FDPROTO_i2m1)));

  return FD_result;
}

/**
 * Evaluate BSSN constraints.
 */
void Baikal_BSSN_constraints_order_2(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS_Baikal_BSSN_constraints_order_2;

  const REAL_SIMD_ARRAY invdxx0 CCTK_ATTRIBUTE_UNUSED = ConstSIMD(1.0 / CCTK_DELTA_SPACE(0));
  const REAL_SIMD_ARRAY invdxx1 CCTK_ATTRIBUTE_UNUSED = ConstSIMD(1.0 / CCTK_DELTA_SPACE(1));
  const REAL_SIMD_ARRAY invdxx2 CCTK_ATTRIBUTE_UNUSED = ConstSIMD(1.0 / CCTK_DELTA_SPACE(2));
  const CCTK_REAL *param_PI CCTK_ATTRIBUTE_UNUSED = CCTK_ParameterGet("PI", "Baikal", NULL);
  const REAL_SIMD_ARRAY PI CCTK_ATTRIBUTE_UNUSED = ConstSIMD(*param_PI);

#pragma omp parallel for
  for (int i2 = cctk_nghostzones[2]; i2 < cctk_lsh[2] - cctk_nghostzones[2]; i2++) {
    for (int i1 = cctk_nghostzones[1]; i1 < cctk_lsh[1] - cctk_nghostzones[1]; i1++) {
      for (int i0 = cctk_nghostzones[0]; i0 < cctk_lsh[0] - cctk_nghostzones[0]; i0 += simd_width) {
        /*
         * NRPy+-Generated GF Access/FD Code, Step 1 of 2:
         * Read gridfunction(s) from main memory and compute FD stencils as needed.
         */
        const REAL_SIMD_ARRAY T4UU00 = ReadSIMD(&T4UU00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY T4UU01 = ReadSIMD(&T4UU01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY T4UU02 = ReadSIMD(&T4UU02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY T4UU03 = ReadSIMD(&T4UU03GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i2m1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD00_i1m1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0m1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i0p1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i1p1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD00_i2p1 = ReadSIMD(&aDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD01_i2m1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD01_i1m1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0m1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i0p1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i1p1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD01_i2p1 = ReadSIMD(&aDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD02_i2m1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD02_i1m1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0m1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i0p1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i1p1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD02_i2p1 = ReadSIMD(&aDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD11_i2m1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD11_i1m1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0m1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i0p1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i1p1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD11_i2p1 = ReadSIMD(&aDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD12_i2m1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD12_i1m1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0m1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i0p1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i1p1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD12_i2p1 = ReadSIMD(&aDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY aDD22_i2m1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY aDD22_i1m1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0m1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i0p1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i1p1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY aDD22_i2p1 = ReadSIMD(&aDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY alpha = ReadSIMD(&alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY cf = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY cf_i0m1_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i0p1_i1p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY cf_i1m1_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0m1_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i0p1_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY cf_i1p1_i2p1 = ReadSIMD(&cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i1m1_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i1p1_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1m1_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i1p1_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i1m1_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i1p1_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1m1_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i1p1_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i1m1_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i1p1_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1m1_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i1p1_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i1m1_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i1p1_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1m1_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i1p1_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i1m1_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i1p1_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1m1_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i1p1_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i1m1_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i1p1_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1m1_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i1p1_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY lambdaU0_i2m1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY lambdaU0_i1m1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0m1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0p1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1p1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i2p1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY lambdaU1_i2m1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY lambdaU1_i1m1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0m1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0p1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1p1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i2p1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY lambdaU2_i2m1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY lambdaU2_i1m1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0m1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0p1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1p1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i2p1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY trK_i2m1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY trK_i1m1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY trK_i0m1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY trK = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i0p1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY trK_i1p1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY trK_i2p1 = ReadSIMD(&trKGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY vetU0 = ReadSIMD(&vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY vetU1 = ReadSIMD(&vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY vetU2 = ReadSIMD(&vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY aDD_dD000 = SIMD_fd_function_dD0_fdorder2(aDD00_i0m1, aDD00_i0p1, invdxx0);
        const REAL_SIMD_ARRAY aDD_dD001 = SIMD_fd_function_dD1_fdorder2(aDD00_i1m1, aDD00_i1p1, invdxx1);
        const REAL_SIMD_ARRAY aDD_dD002 = SIMD_fd_function_dD2_fdorder2(aDD00_i2m1, aDD00_i2p1, invdxx2);
        const REAL_SIMD_ARRAY aDD_dD010 = SIMD_fd_function_dD0_fdorder2(aDD01_i0m1, aDD01_i0p1, invdxx0);
        const REAL_SIMD_ARRAY aDD_dD011 = SIMD_fd_function_dD1_fdorder2(aDD01_i1m1, aDD01_i1p1, invdxx1);
        const REAL_SIMD_ARRAY aDD_dD012 = SIMD_fd_function_dD2_fdorder2(aDD01_i2m1, aDD01_i2p1, invdxx2);
        const REAL_SIMD_ARRAY aDD_dD020 = SIMD_fd_function_dD0_fdorder2(aDD02_i0m1, aDD02_i0p1, invdxx0);
        const REAL_SIMD_ARRAY aDD_dD021 = SIMD_fd_function_dD1_fdorder2(aDD02_i1m1, aDD02_i1p1, invdxx1);
        const REAL_SIMD_ARRAY aDD_dD022 = SIMD_fd_function_dD2_fdorder2(aDD02_i2m1, aDD02_i2p1, invdxx2);
        const REAL_SIMD_ARRAY aDD_dD110 = SIMD_fd_function_dD0_fdorder2(aDD11_i0m1, aDD11_i0p1, invdxx0);
        const REAL_SIMD_ARRAY aDD_dD111 = SIMD_fd_function_dD1_fdorder2(aDD11_i1m1, aDD11_i1p1, invdxx1);
        const REAL_SIMD_ARRAY aDD_dD112 = SIMD_fd_function_dD2_fdorder2(aDD11_i2m1, aDD11_i2p1, invdxx2);
        const REAL_SIMD_ARRAY aDD_dD120 = SIMD_fd_function_dD0_fdorder2(aDD12_i0m1, aDD12_i0p1, invdxx0);
        const REAL_SIMD_ARRAY aDD_dD121 = SIMD_fd_function_dD1_fdorder2(aDD12_i1m1, aDD12_i1p1, invdxx1);
        const REAL_SIMD_ARRAY aDD_dD122 = SIMD_fd_function_dD2_fdorder2(aDD12_i2m1, aDD12_i2p1, invdxx2);
        const REAL_SIMD_ARRAY aDD_dD220 = SIMD_fd_function_dD0_fdorder2(aDD22_i0m1, aDD22_i0p1, invdxx0);
        const REAL_SIMD_ARRAY aDD_dD221 = SIMD_fd_function_dD1_fdorder2(aDD22_i1m1, aDD22_i1p1, invdxx1);
        const REAL_SIMD_ARRAY aDD_dD222 = SIMD_fd_function_dD2_fdorder2(aDD22_i2m1, aDD22_i2p1, invdxx2);
        const REAL_SIMD_ARRAY cf_dD0 = SIMD_fd_function_dD0_fdorder2(cf_i0m1, cf_i0p1, invdxx0);
        const REAL_SIMD_ARRAY cf_dD1 = SIMD_fd_function_dD1_fdorder2(cf_i1m1, cf_i1p1, invdxx1);
        const REAL_SIMD_ARRAY cf_dD2 = SIMD_fd_function_dD2_fdorder2(cf_i2m1, cf_i2p1, invdxx2);
        const REAL_SIMD_ARRAY cf_dDD00 = SIMD_fd_function_dDD00_fdorder2(cf, cf_i0m1, cf_i0p1, invdxx0);
        const REAL_SIMD_ARRAY cf_dDD01 = SIMD_fd_function_dDD01_fdorder2(cf_i0m1_i1m1, cf_i0m1_i1p1, cf_i0p1_i1m1, cf_i0p1_i1p1, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY cf_dDD02 = SIMD_fd_function_dDD02_fdorder2(cf_i0m1_i2m1, cf_i0m1_i2p1, cf_i0p1_i2m1, cf_i0p1_i2p1, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY cf_dDD11 = SIMD_fd_function_dDD11_fdorder2(cf, cf_i1m1, cf_i1p1, invdxx1);
        const REAL_SIMD_ARRAY cf_dDD12 = SIMD_fd_function_dDD12_fdorder2(cf_i1m1_i2m1, cf_i1m1_i2p1, cf_i1p1_i2m1, cf_i1p1_i2p1, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY cf_dDD22 = SIMD_fd_function_dDD22_fdorder2(cf, cf_i2m1, cf_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD000 = SIMD_fd_function_dD0_fdorder2(hDD00_i0m1, hDD00_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD001 = SIMD_fd_function_dD1_fdorder2(hDD00_i1m1, hDD00_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD002 = SIMD_fd_function_dD2_fdorder2(hDD00_i2m1, hDD00_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD010 = SIMD_fd_function_dD0_fdorder2(hDD01_i0m1, hDD01_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD011 = SIMD_fd_function_dD1_fdorder2(hDD01_i1m1, hDD01_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD012 = SIMD_fd_function_dD2_fdorder2(hDD01_i2m1, hDD01_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD020 = SIMD_fd_function_dD0_fdorder2(hDD02_i0m1, hDD02_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD021 = SIMD_fd_function_dD1_fdorder2(hDD02_i1m1, hDD02_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD022 = SIMD_fd_function_dD2_fdorder2(hDD02_i2m1, hDD02_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD110 = SIMD_fd_function_dD0_fdorder2(hDD11_i0m1, hDD11_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD111 = SIMD_fd_function_dD1_fdorder2(hDD11_i1m1, hDD11_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD112 = SIMD_fd_function_dD2_fdorder2(hDD11_i2m1, hDD11_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD120 = SIMD_fd_function_dD0_fdorder2(hDD12_i0m1, hDD12_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD121 = SIMD_fd_function_dD1_fdorder2(hDD12_i1m1, hDD12_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD122 = SIMD_fd_function_dD2_fdorder2(hDD12_i2m1, hDD12_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD220 = SIMD_fd_function_dD0_fdorder2(hDD22_i0m1, hDD22_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD221 = SIMD_fd_function_dD1_fdorder2(hDD22_i1m1, hDD22_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD222 = SIMD_fd_function_dD2_fdorder2(hDD22_i2m1, hDD22_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0000 = SIMD_fd_function_dDD00_fdorder2(hDD00, hDD00_i0m1, hDD00_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD0001 =
            SIMD_fd_function_dDD01_fdorder2(hDD00_i0m1_i1m1, hDD00_i0m1_i1p1, hDD00_i0p1_i1m1, hDD00_i0p1_i1p1, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0002 =
            SIMD_fd_function_dDD02_fdorder2(hDD00_i0m1_i2m1, hDD00_i0m1_i2p1, hDD00_i0p1_i2m1, hDD00_i0p1_i2p1, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0011 = SIMD_fd_function_dDD11_fdorder2(hDD00, hDD00_i1m1, hDD00_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0012 =
            SIMD_fd_function_dDD12_fdorder2(hDD00_i1m1_i2m1, hDD00_i1m1_i2p1, hDD00_i1p1_i2m1, hDD00_i1p1_i2p1, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0022 = SIMD_fd_function_dDD22_fdorder2(hDD00, hDD00_i2m1, hDD00_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0100 = SIMD_fd_function_dDD00_fdorder2(hDD01, hDD01_i0m1, hDD01_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD0101 =
            SIMD_fd_function_dDD01_fdorder2(hDD01_i0m1_i1m1, hDD01_i0m1_i1p1, hDD01_i0p1_i1m1, hDD01_i0p1_i1p1, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0102 =
            SIMD_fd_function_dDD02_fdorder2(hDD01_i0m1_i2m1, hDD01_i0m1_i2p1, hDD01_i0p1_i2m1, hDD01_i0p1_i2p1, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0111 = SIMD_fd_function_dDD11_fdorder2(hDD01, hDD01_i1m1, hDD01_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0112 =
            SIMD_fd_function_dDD12_fdorder2(hDD01_i1m1_i2m1, hDD01_i1m1_i2p1, hDD01_i1p1_i2m1, hDD01_i1p1_i2p1, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0122 = SIMD_fd_function_dDD22_fdorder2(hDD01, hDD01_i2m1, hDD01_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0200 = SIMD_fd_function_dDD00_fdorder2(hDD02, hDD02_i0m1, hDD02_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD0201 =
            SIMD_fd_function_dDD01_fdorder2(hDD02_i0m1_i1m1, hDD02_i0m1_i1p1, hDD02_i0p1_i1m1, hDD02_i0p1_i1p1, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0202 =
            SIMD_fd_function_dDD02_fdorder2(hDD02_i0m1_i2m1, hDD02_i0m1_i2p1, hDD02_i0p1_i2m1, hDD02_i0p1_i2p1, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0211 = SIMD_fd_function_dDD11_fdorder2(hDD02, hDD02_i1m1, hDD02_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0212 =
            SIMD_fd_function_dDD12_fdorder2(hDD02_i1m1_i2m1, hDD02_i1m1_i2p1, hDD02_i1p1_i2m1, hDD02_i1p1_i2p1, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0222 = SIMD_fd_function_dDD22_fdorder2(hDD02, hDD02_i2m1, hDD02_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1100 = SIMD_fd_function_dDD00_fdorder2(hDD11, hDD11_i0m1, hDD11_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD1101 =
            SIMD_fd_function_dDD01_fdorder2(hDD11_i0m1_i1m1, hDD11_i0m1_i1p1, hDD11_i0p1_i1m1, hDD11_i0p1_i1p1, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD1102 =
            SIMD_fd_function_dDD02_fdorder2(hDD11_i0m1_i2m1, hDD11_i0m1_i2p1, hDD11_i0p1_i2m1, hDD11_i0p1_i2p1, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1111 = SIMD_fd_function_dDD11_fdorder2(hDD11, hDD11_i1m1, hDD11_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD1112 =
            SIMD_fd_function_dDD12_fdorder2(hDD11_i1m1_i2m1, hDD11_i1m1_i2p1, hDD11_i1p1_i2m1, hDD11_i1p1_i2p1, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1122 = SIMD_fd_function_dDD22_fdorder2(hDD11, hDD11_i2m1, hDD11_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1200 = SIMD_fd_function_dDD00_fdorder2(hDD12, hDD12_i0m1, hDD12_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD1201 =
            SIMD_fd_function_dDD01_fdorder2(hDD12_i0m1_i1m1, hDD12_i0m1_i1p1, hDD12_i0p1_i1m1, hDD12_i0p1_i1p1, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD1202 =
            SIMD_fd_function_dDD02_fdorder2(hDD12_i0m1_i2m1, hDD12_i0m1_i2p1, hDD12_i0p1_i2m1, hDD12_i0p1_i2p1, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1211 = SIMD_fd_function_dDD11_fdorder2(hDD12, hDD12_i1m1, hDD12_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD1212 =
            SIMD_fd_function_dDD12_fdorder2(hDD12_i1m1_i2m1, hDD12_i1m1_i2p1, hDD12_i1p1_i2m1, hDD12_i1p1_i2p1, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1222 = SIMD_fd_function_dDD22_fdorder2(hDD12, hDD12_i2m1, hDD12_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD2200 = SIMD_fd_function_dDD00_fdorder2(hDD22, hDD22_i0m1, hDD22_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD2201 =
            SIMD_fd_function_dDD01_fdorder2(hDD22_i0m1_i1m1, hDD22_i0m1_i1p1, hDD22_i0p1_i1m1, hDD22_i0p1_i1p1, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD2202 =
            SIMD_fd_function_dDD02_fdorder2(hDD22_i0m1_i2m1, hDD22_i0m1_i2p1, hDD22_i0p1_i2m1, hDD22_i0p1_i2p1, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD2211 = SIMD_fd_function_dDD11_fdorder2(hDD22, hDD22_i1m1, hDD22_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD2212 =
            SIMD_fd_function_dDD12_fdorder2(hDD22_i1m1_i2m1, hDD22_i1m1_i2p1, hDD22_i1p1_i2m1, hDD22_i1p1_i2p1, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD2222 = SIMD_fd_function_dDD22_fdorder2(hDD22, hDD22_i2m1, hDD22_i2p1, invdxx2);
        const REAL_SIMD_ARRAY lambdaU_dD00 = SIMD_fd_function_dD0_fdorder2(lambdaU0_i0m1, lambdaU0_i0p1, invdxx0);
        const REAL_SIMD_ARRAY lambdaU_dD01 = SIMD_fd_function_dD1_fdorder2(lambdaU0_i1m1, lambdaU0_i1p1, invdxx1);
        const REAL_SIMD_ARRAY lambdaU_dD02 = SIMD_fd_function_dD2_fdorder2(lambdaU0_i2m1, lambdaU0_i2p1, invdxx2);
        const REAL_SIMD_ARRAY lambdaU_dD10 = SIMD_fd_function_dD0_fdorder2(lambdaU1_i0m1, lambdaU1_i0p1, invdxx0);
        const REAL_SIMD_ARRAY lambdaU_dD11 = SIMD_fd_function_dD1_fdorder2(lambdaU1_i1m1, lambdaU1_i1p1, invdxx1);
        const REAL_SIMD_ARRAY lambdaU_dD12 = SIMD_fd_function_dD2_fdorder2(lambdaU1_i2m1, lambdaU1_i2p1, invdxx2);
        const REAL_SIMD_ARRAY lambdaU_dD20 = SIMD_fd_function_dD0_fdorder2(lambdaU2_i0m1, lambdaU2_i0p1, invdxx0);
        const REAL_SIMD_ARRAY lambdaU_dD21 = SIMD_fd_function_dD1_fdorder2(lambdaU2_i1m1, lambdaU2_i1p1, invdxx1);
        const REAL_SIMD_ARRAY lambdaU_dD22 = SIMD_fd_function_dD2_fdorder2(lambdaU2_i2m1, lambdaU2_i2p1, invdxx2);
        const REAL_SIMD_ARRAY trK_dD0 = SIMD_fd_function_dD0_fdorder2(trK_i0m1, trK_i0p1, invdxx0);
        const REAL_SIMD_ARRAY trK_dD1 = SIMD_fd_function_dD1_fdorder2(trK_i1m1, trK_i1p1, invdxx1);
        const REAL_SIMD_ARRAY trK_dD2 = SIMD_fd_function_dD2_fdorder2(trK_i2m1, trK_i2p1, invdxx2);

        /*
         * NRPy+-Generated GF Access/FD Code, Step 2 of 2:
         * Evaluate SymPy expressions and write to main memory.
         */
        const double dblFDPart3_Integer_1 = 1.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_1 = ConstSIMD(dblFDPart3_Integer_1);

        const double dblFDPart3_Integer_12 = 12.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_12 = ConstSIMD(dblFDPart3_Integer_12);

        const double dblFDPart3_Integer_16 = 16.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_16 = ConstSIMD(dblFDPart3_Integer_16);

        const double dblFDPart3_Integer_2 = 2.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_2 = ConstSIMD(dblFDPart3_Integer_2);

        const double dblFDPart3_Integer_3 = 3.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_3 = ConstSIMD(dblFDPart3_Integer_3);

        const double dblFDPart3_Integer_4 = 4.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_4 = ConstSIMD(dblFDPart3_Integer_4);

        const double dblFDPart3_Integer_6 = 6.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_6 = ConstSIMD(dblFDPart3_Integer_6);

        const double dblFDPart3_Integer_8 = 8.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_8 = ConstSIMD(dblFDPart3_Integer_8);

        const double dblFDPart3_NegativeOne_ = -1.0;
        const REAL_SIMD_ARRAY FDPart3_NegativeOne_ = ConstSIMD(dblFDPart3_NegativeOne_);

        const double dblFDPart3_Rational_1_2 = 1.0 / 2.0;
        const REAL_SIMD_ARRAY FDPart3_Rational_1_2 = ConstSIMD(dblFDPart3_Rational_1_2);

        const double dblFDPart3_Rational_2_3 = 2.0 / 3.0;
        const REAL_SIMD_ARRAY FDPart3_Rational_2_3 = ConstSIMD(dblFDPart3_Rational_2_3);

        const REAL_SIMD_ARRAY FDPart3tmp1 = AddSIMD(FDPart3_Integer_1, hDD11);
        const REAL_SIMD_ARRAY FDPart3tmp4 = MulSIMD(hDD02, hDD12);
        const REAL_SIMD_ARRAY FDPart3tmp6 = AddSIMD(FDPart3_Integer_1, hDD00);
        const REAL_SIMD_ARRAY FDPart3tmp7 = AddSIMD(FDPart3_Integer_1, hDD22);
        const REAL_SIMD_ARRAY FDPart3tmp66 = MulSIMD(cf, cf);
        const REAL_SIMD_ARRAY FDPart3tmp79 = DivSIMD(FDPart3_Integer_1, cf);
        const REAL_SIMD_ARRAY FDPart3tmp84 = AddSIMD(hDD_dD120, SubSIMD(hDD_dD021, hDD_dD012));
        const REAL_SIMD_ARRAY FDPart3tmp90 = MulSIMD(FDPart3_Rational_1_2, FDPart3_Rational_1_2);
        const REAL_SIMD_ARRAY FDPart3tmp94 = AddSIMD(hDD_dD120, SubSIMD(hDD_dD012, hDD_dD021));
        const REAL_SIMD_ARRAY FDPart3tmp105 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD122, hDD_dD221);
        const REAL_SIMD_ARRAY FDPart3tmp106 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD022, hDD_dD220);
        const REAL_SIMD_ARRAY FDPart3tmp110 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD121, hDD_dD112);
        const REAL_SIMD_ARRAY FDPart3tmp111 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD011, hDD_dD110);
        const REAL_SIMD_ARRAY FDPart3tmp115 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD010, hDD_dD001);
        const REAL_SIMD_ARRAY FDPart3tmp116 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD020, hDD_dD002);
        const REAL_SIMD_ARRAY FDPart3tmp429 = DivSIMD(FDPart3_Integer_1, MulSIMD(MulSIMD(MulSIMD(cf, cf), cf), cf));
        const REAL_SIMD_ARRAY FDPart3tmp431 = DivSIMD(FDPart3_Integer_1, MulSIMD(MulSIMD(MulSIMD(MulSIMD(MulSIMD(cf, cf), cf), cf), cf), cf));
        const REAL_SIMD_ARRAY FDPart3tmp446 = MulSIMD(FDPart3_Integer_8, MulSIMD(FDPart3_NegativeOne_, PI));
        const REAL_SIMD_ARRAY FDPart3tmp2 = MulSIMD(FDPart3_NegativeOne_, MulSIMD(FDPart3tmp1, hDD02));
        const REAL_SIMD_ARRAY FDPart3tmp5 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp4, hDD01));
        const REAL_SIMD_ARRAY FDPart3tmp8 = MulSIMD(FDPart3tmp1, FDPart3tmp7);
        const REAL_SIMD_ARRAY FDPart3tmp11 = MulSIMD(FDPart3tmp7, MulSIMD(hDD01, hDD01));
        const REAL_SIMD_ARRAY FDPart3tmp13 = MulSIMD(FDPart3tmp1, MulSIMD(hDD02, hDD02));
        const REAL_SIMD_ARRAY FDPart3tmp15 = MulSIMD(FDPart3tmp6, MulSIMD(hDD12, hDD12));
        const REAL_SIMD_ARRAY FDPart3tmp18 = MulSIMD(FDPart3_NegativeOne_, MulSIMD(FDPart3tmp7, hDD01));
        const REAL_SIMD_ARRAY FDPart3tmp22 = MulSIMD(FDPart3_NegativeOne_, MulSIMD(hDD12, hDD12));
        const REAL_SIMD_ARRAY FDPart3tmp33 = MulSIMD(FDPart3_NegativeOne_, MulSIMD(FDPart3tmp6, hDD12));
        const REAL_SIMD_ARRAY FDPart3tmp36 = MulSIMD(FDPart3_NegativeOne_, MulSIMD(hDD02, hDD02));
        const REAL_SIMD_ARRAY FDPart3tmp48 = MulSIMD(FDPart3_NegativeOne_, MulSIMD(hDD01, hDD01));
        const REAL_SIMD_ARRAY FDPart3tmp69 = DivSIMD(FDPart3_Integer_1, FDPart3tmp66);
        const REAL_SIMD_ARRAY FDPart3tmp80 = MulSIMD(FDPart3tmp79, cf_dD1);
        const REAL_SIMD_ARRAY FDPart3tmp81 = MulSIMD(FDPart3_Rational_1_2, FDPart3tmp79);
        const REAL_SIMD_ARRAY FDPart3tmp86 = MulSIMD(FDPart3tmp79, cf_dD0);
        const REAL_SIMD_ARRAY FDPart3tmp89 = MulSIMD(FDPart3tmp79, cf_dD2);
        const REAL_SIMD_ARRAY FDPart3tmp91 = MulSIMD(FDPart3_Integer_1, FDPart3tmp90);
        const REAL_SIMD_ARRAY FDPart3tmp132 = MulSIMD(FDPart3_Integer_2, FDPart3tmp90);
        const REAL_SIMD_ARRAY FDPart3tmp3 = FusedMulAddSIMD(hDD01, hDD12, FDPart3tmp2);
        const REAL_SIMD_ARRAY FDPart3tmp19 = AddSIMD(FDPart3tmp18, FDPart3tmp4);
        const REAL_SIMD_ARRAY FDPart3tmp23 = AddSIMD(FDPart3tmp22, FDPart3tmp8);
        const REAL_SIMD_ARRAY FDPart3tmp34 = FusedMulAddSIMD(hDD01, hDD02, FDPart3tmp33);
        const REAL_SIMD_ARRAY FDPart3tmp38 = FusedMulAddSIMD(FDPart3tmp6, FDPart3tmp7, FDPart3tmp36);
        const REAL_SIMD_ARRAY FDPart3tmp50 = FusedMulAddSIMD(FDPart3tmp1, FDPart3tmp6, FDPart3tmp48);
        const REAL_SIMD_ARRAY FDPart3tmp430 = FusedMulAddSIMD(FDPart3tmp18, FDPart3tmp429, MulSIMD(FDPart3tmp4, FDPart3tmp429));
        const REAL_SIMD_ARRAY FDPart3tmp433 = MulSIMD(FDPart3tmp69, alpha);
        const REAL_SIMD_ARRAY FDPart3tmp439 = MulSIMD(FDPart3tmp69, vetU1);
        const REAL_SIMD_ARRAY FDPart3tmp441 = FusedMulAddSIMD(FDPart3tmp2, FDPart3tmp429, MulSIMD(FDPart3tmp429, MulSIMD(hDD01, hDD12)));
        const REAL_SIMD_ARRAY FDPart3tmp442 = MulSIMD(FDPart3tmp69, hDD02);
        const REAL_SIMD_ARRAY FDPart3tmp503 = FusedMulAddSIMD(FDPart3tmp33, FDPart3tmp429, MulSIMD(FDPart3tmp429, MulSIMD(hDD01, hDD02)));
        const REAL_SIMD_ARRAY FDPart3tmp16 =
            FusedMulAddSIMD(FDPart3tmp6, FDPart3tmp8, SubSIMD(FDPart3tmp5, AddSIMD(FDPart3tmp11, AddSIMD(FDPart3tmp13, FDPart3tmp15))));
        const REAL_SIMD_ARRAY FDPart3tmp432 = DivSIMD(
            FDPart3_Integer_1,
            FusedMulAddSIMD(FDPart3tmp431, MulSIMD(FDPart3tmp6, FDPart3tmp8),
                            FusedMulSubSIMD(FDPart3tmp431, FDPart3tmp5,
                                            FusedMulAddSIMD(FDPart3tmp13, FDPart3tmp431,
                                                            FusedMulAddSIMD(FDPart3tmp15, FDPart3tmp431, MulSIMD(FDPart3tmp11, FDPart3tmp431))))));
        const REAL_SIMD_ARRAY FDPart3tmp436 = MulSIMD(FDPart3tmp433, T4UU02);
        const REAL_SIMD_ARRAY FDPart3tmp17 = DivSIMD(FDPart3_Integer_1, MulSIMD(FDPart3tmp16, FDPart3tmp16));
        const REAL_SIMD_ARRAY FDPart3tmp67 = DivSIMD(FDPart3_Integer_1, FDPart3tmp16);
        const REAL_SIMD_ARRAY FDPart3tmp440 =
            MulSIMD(FDPart3tmp432,
                    FusedMulAddSIMD(FDPart3tmp433, MulSIMD(T4UU03, hDD12),
                                    FusedMulAddSIMD(MulSIMD(FDPart3_Integer_1, T4UU00),
                                                    MulSIMD(alpha, FusedMulAddSIMD(FDPart3tmp69, MulSIMD(hDD01, vetU0),
                                                                                   FusedMulAddSIMD(FDPart3tmp69, MulSIMD(hDD12, vetU2),
                                                                                                   MulSIMD(FDPart3tmp1, FDPart3tmp439)))),
                                                    FusedMulAddSIMD(FDPart3tmp1, FDPart3tmp436, MulSIMD(FDPart3tmp433, MulSIMD(T4UU01, hDD01))))));
        const REAL_SIMD_ARRAY FDPart3tmp444 =
            MulSIMD(FDPart3tmp432,
                    FusedMulAddSIMD(FDPart3tmp442, MulSIMD(T4UU01, alpha),
                                    FusedMulAddSIMD(MulSIMD(FDPart3_Integer_1, T4UU00),
                                                    MulSIMD(alpha, FusedMulAddSIMD(FDPart3tmp442, vetU0,
                                                                                   FusedMulAddSIMD(FDPart3tmp69, MulSIMD(FDPart3tmp7, vetU2),
                                                                                                   MulSIMD(FDPart3tmp439, hDD12)))),
                                                    FusedMulAddSIMD(FDPart3tmp436, hDD12, MulSIMD(FDPart3tmp433, MulSIMD(FDPart3tmp7, T4UU03))))));
        const REAL_SIMD_ARRAY FDPart3tmp445 =
            MulSIMD(FDPart3tmp432,
                    FusedMulAddSIMD(FDPart3tmp442, MulSIMD(T4UU03, alpha),
                                    FusedMulAddSIMD(MulSIMD(FDPart3_Integer_1, T4UU00),
                                                    MulSIMD(alpha, FusedMulAddSIMD(FDPart3tmp442, vetU2,
                                                                                   FusedMulAddSIMD(FDPart3tmp6, MulSIMD(FDPart3tmp69, vetU0),
                                                                                                   MulSIMD(FDPart3tmp439, hDD01)))),
                                                    FusedMulAddSIMD(FDPart3tmp436, hDD01, MulSIMD(FDPart3tmp433, MulSIMD(FDPart3tmp6, T4UU01))))));
        const REAL_SIMD_ARRAY FDPart3tmp20 = MulSIMD(FDPart3tmp17, FDPart3tmp19);
        const REAL_SIMD_ARRAY FDPart3tmp24 = MulSIMD(FDPart3tmp17, FDPart3tmp23);
        const REAL_SIMD_ARRAY FDPart3tmp28 = MulSIMD(FDPart3tmp17, MulSIMD(FDPart3tmp19, FDPart3tmp19));
        const REAL_SIMD_ARRAY FDPart3tmp29 = MulSIMD(FDPart3tmp17, MulSIMD(FDPart3tmp3, FDPart3tmp3));
        const REAL_SIMD_ARRAY FDPart3tmp30 = MulSIMD(FDPart3tmp17, MulSIMD(FDPart3tmp23, FDPart3tmp23));
        const REAL_SIMD_ARRAY FDPart3tmp40 = MulSIMD(FDPart3tmp17, FDPart3tmp38);
        const REAL_SIMD_ARRAY FDPart3tmp43 = MulSIMD(FDPart3tmp17, MulSIMD(FDPart3tmp34, FDPart3tmp34));
        const REAL_SIMD_ARRAY FDPart3tmp44 = MulSIMD(FDPart3tmp17, MulSIMD(FDPart3tmp38, FDPart3tmp38));
        const REAL_SIMD_ARRAY FDPart3tmp46 = MulSIMD(FDPart3tmp17, FDPart3tmp3);
        const REAL_SIMD_ARRAY FDPart3tmp55 = MulSIMD(FDPart3tmp17, MulSIMD(FDPart3tmp50, FDPart3tmp50));
        const REAL_SIMD_ARRAY FDPart3tmp68 = MulSIMD(FDPart3tmp19, FDPart3tmp67);
        const REAL_SIMD_ARRAY FDPart3tmp71 = MulSIMD(FDPart3tmp3, FDPart3tmp67);
        const REAL_SIMD_ARRAY FDPart3tmp72 = MulSIMD(FDPart3tmp34, FDPart3tmp67);
        const REAL_SIMD_ARRAY FDPart3tmp74 = MulSIMD(FDPart3tmp23, FDPart3tmp67);
        const REAL_SIMD_ARRAY FDPart3tmp76 = MulSIMD(FDPart3tmp38, FDPart3tmp67);
        const REAL_SIMD_ARRAY FDPart3tmp78 = MulSIMD(FDPart3tmp50, FDPart3tmp67);
        const REAL_SIMD_ARRAY FDPart3tmp93 = MulSIMD(FDPart3tmp67, hDD_dD002);
        const REAL_SIMD_ARRAY FDPart3tmp99 = MulSIMD(FDPart3tmp67, hDD_dD112);
        const REAL_SIMD_ARRAY FDPart3tmp100 = MulSIMD(FDPart3tmp67, hDD_dD221);
        const REAL_SIMD_ARRAY FDPart3tmp21 = MulSIMD(FDPart3tmp20, FDPart3tmp3);
        const REAL_SIMD_ARRAY FDPart3tmp25 = MulSIMD(FDPart3tmp24, aDD01);
        const REAL_SIMD_ARRAY FDPart3tmp26 = MulSIMD(FDPart3tmp24, aDD02);
        const REAL_SIMD_ARRAY FDPart3tmp31 = FusedMulAddSIMD(FDPart3tmp29, aDD22, FusedMulAddSIMD(FDPart3tmp30, aDD00, MulSIMD(FDPart3tmp28, aDD11)));
        const REAL_SIMD_ARRAY FDPart3tmp35 = MulSIMD(FDPart3tmp20, FDPart3tmp34);
        const REAL_SIMD_ARRAY FDPart3tmp41 = MulSIMD(FDPart3tmp34, FDPart3tmp40);
        const REAL_SIMD_ARRAY FDPart3tmp45 = FusedMulAddSIMD(FDPart3tmp43, aDD22, FusedMulAddSIMD(FDPart3tmp44, aDD11, MulSIMD(FDPart3tmp28, aDD00)));
        const REAL_SIMD_ARRAY FDPart3tmp47 = MulSIMD(FDPart3tmp34, FDPart3tmp46);
        const REAL_SIMD_ARRAY FDPart3tmp56 = FusedMulAddSIMD(FDPart3tmp43, aDD11, FusedMulAddSIMD(FDPart3tmp55, aDD22, MulSIMD(FDPart3tmp29, aDD00)));
        const REAL_SIMD_ARRAY FDPart3tmp64 = MulSIMD(FDPart3tmp17, MulSIMD(FDPart3tmp34, FDPart3tmp50));
        const REAL_SIMD_ARRAY FDPart3tmp85 =
            FusedMulAddSIMD(FDPart3tmp19, MulSIMD(FDPart3tmp67, hDD_dD110),
                            FusedMulAddSIMD(FDPart3tmp23, MulSIMD(FDPart3tmp67, hDD_dD001), MulSIMD(FDPart3tmp71, FDPart3tmp84)));
        const REAL_SIMD_ARRAY FDPart3tmp87 =
            FusedMulAddSIMD(FDPart3tmp19, MulSIMD(FDPart3tmp67, hDD_dD001),
                            FusedMulAddSIMD(FDPart3tmp38, MulSIMD(FDPart3tmp67, hDD_dD110), MulSIMD(FDPart3tmp72, FDPart3tmp84)));
        const REAL_SIMD_ARRAY FDPart3tmp88 =
            FusedMulAddSIMD(FDPart3tmp72, hDD_dD110, FusedMulAddSIMD(FDPart3tmp78, FDPart3tmp84, MulSIMD(FDPart3tmp71, hDD_dD001)));
        const REAL_SIMD_ARRAY FDPart3tmp96 =
            FusedMulAddSIMD(FDPart3tmp19, MulSIMD(FDPart3tmp67, FDPart3tmp94),
                            FusedMulAddSIMD(FDPart3tmp3, MulSIMD(FDPart3tmp67, hDD_dD220), MulSIMD(FDPart3tmp23, FDPart3tmp93)));
        const REAL_SIMD_ARRAY FDPart3tmp97 = FusedMulAddSIMD(
            FDPart3tmp72, hDD_dD220, FusedMulAddSIMD(FDPart3tmp38, MulSIMD(FDPart3tmp67, FDPart3tmp94), MulSIMD(FDPart3tmp19, FDPart3tmp93)));
        const REAL_SIMD_ARRAY FDPart3tmp98 = FusedMulAddSIMD(
            FDPart3tmp72, FDPart3tmp94, FusedMulAddSIMD(FDPart3tmp50, MulSIMD(FDPart3tmp67, hDD_dD220), MulSIMD(FDPart3tmp3, FDPart3tmp93)));
        const REAL_SIMD_ARRAY FDPart3tmp102 =
            FusedMulAddSIMD(FDPart3tmp19, FDPart3tmp99,
                            FusedMulAddSIMD(FDPart3tmp23, MulSIMD(FDPart3tmp67, AddSIMD(hDD_dD021, SubSIMD(hDD_dD012, hDD_dD120))),
                                            MulSIMD(FDPart3tmp100, FDPart3tmp3)));
        const REAL_SIMD_ARRAY FDPart3tmp103 =
            FusedMulAddSIMD(FDPart3tmp38, FDPart3tmp99,
                            FusedMulAddSIMD(FDPart3tmp19, MulSIMD(FDPart3tmp67, AddSIMD(hDD_dD021, SubSIMD(hDD_dD012, hDD_dD120))),
                                            MulSIMD(FDPart3tmp100, FDPart3tmp34)));
        const REAL_SIMD_ARRAY FDPart3tmp104 =
            FusedMulAddSIMD(FDPart3tmp34, FDPart3tmp99,
                            FusedMulAddSIMD(FDPart3tmp3, MulSIMD(FDPart3tmp67, AddSIMD(hDD_dD021, SubSIMD(hDD_dD012, hDD_dD120))),
                                            MulSIMD(FDPart3tmp100, FDPart3tmp50)));
        const REAL_SIMD_ARRAY FDPart3tmp107 =
            FusedMulAddSIMD(FDPart3tmp106, FDPart3tmp74, FusedMulAddSIMD(FDPart3tmp71, hDD_dD222, MulSIMD(FDPart3tmp105, FDPart3tmp68)));
        const REAL_SIMD_ARRAY FDPart3tmp108 =
            FusedMulAddSIMD(FDPart3tmp106, FDPart3tmp68, FusedMulAddSIMD(FDPart3tmp72, hDD_dD222, MulSIMD(FDPart3tmp105, FDPart3tmp76)));
        const REAL_SIMD_ARRAY FDPart3tmp109 =
            FusedMulAddSIMD(FDPart3tmp106, FDPart3tmp71, FusedMulAddSIMD(FDPart3tmp78, hDD_dD222, MulSIMD(FDPart3tmp105, FDPart3tmp72)));
        const REAL_SIMD_ARRAY FDPart3tmp112 =
            FusedMulAddSIMD(FDPart3tmp111, FDPart3tmp74, FusedMulAddSIMD(FDPart3tmp68, hDD_dD111, MulSIMD(FDPart3tmp110, FDPart3tmp71)));
        const REAL_SIMD_ARRAY FDPart3tmp113 =
            FusedMulAddSIMD(FDPart3tmp111, FDPart3tmp68, FusedMulAddSIMD(FDPart3tmp76, hDD_dD111, MulSIMD(FDPart3tmp110, FDPart3tmp72)));
        const REAL_SIMD_ARRAY FDPart3tmp114 =
            FusedMulAddSIMD(FDPart3tmp111, FDPart3tmp71, FusedMulAddSIMD(FDPart3tmp72, hDD_dD111, MulSIMD(FDPart3tmp110, FDPart3tmp78)));
        const REAL_SIMD_ARRAY FDPart3tmp117 =
            FusedMulAddSIMD(FDPart3tmp116, FDPart3tmp71, FusedMulAddSIMD(FDPart3tmp74, hDD_dD000, MulSIMD(FDPart3tmp115, FDPart3tmp68)));
        const REAL_SIMD_ARRAY FDPart3tmp118 =
            FusedMulAddSIMD(FDPart3tmp116, FDPart3tmp72, FusedMulAddSIMD(FDPart3tmp68, hDD_dD000, MulSIMD(FDPart3tmp115, FDPart3tmp76)));
        const REAL_SIMD_ARRAY FDPart3tmp119 =
            FusedMulAddSIMD(FDPart3tmp116, FDPart3tmp78, FusedMulAddSIMD(FDPart3tmp71, hDD_dD000, MulSIMD(FDPart3tmp115, FDPart3tmp72)));
        const REAL_SIMD_ARRAY FDPart3tmp200 = MulSIMD(FDPart3_Integer_3, FDPart3tmp68);
        const REAL_SIMD_ARRAY FDPart3tmp207 = MulSIMD(FDPart3_Integer_3, FDPart3tmp71);
        const REAL_SIMD_ARRAY FDPart3tmp209 = MulSIMD(FDPart3_Integer_3, FDPart3tmp72);
        const REAL_SIMD_ARRAY FDPart3tmp468 = MulSIMD(FDPart3tmp19, FDPart3tmp24);
        const REAL_SIMD_ARRAY FDPart3tmp473 = MulSIMD(FDPart3tmp24, FDPart3tmp3);
        const REAL_SIMD_ARRAY FDPart3tmp488 = MulSIMD(FDPart3tmp24, FDPart3tmp34);
        const REAL_SIMD_ARRAY FDPart3tmp494 = MulSIMD(FDPart3tmp38, FDPart3tmp46);
        const REAL_SIMD_ARRAY FDPart3tmp27 =
            FusedMulAddSIMD(FDPart3tmp21, aDD12, FusedMulAddSIMD(FDPart3tmp26, FDPart3tmp3, MulSIMD(FDPart3tmp19, FDPart3tmp25)));
        const REAL_SIMD_ARRAY FDPart3tmp42 =
            FusedMulAddSIMD(FDPart3tmp41, aDD12, FusedMulAddSIMD(FDPart3tmp20, MulSIMD(FDPart3tmp38, aDD01), MulSIMD(FDPart3tmp35, aDD02)));
        const REAL_SIMD_ARRAY FDPart3tmp54 =
            FusedMulAddSIMD(FDPart3tmp46, MulSIMD(FDPart3tmp50, aDD02),
                            FusedMulAddSIMD(MulSIMD(FDPart3tmp17, FDPart3tmp34), MulSIMD(FDPart3tmp50, aDD12), MulSIMD(FDPart3tmp47, aDD01)));
        const REAL_SIMD_ARRAY FDPart3tmp62 = FusedMulAddSIMD(
            FDPart3tmp20, MulSIMD(FDPart3tmp38, aDD11),
            FusedMulAddSIMD(
                FDPart3tmp19, MulSIMD(FDPart3tmp24, aDD00),
                FusedMulAddSIMD(
                    FDPart3tmp20, MulSIMD(FDPart3tmp34, aDD12),
                    FusedMulAddSIMD(FDPart3tmp26, FDPart3tmp34,
                                    FusedMulAddSIMD(FDPart3tmp28, aDD01,
                                                    FusedMulAddSIMD(FDPart3tmp34, MulSIMD(FDPart3tmp46, aDD22),
                                                                    FusedMulAddSIMD(FDPart3tmp38, MulSIMD(FDPart3tmp46, aDD12),
                                                                                    FusedMulAddSIMD(FDPart3tmp21, aDD02,
                                                                                                    MulSIMD(FDPart3tmp25, FDPart3tmp38)))))))));
        const REAL_SIMD_ARRAY FDPart3tmp63 = FusedMulAddSIMD(
            FDPart3tmp24, MulSIMD(FDPart3tmp3, aDD00),
            FusedMulAddSIMD(
                FDPart3tmp20, MulSIMD(FDPart3tmp34, aDD11),
                FusedMulAddSIMD(
                    FDPart3tmp20, MulSIMD(FDPart3tmp50, aDD12),
                    FusedMulAddSIMD(FDPart3tmp26, FDPart3tmp50,
                                    FusedMulAddSIMD(FDPart3tmp29, aDD02,
                                                    FusedMulAddSIMD(FDPart3tmp34, MulSIMD(FDPart3tmp46, aDD12),
                                                                    FusedMulAddSIMD(FDPart3tmp46, MulSIMD(FDPart3tmp50, aDD22),
                                                                                    FusedMulAddSIMD(FDPart3tmp21, aDD01,
                                                                                                    MulSIMD(FDPart3tmp25, FDPart3tmp34)))))))));
        const REAL_SIMD_ARRAY FDPart3tmp65 = FusedMulAddSIMD(
            FDPart3tmp20, MulSIMD(FDPart3tmp50, aDD02),
            FusedMulAddSIMD(
                FDPart3tmp47, aDD02,
                FusedMulAddSIMD(FDPart3tmp64, aDD22,
                                FusedMulAddSIMD(FDPart3tmp41, aDD11,
                                                FusedMulAddSIMD(FDPart3tmp43, aDD12,
                                                                FusedMulAddSIMD(FDPart3tmp38, MulSIMD(FDPart3tmp46, aDD01),
                                                                                FusedMulAddSIMD(FDPart3tmp40, MulSIMD(FDPart3tmp50, aDD12),
                                                                                                FusedMulAddSIMD(FDPart3tmp21, aDD00,
                                                                                                                MulSIMD(FDPart3tmp35, aDD01)))))))));
        const REAL_SIMD_ARRAY FDPart3tmp120 =
            FusedMulAddSIMD(FDPart3tmp103, hDD01, FusedMulAddSIMD(FDPart3tmp104, hDD02, MulSIMD(FDPart3tmp102, FDPart3tmp6)));
        const REAL_SIMD_ARRAY FDPart3tmp122 =
            FusedMulAddSIMD(FDPart3tmp85, hDD02, FusedMulAddSIMD(FDPart3tmp87, hDD12, MulSIMD(FDPart3tmp7, FDPart3tmp88)));
        const REAL_SIMD_ARRAY FDPart3tmp125 =
            FusedMulAddSIMD(FDPart3tmp96, hDD01, FusedMulAddSIMD(FDPart3tmp98, hDD12, MulSIMD(FDPart3tmp1, FDPart3tmp97)));
        const REAL_SIMD_ARRAY FDPart3tmp148 =
            FusedMulAddSIMD(FDPart3tmp118, hDD12, FusedMulAddSIMD(FDPart3tmp119, FDPart3tmp7, MulSIMD(FDPart3tmp117, hDD02)));
        const REAL_SIMD_ARRAY FDPart3tmp156 =
            FusedMulAddSIMD(FDPart3tmp113, hDD12, FusedMulAddSIMD(FDPart3tmp114, FDPart3tmp7, MulSIMD(FDPart3tmp112, hDD02)));
        const REAL_SIMD_ARRAY FDPart3tmp164 =
            FusedMulAddSIMD(FDPart3tmp108, hDD01, FusedMulAddSIMD(FDPart3tmp109, hDD02, MulSIMD(FDPart3tmp107, FDPart3tmp6)));
        const REAL_SIMD_ARRAY FDPart3tmp171 =
            FusedMulAddSIMD(FDPart3tmp107, hDD01, FusedMulAddSIMD(FDPart3tmp109, hDD12, MulSIMD(FDPart3tmp1, FDPart3tmp108)));
        const REAL_SIMD_ARRAY FDPart3tmp235 =
            FusedMulAddSIMD(FDPart3tmp113, hDD01, FusedMulAddSIMD(FDPart3tmp114, hDD02, MulSIMD(FDPart3tmp112, FDPart3tmp6)));
        const REAL_SIMD_ARRAY FDPart3tmp245 =
            FusedMulAddSIMD(FDPart3tmp117, hDD01, FusedMulAddSIMD(FDPart3tmp119, hDD12, MulSIMD(FDPart3tmp1, FDPart3tmp118)));
        const REAL_SIMD_ARRAY FDPart3tmp476 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp118, aDD01),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp119, aDD02),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp117, aDD00)))),
                               aDD_dD000);
        const REAL_SIMD_ARRAY FDPart3tmp478 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp108, aDD12),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp109, aDD22),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp107, aDD02)))),
                               aDD_dD222);
        const REAL_SIMD_ARRAY FDPart3tmp480 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp113, aDD11),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp114, aDD12),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp112, aDD01)))),
                               aDD_dD111);
        const REAL_SIMD_ARRAY FDPart3tmp482 =
            FusedMulAddSIMD(FDPart3tmp97, aDD11, FusedMulAddSIMD(FDPart3tmp98, aDD12, MulSIMD(FDPart3tmp96, aDD01)));
        const REAL_SIMD_ARRAY FDPart3tmp483 =
            FusedMulAddSIMD(FDPart3tmp103, aDD01, FusedMulAddSIMD(FDPart3tmp104, aDD02, MulSIMD(FDPart3tmp102, aDD00)));
        const REAL_SIMD_ARRAY FDPart3tmp486 =
            FusedMulAddSIMD(FDPart3tmp87, aDD12, FusedMulAddSIMD(FDPart3tmp88, aDD22, MulSIMD(FDPart3tmp85, aDD02)));
        const REAL_SIMD_ARRAY FDPart3tmp121 = MulSIMD(FDPart3tmp120, FDPart3tmp96);
        const REAL_SIMD_ARRAY FDPart3tmp126 = MulSIMD(FDPart3tmp103, FDPart3tmp125);
        const REAL_SIMD_ARRAY FDPart3tmp129 = MulSIMD(FDPart3tmp102, FDPart3tmp120);
        const REAL_SIMD_ARRAY FDPart3tmp133 = MulSIMD(FDPart3tmp125, FDPart3tmp97);
        const REAL_SIMD_ARRAY FDPart3tmp138 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp108, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp141 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp107, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp145 =
            FusedMulAddSIMD(FDPart3tmp97, hDD01, FusedMulAddSIMD(FDPart3tmp98, hDD02, MulSIMD(FDPart3tmp6, FDPart3tmp96)));
        const REAL_SIMD_ARRAY FDPart3tmp153 =
            FusedMulAddSIMD(FDPart3tmp102, hDD01, FusedMulAddSIMD(FDPart3tmp104, hDD12, MulSIMD(FDPart3tmp1, FDPart3tmp103)));
        const REAL_SIMD_ARRAY FDPart3tmp161 =
            FusedMulAddSIMD(FDPart3tmp96, hDD02, FusedMulAddSIMD(FDPart3tmp97, hDD12, MulSIMD(FDPart3tmp7, FDPart3tmp98)));
        const REAL_SIMD_ARRAY FDPart3tmp169 =
            FusedMulAddSIMD(FDPart3tmp103, hDD12, FusedMulAddSIMD(FDPart3tmp104, FDPart3tmp7, MulSIMD(FDPart3tmp102, hDD02)));
        const REAL_SIMD_ARRAY FDPart3tmp191 = FusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp68, FDPart3tmp85, FusedMulAddSIMD(FDPart3tmp71, FDPart3tmp96, MulSIMD(FDPart3tmp102, FDPart3tmp72))),
            MulSIMD(FDPart3tmp90, FusedMulAddSIMD(FDPart3tmp112, FDPart3tmp76,
                                                  FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp74, MulSIMD(FDPart3tmp107, FDPart3tmp78)))));
        const REAL_SIMD_ARRAY FDPart3tmp193 = FusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp68, FDPart3tmp87, FusedMulAddSIMD(FDPart3tmp71, FDPart3tmp97, MulSIMD(FDPart3tmp103, FDPart3tmp72))),
            MulSIMD(FDPart3tmp90, FusedMulAddSIMD(FDPart3tmp113, FDPart3tmp76,
                                                  FusedMulAddSIMD(FDPart3tmp118, FDPart3tmp74, MulSIMD(FDPart3tmp108, FDPart3tmp78)))));
        const REAL_SIMD_ARRAY FDPart3tmp198 = FusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp68, FDPart3tmp88, FusedMulAddSIMD(FDPart3tmp71, FDPart3tmp98, MulSIMD(FDPart3tmp104, FDPart3tmp72))),
            MulSIMD(FDPart3tmp90, FusedMulAddSIMD(FDPart3tmp114, FDPart3tmp76,
                                                  FusedMulAddSIMD(FDPart3tmp119, FDPart3tmp74, MulSIMD(FDPart3tmp109, FDPart3tmp78)))));
        const REAL_SIMD_ARRAY FDPart3tmp210 =
            FusedMulAddSIMD(FDPart3tmp108, hDD12, FusedMulAddSIMD(FDPart3tmp109, FDPart3tmp7, MulSIMD(FDPart3tmp107, hDD02)));
        const REAL_SIMD_ARRAY FDPart3tmp213 = MulSIMD(FDPart3tmp120, FDPart3tmp85);
        const REAL_SIMD_ARRAY FDPart3tmp216 = MulSIMD(FDPart3tmp104, FDPart3tmp122);
        const REAL_SIMD_ARRAY FDPart3tmp220 = MulSIMD(FDPart3tmp122, FDPart3tmp88);
        const REAL_SIMD_ARRAY FDPart3tmp225 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp114, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp228 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp112, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp232 =
            FusedMulAddSIMD(FDPart3tmp85, hDD01, FusedMulAddSIMD(FDPart3tmp88, hDD12, MulSIMD(FDPart3tmp1, FDPart3tmp87)));
        const REAL_SIMD_ARRAY FDPart3tmp242 =
            FusedMulAddSIMD(FDPart3tmp87, hDD01, FusedMulAddSIMD(FDPart3tmp88, hDD02, MulSIMD(FDPart3tmp6, FDPart3tmp85)));
        const REAL_SIMD_ARRAY FDPart3tmp275 =
            FusedMulAddSIMD(FDPart3tmp112, hDD01, FusedMulAddSIMD(FDPart3tmp114, hDD12, MulSIMD(FDPart3tmp1, FDPart3tmp113)));
        const REAL_SIMD_ARRAY FDPart3tmp278 = MulSIMD(FDPart3tmp125, FDPart3tmp87);
        const REAL_SIMD_ARRAY FDPart3tmp281 = MulSIMD(FDPart3tmp122, FDPart3tmp98);
        const REAL_SIMD_ARRAY FDPart3tmp288 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp119, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp291 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp118, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp325 =
            FusedMulAddSIMD(FDPart3tmp118, hDD01, FusedMulAddSIMD(FDPart3tmp119, hDD02, MulSIMD(FDPart3tmp117, FDPart3tmp6)));
        const REAL_SIMD_ARRAY FDPart3tmp329 = MulSIMD(FDPart3tmp125, FDPart3tmp98);
        const REAL_SIMD_ARRAY FDPart3tmp331 = MulSIMD(FDPart3tmp125, FDPart3tmp96);
        const REAL_SIMD_ARRAY FDPart3tmp339 = MulSIMD(FDPart3tmp104, FDPart3tmp120);
        const REAL_SIMD_ARRAY FDPart3tmp345 = MulSIMD(FDPart3tmp245, FDPart3tmp96);
        const REAL_SIMD_ARRAY FDPart3tmp347 = MulSIMD(FDPart3tmp104, FDPart3tmp164);
        const REAL_SIMD_ARRAY FDPart3tmp348 = MulSIMD(FDPart3tmp171, FDPart3tmp98);
        const REAL_SIMD_ARRAY FDPart3tmp355 = MulSIMD(FDPart3tmp103, FDPart3tmp120);
        const REAL_SIMD_ARRAY FDPart3tmp358 = MulSIMD(FDPart3tmp103, FDPart3tmp235);
        const REAL_SIMD_ARRAY FDPart3tmp384 = MulSIMD(FDPart3tmp122, FDPart3tmp87);
        const REAL_SIMD_ARRAY FDPart3tmp389 = MulSIMD(FDPart3tmp156, FDPart3tmp87);
        const REAL_SIMD_ARRAY FDPart3tmp395 = MulSIMD(FDPart3tmp122, FDPart3tmp85);
        const REAL_SIMD_ARRAY FDPart3tmp401 = MulSIMD(FDPart3tmp148, FDPart3tmp85);
        const REAL_SIMD_ARRAY FDPart3tmp451 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp87, aDD11),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp88, aDD12),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp85, aDD01)))),
                               aDD_dD110);
        const REAL_SIMD_ARRAY FDPart3tmp455 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp97, aDD12),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp98, aDD22),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp96, aDD02)))),
                               aDD_dD220);
        const REAL_SIMD_ARRAY FDPart3tmp459 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp103, aDD11),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp104, aDD12),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp102, aDD01)))),
                               aDD_dD112);
        const REAL_SIMD_ARRAY FDPart3tmp463 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp103, aDD12),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp104, aDD22),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp102, aDD02)))),
                               aDD_dD221);
        const REAL_SIMD_ARRAY FDPart3tmp467 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp87, aDD01),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp88, aDD02),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp85, aDD00)))),
                               aDD_dD001);
        const REAL_SIMD_ARRAY FDPart3tmp472 =
            NegFusedMulAddSIMD(FDPart3_Integer_2,
                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp97, aDD01),
                                               FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp98, aDD02),
                                                               MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp96, aDD00)))),
                               aDD_dD002);
        const REAL_SIMD_ARRAY FDPart3tmp485 = NegFusedMulAddSIMD(FDPart3_Rational_1_2, AddSIMD(FDPart3tmp482, FDPart3tmp483), aDD_dD012);
        const REAL_SIMD_ARRAY FDPart3tmp487 = NegFusedMulAddSIMD(FDPart3_Rational_1_2, AddSIMD(FDPart3tmp483, FDPart3tmp486), aDD_dD021);
        const REAL_SIMD_ARRAY FDPart3tmp489 = NegFusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp114, aDD02,
                            FusedMulAddSIMD(FDPart3tmp85, aDD01,
                                            FusedMulAddSIMD(FDPart3tmp87, aDD11,
                                                            FusedMulAddSIMD(FDPart3tmp88, aDD12,
                                                                            FusedMulAddSIMD(FDPart3tmp112, aDD00, MulSIMD(FDPart3tmp113, aDD01)))))),
            aDD_dD011);
        const REAL_SIMD_ARRAY FDPart3tmp490 = NegFusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp109, aDD02,
                            FusedMulAddSIMD(FDPart3tmp96, aDD02,
                                            FusedMulAddSIMD(FDPart3tmp97, aDD12,
                                                            FusedMulAddSIMD(FDPart3tmp98, aDD22,
                                                                            FusedMulAddSIMD(FDPart3tmp107, aDD00, MulSIMD(FDPart3tmp108, aDD01)))))),
            aDD_dD022);
        const REAL_SIMD_ARRAY FDPart3tmp491 = NegFusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp104, aDD12,
                            FusedMulAddSIMD(FDPart3tmp112, aDD02,
                                            FusedMulAddSIMD(FDPart3tmp113, aDD12,
                                                            FusedMulAddSIMD(FDPart3tmp114, aDD22,
                                                                            FusedMulAddSIMD(FDPart3tmp102, aDD01, MulSIMD(FDPart3tmp103, aDD11)))))),
            aDD_dD121);
        const REAL_SIMD_ARRAY FDPart3tmp492 = NegFusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp104, aDD22,
                            FusedMulAddSIMD(FDPart3tmp107, aDD01,
                                            FusedMulAddSIMD(FDPart3tmp108, aDD11,
                                                            FusedMulAddSIMD(FDPart3tmp109, aDD12,
                                                                            FusedMulAddSIMD(FDPart3tmp102, aDD02, MulSIMD(FDPart3tmp103, aDD12)))))),
            aDD_dD122);
        const REAL_SIMD_ARRAY FDPart3tmp499 = NegFusedMulAddSIMD(FDPart3_Rational_1_2, AddSIMD(FDPart3tmp482, FDPart3tmp486), aDD_dD120);
        const REAL_SIMD_ARRAY FDPart3tmp500 = NegFusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp119, aDD12,
                            FusedMulAddSIMD(FDPart3tmp85, aDD00,
                                            FusedMulAddSIMD(FDPart3tmp87, aDD01,
                                                            FusedMulAddSIMD(FDPart3tmp88, aDD02,
                                                                            FusedMulAddSIMD(FDPart3tmp117, aDD01, MulSIMD(FDPart3tmp118, aDD11)))))),
            aDD_dD010);
        const REAL_SIMD_ARRAY FDPart3tmp501 = NegFusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp119, aDD22,
                            FusedMulAddSIMD(FDPart3tmp96, aDD00,
                                            FusedMulAddSIMD(FDPart3tmp97, aDD01,
                                                            FusedMulAddSIMD(FDPart3tmp98, aDD02,
                                                                            FusedMulAddSIMD(FDPart3tmp117, aDD02, MulSIMD(FDPart3tmp118, aDD12)))))),
            aDD_dD020);
        const REAL_SIMD_ARRAY FDPart3tmp124 = MulSIMD(FDPart3tmp122, MulSIMD(FDPart3tmp90, FDPart3tmp96));
        const REAL_SIMD_ARRAY FDPart3tmp128 = MulSIMD(FDPart3tmp103, MulSIMD(FDPart3tmp122, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp146 = MulSIMD(FDPart3tmp102, FDPart3tmp145);
        const REAL_SIMD_ARRAY FDPart3tmp154 = MulSIMD(FDPart3tmp153, FDPart3tmp97);
        const REAL_SIMD_ARRAY FDPart3tmp178 = MulSIMD(FDPart3tmp103, FDPart3tmp153);
        const REAL_SIMD_ARRAY FDPart3tmp181 = MulSIMD(FDPart3tmp103, MulSIMD(FDPart3tmp156, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp182 = MulSIMD(FDPart3tmp145, FDPart3tmp96);
        const REAL_SIMD_ARRAY FDPart3tmp185 = MulSIMD(FDPart3tmp148, MulSIMD(FDPart3tmp90, FDPart3tmp96));
        const REAL_SIMD_ARRAY FDPart3tmp199 = MulSIMD(FDPart3tmp104, FDPart3tmp161);
        const REAL_SIMD_ARRAY FDPart3tmp201 = MulSIMD(FDPart3tmp169, FDPart3tmp98);
        const REAL_SIMD_ARRAY FDPart3tmp202 = MulSIMD(FDPart3tmp104, FDPart3tmp169);
        const REAL_SIMD_ARRAY FDPart3tmp204 = MulSIMD(FDPart3tmp161, FDPart3tmp98);
        const REAL_SIMD_ARRAY FDPart3tmp211 = MulSIMD(FDPart3tmp210, FDPart3tmp98);
        const REAL_SIMD_ARRAY FDPart3tmp212 = MulSIMD(FDPart3tmp104, FDPart3tmp210);
        const REAL_SIMD_ARRAY FDPart3tmp215 = MulSIMD(FDPart3tmp125, MulSIMD(FDPart3tmp85, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp218 = MulSIMD(FDPart3tmp104, MulSIMD(FDPart3tmp125, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp243 = MulSIMD(FDPart3tmp102, FDPart3tmp242);
        const REAL_SIMD_ARRAY FDPart3tmp247 = MulSIMD(FDPart3tmp169, FDPart3tmp88);
        const REAL_SIMD_ARRAY FDPart3tmp257 = MulSIMD(FDPart3tmp104, MulSIMD(FDPart3tmp171, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp258 = MulSIMD(FDPart3tmp242, FDPart3tmp85);
        const REAL_SIMD_ARRAY FDPart3tmp261 = MulSIMD(FDPart3tmp245, MulSIMD(FDPart3tmp85, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp269 = MulSIMD(FDPart3tmp103, FDPart3tmp232);
        const REAL_SIMD_ARRAY FDPart3tmp270 = MulSIMD(FDPart3tmp153, FDPart3tmp87);
        const REAL_SIMD_ARRAY FDPart3tmp272 = MulSIMD(FDPart3tmp232, FDPart3tmp87);
        const REAL_SIMD_ARRAY FDPart3tmp276 = MulSIMD(FDPart3tmp275, FDPart3tmp87);
        const REAL_SIMD_ARRAY FDPart3tmp277 = MulSIMD(FDPart3tmp103, FDPart3tmp275);
        const REAL_SIMD_ARRAY FDPart3tmp280 = MulSIMD(FDPart3tmp120, MulSIMD(FDPart3tmp87, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp283 = MulSIMD(FDPart3tmp120, MulSIMD(FDPart3tmp90, FDPart3tmp98));
        const REAL_SIMD_ARRAY FDPart3tmp302 = MulSIMD(FDPart3tmp232, FDPart3tmp97);
        const REAL_SIMD_ARRAY FDPart3tmp305 = MulSIMD(FDPart3tmp161, FDPart3tmp88);
        const REAL_SIMD_ARRAY FDPart3tmp310 = MulSIMD(FDPart3tmp164, MulSIMD(FDPart3tmp90, FDPart3tmp98));
        const REAL_SIMD_ARRAY FDPart3tmp313 = MulSIMD(FDPart3tmp235, MulSIMD(FDPart3tmp87, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp321 = MulSIMD(FDPart3tmp242, FDPart3tmp96);
        const REAL_SIMD_ARRAY FDPart3tmp322 = MulSIMD(FDPart3tmp145, FDPart3tmp85);
        const REAL_SIMD_ARRAY FDPart3tmp326 = MulSIMD(FDPart3tmp325, FDPart3tmp85);
        const REAL_SIMD_ARRAY FDPart3tmp327 = MulSIMD(FDPart3tmp325, FDPart3tmp96);
        const REAL_SIMD_ARRAY FDPart3tmp328 = MulSIMD(FDPart3tmp104, FDPart3tmp145);
        const REAL_SIMD_ARRAY FDPart3tmp332 = FusedMulAddSIMD(FDPart3tmp114, FDPart3tmp145, MulSIMD(FDPart3tmp125, FDPart3tmp88));
        const REAL_SIMD_ARRAY FDPart3tmp333 = MulSIMD(FDPart3tmp112, FDPart3tmp145);
        const REAL_SIMD_ARRAY FDPart3tmp337 = MulSIMD(FDPart3tmp232, FDPart3tmp96);
        const REAL_SIMD_ARRAY FDPart3tmp340 = MulSIMD(FDPart3tmp153, FDPart3tmp98);
        const REAL_SIMD_ARRAY FDPart3tmp344 = FusedMulAddSIMD(FDPart3tmp119, FDPart3tmp153, MulSIMD(FDPart3tmp120, FDPart3tmp88));
        const REAL_SIMD_ARRAY FDPart3tmp346 = MulSIMD(FDPart3tmp102, FDPart3tmp325);
        const REAL_SIMD_ARRAY FDPart3tmp351 = FusedMulAddSIMD(FDPart3tmp114, FDPart3tmp164, MulSIMD(FDPart3tmp171, FDPart3tmp88));
        const REAL_SIMD_ARRAY FDPart3tmp352 = MulSIMD(FDPart3tmp119, FDPart3tmp169);
        const REAL_SIMD_ARRAY FDPart3tmp353 = FusedMulAddSIMD(FDPart3tmp119, FDPart3tmp171, MulSIMD(FDPart3tmp164, FDPart3tmp88));
        const REAL_SIMD_ARRAY FDPart3tmp359 = MulSIMD(FDPart3tmp275, FDPart3tmp97);
        const REAL_SIMD_ARRAY FDPart3tmp362 = MulSIMD(FDPart3tmp103, FDPart3tmp242);
        const REAL_SIMD_ARRAY FDPart3tmp364 = MulSIMD(FDPart3tmp118, FDPart3tmp153);
        const REAL_SIMD_ARRAY FDPart3tmp367 = FusedMulSubSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(
                hDD02, lambdaU_dD21,
                FusedMulAddSIMD(
                    hDD01, lambdaU_dD00,
                    FusedMulAddSIMD(
                        hDD01, lambdaU_dD11,
                        FusedMulAddSIMD(
                            FDPart3tmp198, AddSIMD(FDPart3tmp120, FDPart3tmp125),
                            FusedMulAddSIMD(
                                FDPart3tmp6, lambdaU_dD01,
                                FusedMulAddSIMD(
                                    FDPart3tmp191, AddSIMD(FDPart3tmp242, FDPart3tmp245),
                                    FusedMulAddSIMD(
                                        FDPart3tmp193, AddSIMD(FDPart3tmp232, FDPart3tmp235),
                                        FusedMulAddSIMD(hDD12, lambdaU_dD20,
                                                        FusedMulSubSIMD(FDPart3tmp1, lambdaU_dD10,
                                                                        FusedMulAddSIMD(FDPart3tmp76, hDD_dDD0111,
                                                                                        FusedMulAddSIMD(FDPart3tmp78, hDD_dDD0122,
                                                                                                        MulSIMD(FDPart3tmp74, hDD_dDD0100)))))))))))),
            FusedMulAddSIMD(FDPart3tmp71, hDD_dDD0102, FusedMulAddSIMD(FDPart3tmp72, hDD_dDD0112, MulSIMD(FDPart3tmp68, hDD_dDD0101))));
        const REAL_SIMD_ARRAY FDPart3tmp375 = MulSIMD(FDPart3tmp114, FDPart3tmp161);
        const REAL_SIMD_ARRAY FDPart3tmp379 = MulSIMD(FDPart3tmp169, FDPart3tmp87);
        const REAL_SIMD_ARRAY FDPart3tmp380 = FusedMulAddSIMD(FDPart3tmp118, FDPart3tmp169, MulSIMD(FDPart3tmp120, FDPart3tmp97));
        const REAL_SIMD_ARRAY FDPart3tmp386 = FusedMulAddSIMD(FDPart3tmp108, FDPart3tmp242, MulSIMD(FDPart3tmp122, FDPart3tmp97));
        const REAL_SIMD_ARRAY FDPart3tmp388 = MulSIMD(FDPart3tmp210, FDPart3tmp88);
        const REAL_SIMD_ARRAY FDPart3tmp390 = FusedMulAddSIMD(FDPart3tmp118, FDPart3tmp156, MulSIMD(FDPart3tmp235, FDPart3tmp97));
        const REAL_SIMD_ARRAY FDPart3tmp392 = MulSIMD(FDPart3tmp108, FDPart3tmp232);
        const REAL_SIMD_ARRAY FDPart3tmp393 = FusedMulAddSIMD(FDPart3tmp108, FDPart3tmp235, MulSIMD(FDPart3tmp156, FDPart3tmp97));
        const REAL_SIMD_ARRAY FDPart3tmp397 = MulSIMD(FDPart3tmp107, FDPart3tmp242);
        const REAL_SIMD_ARRAY FDPart3tmp398 = MulSIMD(FDPart3tmp161, FDPart3tmp85);
        const REAL_SIMD_ARRAY FDPart3tmp403 = FusedMulSubSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(
                hDD02, lambdaU_dD22,
                FusedMulAddSIMD(
                    hDD01, lambdaU_dD12,
                    FusedMulAddSIMD(
                        hDD02, lambdaU_dD00,
                        FusedMulAddSIMD(
                            FDPart3tmp6, lambdaU_dD02,
                            FusedMulAddSIMD(
                                FDPart3tmp7, lambdaU_dD20,
                                FusedMulAddSIMD(
                                    FDPart3tmp193, AddSIMD(FDPart3tmp120, FDPart3tmp122),
                                    FusedMulAddSIMD(
                                        FDPart3tmp198, AddSIMD(FDPart3tmp161, FDPart3tmp164),
                                        FusedMulAddSIMD(hDD12, lambdaU_dD10,
                                                        FusedMulSubSIMD(FDPart3tmp191, AddSIMD(FDPart3tmp145, FDPart3tmp148),
                                                                        FusedMulAddSIMD(FDPart3tmp76, hDD_dDD0211,
                                                                                        FusedMulAddSIMD(FDPart3tmp78, hDD_dDD0222,
                                                                                                        MulSIMD(FDPart3tmp74, hDD_dDD0200)))))))))))),
            FusedMulAddSIMD(FDPart3tmp71, hDD_dDD0202, FusedMulAddSIMD(FDPart3tmp72, hDD_dDD0212, MulSIMD(FDPart3tmp68, hDD_dDD0201))));
        const REAL_SIMD_ARRAY FDPart3tmp412 = FusedMulAddSIMD(FDPart3tmp102, FDPart3tmp125, MulSIMD(FDPart3tmp112, FDPart3tmp161));
        const REAL_SIMD_ARRAY FDPart3tmp417 = FusedMulAddSIMD(FDPart3tmp102, FDPart3tmp122, MulSIMD(FDPart3tmp107, FDPart3tmp232));
        const REAL_SIMD_ARRAY FDPart3tmp420 = FusedMulAddSIMD(FDPart3tmp102, FDPart3tmp245, MulSIMD(FDPart3tmp112, FDPart3tmp148));
        const REAL_SIMD_ARRAY FDPart3tmp422 = FusedMulAddSIMD(FDPart3tmp102, FDPart3tmp148, MulSIMD(FDPart3tmp107, FDPart3tmp245));
        const REAL_SIMD_ARRAY FDPart3tmp428 = FusedMulSubSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(
                hDD12, lambdaU_dD11,
                FusedMulAddSIMD(
                    hDD01, lambdaU_dD02,
                    FusedMulAddSIMD(
                        hDD02, lambdaU_dD01,
                        FusedMulAddSIMD(
                            FDPart3tmp198, AddSIMD(FDPart3tmp169, FDPart3tmp171),
                            FusedMulAddSIMD(
                                FDPart3tmp7, lambdaU_dD21,
                                FusedMulAddSIMD(
                                    FDPart3tmp191, AddSIMD(FDPart3tmp122, FDPart3tmp125),
                                    FusedMulAddSIMD(
                                        FDPart3tmp193, AddSIMD(FDPart3tmp153, FDPart3tmp156),
                                        FusedMulAddSIMD(hDD12, lambdaU_dD22,
                                                        FusedMulSubSIMD(FDPart3tmp1, lambdaU_dD12,
                                                                        FusedMulAddSIMD(FDPart3tmp76, hDD_dDD1211,
                                                                                        FusedMulAddSIMD(FDPart3tmp78, hDD_dDD1222,
                                                                                                        MulSIMD(FDPart3tmp74, hDD_dDD1200)))))))))))),
            FusedMulAddSIMD(FDPart3tmp71, hDD_dDD1202, FusedMulAddSIMD(FDPart3tmp72, hDD_dDD1212, MulSIMD(FDPart3tmp68, hDD_dDD1201))));
        const REAL_SIMD_ARRAY FDPart3tmp495 = MulSIMD(FDPart3tmp490, FDPart3tmp50);
        const REAL_SIMD_ARRAY FDPart3tmp496 = MulSIMD(FDPart3tmp38, FDPart3tmp489);
        const REAL_SIMD_ARRAY FDPart3tmp163 = MulSIMD(FDPart3tmp161, MulSIMD(FDPart3tmp90, FDPart3tmp96));
        const REAL_SIMD_ARRAY FDPart3tmp176 = MulSIMD(FDPart3tmp103, MulSIMD(FDPart3tmp169, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp187 = MulSIMD(FDPart3tmp107, MulSIMD(FDPart3tmp145, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp189 = MulSIMD(FDPart3tmp108, MulSIMD(FDPart3tmp153, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp234 = MulSIMD(FDPart3tmp232, MulSIMD(FDPart3tmp85, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp253 = MulSIMD(FDPart3tmp104, MulSIMD(FDPart3tmp153, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp263 = MulSIMD(FDPart3tmp112, MulSIMD(FDPart3tmp242, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp265 = MulSIMD(FDPart3tmp114, MulSIMD(FDPart3tmp169, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp293 = MulSIMD(FDPart3tmp242, MulSIMD(FDPart3tmp87, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp300 = MulSIMD(FDPart3tmp145, MulSIMD(FDPart3tmp90, FDPart3tmp98));
        const REAL_SIMD_ARRAY FDPart3tmp315 = MulSIMD(FDPart3tmp118, MulSIMD(FDPart3tmp232, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp317 = MulSIMD(FDPart3tmp119, MulSIMD(FDPart3tmp161, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp336 = FusedMulAddSIMD(FDPart3tmp119, FDPart3tmp125, MulSIMD(FDPart3tmp145, FDPart3tmp88));
        const REAL_SIMD_ARRAY FDPart3tmp341 = FusedMulAddSIMD(FDPart3tmp156, FDPart3tmp98, FDPart3tmp340);
        const REAL_SIMD_ARRAY FDPart3tmp342 = FusedMulAddSIMD(FDPart3tmp114, FDPart3tmp120, MulSIMD(FDPart3tmp153, FDPart3tmp88));
        const REAL_SIMD_ARRAY FDPart3tmp371 = FusedMulAddSIMD(FDPart3tmp103, FDPart3tmp245, FDPart3tmp362);
        const REAL_SIMD_ARRAY FDPart3tmp372 = FusedMulAddSIMD(FDPart3tmp104, FDPart3tmp148, FDPart3tmp328);
        const REAL_SIMD_ARRAY FDPart3tmp383 = FusedMulAddSIMD(FDPart3tmp108, FDPart3tmp120, MulSIMD(FDPart3tmp169, FDPart3tmp97));
        const REAL_SIMD_ARRAY FDPart3tmp385 = FusedMulAddSIMD(FDPart3tmp118, FDPart3tmp122, MulSIMD(FDPart3tmp242, FDPart3tmp97));
        const REAL_SIMD_ARRAY FDPart3tmp406 = FusedMulAddSIMD(FDPart3tmp171, FDPart3tmp87, FDPart3tmp379);
        const REAL_SIMD_ARRAY FDPart3tmp414 = FusedMulAddSIMD(FDPart3tmp102, FDPart3tmp161, MulSIMD(FDPart3tmp107, FDPart3tmp125));
        const REAL_SIMD_ARRAY FDPart3tmp416 = FusedMulAddSIMD(FDPart3tmp102, FDPart3tmp232, MulSIMD(FDPart3tmp112, FDPart3tmp122));
        const REAL_SIMD_ARRAY FDPart3tmp502 = FusedMulAddSIMD(
            FDPart3tmp446,
            FusedMulAddSIMD(FDPart3tmp441, FDPart3tmp444,
                            FusedMulAddSIMD(FDPart3tmp445, FusedMulAddSIMD(FDPart3tmp22, FDPart3tmp429, MulSIMD(FDPart3tmp429, FDPart3tmp8)),
                                            MulSIMD(FDPart3tmp430, FDPart3tmp440))),
            MulSIMD(
                FDPart3tmp66,
                FusedMulAddSIMD(
                    FDPart3tmp20, MulSIMD(FDPart3tmp38, FDPart3tmp480),
                    FusedMulAddSIMD(
                        FDPart3tmp20, MulSIMD(FDPart3tmp492, FDPart3tmp50),
                        FusedMulAddSIMD(
                            FDPart3tmp487, FDPart3tmp488,
                            FusedMulAddSIMD(
                                FDPart3tmp491, FDPart3tmp494,
                                FusedMulAddSIMD(
                                    FDPart3tmp472, FDPart3tmp473,
                                    FusedMulAddSIMD(
                                        FDPart3tmp485, FDPart3tmp488,
                                        FusedMulAddSIMD(
                                            FDPart3tmp467, FDPart3tmp468,
                                            FusedMulAddSIMD(
                                                FDPart3tmp47, FDPart3tmp492,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp35, FDPart3tmp491,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp463, FDPart3tmp47,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp30, FDPart3tmp476,
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp35, FDPart3tmp459,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp29, FDPart3tmp455,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp29, FDPart3tmp490,
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp28, FDPart3tmp451,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp28, FDPart3tmp489,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp24, FDPart3tmp495,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp24, FDPart3tmp496,
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp21, FDPart3tmp485,
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp21, FDPart3tmp487,
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp46,
                                                                                                    MulSIMD(FDPart3tmp478, FDPart3tmp50),
                                                                                                    FusedMulSubSIMD(
                                                                                                        FDPart3_Integer_2,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp468, FDPart3tmp500,
                                                                                                            FusedMulAddSIMD(FDPart3tmp473,
                                                                                                                            FDPart3tmp501,
                                                                                                                            MulSIMD(FDPart3tmp21,
                                                                                                                                    FDPart3tmp499))),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3_Rational_1_2,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3_Integer_6,
                                                                                                                MulSIMD(FDPart3tmp62, FDPart3tmp80),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3_Integer_6,
                                                                                                                    MulSIMD(FDPart3tmp63,
                                                                                                                            FDPart3tmp89),
                                                                                                                    MulSIMD(
                                                                                                                        FDPart3tmp86,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3_Integer_12,
                                                                                                                            FDPart3tmp27,
                                                                                                                            MulSIMD(FDPart3_Integer_6,
                                                                                                                                    FDPart3tmp31))))),
                                                                                                            MulSIMD(
                                                                                                                FDPart3_Rational_2_3,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp71, trK_dD2,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp74, trK_dD0,
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3tmp68,
                                                                                                                            trK_dD1)))))))))))))))))))))))))))));
        const REAL_SIMD_ARRAY FDPart3tmp504 = FusedMulAddSIMD(
            FDPart3tmp446,
            FusedMulAddSIMD(FDPart3tmp440, FusedMulAddSIMD(FDPart3tmp36, FDPart3tmp429, MulSIMD(FDPart3tmp429, MulSIMD(FDPart3tmp6, FDPart3tmp7))),
                            FusedMulAddSIMD(FDPart3tmp444, FDPart3tmp503, MulSIMD(FDPart3tmp430, FDPart3tmp445))),
            MulSIMD(
                FDPart3tmp66,
                FusedMulAddSIMD(
                    FDPart3tmp20, MulSIMD(FDPart3tmp38, FDPart3tmp451),
                    FusedMulAddSIMD(
                        FDPart3tmp24, MulSIMD(FDPart3tmp38, FDPart3tmp500),
                        FusedMulAddSIMD(
                            FDPart3tmp488, FDPart3tmp501,
                            FusedMulAddSIMD(
                                FDPart3tmp494, FDPart3tmp499,
                                FusedMulAddSIMD(
                                    FDPart3tmp478, FDPart3tmp64,
                                    FusedMulAddSIMD(
                                        FDPart3tmp485, FDPart3tmp494,
                                        FusedMulAddSIMD(
                                            FDPart3tmp468, FDPart3tmp476,
                                            FusedMulAddSIMD(
                                                FDPart3tmp47, FDPart3tmp490,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp44, FDPart3tmp480,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp455, FDPart3tmp47,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp43, FDPart3tmp463,
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp43, FDPart3tmp492,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp35, FDPart3tmp499,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp41, FDPart3tmp459,
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp28, FDPart3tmp500,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp35, FDPart3tmp485,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp21, FDPart3tmp501,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp28, FDPart3tmp467,
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp20, FDPart3tmp495,
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp21, FDPart3tmp472,
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp40,
                                                                                                    MulSIMD(FDPart3tmp492, FDPart3tmp50),
                                                                                                    FusedMulSubSIMD(
                                                                                                        FDPart3_Integer_2,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp35, FDPart3tmp487,
                                                                                                            FusedMulAddSIMD(FDPart3tmp41,
                                                                                                                            FDPart3tmp491,
                                                                                                                            MulSIMD(FDPart3tmp20,
                                                                                                                                    FDPart3tmp496))),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3_Rational_1_2,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3_Integer_6,
                                                                                                                MulSIMD(FDPart3tmp62, FDPart3tmp86),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3_Integer_6,
                                                                                                                    MulSIMD(FDPart3tmp65,
                                                                                                                            FDPart3tmp89),
                                                                                                                    MulSIMD(
                                                                                                                        FDPart3tmp80,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3_Integer_12,
                                                                                                                            FDPart3tmp42,
                                                                                                                            MulSIMD(FDPart3_Integer_6,
                                                                                                                                    FDPart3tmp45))))),
                                                                                                            MulSIMD(
                                                                                                                FDPart3_Rational_2_3,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp72, trK_dD2,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp76, trK_dD1,
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3tmp68,
                                                                                                                            trK_dD0)))))))))))))))))))))))))))));
        const REAL_SIMD_ARRAY FDPart3tmp506 = FusedMulAddSIMD(
            FDPart3tmp446,
            FusedMulAddSIMD(FDPart3tmp441, FDPart3tmp445,
                            FusedMulAddSIMD(FDPart3tmp444,
                                            FusedMulAddSIMD(FDPart3tmp429, FDPart3tmp48, MulSIMD(FDPart3tmp1, MulSIMD(FDPart3tmp429, FDPart3tmp6))),
                                            MulSIMD(FDPart3tmp440, FDPart3tmp503))),
            MulSIMD(
                FDPart3tmp66,
                FusedMulAddSIMD(
                    FDPart3tmp24, MulSIMD(FDPart3tmp50, FDPart3tmp501),
                    FusedMulAddSIMD(
                        FDPart3tmp40, MulSIMD(FDPart3tmp491, FDPart3tmp50),
                        FusedMulAddSIMD(
                            FDPart3tmp20, MulSIMD(FDPart3tmp487, FDPart3tmp50),
                            FusedMulAddSIMD(
                                FDPart3tmp20, MulSIMD(FDPart3tmp499, FDPart3tmp50),
                                FusedMulAddSIMD(
                                    FDPart3tmp478, FDPart3tmp55,
                                    FusedMulAddSIMD(
                                        FDPart3tmp488, FDPart3tmp500,
                                        FusedMulAddSIMD(
                                            FDPart3tmp47, FDPart3tmp499,
                                            FusedMulAddSIMD(
                                                FDPart3tmp473, FDPart3tmp476,
                                                FusedMulAddSIMD(
                                                    FDPart3tmp463, FDPart3tmp64,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp47, FDPart3tmp487,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp43, FDPart3tmp491,
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp46, FDPart3tmp496,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp41, FDPart3tmp480,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp43, FDPart3tmp459,
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp35, FDPart3tmp451,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp35, FDPart3tmp489,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp29, FDPart3tmp472,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp29, FDPart3tmp501,
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp21, FDPart3tmp467,
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp21, FDPart3tmp500,
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp455,
                                                                                                    MulSIMD(FDPart3tmp46, FDPart3tmp50),
                                                                                                    FusedMulSubSIMD(
                                                                                                        FDPart3_Integer_2,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp47, FDPart3tmp485,
                                                                                                            FusedMulAddSIMD(FDPart3tmp492,
                                                                                                                            FDPart3tmp64,
                                                                                                                            MulSIMD(FDPart3tmp46,
                                                                                                                                    FDPart3tmp495))),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3_Rational_1_2,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3_Integer_6,
                                                                                                                MulSIMD(FDPart3tmp63, FDPart3tmp86),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3_Integer_6,
                                                                                                                    MulSIMD(FDPart3tmp65,
                                                                                                                            FDPart3tmp80),
                                                                                                                    MulSIMD(
                                                                                                                        FDPart3tmp89,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3_Integer_12,
                                                                                                                            FDPart3tmp54,
                                                                                                                            MulSIMD(FDPart3_Integer_6,
                                                                                                                                    FDPart3tmp56))))),
                                                                                                            MulSIMD(
                                                                                                                FDPart3_Rational_2_3,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp72, trK_dD1,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp78, trK_dD2,
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3tmp71,
                                                                                                                            trK_dD0)))))))))))))))))))))))))))));
        const REAL_SIMD_ARRAY FDPart3tmp356 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp154, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp363 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp302, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp376 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp146, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp377 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp243, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp408 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp247, FDPart3tmp90));
        const REAL_SIMD_ARRAY FDPart3tmp410 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp305, FDPart3tmp90));
        const REAL_SIMD_ARRAY __RHS_exp_0 = FusedMulAddSIMD(
            FDPart3tmp66,
            FusedMulAddSIMD(
                FDPart3tmp76,
                FusedMulAddSIMD(
                    hDD01, lambdaU_dD01,
                    FusedMulAddSIMD(
                        FDPart3tmp78, FusedMulAddSIMD(FDPart3tmp129, FDPart3tmp90, MulSIMD(FDPart3tmp102, MulSIMD(FDPart3tmp125, FDPart3tmp132))),
                        FusedMulAddSIMD(
                            FDPart3tmp90,
                            FusedMulAddSIMD(
                                FDPart3tmp113, MulSIMD(FDPart3tmp153, FDPart3tmp209),
                                FusedMulAddSIMD(
                                    FDPart3_Integer_3, MulSIMD(FDPart3tmp178, FDPart3tmp78),
                                    FusedMulAddSIMD(
                                        FDPart3_Integer_3, MulSIMD(FDPart3tmp272, FDPart3tmp74),
                                        FusedMulAddSIMD(
                                            FDPart3tmp207, FDPart3tmp270,
                                            FusedMulAddSIMD(
                                                FDPart3tmp209, FDPart3tmp277,
                                                FusedMulAddSIMD(FDPart3tmp113, MulSIMD(FDPart3tmp200, FDPart3tmp232),
                                                                FusedMulAddSIMD(MulSIMD(FDPart3_Integer_3, FDPart3tmp113),
                                                                                MulSIMD(FDPart3tmp275, FDPart3tmp76),
                                                                                FusedMulAddSIMD(FDPart3tmp200, FDPart3tmp276,
                                                                                                MulSIMD(FDPart3tmp207, FDPart3tmp269))))))))),
                            FusedMulAddSIMD(
                                FDPart3tmp76,
                                FusedMulAddSIMD(FDPart3tmp228, FDPart3tmp232, MulSIMD(FDPart3tmp112, MulSIMD(FDPart3tmp235, FDPart3tmp90))),
                                FusedMulAddSIMD(
                                    FDPart3tmp78, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp257, MulSIMD(FDPart3tmp202, FDPart3tmp90)),
                                    FusedMulAddSIMD(
                                        FDPart3tmp74,
                                        FusedMulAddSIMD(FDPart3tmp220, FDPart3tmp90, MulSIMD(FDPart3tmp125, MulSIMD(FDPart3tmp132, FDPart3tmp88))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp76,
                                            FusedMulAddSIMD(FDPart3tmp153, FDPart3tmp225,
                                                            MulSIMD(FDPart3tmp114, MulSIMD(FDPart3tmp156, FDPart3tmp90))),
                                            FusedMulAddSIMD(
                                                FDPart3tmp72,
                                                FusedMulAddSIMD(FDPart3tmp102, MulSIMD(FDPart3tmp132, FDPart3tmp232),
                                                                MulSIMD(FDPart3tmp102, MulSIMD(FDPart3tmp235, FDPart3tmp90))),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp74,
                                                    FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp261, MulSIMD(FDPart3tmp258, FDPart3tmp90)),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp72,
                                                        FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp253,
                                                                        MulSIMD(FDPart3tmp104, MulSIMD(FDPart3tmp156, FDPart3tmp90))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp72,
                                                            FusedMulAddSIMD(FDPart3tmp125, FDPart3tmp228,
                                                                            MulSIMD(FDPart3tmp112, MulSIMD(FDPart3tmp120, FDPart3tmp90))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp71,
                                                                FusedMulAddSIMD(FDPart3tmp247, FDPart3tmp90,
                                                                                MulSIMD(FDPart3tmp132, MulSIMD(FDPart3tmp171, FDPart3tmp88))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp72, FusedMulAddSIMD(FDPart3tmp171, FDPart3tmp225, FDPart3tmp265),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp71,
                                                                        FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp218,
                                                                                        MulSIMD(FDPart3tmp216, FDPart3tmp90)),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp71,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp243, FDPart3tmp90,
                                                                                MulSIMD(FDPart3tmp102, MulSIMD(FDPart3tmp132, FDPart3tmp245))),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp68,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp132, MulSIMD(FDPart3tmp153, FDPart3tmp88),
                                                                                    MulSIMD(FDPart3tmp156, MulSIMD(FDPart3tmp88, FDPart3tmp90))),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp71,
                                                                                    FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp215,
                                                                                                    MulSIMD(FDPart3tmp213, FDPart3tmp90)),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp68,
                                                                                        FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp234,
                                                                                                        MulSIMD(FDPart3tmp235,
                                                                                                                MulSIMD(FDPart3tmp85, FDPart3tmp90))),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp68,
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp125, FDPart3tmp225,
                                                                                                MulSIMD(FDPart3tmp114,
                                                                                                        MulSIMD(FDPart3tmp122, FDPart3tmp90))),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp1, lambdaU_dD11,
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp68,
                                                                                                    FusedMulAddSIMD(FDPart3tmp228, FDPart3tmp245,
                                                                                                                    FDPart3tmp263),
                                                                                                    FusedMulAddSIMD(
                                                                                                        hDD12, lambdaU_dD21,
                                                                                                        FusedMulSubSIMD(
                                                                                                            FDPart3_Integer_2,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp193,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                    MulSIMD(FDPart3tmp112, hDD01),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                        MulSIMD(FDPart3tmp114, hDD12),
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(FDPart3tmp1,
                                                                                                                                    FDPart3tmp113)))),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp198,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                        MulSIMD(FDPart3tmp102, hDD01),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(FDPart3tmp104,
                                                                                                                                    hDD12),
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3_Rational_1_2,
                                                                                                                                MulSIMD(
                                                                                                                                    FDPart3tmp1,
                                                                                                                                    FDPart3tmp103)))),
                                                                                                                    MulSIMD(
                                                                                                                        FDPart3tmp191,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(FDPart3tmp85,
                                                                                                                                    hDD01),
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3_Rational_1_2,
                                                                                                                                MulSIMD(FDPart3tmp88,
                                                                                                                                        hDD12),
                                                                                                                                MulSIMD(
                                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp1,
                                                                                                                                        FDPart3tmp87))))))),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp71, hDD_dDD1102,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp72, hDD_dDD1112,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp76, hDD_dDD1111,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp78,
                                                                                                                                hDD_dDD1122,
                                                                                                                                MulSIMD(
                                                                                                                                    FDPart3tmp74,
                                                                                                                                    hDD_dDD1100))),
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3tmp68,
                                                                                                                            hDD_dDD1101))))))))))))))))))))))))))),
                FusedMulAddSIMD(
                    FDPart3tmp72,
                    FusedMulAddSIMD(
                        FDPart3tmp76, FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp277, MulSIMD(FDPart3tmp113, MulSIMD(FDPart3tmp156, FDPart3tmp90))),
                        FusedMulAddSIMD(
                            FDPart3tmp72,
                            FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp178, FDPart3tmp90),
                                            MulSIMD(FDPart3tmp113, MulSIMD(FDPart3tmp169, FDPart3tmp90))),
                            FusedMulAddSIMD(
                                FDPart3tmp74, FusedMulAddSIMD(FDPart3tmp384, FDPart3tmp90, FDPart3tmp363),
                                FusedMulAddSIMD(
                                    FDPart3tmp71, FusedMulAddSIMD(FDPart3tmp379, FDPart3tmp90, FDPart3tmp356),
                                    FusedMulAddSIMD(
                                        FDPart3tmp72, FusedMulAddSIMD(FDPart3tmp108, MulSIMD(FDPart3tmp132, FDPart3tmp275), FDPart3tmp181),
                                        FusedMulAddSIMD(
                                            FDPart3tmp68, FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp359, MulSIMD(FDPart3tmp389, FDPart3tmp90)),
                                            FusedMulAddSIMD(
                                                FDPart3tmp71, FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp392, FDPart3tmp128),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp78, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp189, FDPart3tmp176),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp90,
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp76, FusedMulAddSIMD(FDPart3tmp102, FDPart3tmp235, FDPart3tmp416),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp76,
                                                                FusedMulAddSIMD(FDPart3tmp104, FDPart3tmp156,
                                                                                FusedMulAddSIMD(FDPart3tmp114, FDPart3tmp169,
                                                                                                MulSIMD(FDPart3tmp104, FDPart3tmp153))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp74, AddSIMD(FDPart3tmp281, AddSIMD(FDPart3tmp305, FDPart3tmp329)),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp74, AddSIMD(FDPart3tmp321, AddSIMD(FDPart3tmp345, FDPart3tmp401)),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp72,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp104, FDPart3tmp171,
                                                                                FusedMulAddSIMD(FDPart3tmp114, FDPart3tmp210, FDPart3tmp202)),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp72,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp109, FDPart3tmp153,
                                                                                    FusedMulAddSIMD(FDPart3tmp109, FDPart3tmp156, FDPart3tmp202)),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp72, AddSIMD(FDPart3tmp129, FDPart3tmp412),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp72,
                                                                                        FusedMulAddSIMD(FDPart3tmp107, FDPart3tmp235, FDPart3tmp417),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp71,
                                                                                            FusedMulAddSIMD(FDPart3tmp109, FDPart3tmp122,
                                                                                                            FusedMulAddSIMD(FDPart3tmp109,
                                                                                                                            FDPart3tmp125,
                                                                                                                            FDPart3tmp199)),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp71,
                                                                                                AddSIMD(FDPart3tmp201,
                                                                                                        AddSIMD(FDPart3tmp348, FDPart3tmp388)),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp71,
                                                                                                    AddSIMD(FDPart3tmp397, FDPart3tmp422),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp71,
                                                                                                        AddSIMD(
                                                                                                            FDPart3tmp121,
                                                                                                            AddSIMD(FDPart3tmp331, FDPart3tmp398)),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp68,
                                                                                                            AddSIMD(FDPart3tmp216,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp104,
                                                                                                                                    FDPart3tmp125,
                                                                                                                                    FDPart3tmp375)),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp68,
                                                                                                                AddSIMD(
                                                                                                                    FDPart3tmp337,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp235,
                                                                                                                                    FDPart3tmp96,
                                                                                                                                    FDPart3tmp395)),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp78,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp107,
                                                                                                                                    FDPart3tmp120,
                                                                                                                                    FDPart3tmp414),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp78,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp109,
                                                                                                                            FDPart3tmp169,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp109,
                                                                                                                                FDPart3tmp171,
                                                                                                                                FDPart3tmp212)),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp68,
                                                                                                                            AddSIMD(FDPart3tmp243,
                                                                                                                                    FDPart3tmp420),
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3tmp68,
                                                                                                                                AddSIMD(
                                                                                                                                    FDPart3tmp247,
                                                                                                                                    FDPart3tmp341))))))))))))))))))),
                                                        FusedMulAddSIMD(FDPart3tmp68,
                                                                        FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp269,
                                                                                        MulSIMD(FDPart3tmp113, MulSIMD(FDPart3tmp122, FDPart3tmp90))),
                                                                        FDPart3tmp428)))))))))),
                    FusedMulAddSIMD(
                        FDPart3tmp74,
                        FusedMulAddSIMD(
                            hDD01, lambdaU_dD10,
                            FusedMulAddSIMD(
                                FDPart3tmp78,
                                FusedMulAddSIMD(FDPart3tmp133, FDPart3tmp90, MulSIMD(FDPart3tmp120, MulSIMD(FDPart3tmp132, FDPart3tmp97))),
                                FusedMulAddSIMD(
                                    FDPart3tmp90,
                                    FusedMulAddSIMD(
                                        FDPart3tmp117, MulSIMD(FDPart3tmp145, FDPart3tmp207),
                                        FusedMulAddSIMD(
                                            FDPart3_Integer_3, MulSIMD(FDPart3tmp182, FDPart3tmp78),
                                            FusedMulAddSIMD(
                                                FDPart3_Integer_3, MulSIMD(FDPart3tmp258, FDPart3tmp76),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp209, FDPart3tmp321,
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp209, FDPart3tmp322,
                                                        FusedMulAddSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp200, FDPart3tmp242),
                                                                        FusedMulAddSIMD(MulSIMD(FDPart3_Integer_3, FDPart3tmp117),
                                                                                        MulSIMD(FDPart3tmp325, FDPart3tmp74),
                                                                                        FusedMulAddSIMD(FDPart3tmp200, FDPart3tmp326,
                                                                                                        MulSIMD(FDPart3tmp207, FDPart3tmp327))))))))),
                                    FusedMulAddSIMD(
                                        FDPart3tmp76,
                                        FusedMulAddSIMD(FDPart3tmp220, FDPart3tmp90, MulSIMD(FDPart3tmp120, MulSIMD(FDPart3tmp132, FDPart3tmp88))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp78, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp310, MulSIMD(FDPart3tmp204, FDPart3tmp90)),
                                            FusedMulAddSIMD(
                                                FDPart3tmp74,
                                                FusedMulAddSIMD(FDPart3tmp242, FDPart3tmp291,
                                                                MulSIMD(FDPart3tmp118, MulSIMD(FDPart3tmp245, FDPart3tmp90))),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp76,
                                                    FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp313, MulSIMD(FDPart3tmp272, FDPart3tmp90)),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp72,
                                                        FusedMulAddSIMD(FDPart3tmp305, FDPart3tmp90,
                                                                        MulSIMD(FDPart3tmp132, MulSIMD(FDPart3tmp164, FDPart3tmp88))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp74,
                                                            FusedMulAddSIMD(FDPart3tmp145, FDPart3tmp288,
                                                                            MulSIMD(FDPart3tmp119, MulSIMD(FDPart3tmp148, FDPart3tmp90))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp72,
                                                                FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp283,
                                                                                MulSIMD(FDPart3tmp281, FDPart3tmp90)),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp72,
                                                                    FusedMulAddSIMD(FDPart3tmp302, FDPart3tmp90,
                                                                                    MulSIMD(FDPart3tmp132, MulSIMD(FDPart3tmp235, FDPart3tmp97))),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp71,
                                                                        FusedMulAddSIMD(FDPart3tmp132, MulSIMD(FDPart3tmp242, FDPart3tmp97),
                                                                                        MulSIMD(FDPart3tmp245, MulSIMD(FDPart3tmp90, FDPart3tmp97))),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp72,
                                                                            FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp280,
                                                                                            MulSIMD(FDPart3tmp278, FDPart3tmp90)),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp71,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3_Integer_2, FDPart3tmp300,
                                                                                    MulSIMD(FDPart3tmp148, MulSIMD(FDPart3tmp90, FDPart3tmp98))),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp71,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp120, FDPart3tmp291,
                                                                                        MulSIMD(FDPart3tmp118, MulSIMD(FDPart3tmp125, FDPart3tmp90))),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp68,
                                                                                        FusedMulAddSIMD(FDPart3tmp132,
                                                                                                        MulSIMD(FDPart3tmp145, FDPart3tmp88),
                                                                                                        MulSIMD(FDPart3tmp148,
                                                                                                                MulSIMD(FDPart3tmp88, FDPart3tmp90))),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp71,
                                                                                            FusedMulAddSIMD(FDPart3tmp164, FDPart3tmp288,
                                                                                                            FDPart3tmp317),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp68,
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3_Integer_2, FDPart3tmp293,
                                                                                                    MulSIMD(FDPart3tmp245,
                                                                                                            MulSIMD(FDPart3tmp87, FDPart3tmp90))),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp68,
                                                                                                    FusedMulAddSIMD(FDPart3tmp120, FDPart3tmp288,
                                                                                                                    MulSIMD(FDPart3tmp119,
                                                                                                                            MulSIMD(FDPart3tmp122,
                                                                                                                                    FDPart3tmp90))),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp6, lambdaU_dD00,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp68,
                                                                                                            FusedMulAddSIMD(FDPart3tmp235,
                                                                                                                            FDPart3tmp291,
                                                                                                                            FDPart3tmp315),
                                                                                                            FusedMulAddSIMD(
                                                                                                                hDD02, lambdaU_dD20,
                                                                                                                FusedMulSubSIMD(
                                                                                                                    FDPart3_Integer_2,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp193,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(FDPart3tmp87,
                                                                                                                                    hDD01),
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3_Rational_1_2,
                                                                                                                                MulSIMD(FDPart3tmp88,
                                                                                                                                        hDD02),
                                                                                                                                MulSIMD(
                                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp6,
                                                                                                                                        FDPart3tmp85)))),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp198,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3_Rational_1_2,
                                                                                                                                MulSIMD(FDPart3tmp97,
                                                                                                                                        hDD01),
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp98,
                                                                                                                                        hDD02),
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                                        MulSIMD(
                                                                                                                                            FDPart3tmp6,
                                                                                                                                            FDPart3tmp96)))),
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3tmp191,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp118,
                                                                                                                                        hDD01),
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                                        MulSIMD(
                                                                                                                                            FDPart3tmp119,
                                                                                                                                            hDD02),
                                                                                                                                        MulSIMD(
                                                                                                                                            FDPart3_Rational_1_2,
                                                                                                                                            MulSIMD(
                                                                                                                                                FDPart3tmp117,
                                                                                                                                                FDPart3tmp6))))))),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp71, hDD_dDD0002,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp72, hDD_dDD0012,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3_Rational_1_2,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp76,
                                                                                                                                    hDD_dDD0011,
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp78,
                                                                                                                                        hDD_dDD0022,
                                                                                                                                        MulSIMD(
                                                                                                                                            FDPart3tmp74,
                                                                                                                                            hDD_dDD0000))),
                                                                                                                                MulSIMD(
                                                                                                                                    FDPart3tmp68,
                                                                                                                                    hDD_dDD0001))))))))))))))))))))))))))),
                        FusedMulAddSIMD(
                            FDPart3tmp71,
                            FusedMulAddSIMD(
                                FDPart3tmp76, FusedMulAddSIMD(FDPart3tmp395, FDPart3tmp90, FDPart3tmp377),
                                FusedMulAddSIMD(
                                    FDPart3tmp72, FusedMulAddSIMD(FDPart3tmp398, FDPart3tmp90, FDPart3tmp376),
                                    FusedMulAddSIMD(
                                        FDPart3tmp74,
                                        FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp327, MulSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp148, FDPart3tmp90))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp71,
                                            FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp182, FDPart3tmp90),
                                                            MulSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp161, FDPart3tmp90))),
                                            FusedMulAddSIMD(
                                                FDPart3tmp72, FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp397, FDPart3tmp124),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp68, FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp346, MulSIMD(FDPart3tmp401, FDPart3tmp90)),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp71,
                                                        FusedMulAddSIMD(FDPart3tmp107, MulSIMD(FDPart3tmp132, FDPart3tmp325), FDPart3tmp185),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp78, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp187, FDPart3tmp163),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp90,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp76, AddSIMD(FDPart3tmp216, AddSIMD(FDPart3tmp247, FDPart3tmp339)),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp76, AddSIMD(FDPart3tmp269, AddSIMD(FDPart3tmp358, FDPart3tmp389)),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp74, FusedMulAddSIMD(FDPart3tmp245, FDPart3tmp97, FDPart3tmp385),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp74,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp145, FDPart3tmp98,
                                                                                    FusedMulAddSIMD(FDPart3tmp148, FDPart3tmp98,
                                                                                                    MulSIMD(FDPart3tmp119, FDPart3tmp161))),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp72,
                                                                                    AddSIMD(FDPart3tmp199, AddSIMD(FDPart3tmp347, FDPart3tmp388)),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp72,
                                                                                        FusedMulAddSIMD(FDPart3tmp109, FDPart3tmp120,
                                                                                                        FusedMulAddSIMD(FDPart3tmp109, FDPart3tmp122,
                                                                                                                        FDPart3tmp201)),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp72, AddSIMD(FDPart3tmp392, FDPart3tmp393),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp72,
                                                                                                AddSIMD(FDPart3tmp126,
                                                                                                        AddSIMD(FDPart3tmp355, FDPart3tmp379)),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp71,
                                                                                                    FusedMulAddSIMD(FDPart3tmp109, FDPart3tmp145,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp109,
                                                                                                                                    FDPart3tmp148,
                                                                                                                                    FDPart3tmp204)),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp71,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp119, FDPart3tmp210,
                                                                                                            FusedMulAddSIMD(FDPart3tmp164,
                                                                                                                            FDPart3tmp98,
                                                                                                                            FDPart3tmp204)),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp71,
                                                                                                            AddSIMD(FDPart3tmp133, FDPart3tmp380),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp71,
                                                                                                                FusedMulAddSIMD(FDPart3tmp108,
                                                                                                                                FDPart3tmp245,
                                                                                                                                FDPart3tmp386),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp68,
                                                                                                                    AddSIMD(FDPart3tmp371,
                                                                                                                            FDPart3tmp384),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp68,
                                                                                                                        AddSIMD(FDPart3tmp281,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp120,
                                                                                                                                    FDPart3tmp98,
                                                                                                                                    FDPart3tmp352)),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp78,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp108,
                                                                                                                                FDPart3tmp125,
                                                                                                                                FDPart3tmp383),
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp78,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp109,
                                                                                                                                    FDPart3tmp161,
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp109,
                                                                                                                                        FDPart3tmp164,
                                                                                                                                        FDPart3tmp211)),
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp68,
                                                                                                                                    AddSIMD(
                                                                                                                                        FDPart3tmp302,
                                                                                                                                        FDPart3tmp390),
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp68,
                                                                                                                                        AddSIMD(
                                                                                                                                            FDPart3tmp305,
                                                                                                                                            FDPart3tmp372))))))))))))))))))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp68,
                                                                    FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp321,
                                                                                    MulSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp122, FDPart3tmp90))),
                                                                    FDPart3tmp403)))))))))),
                            FusedMulAddSIMD(
                                FDPart3tmp72,
                                FusedMulAddSIMD(
                                    FDPart3tmp76, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp265, FDPart3tmp253),
                                    FusedMulAddSIMD(
                                        FDPart3tmp72,
                                        FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp202, FDPart3tmp90),
                                                        MulSIMD(FDPart3tmp109, MulSIMD(FDPart3tmp153, FDPart3tmp90))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp74, FusedMulAddSIMD(FDPart3tmp329, FDPart3tmp90, FDPart3tmp410),
                                            FusedMulAddSIMD(
                                                FDPart3tmp71, FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp388, MulSIMD(FDPart3tmp348, FDPart3tmp90)),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp72,
                                                    FusedMulAddSIMD(FDPart3tmp114, MulSIMD(FDPart3tmp132, FDPart3tmp210), FDPart3tmp257),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp68, FusedMulAddSIMD(FDPart3tmp340, FDPart3tmp90, FDPart3tmp408),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp71,
                                                            FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp199,
                                                                            MulSIMD(FDPart3tmp109, MulSIMD(FDPart3tmp125, FDPart3tmp90))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp78,
                                                                FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp212,
                                                                                MulSIMD(FDPart3tmp109, MulSIMD(FDPart3tmp171, FDPart3tmp90))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp90,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp76, FusedMulAddSIMD(FDPart3tmp112, FDPart3tmp120, FDPart3tmp416),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp76,
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp113, FDPart3tmp153,
                                                                                FusedMulAddSIMD(FDPart3tmp113, FDPart3tmp156, FDPart3tmp277)),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp74,
                                                                                AddSIMD(FDPart3tmp278, AddSIMD(FDPart3tmp302, FDPart3tmp384)),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp74,
                                                                                    AddSIMD(FDPart3tmp322, AddSIMD(FDPart3tmp345, FDPart3tmp401)),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp72,
                                                                                        FusedMulAddSIMD(FDPart3tmp103, FDPart3tmp156,
                                                                                                        FusedMulAddSIMD(FDPart3tmp108, FDPart3tmp275,
                                                                                                                        FDPart3tmp178)),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp72,
                                                                                            FusedMulAddSIMD(FDPart3tmp113, FDPart3tmp169,
                                                                                                            FusedMulAddSIMD(FDPart3tmp113,
                                                                                                                            FDPart3tmp171,
                                                                                                                            FDPart3tmp178)),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp72, AddSIMD(FDPart3tmp129, FDPart3tmp417),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp72,
                                                                                                    FusedMulAddSIMD(FDPart3tmp112, FDPart3tmp164,
                                                                                                                    FDPart3tmp412),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp71,
                                                                                                        AddSIMD(FDPart3tmp126,
                                                                                                                FusedMulAddSIMD(FDPart3tmp103,
                                                                                                                                FDPart3tmp122,
                                                                                                                                FDPart3tmp392)),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp71,
                                                                                                            AddSIMD(FDPart3tmp331,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp164,
                                                                                                                                    FDPart3tmp85,
                                                                                                                                    FDPart3tmp398)),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp71,
                                                                                                                AddSIMD(FDPart3tmp146, FDPart3tmp422),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp71,
                                                                                                                    AddSIMD(FDPart3tmp154,
                                                                                                                            FDPart3tmp406),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp68,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp113,
                                                                                                                            FDPart3tmp122,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp113,
                                                                                                                                FDPart3tmp125,
                                                                                                                                FDPart3tmp269)),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp68,
                                                                                                                            AddSIMD(
                                                                                                                                FDPart3tmp270,
                                                                                                                                AddSIMD(
                                                                                                                                    FDPart3tmp359,
                                                                                                                                    FDPart3tmp389)),
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp78,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp102,
                                                                                                                                    FDPart3tmp164,
                                                                                                                                    FDPart3tmp414),
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp78,
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp103,
                                                                                                                                        FDPart3tmp171,
                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                            FDPart3tmp108,
                                                                                                                                            FDPart3tmp153,
                                                                                                                                            MulSIMD(
                                                                                                                                                FDPart3tmp103,
                                                                                                                                                FDPart3tmp169))),
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp68,
                                                                                                                                        AddSIMD(
                                                                                                                                            FDPart3tmp333,
                                                                                                                                            FDPart3tmp420),
                                                                                                                                        MulSIMD(
                                                                                                                                            FDPart3tmp68,
                                                                                                                                            AddSIMD(
                                                                                                                                                FDPart3tmp213,
                                                                                                                                                AddSIMD(
                                                                                                                                                    FDPart3tmp337,
                                                                                                                                                    FDPart3tmp395)))))))))))))))))))),
                                                                    FusedMulAddSIMD(FDPart3tmp68,
                                                                                    FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp375, FDPart3tmp218),
                                                                                    FDPart3tmp428)))))))))),
                                FusedMulAddSIMD(
                                    FDPart3tmp68,
                                    FusedMulAddSIMD(
                                        FDPart3tmp76,
                                        FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp276, MulSIMD(FDPart3tmp113, MulSIMD(FDPart3tmp235, FDPart3tmp90))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp72, FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp359, MulSIMD(FDPart3tmp358, FDPart3tmp90)),
                                            FusedMulAddSIMD(
                                                FDPart3tmp74, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp315, FDPart3tmp293),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp71, FusedMulAddSIMD(FDPart3tmp362, FDPart3tmp90, FDPart3tmp363),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp72,
                                                        FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp270,
                                                                        MulSIMD(FDPart3tmp113, MulSIMD(FDPart3tmp120, FDPart3tmp90))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp68,
                                                            FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp272, FDPart3tmp90),
                                                                            MulSIMD(FDPart3tmp113, MulSIMD(FDPart3tmp242, FDPart3tmp90))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp71, FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp364, FDPart3tmp280),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp78, FusedMulAddSIMD(FDPart3tmp355, FDPart3tmp90, FDPart3tmp356),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp90,
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp76, FusedMulAddSIMD(FDPart3tmp156, FDPart3tmp88, FDPart3tmp342),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp76,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp232, FDPart3tmp85,
                                                                                    FusedMulAddSIMD(FDPart3tmp235, FDPart3tmp85,
                                                                                                    MulSIMD(FDPart3tmp112, FDPart3tmp242))),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp74,
                                                                                    FusedMulAddSIMD(FDPart3tmp119, FDPart3tmp122, FDPart3tmp336),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp74,
                                                                                        FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp242,
                                                                                                        FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp245,
                                                                                                                        FDPart3tmp326)),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp72,
                                                                                            AddSIMD(FDPart3tmp213,
                                                                                                    FusedMulAddSIMD(FDPart3tmp125, FDPart3tmp85,
                                                                                                                    FDPart3tmp333)),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp72,
                                                                                                AddSIMD(FDPart3tmp243,
                                                                                                        FusedMulAddSIMD(FDPart3tmp235, FDPart3tmp96,
                                                                                                                        FDPart3tmp337)),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp72,
                                                                                                    AddSIMD(FDPart3tmp247, FDPart3tmp351),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp72,
                                                                                                        AddSIMD(FDPart3tmp339, FDPart3tmp341),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp71,
                                                                                                            AddSIMD(FDPart3tmp321,
                                                                                                                    AddSIMD(FDPart3tmp345,
                                                                                                                            FDPart3tmp346)),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp71,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp117, FDPart3tmp120,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp117,
                                                                                                                                    FDPart3tmp125,
                                                                                                                                    FDPart3tmp322)),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp71,
                                                                                                                    AddSIMD(FDPart3tmp352,
                                                                                                                            FDPart3tmp353),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp71,
                                                                                                                        AddSIMD(
                                                                                                                            FDPart3tmp281,
                                                                                                                            AddSIMD(FDPart3tmp328,
                                                                                                                                    FDPart3tmp329)),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp68,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp112,
                                                                                                                                FDPart3tmp325,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp245,
                                                                                                                                    FDPart3tmp85,
                                                                                                                                    FDPart3tmp258)),
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp68,
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp117,
                                                                                                                                    FDPart3tmp232,
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp117,
                                                                                                                                        FDPart3tmp235,
                                                                                                                                        FDPart3tmp258)),
                                                                                                                                FusedMulAddSIMD(
                                                                                                                                    FDPart3tmp78,
                                                                                                                                    AddSIMD(
                                                                                                                                        FDPart3tmp121,
                                                                                                                                        AddSIMD(
                                                                                                                                            FDPart3tmp146,
                                                                                                                                            FDPart3tmp331)),
                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                        FDPart3tmp78,
                                                                                                                                        AddSIMD(
                                                                                                                                            FDPart3tmp201,
                                                                                                                                            AddSIMD(
                                                                                                                                                FDPart3tmp347,
                                                                                                                                                FDPart3tmp348)),
                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                            FDPart3tmp68,
                                                                                                                                            AddSIMD(
                                                                                                                                                FDPart3tmp220,
                                                                                                                                                FDPart3tmp332),
                                                                                                                                            MulSIMD(
                                                                                                                                                FDPart3tmp68,
                                                                                                                                                FusedMulAddSIMD(
                                                                                                                                                    FDPart3tmp119,
                                                                                                                                                    FDPart3tmp156,
                                                                                                                                                    FDPart3tmp344))))))))))))))))))),
                                                                        FusedMulAddSIMD(FDPart3tmp68,
                                                                                        FusedMulAddSIMD(FDPart3tmp118,
                                                                                                        MulSIMD(FDPart3tmp132, FDPart3tmp275),
                                                                                                        FDPart3tmp313),
                                                                                        FDPart3tmp367)))))))))),
                                    FusedMulAddSIMD(FDPart3tmp71,
                                                    FusedMulAddSIMD(FDPart3tmp76, FusedMulAddSIMD(FDPart3tmp339, FDPart3tmp90, FDPart3tmp408),
                                                                    FusedMulAddSIMD(FDPart3tmp72,
                                                                                    FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp388,
                                                                                                    MulSIMD(FDPart3tmp347, FDPart3tmp90)),
                                                                                    FusedMulAddSIMD(FDPart3tmp74,
                                                                                                    FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp317,
                                                                                                                    FDPart3tmp300),
                                                                                                    FusedMulAddSIMD(FDPart3tmp71,
                                                                                                                    FusedMulAddSIMD(FDPart3_Integer_2,
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp204,
                                                                                                                                        FDPart3tmp90),
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp109,
                                                                                                                                        MulSIMD(
                                                                                                                                            FDPart3tmp145,
                                                                                                                                            FDPart3tmp90))),
                                                                                                                    FusedMulAddSIMD(FDPart3tmp72,
                                                                                                                                    FusedMulAddSIMD(FDPart3tmp132,
                                                                                                                                                    FDPart3tmp201,
                                                                                                                                                    MulSIMD(
                                                                                                                                                        FDPart3tmp109,
                                                                                                                                                        MulSIMD(
                                                                                                                                                            FDPart3tmp120,
                                                                                                                                                            FDPart3tmp90))),
                                                                                                                                    FusedMulAddSIMD(FDPart3tmp68,
                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp328,
                                                                                                                                                                    FDPart3tmp90,
                                                                                                                                                                    FDPart3tmp410),
                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp71,
                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp119,
                                                                                                                                                                                    MulSIMD(
                                                                                                                                                                                        FDPart3tmp132,
                                                                                                                                                                                        FDPart3tmp210),
                                                                                                                                                                                    FDPart3tmp310),
                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp78, FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp211, MulSIMD(FDPart3tmp109, MulSIMD(FDPart3tmp164, FDPart3tmp90))),
                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp90,
                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp76,
                                                                                                                                                                                                                    AddSIMD(
                                                                                                                                                                                                                        FDPart3tmp213,
                                                                                                                                                                                                                        AddSIMD(
                                                                                                                                                                                                                            FDPart3tmp243,
                                                                                                                                                                                                                            FDPart3tmp395)),
                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp76,
                                                                                                                                                                                                                                    AddSIMD(FDPart3tmp270,
                                                                                                                                                                                                                                            AddSIMD(FDPart3tmp358, FDPart3tmp389)),
                                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp74,
                                                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp118,
                                                                                                                                                                                                                                                                    FDPart3tmp125,
                                                                                                                                                                                                                                                                    FDPart3tmp385),
                                                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp74,
                                                                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp117,
                                                                                                                                                                                                                                                                                    FDPart3tmp145, FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp148, FDPart3tmp327)),
                                                                                                                                                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                                                                                                                                                        FDPart3tmp72,
                                                                                                                                                                                                                                                                        AddSIMD(
                                                                                                                                                                                                                                                                            FDPart3tmp121,
                                                                                                                                                                                                                                                                            FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                FDPart3tmp122,
                                                                                                                                                                                                                                                                                FDPart3tmp96,
                                                                                                                                                                                                                                                                                FDPart3tmp397)),
                                                                                                                                                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                                                                                                                                                            FDPart3tmp72,
                                                                                                                                                                                                                                                                            AddSIMD(
                                                                                                                                                                                                                                                                                FDPart3tmp146, FusedMulAddSIMD(FDPart3tmp164,
                                                                                                                                                                                                                                                                                                               FDPart3tmp85,
                                                                                                                                                                                                                                                                                                               FDPart3tmp398)),
                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp72,
                                                                                                                                                                                                                                                                                            AddSIMD(
                                                                                                                                                                                                                                                                                                FDPart3tmp154,
                                                                                                                                                                                                                                                                                                FDPart3tmp393),
                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp72, AddSIMD(FDPart3tmp355, FDPart3tmp406),
                                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp71,
                                                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp107,
                                                                                                                                                                                                                                                                                                                                            FDPart3tmp325, FusedMulAddSIMD(FDPart3tmp148, FDPart3tmp96, FDPart3tmp182)),
                                                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                FDPart3tmp71,
                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp161, FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp164, FDPart3tmp182)),
                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                    FDPart3tmp71,
                                                                                                                                                                                                                                                                                                                                    AddSIMD(
                                                                                                                                                                                                                                                                                                                                        FDPart3tmp133,
                                                                                                                                                                                                                                                                                                                                        FDPart3tmp386),
                                                                                                                                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp71, FusedMulAddSIMD(FDPart3tmp118, FDPart3tmp171, FDPart3tmp380),
                                                                                                                                                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp68,
                                                                                                                                                                                                                                                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                                        FDPart3tmp117, FDPart3tmp120, FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp122, FDPart3tmp321)),
                                                                                                                                                                                                                                                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                                        FDPart3tmp68,
                                                                                                                                                                                                                                                                                                                                                                        AddSIMD(
                                                                                                                                                                                                                                                                                                                                                                            FDPart3tmp322, AddSIMD(
                                                                                                                                                                                                                                                                                                                                                                                               FDPart3tmp346,
                                                                                                                                                                                                                                                                                                                                                                                               FDPart3tmp401)),
                                                                                                                                                                                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp78, FusedMulAddSIMD(FDPart3tmp171, FDPart3tmp97, FDPart3tmp383),
                                                                                                                                                                                                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp78, FusedMulAddSIMD(FDPart3tmp161, FDPart3tmp96, FusedMulAddSIMD(FDPart3tmp164, FDPart3tmp96, MulSIMD(FDPart3tmp107, FDPart3tmp145))),
                                                                                                                                                                                                                                                                                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                            FDPart3tmp68, AddSIMD(FDPart3tmp364, FDPart3tmp390), MulSIMD(FDPart3tmp68, AddSIMD(FDPart3tmp278, AddSIMD(FDPart3tmp362, FDPart3tmp384)))))))))))))))))))),
                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp68, FusedMulAddSIMD(FDPart3tmp132, FDPart3tmp352, FDPart3tmp283), FDPart3tmp403)))))))))),
                                                    FusedMulAddSIMD(FDPart3tmp78,
                                                                    FusedMulAddSIMD(hDD02, lambdaU_dD02,
                                                                                    FusedMulAddSIMD(FDPart3tmp78,
                                                                                                    FusedMulAddSIMD(FDPart3tmp141, FDPart3tmp161,
                                                                                                                    MulSIMD(FDPart3tmp107,
                                                                                                                            MulSIMD(FDPart3tmp164,
                                                                                                                                    FDPart3tmp90))),
                                                                                                    FusedMulAddSIMD(FDPart3tmp90,
                                                                                                                    FusedMulAddSIMD(FDPart3tmp109,
                                                                                                                                    MulSIMD(
                                                                                                                                        FDPart3tmp161,
                                                                                                                                        FDPart3tmp207),
                                                                                                                                    FusedMulAddSIMD(FDPart3_Integer_3,
                                                                                                                                                    MulSIMD(
                                                                                                                                                        FDPart3tmp202,
                                                                                                                                                        FDPart3tmp76),
                                                                                                                                                    FusedMulAddSIMD(FDPart3_Integer_3,
                                                                                                                                                                    MulSIMD(FDPart3tmp204, FDPart3tmp74), FusedMulAddSIMD(FDPart3tmp207, FDPart3tmp211, FusedMulAddSIMD(FDPart3tmp209, FDPart3tmp212, FusedMulAddSIMD(FDPart3tmp109, MulSIMD(FDPart3tmp169, FDPart3tmp209), FusedMulAddSIMD(MulSIMD(FDPart3_Integer_3, FDPart3tmp109), MulSIMD(FDPart3tmp210, FDPart3tmp78), FusedMulAddSIMD(FDPart3tmp199, FDPart3tmp200, MulSIMD(FDPart3tmp200, FDPart3tmp201))))))))),
                                                                                                                    FusedMulAddSIMD(FDPart3tmp76,
                                                                                                                                    FusedMulAddSIMD(FDPart3tmp129,
                                                                                                                                                    FDPart3tmp90,
                                                                                                                                                    MulSIMD(
                                                                                                                                                        FDPart3tmp102,
                                                                                                                                                        MulSIMD(
                                                                                                                                                            FDPart3tmp122,
                                                                                                                                                            FDPart3tmp132))),
                                                                                                                                    FusedMulAddSIMD(FDPart3tmp78,
                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp138,
                                                                                                                                                                    FDPart3tmp169,
                                                                                                                                                                    MulSIMD(
                                                                                                                                                                        FDPart3tmp108,
                                                                                                                                                                        MulSIMD(
                                                                                                                                                                            FDPart3tmp171,
                                                                                                                                                                            FDPart3tmp90))),
                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp74,
                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp133,
                                                                                                                                                                                    FDPart3tmp90,
                                                                                                                                                                                    MulSIMD(
                                                                                                                                                                                        FDPart3tmp122,
                                                                                                                                                                                        MulSIMD(
                                                                                                                                                                                            FDPart3tmp132,
                                                                                                                                                                                            FDPart3tmp97))),
                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp76,
                                                                                                                                                                                    FusedMulAddSIMD(FDPart3_Integer_2,
                                                                                                                                                                                                    FDPart3tmp181,
                                                                                                                                                                                                    MulSIMD(
                                                                                                                                                                                                        FDPart3tmp178,
                                                                                                                                                                                                        FDPart3tmp90)),
                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp72,
                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp102,
                                                                                                                                                                                                                    MulSIMD(FDPart3tmp132, FDPart3tmp161),
                                                                                                                                                                                                                    MulSIMD(
                                                                                                                                                                                                                        FDPart3tmp102,
                                                                                                                                                                                                                        MulSIMD(
                                                                                                                                                                                                                            FDPart3tmp164,
                                                                                                                                                                                                                            FDPart3tmp90))),
                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp74,
                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3_Integer_2,
                                                                                                                                                                                                                                    FDPart3tmp185,
                                                                                                                                                                                                                                    MulSIMD(
                                                                                                                                                                                                                                        FDPart3tmp182,
                                                                                                                                                                                                                                        FDPart3tmp90)),
                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp72,
                                                                                                                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                                                                                                                        FDPart3_Integer_2, FDPart3tmp176, MulSIMD(FDPart3tmp103, MulSIMD(FDPart3tmp171, FDPart3tmp90))),
                                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp72,
                                                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp122,
                                                                                                                                                                                                                                                                    FDPart3tmp141,
                                                                                                                                                                                                                                                                    MulSIMD(
                                                                                                                                                                                                                                                                        FDPart3tmp107, MulSIMD(
                                                                                                                                                                                                                                                                                           FDPart3tmp120, FDPart3tmp90))),
                                                                                                                                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                                                                                                                                        FDPart3tmp71,
                                                                                                                                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                                                                                                                                            FDPart3tmp132,
                                                                                                                                                                                                                                                            MulSIMD(
                                                                                                                                                                                                                                                                FDPart3tmp169,
                                                                                                                                                                                                                                                                FDPart3tmp97),
                                                                                                                                                                                                                                                            MulSIMD(
                                                                                                                                                                                                                                                                FDPart3tmp171,
                                                                                                                                                                                                                                                                MulSIMD(
                                                                                                                                                                                                                                                                    FDPart3tmp90,
                                                                                                                                                                                                                                                                    FDPart3tmp97))),
                                                                                                                                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                                                                                                                                            FDPart3tmp72, FusedMulAddSIMD(FDPart3tmp138, FDPart3tmp156, FDPart3tmp189),
                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp71,
                                                                                                                                                                                                                                                                            FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                FDPart3_Integer_2, FDPart3tmp163, MulSIMD(FDPart3tmp164, MulSIMD(FDPart3tmp90, FDPart3tmp96))),
                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp71, FusedMulAddSIMD(FDPart3tmp122, FDPart3tmp138, MulSIMD(FDPart3tmp108, MulSIMD(FDPart3tmp125, FDPart3tmp90))),
                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp7,
                                                                                                                                                                                                                                                                                                            lambdaU_dD22,
                                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp71,
                                                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp141, FDPart3tmp148, FDPart3tmp187),
                                                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp68,
                                                                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp146,
                                                                                                                                                                                                                                                                                                                                                            FDPart3tmp90,
                                                                                                                                                                                                                                                                                                                                                            MulSIMD(FDPart3tmp102, MulSIMD(FDPart3tmp132, FDPart3tmp148))),
                                                                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp68,
                                                                                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp154,
                                                                                                                                                                                                                                                                                                                                                                            FDPart3tmp90,
                                                                                                                                                                                                                                                                                                                                                                            MulSIMD(
                                                                                                                                                                                                                                                                                                                                                                                FDPart3tmp132,
                                                                                                                                                                                                                                                                                                                                                                                MulSIMD(
                                                                                                                                                                                                                                                                                                                                                                                    FDPart3tmp156,
                                                                                                                                                                                                                                                                                                                                                                                    FDPart3tmp97))),
                                                                                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp68, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp124, MulSIMD(FDPart3tmp121, FDPart3tmp90)),
                                                                                                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                                                FDPart3tmp68,
                                                                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                                                    FDPart3_Integer_2,
                                                                                                                                                                                                                                                                                                                                                                                    FDPart3tmp128,
                                                                                                                                                                                                                                                                                                                                                                                    MulSIMD(
                                                                                                                                                                                                                                                                                                                                                                                        FDPart3tmp126,
                                                                                                                                                                                                                                                                                                                                                                                        FDPart3tmp90)),
                                                                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(hDD12,
                                                                                                                                                                                                                                                                                                                                                                                                lambdaU_dD12,
                                                                                                                                                                                                                                                                                                                                                                                                FusedMulSubSIMD(FDPart3_Integer_2,
                                                                                                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(FDPart3tmp193,
                                                                                                                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(FDPart3_Rational_1_2,
                                                                                                                                                                                                                                                                                                                                                                                                                                                MulSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                                                                    FDPart3tmp103,
                                                                                                                                                                                                                                                                                                                                                                                                                                                    hDD12),
                                                                                                                                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp104, FDPart3tmp7),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                MulSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                    FDPart3_Rational_1_2, MulSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              FDPart3tmp102,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              hDD02)))),
                                                                                                                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(FDPart3tmp198,
                                                                                                                                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(FDPart3_Rational_1_2,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                MulSIMD(FDPart3tmp108, hDD12),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(FDPart3_Rational_1_2,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                MulSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    FDPart3tmp109, FDPart3tmp7),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                MulSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp107, hDD02)))),
                                                                                                                                                                                                                                                                                                                                                                                                                                                MulSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                                                                    FDPart3tmp191, FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp96, hDD02),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   FusedMulAddSIMD(FDPart3_Rational_1_2, MulSIMD(FDPart3tmp97, hDD12),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   MulSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       FDPart3_Rational_1_2,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       MulSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           FDPart3tmp7, FDPart3tmp98))))))),
                                                                                                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(FDPart3tmp71, hDD_dDD2202,
                                                                                                                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                                                    FDPart3tmp72,
                                                                                                                                                                                                                                                                                                                                                                                                                                    hDD_dDD2212,
                                                                                                                                                                                                                                                                                                                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                                                                                                                                                                                                                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                                                            FDPart3tmp76,
                                                                                                                                                                                                                                                                                                                                                                                                                                            hDD_dDD2211, FusedMulAddSIMD(FDPart3tmp78, hDD_dDD2222, MulSIMD(FDPart3tmp74, hDD_dDD2200))),
                                                                                                                                                                                                                                                                                                                                                                                                                                        MulSIMD(FDPart3tmp68,
                                                                                                                                                                                                                                                                                                                                                                                                                                                hDD_dDD2201))))))))))))))))))))))))))),
                                                                    FusedMulSubSIMD(
                                                                        FDPart3tmp68,
                                                                        FusedMulAddSIMD(FDPart3tmp76,
                                                                                        FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp263,
                                                                                                        FDPart3tmp234),
                                                                                        FusedMulAddSIMD(FDPart3tmp72,
                                                                                                        FusedMulAddSIMD(FDPart3tmp337, FDPart3tmp90,
                                                                                                                        FDPart3tmp377),
                                                                                                        FusedMulAddSIMD(FDPart3tmp74,
                                                                                                                        FusedMulAddSIMD(FDPart3tmp132,
                                                                                                                                        FDPart3tmp326,
                                                                                                                                        MulSIMD(
                                                                                                                                            FDPart3tmp117,
                                                                                                                                            MulSIMD(
                                                                                                                                                FDPart3tmp245,
                                                                                                                                                FDPart3tmp90))),
                                                                                                                        FusedMulAddSIMD(FDPart3tmp71,
                                                                                                                                        FusedMulAddSIMD(FDPart3tmp132,
                                                                                                                                                        FDPart3tmp346,
                                                                                                                                                        MulSIMD(
                                                                                                                                                            FDPart3tmp345,
                                                                                                                                                            FDPart3tmp90)),
                                                                                                                                        FusedMulAddSIMD(FDPart3tmp72,
                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp132,
                                                                                                                                                                        FDPart3tmp333,
                                                                                                                                                                        FDPart3tmp215),
                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp68,
                                                                                                                                                                        FusedMulAddSIMD(FDPart3_Integer_2,
                                                                                                                                                                                        MulSIMD(FDPart3tmp258, FDPart3tmp90), MulSIMD(FDPart3tmp117, MulSIMD(FDPart3tmp232, FDPart3tmp90))),
                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp71,
                                                                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                                                                            FDPart3tmp132, FDPart3tmp322,
                                                                                                                                                                                            MulSIMD(
                                                                                                                                                                                                FDPart3tmp117,
                                                                                                                                                                                                MulSIMD(
                                                                                                                                                                                                    FDPart3tmp125,
                                                                                                                                                                                                    FDPart3tmp90))),
                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp78,
                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp331,
                                                                                                                                                                                                                        FDPart3tmp90,
                                                                                                                                                                                                                        FDPart3tmp376),
                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp90,
                                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp76,
                                                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp114, FDPart3tmp122,
                                                                                                                                                                                                                                                        FDPart3tmp342),
                                                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp76,
                                                                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp113,
                                                                                                                                                                                                                                                                        FDPart3tmp232,
                                                                                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp113,
                                                                                                                                                                                                                                                                                        FDPart3tmp235,
                                                                                                                                                                                                                                                                                        FDPart3tmp276)),
                                                                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp74,
                                                                                                                                                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                                                                                                                                                            FDPart3tmp148, FDPart3tmp88, FDPart3tmp336),
                                                                                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp74,
                                                                                                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp242,
                                                                                                                                                                                                                                                                                                        FDPart3tmp87,
                                                                                                                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp245, FDPart3tmp87, MulSIMD(FDPart3tmp118, FDPart3tmp232))),
                                                                                                                                                                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                            FDPart3tmp72,
                                                                                                                                                                                                                                                                                            AddSIMD(
                                                                                                                                                                                                                                                                                                FDPart3tmp269,
                                                                                                                                                                                                                                                                                                AddSIMD(
                                                                                                                                                                                                                                                                                                    FDPart3tmp358,
                                                                                                                                                                                                                                                                                                    FDPart3tmp359)),
                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                FDPart3tmp72,
                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                    FDPart3tmp113, FDPart3tmp120, FusedMulAddSIMD(FDPart3tmp113, FDPart3tmp125, FDPart3tmp270)),
                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(FDPart3tmp72, AddSIMD(FDPart3tmp351, FDPart3tmp375),
                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(FDPart3tmp72,
                                                                                                                                                                                                                                                                                                                                AddSIMD(
                                                                                                                                                                                                                                                                                                                                    FDPart3tmp216,
                                                                                                                                                                                                                                                                                                                                    AddSIMD(
                                                                                                                                                                                                                                                                                                                                        FDPart3tmp339,
                                                                                                                                                                                                                                                                                                                                        FDPart3tmp340)),
                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(FDPart3tmp71,
                                                                                                                                                                                                                                                                                                                                                AddSIMD(
                                                                                                                                                                                                                                                                                                                                                    FDPart3tmp329,
                                                                                                                                                                                                                                                                                                                                                    FDPart3tmp372),
                                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                    FDPart3tmp71,
                                                                                                                                                                                                                                                                                                                                                    AddSIMD(
                                                                                                                                                                                                                                                                                                                                                        FDPart3tmp278, FusedMulAddSIMD(FDPart3tmp120,
                                                                                                                                                                                                                                                                                                                                                                                       FDPart3tmp87, FDPart3tmp364)),
                                                                                                                                                                                                                                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                        FDPart3tmp71,
                                                                                                                                                                                                                                                                                                                                                        AddSIMD(
                                                                                                                                                                                                                                                                                                                                                            FDPart3tmp302,
                                                                                                                                                                                                                                                                                                                                                            FDPart3tmp371),
                                                                                                                                                                                                                                                                                                                                                        FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                            FDPart3tmp71,
                                                                                                                                                                                                                                                                                                                                                            AddSIMD(
                                                                                                                                                                                                                                                                                                                                                                FDPart3tmp305,
                                                                                                                                                                                                                                                                                                                                                                FDPart3tmp353),
                                                                                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                                FDPart3tmp68,
                                                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                                    FDPart3tmp113, FDPart3tmp242, FusedMulAddSIMD(FDPart3tmp113, FDPart3tmp245, FDPart3tmp272)),
                                                                                                                                                                                                                                                                                                                                                                FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                                    FDPart3tmp68,
                                                                                                                                                                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp118, FDPart3tmp275,
                                                                                                                                                                                                                                                                                                                                                                                    FusedMulAddSIMD(
                                                                                                                                                                                                                                                                                                                                                                                        FDPart3tmp235,
                                                                                                                                                                                                                                                                                                                                                                                        FDPart3tmp87,
                                                                                                                                                                                                                                                                                                                                                                                        FDPart3tmp272)),
                                                                                                                                                                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp78, AddSIMD(FDPart3tmp126, AddSIMD(FDPart3tmp154, FDPart3tmp355)),
                                                                                                                                                                                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp78,
                                                                                                                                                                                                                                                                                                                                                                                                    AddSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                        FDPart3tmp199, AddSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                                           FDPart3tmp347,
                                                                                                                                                                                                                                                                                                                                                                                                                           FDPart3tmp348)),
                                                                                                                                                                                                                                                                                                                                                                                                    FusedMulAddSIMD(FDPart3tmp68,
                                                                                                                                                                                                                                                                                                                                                                                                                    AddSIMD(
                                                                                                                                                                                                                                                                                                                                                                                                                        FDPart3tmp220,
                                                                                                                                                                                                                                                                                                                                                                                                                        FDPart3tmp344),
                                                                                                                                                                                                                                                                                                                                                                                                                    MulSIMD(FDPart3tmp68,
                                                                                                                                                                                                                                                                                                                                                                                                                            FusedMulAddSIMD(FDPart3tmp114, FDPart3tmp148, FDPart3tmp332))))))))))))))))))),
                                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp68,
                                                                                                                                                                                                                                        FusedMulAddSIMD(FDPart3tmp112, MulSIMD(FDPart3tmp132, FDPart3tmp325), FDPart3tmp261), FDPart3tmp367)))))))))),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3_Integer_4,
                                                                            FusedMulAddSIMD(
                                                                                MulSIMD(FDPart3tmp69, FDPart3tmp71), MulSIMD(cf_dD0, cf_dD2),
                                                                                FusedMulAddSIMD(MulSIMD(FDPart3tmp69, FDPart3tmp72),
                                                                                                MulSIMD(cf_dD1, cf_dD2),
                                                                                                MulSIMD(MulSIMD(FDPart3tmp68, FDPart3tmp69),
                                                                                                        MulSIMD(cf_dD0, cf_dD1)))),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3_Integer_8,
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp76,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp81,
                                                                                        FusedMulSubSIMD(FDPart3tmp79, MulSIMD(cf_dD1, cf_dD1),
                                                                                                        cf_dDD11),
                                                                                        MulSIMD(FDPart3tmp91,
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp113, FDPart3tmp80,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp114, FDPart3tmp89,
                                                                                                        MulSIMD(FDPart3tmp112, FDPart3tmp86))))),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp78,
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp81,
                                                                                            FusedMulSubSIMD(FDPart3tmp79, MulSIMD(cf_dD2, cf_dD2),
                                                                                                            cf_dDD22),
                                                                                            MulSIMD(FDPart3tmp91,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp108, FDPart3tmp80,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp109, FDPart3tmp89,
                                                                                                            MulSIMD(FDPart3tmp107, FDPart3tmp86))))),
                                                                                        MulSIMD(
                                                                                            FDPart3tmp74,
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp81,
                                                                                                FusedMulSubSIMD(FDPart3tmp79, MulSIMD(cf_dD0, cf_dD0),
                                                                                                                cf_dDD00),
                                                                                                MulSIMD(
                                                                                                    FDPart3tmp91,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp118, FDPart3tmp80,
                                                                                                        FusedMulAddSIMD(FDPart3tmp119, FDPart3tmp89,
                                                                                                                        MulSIMD(FDPart3tmp117,
                                                                                                                                FDPart3tmp86)))))))),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3_Integer_16,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp71,
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp81,
                                                                                            FusedMulSubSIMD(FDPart3tmp86, cf_dD2, cf_dDD02),
                                                                                            MulSIMD(FDPart3tmp91,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp86, FDPart3tmp96,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp89, FDPart3tmp98,
                                                                                                            MulSIMD(FDPart3tmp80, FDPart3tmp97))))),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp72,
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp81,
                                                                                                FusedMulSubSIMD(FDPart3tmp80, cf_dD2, cf_dDD12),
                                                                                                MulSIMD(
                                                                                                    FDPart3tmp91,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp103, FDPart3tmp80,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp104, FDPart3tmp89,
                                                                                                            MulSIMD(FDPart3tmp102, FDPart3tmp86))))),
                                                                                            MulSIMD(
                                                                                                FDPart3tmp68,
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp81,
                                                                                                    FusedMulSubSIMD(FDPart3tmp80, cf_dD0, cf_dDD01),
                                                                                                    MulSIMD(FDPart3tmp91,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp85, FDPart3tmp86,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp88, FDPart3tmp89,
                                                                                                                    MulSIMD(FDPart3tmp80,
                                                                                                                            FDPart3tmp87)))))))),
                                                                                    MulSIMD(FDPart3_Integer_2,
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp69,
                                                                                                MulSIMD(FDPart3tmp76, MulSIMD(cf_dD1, cf_dD1)),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp69,
                                                                                                    MulSIMD(FDPart3tmp78, MulSIMD(cf_dD2, cf_dD2)),
                                                                                                    MulSIMD(FDPart3tmp69,
                                                                                                            MulSIMD(FDPart3tmp74,
                                                                                                                    MulSIMD(cf_dD0,
                                                                                                                            cf_dD0)))))))))))))))))),
            FusedMulSubSIMD(
                FDPart3_Rational_2_3, MulSIMD(trK, trK),
                FusedMulAddSIMD(
                    aDD11, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp42, FDPart3tmp45),
                    FusedMulAddSIMD(
                        aDD22, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp54, FDPart3tmp56),
                        FusedMulAddSIMD(
                            MulSIMD(FDPart3_Integer_16, T4UU00), MulSIMD(PI, MulSIMD(alpha, alpha)),
                            FusedMulAddSIMD(FDPart3_Integer_2,
                                            FusedMulAddSIMD(FDPart3tmp63, aDD02, FusedMulAddSIMD(FDPart3tmp65, aDD12, MulSIMD(FDPart3tmp62, aDD01))),
                                            MulSIMD(aDD00, FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp27, FDPart3tmp31))))))));
        const REAL_SIMD_ARRAY __RHS_exp_1 = FDPart3tmp502;
        const REAL_SIMD_ARRAY __RHS_exp_2 = FDPart3tmp504;
        const REAL_SIMD_ARRAY __RHS_exp_3 = FDPart3tmp506;
        const REAL_SIMD_ARRAY __RHS_exp_4 =
            FusedMulAddSIMD(FDPart3tmp6, MulSIMD(FDPart3tmp502, FDPart3tmp502),
                            FusedMulAddSIMD(FDPart3tmp7, MulSIMD(FDPart3tmp506, FDPart3tmp506),
                                            FusedMulAddSIMD(FDPart3_Integer_2,
                                                            FusedMulAddSIMD(FDPart3tmp502, MulSIMD(FDPart3tmp506, hDD02),
                                                                            FusedMulAddSIMD(FDPart3tmp504, MulSIMD(FDPart3tmp506, hDD12),
                                                                                            MulSIMD(FDPart3tmp502, MulSIMD(FDPart3tmp504, hDD01)))),
                                                            MulSIMD(FDPart3tmp1, MulSIMD(FDPart3tmp504, FDPart3tmp504)))));

        WriteSIMD(&HGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_0);
        WriteSIMD(&MU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_1);
        WriteSIMD(&MU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_2);
        WriteSIMD(&MU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_3);
        WriteSIMD(&MSQUAREDGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_4);

      } // END LOOP: for (int i0 = cctk_nghostzones[0]; i0 < cctk_lsh[0]-cctk_nghostzones[0]; i0 += simd_width)
    } // END LOOP: for (int i1 = cctk_nghostzones[1]; i1 < cctk_lsh[1]-cctk_nghostzones[1]; i1++)
  } // END LOOP: for (int i2 = cctk_nghostzones[2]; i2 < cctk_lsh[2]-cctk_nghostzones[2]; i2++)
}
