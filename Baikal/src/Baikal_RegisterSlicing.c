#include "Slicing.h"
#include "cctk.h"
/**
 * Register slicing condition for NRPy+-generated thorn Baikal.
 */
int Baikal_RegisterSlicing() {

  Einstein_RegisterSlicing("Baikal");
  return 0;
}
