#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "math.h"
/**
 * Compute T4UU from T4DD (provided by TmunuBase),
 * using BSSN quantities as inputs for the 4D raising operation
 *
 * WARNING: Do not enable SIMD here, as it is not guaranteed that
 * cctk_lsh[0]*cctk_lsh[1]*cctk_lsh[2] is a multiple of
 * SIMD_width!
 */
void Baikal_T4DD_to_T4UU(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS_Baikal_T4DD_to_T4UU;
  DECLARE_CCTK_PARAMETERS;

#pragma omp parallel for
  for (int i2 = 0; i2 < cctk_lsh[2]; i2++) {
    for (int i1 = 0; i1 < cctk_lsh[1]; i1++) {
      for (int i0 = 0; i0 < cctk_lsh[0]; i0++) {
        const CCTK_REAL alpha = alphaGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL cf = cfGF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL vetU0 = vetU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL vetU1 = vetU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL vetU2 = vetU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD00 = hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD01 = hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD02 = hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD11 = hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD12 = hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL hDD22 = hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL T4DD00 = eTtt[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL T4DD01 = eTtx[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL T4DD02 = eTty[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL T4DD03 = eTtz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL T4DD11 = eTxx[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL T4DD12 = eTxy[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL T4DD13 = eTxz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL T4DD22 = eTyy[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL T4DD23 = eTyz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        const CCTK_REAL T4DD33 = eTzz[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)];
        /*
         *  Original SymPy expressions:
         *  "[T4UU00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD00/alpha**4 - 2*T4DD01*vetU0/alpha**4 - 2*T4DD02*vetU1/alpha**4 -
         * 2*T4DD03*vetU2/alpha**4 + T4DD11*vetU0**2/alpha**4 + 2*T4DD12*vetU0*vetU1/alpha**4 + 2*T4DD13*vetU0*vetU2/alpha**4 +
         * T4DD22*vetU1**2/alpha**4 + 2*T4DD23*vetU1*vetU2/alpha**4 + T4DD33*vetU2**2/alpha**4]"
         *  "[T4UU01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = -T4DD00*vetU0/alpha**4 - T4DD01*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0**2/alpha**2)/alpha**2 + T4DD01*vetU0**2/alpha**4 - T4DD02*((-hDD01*(hDD22 + 1)/cf**4 +
         * hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 + T4DD02*vetU0*vetU1/alpha**4 - T4DD03*((hDD01*hDD12/cf**4 -
         * hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6
         * + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2 + T4DD03*vetU0*vetU2/alpha**4 +
         * T4DD11*vetU0*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11
         * + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0**2/alpha**2)/alpha**2 +
         * T4DD12*vetU0*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 +
         * T4DD12*vetU1*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11
         * + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0**2/alpha**2)/alpha**2 +
         * T4DD13*vetU0*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2 +
         * T4DD13*vetU2*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11
         * + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0**2/alpha**2)/alpha**2 +
         * T4DD22*vetU1*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 +
         * T4DD23*vetU1*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2 +
         * T4DD23*vetU2*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 +
         * T4DD33*vetU2*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2]"
         *  "[T4UU02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = -T4DD00*vetU1/alpha**4 - T4DD01*((-hDD01*(hDD22 + 1)/cf**4 +
         * hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 + T4DD01*vetU0*vetU1/alpha**4 - T4DD02*((-hDD02**2/cf**4 +
         * (hDD00 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 +
         * 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2)/alpha**2 + T4DD02*vetU1**2/alpha**4 - T4DD03*((hDD01*hDD02/cf**4
         * - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 +
         * 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2 + T4DD03*vetU1*vetU2/alpha**4 +
         * T4DD11*vetU0*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 +
         * T4DD12*vetU0*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11
         * + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2)/alpha**2 +
         * T4DD12*vetU1*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 +
         * T4DD13*vetU0*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2 +
         * T4DD13*vetU2*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 +
         * T4DD22*vetU1*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11
         * + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2)/alpha**2 +
         * T4DD23*vetU1*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2 +
         * T4DD23*vetU2*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11
         * + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2)/alpha**2 +
         * T4DD33*vetU2*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2]"
         *  "[T4UU03GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = -T4DD00*vetU2/alpha**4 - T4DD01*((hDD01*hDD12/cf**4 - hDD02*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2 + T4DD01*vetU0*vetU2/alpha**4 - T4DD02*((hDD01*hDD02/cf**4 -
         * hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6
         * + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2 + T4DD02*vetU1*vetU2/alpha**4 - T4DD03*((-hDD01**2/cf**4 +
         * (hDD00 + 1)*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 +
         * 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)/alpha**2 + T4DD03*vetU2**2/alpha**4 +
         * T4DD11*vetU0*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2 +
         * T4DD12*vetU0*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2 +
         * T4DD12*vetU1*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2 +
         * T4DD13*vetU0*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11
         * + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)/alpha**2 +
         * T4DD13*vetU2*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2 +
         * T4DD22*vetU1*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2 +
         * T4DD23*vetU1*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11
         * + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)/alpha**2 +
         * T4DD23*vetU2*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2 +
         * T4DD33*vetU2*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11
         * + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)/alpha**2]"
         *  "[T4UU11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD00*vetU0**2/alpha**4 + 2*T4DD01*vetU0*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0**2/alpha**2)/alpha**2 + 2*T4DD02*vetU0*((-hDD01*(hDD22 + 1)/cf**4 +
         * hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 + 2*T4DD03*vetU0*((hDD01*hDD12/cf**4 - hDD02*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2 + T4DD11*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0**2/alpha**2)**2 + 2*T4DD12*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 +
         * 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6)
         * - vetU0**2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2) +
         * 2*T4DD13*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0**2/alpha**2)*((hDD01*hDD12/cf**4 - hDD02*(hDD11
         * + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2) + T4DD22*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 +
         * 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6)
         * - vetU0*vetU1/alpha**2)**2 + 2*T4DD23*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 +
         * 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU0*vetU2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2) +
         * T4DD33*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6
         * - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)**2]"
         *  "[T4UU12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD00*vetU0*vetU1/alpha**4 + T4DD01*vetU0*((-hDD01*(hDD22 + 1)/cf**4 +
         * hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 + T4DD01*vetU1*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0**2/alpha**2)/alpha**2 + T4DD02*vetU0*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2)/alpha**2 + T4DD02*vetU1*((-hDD01*(hDD22 + 1)/cf**4 +
         * hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 + T4DD03*vetU0*((hDD01*hDD02/cf**4 - hDD12*(hDD00 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2 + T4DD03*vetU1*((hDD01*hDD12/cf**4 - hDD02*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2 + T4DD11*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0**2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 +
         * 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU0*vetU1/alpha**2) + T4DD12*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6
         * - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU1**2/alpha**2)*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0**2/alpha**2) +
         * T4DD12*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)**2 + T4DD13*((-hDD12**2/cf**4 +
         * (hDD11 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 +
         * 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0**2/alpha**2)*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22
         * + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 +
         * 1)/cf**6) - vetU1*vetU2/alpha**2) + T4DD13*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 +
         * 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU0*vetU2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2) +
         * T4DD22*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 +
         * hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2) + T4DD23*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2)*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 +
         * 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU0*vetU2/alpha**2) + T4DD23*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU1*vetU2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2) +
         * T4DD33*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6
         * - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)*((hDD01*hDD12/cf**4 - hDD02*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)]"
         *  "[T4UU13GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD00*vetU0*vetU2/alpha**4 + T4DD01*vetU0*((hDD01*hDD12/cf**4 - hDD02*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2 + T4DD01*vetU2*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0**2/alpha**2)/alpha**2 + T4DD02*vetU0*((hDD01*hDD02/cf**4 - hDD12*(hDD00 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2 + T4DD02*vetU2*((-hDD01*(hDD22 + 1)/cf**4 +
         * hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 + T4DD03*vetU0*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)/alpha**2 + T4DD03*vetU2*((hDD01*hDD12/cf**4 - hDD02*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2 + T4DD11*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0**2/alpha**2)*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 +
         * 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU0*vetU2/alpha**2) + T4DD12*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6
         * - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU0**2/alpha**2)*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2) +
         * T4DD12*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6
         * - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 +
         * hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2) + T4DD13*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)*((-hDD12**2/cf**4 + (hDD11 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 +
         * 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU0**2/alpha**2) + T4DD13*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)**2 +
         * T4DD22*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6
         * - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 +
         * hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2) + T4DD23*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 +
         * 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU0*vetU1/alpha**2) + T4DD23*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU1*vetU2/alpha**2)*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2) +
         * T4DD33*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)*((hDD01*hDD12/cf**4 - hDD02*(hDD11
         * + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)]"
         *  "[T4UU22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD00*vetU1**2/alpha**4 + 2*T4DD01*vetU1*((-hDD01*(hDD22 + 1)/cf**4 +
         * hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 + 2*T4DD02*vetU1*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2)/alpha**2 + 2*T4DD03*vetU1*((hDD01*hDD02/cf**4 - hDD12*(hDD00 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2 + T4DD11*((-hDD01*(hDD22 + 1)/cf**4 +
         * hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)**2 + 2*T4DD12*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 +
         * 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU0*vetU1/alpha**2) + 2*T4DD13*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU1*vetU2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2) +
         * T4DD22*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2)**2 + 2*T4DD23*((-hDD02**2/cf**4 +
         * (hDD00 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 +
         * 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2)*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22
         * + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 +
         * 1)/cf**6) - vetU1*vetU2/alpha**2) + T4DD33*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 +
         * 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU1*vetU2/alpha**2)**2]"
         *  "[T4UU23GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD00*vetU1*vetU2/alpha**4 + T4DD01*vetU1*((hDD01*hDD12/cf**4 - hDD02*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2 + T4DD01*vetU2*((-hDD01*(hDD22 + 1)/cf**4 +
         * hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2)/alpha**2 + T4DD02*vetU1*((hDD01*hDD02/cf**4 - hDD12*(hDD00 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2 + T4DD02*vetU2*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2)/alpha**2 + T4DD03*vetU1*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)/alpha**2 + T4DD03*vetU2*((hDD01*hDD02/cf**4 - hDD12*(hDD00 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2 + T4DD11*((hDD01*hDD12/cf**4 - hDD02*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 +
         * 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU0*vetU1/alpha**2) + T4DD12*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6
         * - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU1**2/alpha**2)*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2) +
         * T4DD12*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6
         * - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 +
         * hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU1/alpha**2) + T4DD13*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)*((-hDD01*(hDD22 + 1)/cf**4 + hDD02*hDD12/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 +
         * 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU0*vetU1/alpha**2) + T4DD13*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU1*vetU2/alpha**2)*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2) +
         * T4DD22*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2)*((hDD01*hDD02/cf**4 - hDD12*(hDD00
         * + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2) + T4DD23*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 +
         * 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6)
         * - vetU2**2/alpha**2)*((-hDD02**2/cf**4 + (hDD00 + 1)*(hDD22 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1**2/alpha**2) +
         * T4DD23*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6
         * - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)**2 + T4DD33*((-hDD01**2/cf**4 + (hDD00 +
         * 1)*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 +
         * (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6
         * + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU1*vetU2/alpha**2)]"
         *  "[T4UU33GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD00*vetU2**2/alpha**4 + 2*T4DD01*vetU2*((hDD01*hDD12/cf**4 - hDD02*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2)/alpha**2 + 2*T4DD02*vetU2*((hDD01*hDD02/cf**4 - hDD12*(hDD00 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2)/alpha**2 + 2*T4DD03*vetU2*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 +
         * 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)/alpha**2 + T4DD11*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 +
         * 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6)
         * - vetU0*vetU2/alpha**2)**2 + 2*T4DD12*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 +
         * 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU1*vetU2/alpha**2)*((hDD01*hDD12/cf**4 - hDD02*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2) +
         * 2*T4DD13*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)*((hDD01*hDD12/cf**4 - hDD02*(hDD11
         * + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 +
         * 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU0*vetU2/alpha**2) + T4DD22*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 +
         * 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6)
         * - vetU1*vetU2/alpha**2)**2 + 2*T4DD23*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 +
         * 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) -
         * vetU2**2/alpha**2)*((hDD01*hDD02/cf**4 - hDD12*(hDD00 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 -
         * hDD02**2*(hDD11 + 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU1*vetU2/alpha**2) +
         * T4DD33*((-hDD01**2/cf**4 + (hDD00 + 1)*(hDD11 + 1)/cf**4)/(-hDD01**2*(hDD22 + 1)/cf**6 + 2*hDD01*hDD02*hDD12/cf**6 - hDD02**2*(hDD11 +
         * 1)/cf**6 - hDD12**2*(hDD00 + 1)/cf**6 + (hDD00 + 1)*(hDD11 + 1)*(hDD22 + 1)/cf**6) - vetU2**2/alpha**2)**2]"
         */
        {
          const CCTK_REAL tmp0 = (1.0 / ((alpha) * (alpha) * (alpha) * (alpha)));
          const CCTK_REAL tmp3 = T4DD01 * vetU0;
          const CCTK_REAL tmp4 = T4DD02 * vetU1;
          const CCTK_REAL tmp5 = T4DD03 * vetU2;
          const CCTK_REAL tmp6 = ((vetU0) * (vetU0));
          const CCTK_REAL tmp8 = ((vetU1) * (vetU1));
          const CCTK_REAL tmp10 = ((vetU2) * (vetU2));
          const CCTK_REAL tmp15 = (1.0 / ((alpha) * (alpha)));
          const CCTK_REAL tmp17 = (1.0 / ((cf) * (cf) * (cf) * (cf)));
          const CCTK_REAL tmp18 = hDD22 + 1;
          const CCTK_REAL tmp19 = pow(cf, -6);
          const CCTK_REAL tmp22 = hDD11 + 1;
          const CCTK_REAL tmp24 = hDD00 + 1;
          const CCTK_REAL tmp78 = 2 * vetU1;
          const CCTK_REAL tmp80 = 2 * vetU2;
          const CCTK_REAL tmp1 = T4DD00 * tmp0;
          const CCTK_REAL tmp2 = 2 * tmp0;
          const CCTK_REAL tmp16 = tmp15 * vetU0;
          const CCTK_REAL tmp25 = (1.0 / (-((hDD01) * (hDD01)) * tmp18 * tmp19 + 2 * hDD01 * hDD02 * hDD12 * tmp19 -
                                          ((hDD02) * (hDD02)) * tmp19 * tmp22 - ((hDD12) * (hDD12)) * tmp19 * tmp24 + tmp18 * tmp19 * tmp22 * tmp24));
          const CCTK_REAL tmp27 = T4DD02 * tmp15;
          const CCTK_REAL tmp32 = T4DD03 * tmp15;
          const CCTK_REAL tmp36 = tmp15 * vetU1;
          const CCTK_REAL tmp40 = tmp15 * vetU2;
          const CCTK_REAL tmp43 = T4DD01 * tmp15;
          const CCTK_REAL tmp13 = tmp1 * vetU0;
          const CCTK_REAL tmp26 = -tmp16 * vetU1 + tmp25 * (-hDD01 * tmp17 * tmp18 + hDD02 * hDD12 * tmp17);
          const CCTK_REAL tmp31 = -tmp16 * vetU2 + tmp25 * (hDD01 * hDD12 * tmp17 - hDD02 * tmp17 * tmp22);
          const CCTK_REAL tmp42 = -tmp15 * tmp6 + tmp25 * (-((hDD12) * (hDD12)) * tmp17 + tmp17 * tmp18 * tmp22);
          const CCTK_REAL tmp51 = tmp25 * (hDD01 * hDD02 * tmp17 - hDD12 * tmp17 * tmp24) - tmp36 * vetU2;
          const CCTK_REAL tmp58 = -tmp15 * tmp8 + tmp25 * (-((hDD02) * (hDD02)) * tmp17 + tmp17 * tmp18 * tmp24);
          const CCTK_REAL tmp67 = -tmp10 * tmp15 + tmp25 * (-((hDD01) * (hDD01)) * tmp17 + tmp17 * tmp22 * tmp24);
          const CCTK_REAL tmp28 = tmp26 * tmp27;
          const CCTK_REAL tmp33 = tmp31 * tmp32;
          const CCTK_REAL tmp34 = T4DD12 * tmp26;
          const CCTK_REAL tmp35 = T4DD13 * tmp31;
          const CCTK_REAL tmp37 = T4DD22 * tmp26;
          const CCTK_REAL tmp38 = T4DD23 * tmp31;
          const CCTK_REAL tmp39 = T4DD23 * tmp26;
          const CCTK_REAL tmp41 = T4DD33 * tmp31;
          const CCTK_REAL tmp44 = tmp42 * tmp43;
          const CCTK_REAL tmp45 = T4DD11 * tmp42;
          const CCTK_REAL tmp46 = T4DD12 * tmp42;
          const CCTK_REAL tmp47 = T4DD13 * tmp42;
          const CCTK_REAL tmp50 = tmp26 * tmp43;
          const CCTK_REAL tmp52 = tmp32 * tmp51;
          const CCTK_REAL tmp54 = tmp16 * tmp51;
          const CCTK_REAL tmp55 = T4DD13 * tmp26;
          const CCTK_REAL tmp59 = tmp27 * tmp58;
          const CCTK_REAL tmp60 = T4DD12 * tmp58;
          const CCTK_REAL tmp62 = T4DD23 * tmp58;
          const CCTK_REAL tmp64 = tmp31 * tmp43;
          const CCTK_REAL tmp65 = tmp27 * tmp51;
          const CCTK_REAL tmp68 = tmp32 * tmp67;
          const CCTK_REAL tmp71 = ((tmp26) * (tmp26));
          const CCTK_REAL tmp72 = ((tmp31) * (tmp31));
          const CCTK_REAL tmp73 = 2 * tmp26;
          const CCTK_REAL tmp76 = tmp26 * tmp31;
          const CCTK_REAL tmp77 = ((tmp51) * (tmp51));
          const CCTK_REAL tmp79 = 2 * tmp51;
          T4UU00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD11 * tmp0 * tmp6 + T4DD12 * tmp2 * vetU0 * vetU1 + T4DD13 * tmp2 * vetU0 * vetU2 +
                                                         T4DD22 * tmp0 * tmp8 + T4DD23 * tmp2 * vetU1 * vetU2 + T4DD33 * tmp0 * tmp10 + tmp1 -
                                                         tmp2 * tmp3 - tmp2 * tmp4 - tmp2 * tmp5;
          T4UU01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD01 * tmp0 * tmp6 + tmp0 * tmp4 * vetU0 + tmp0 * tmp5 * vetU0 - tmp13 + tmp16 * tmp34 +
                                                         tmp16 * tmp35 + tmp16 * tmp45 - tmp28 - tmp33 + tmp36 * tmp37 + tmp36 * tmp38 +
                                                         tmp36 * tmp46 + tmp39 * tmp40 + tmp40 * tmp41 + tmp40 * tmp47 - tmp44;
          T4UU02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD02 * tmp0 * tmp8 + T4DD11 * tmp16 * tmp26 + T4DD13 * tmp54 + T4DD22 * tmp36 * tmp58 +
                                                         T4DD23 * tmp36 * tmp51 + T4DD33 * tmp40 * tmp51 + tmp0 * tmp3 * vetU1 + tmp0 * tmp5 * vetU1 -
                                                         tmp1 * vetU1 + tmp16 * tmp60 + tmp34 * tmp36 + tmp40 * tmp55 + tmp40 * tmp62 - tmp50 -
                                                         tmp52 - tmp59;
          T4UU03GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD03 * tmp0 * tmp10 + T4DD11 * tmp16 * tmp31 + T4DD12 * tmp31 * tmp36 + T4DD12 * tmp54 +
                                                         T4DD13 * tmp16 * tmp67 + T4DD22 * tmp36 * tmp51 + T4DD23 * tmp36 * tmp67 +
                                                         T4DD23 * tmp40 * tmp51 + T4DD33 * tmp40 * tmp67 + tmp0 * tmp3 * vetU2 + tmp0 * tmp4 * vetU2 -
                                                         tmp1 * vetU2 + tmp35 * tmp40 - tmp64 - tmp65 - tmp68;
          T4UU11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD02 * tmp16 * tmp73 + 2 * T4DD03 * tmp16 * tmp31 + T4DD11 * ((tmp42) * (tmp42)) +
                                                         T4DD22 * tmp71 + 2 * T4DD23 * tmp76 + T4DD33 * tmp72 + tmp1 * tmp6 + 2 * tmp31 * tmp47 +
                                                         2 * tmp44 * vetU0 + tmp46 * tmp73;
          T4UU12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD02 * tmp16 * tmp58 + T4DD03 * tmp54 + T4DD12 * tmp71 + T4DD13 * tmp76 + tmp13 * vetU1 +
                                                         tmp26 * tmp45 + tmp28 * vetU1 + tmp33 * vetU1 + tmp37 * tmp58 + tmp38 * tmp58 +
                                                         tmp39 * tmp51 + tmp41 * tmp51 + tmp44 * vetU1 + tmp46 * tmp58 + tmp47 * tmp51 +
                                                         tmp50 * vetU0;
          T4UU13GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD02 * tmp54 + T4DD03 * tmp16 * tmp67 + T4DD12 * tmp76 + T4DD13 * tmp72 + tmp13 * vetU2 +
                                                         tmp28 * vetU2 + tmp31 * tmp45 + tmp33 * vetU2 + tmp37 * tmp51 + tmp38 * tmp51 +
                                                         tmp39 * tmp67 + tmp41 * tmp67 + tmp44 * vetU2 + tmp46 * tmp51 + tmp47 * tmp67 +
                                                         tmp64 * vetU0;
          T4UU22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD11 * tmp71 + T4DD22 * ((tmp58) * (tmp58)) + T4DD33 * tmp77 + tmp1 * tmp8 +
                                                         tmp50 * tmp78 + tmp52 * tmp78 + tmp55 * tmp79 + tmp59 * tmp78 + tmp60 * tmp73 +
                                                         tmp62 * tmp79;
          T4UU23GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD11 * tmp76 + T4DD22 * tmp51 * tmp58 + T4DD23 * tmp77 + T4DD33 * tmp51 * tmp67 +
                                                         tmp1 * vetU1 * vetU2 + tmp31 * tmp60 + tmp34 * tmp51 + tmp35 * tmp51 + tmp50 * vetU2 +
                                                         tmp52 * vetU2 + tmp55 * tmp67 + tmp59 * vetU2 + tmp62 * tmp67 + tmp64 * vetU1 +
                                                         tmp65 * vetU1 + tmp68 * vetU1;
          T4UU33GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)] = T4DD11 * tmp72 + T4DD12 * tmp31 * tmp79 + T4DD22 * tmp77 + T4DD23 * tmp67 * tmp79 +
                                                         T4DD33 * ((tmp67) * (tmp67)) + tmp1 * tmp10 + 2 * tmp35 * tmp67 + tmp64 * tmp80 +
                                                         tmp65 * tmp80 + tmp68 * tmp80;
        }

      } // END LOOP: for (int i0 = 0; i0 < cctk_lsh[0]; i0++)
    } // END LOOP: for (int i1 = 0; i1 < cctk_lsh[1]; i1++)
  } // END LOOP: for (int i2 = 0; i2 < cctk_lsh[2]; i2++)
}
