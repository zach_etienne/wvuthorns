#include "./simd/simd_intrinsics.h"
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "math.h"
/**
 * Finite difference function for operator dD0, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dD0_fdorder2(const REAL_SIMD_ARRAY FDPROTO_i0m1, const REAL_SIMD_ARRAY FDPROTO_i0p1,
                                                                             const REAL_SIMD_ARRAY invdxx0) {
  const double dblFDPart1_Rational_1_2 = 1.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_2 = ConstSIMD(dblFDPart1_Rational_1_2);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(FDPart1_Rational_1_2, MulSIMD(invdxx0, SubSIMD(FDPROTO_i0p1, FDPROTO_i0m1)));

  return FD_result;
}
/**
 * Finite difference function for operator dD1, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dD1_fdorder2(const REAL_SIMD_ARRAY FDPROTO_i1m1, const REAL_SIMD_ARRAY FDPROTO_i1p1,
                                                                             const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Rational_1_2 = 1.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_2 = ConstSIMD(dblFDPart1_Rational_1_2);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(FDPart1_Rational_1_2, MulSIMD(invdxx1, SubSIMD(FDPROTO_i1p1, FDPROTO_i1m1)));

  return FD_result;
}
/**
 * Finite difference function for operator dD2, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dD2_fdorder2(const REAL_SIMD_ARRAY FDPROTO_i2m1, const REAL_SIMD_ARRAY FDPROTO_i2p1,
                                                                             const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_2 = 1.0 / 2.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_2 = ConstSIMD(dblFDPart1_Rational_1_2);

  const REAL_SIMD_ARRAY FD_result = MulSIMD(FDPart1_Rational_1_2, MulSIMD(invdxx2, SubSIMD(FDPROTO_i2p1, FDPROTO_i2m1)));

  return FD_result;
}
/**
 * Finite difference function for operator dDD00, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD00_fdorder2(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i0m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p1, const REAL_SIMD_ARRAY invdxx0) {
  const double dblFDPart1_Integer_2 = 2.0;
  const REAL_SIMD_ARRAY FDPart1_Integer_2 = ConstSIMD(dblFDPart1_Integer_2);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(invdxx0, invdxx0), AddSIMD(FDPROTO_i0p1, NegFusedMulAddSIMD(FDPROTO, FDPart1_Integer_2, FDPROTO_i0m1)));

  return FD_result;
}
/**
 * Finite difference function for operator dDD01, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD01_fdorder2(const REAL_SIMD_ARRAY FDPROTO_i0m1_i1m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0m1_i1p1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p1_i1m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p1_i1p1, const REAL_SIMD_ARRAY invdxx0,
                                                                               const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Rational_1_4 = 1.0 / 4.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_4 = ConstSIMD(dblFDPart1_Rational_1_4);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(FDPart1_Rational_1_4, invdxx0),
              MulSIMD(invdxx1, AddSIMD(FDPROTO_i0p1_i1p1, SubSIMD(FDPROTO_i0m1_i1m1, AddSIMD(FDPROTO_i0m1_i1p1, FDPROTO_i0p1_i1m1)))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD02, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD02_fdorder2(const REAL_SIMD_ARRAY FDPROTO_i0m1_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0m1_i2p1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p1_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i0p1_i2p1, const REAL_SIMD_ARRAY invdxx0,
                                                                               const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_4 = 1.0 / 4.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_4 = ConstSIMD(dblFDPart1_Rational_1_4);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(FDPart1_Rational_1_4, invdxx0),
              MulSIMD(invdxx2, AddSIMD(FDPROTO_i0p1_i2p1, SubSIMD(FDPROTO_i0m1_i2m1, AddSIMD(FDPROTO_i0m1_i2p1, FDPROTO_i0p1_i2m1)))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD11, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD11_fdorder2(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i1m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p1, const REAL_SIMD_ARRAY invdxx1) {
  const double dblFDPart1_Integer_2 = 2.0;
  const REAL_SIMD_ARRAY FDPart1_Integer_2 = ConstSIMD(dblFDPart1_Integer_2);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(invdxx1, invdxx1), AddSIMD(FDPROTO_i1p1, NegFusedMulAddSIMD(FDPROTO, FDPart1_Integer_2, FDPROTO_i1m1)));

  return FD_result;
}
/**
 * Finite difference function for operator dDD12, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD12_fdorder2(const REAL_SIMD_ARRAY FDPROTO_i1m1_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1m1_i2p1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p1_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i1p1_i2p1, const REAL_SIMD_ARRAY invdxx1,
                                                                               const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Rational_1_4 = 1.0 / 4.0;
  const REAL_SIMD_ARRAY FDPart1_Rational_1_4 = ConstSIMD(dblFDPart1_Rational_1_4);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(FDPart1_Rational_1_4, invdxx1),
              MulSIMD(invdxx2, AddSIMD(FDPROTO_i1p1_i2p1, SubSIMD(FDPROTO_i1m1_i2m1, AddSIMD(FDPROTO_i1m1_i2p1, FDPROTO_i1p1_i2m1)))));

  return FD_result;
}
/**
 * Finite difference function for operator dDD22, with FD accuracy order 2.
 */
static CCTK_ATTRIBUTE_NOINLINE REAL_SIMD_ARRAY SIMD_fd_function_dDD22_fdorder2(const REAL_SIMD_ARRAY FDPROTO, const REAL_SIMD_ARRAY FDPROTO_i2m1,
                                                                               const REAL_SIMD_ARRAY FDPROTO_i2p1, const REAL_SIMD_ARRAY invdxx2) {
  const double dblFDPart1_Integer_2 = 2.0;
  const REAL_SIMD_ARRAY FDPart1_Integer_2 = ConstSIMD(dblFDPart1_Integer_2);

  const REAL_SIMD_ARRAY FD_result =
      MulSIMD(MulSIMD(invdxx2, invdxx2), AddSIMD(FDPROTO_i2p1, NegFusedMulAddSIMD(FDPROTO, FDPart1_Integer_2, FDPROTO_i2m1)));

  return FD_result;
}

/**
 * Compute Ricci tensor for the BSSN evolution equations.
 */
void Baikal_Ricci_eval_order_2(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS_Baikal_Ricci_eval_order_2;

  const REAL_SIMD_ARRAY invdxx0 CCTK_ATTRIBUTE_UNUSED = ConstSIMD(1.0 / CCTK_DELTA_SPACE(0));
  const REAL_SIMD_ARRAY invdxx1 CCTK_ATTRIBUTE_UNUSED = ConstSIMD(1.0 / CCTK_DELTA_SPACE(1));
  const REAL_SIMD_ARRAY invdxx2 CCTK_ATTRIBUTE_UNUSED = ConstSIMD(1.0 / CCTK_DELTA_SPACE(2));

#pragma omp parallel for
  for (int i2 = cctk_nghostzones[2]; i2 < cctk_lsh[2] - cctk_nghostzones[2]; i2++) {
    for (int i1 = cctk_nghostzones[1]; i1 < cctk_lsh[1] - cctk_nghostzones[1]; i1++) {
      for (int i0 = cctk_nghostzones[0]; i0 < cctk_lsh[0] - cctk_nghostzones[0]; i0 += simd_width) {
        /*
         * NRPy+-Generated GF Access/FD Code, Step 1 of 2:
         * Read gridfunction(s) from main memory and compute FD stencils as needed.
         */
        const REAL_SIMD_ARRAY hDD00_i1m1_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i1p1_i2m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i1m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i1p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD00_i1m1_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i0m1_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i0p1_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD00_i1p1_i2p1 = ReadSIMD(&hDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i1m1_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i1p1_i2m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i1m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i1p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD01_i1m1_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i0m1_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i0p1_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD01_i1p1_i2p1 = ReadSIMD(&hDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i1m1_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i1p1_i2m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i1m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i1p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD02_i1m1_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i0m1_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i0p1_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD02_i1p1_i2p1 = ReadSIMD(&hDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i1m1_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i1p1_i2m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i1m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i1p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD11_i1m1_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i0m1_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i0p1_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD11_i1p1_i2p1 = ReadSIMD(&hDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i1m1_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i1p1_i2m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i1m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i1p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD12_i1m1_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i0m1_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i0p1_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD12_i1p1_i2p1 = ReadSIMD(&hDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i1m1_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i1p1_i2m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 - 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i1m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i1p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY hDD22_i1m1_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i0m1_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i0p1_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD22_i1p1_i2p1 = ReadSIMD(&hDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2 + 1)]);
        const REAL_SIMD_ARRAY lambdaU0_i2m1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY lambdaU0_i1m1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0m1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i0p1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i1p1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU0_i2p1 = ReadSIMD(&lambdaU0GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY lambdaU1_i2m1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY lambdaU1_i1m1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0m1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i0p1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i1p1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU1_i2p1 = ReadSIMD(&lambdaU1GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY lambdaU2_i2m1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 - 1)]);
        const REAL_SIMD_ARRAY lambdaU2_i1m1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 - 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0m1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 - 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i0p1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0 + 1, i1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i1p1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1 + 1, i2)]);
        const REAL_SIMD_ARRAY lambdaU2_i2p1 = ReadSIMD(&lambdaU2GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2 + 1)]);
        const REAL_SIMD_ARRAY hDD_dD000 = SIMD_fd_function_dD0_fdorder2(hDD00_i0m1, hDD00_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD001 = SIMD_fd_function_dD1_fdorder2(hDD00_i1m1, hDD00_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD002 = SIMD_fd_function_dD2_fdorder2(hDD00_i2m1, hDD00_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD010 = SIMD_fd_function_dD0_fdorder2(hDD01_i0m1, hDD01_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD011 = SIMD_fd_function_dD1_fdorder2(hDD01_i1m1, hDD01_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD012 = SIMD_fd_function_dD2_fdorder2(hDD01_i2m1, hDD01_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD020 = SIMD_fd_function_dD0_fdorder2(hDD02_i0m1, hDD02_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD021 = SIMD_fd_function_dD1_fdorder2(hDD02_i1m1, hDD02_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD022 = SIMD_fd_function_dD2_fdorder2(hDD02_i2m1, hDD02_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD110 = SIMD_fd_function_dD0_fdorder2(hDD11_i0m1, hDD11_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD111 = SIMD_fd_function_dD1_fdorder2(hDD11_i1m1, hDD11_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD112 = SIMD_fd_function_dD2_fdorder2(hDD11_i2m1, hDD11_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD120 = SIMD_fd_function_dD0_fdorder2(hDD12_i0m1, hDD12_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD121 = SIMD_fd_function_dD1_fdorder2(hDD12_i1m1, hDD12_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD122 = SIMD_fd_function_dD2_fdorder2(hDD12_i2m1, hDD12_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dD220 = SIMD_fd_function_dD0_fdorder2(hDD22_i0m1, hDD22_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dD221 = SIMD_fd_function_dD1_fdorder2(hDD22_i1m1, hDD22_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dD222 = SIMD_fd_function_dD2_fdorder2(hDD22_i2m1, hDD22_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0000 = SIMD_fd_function_dDD00_fdorder2(hDD00, hDD00_i0m1, hDD00_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD0001 =
            SIMD_fd_function_dDD01_fdorder2(hDD00_i0m1_i1m1, hDD00_i0m1_i1p1, hDD00_i0p1_i1m1, hDD00_i0p1_i1p1, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0002 =
            SIMD_fd_function_dDD02_fdorder2(hDD00_i0m1_i2m1, hDD00_i0m1_i2p1, hDD00_i0p1_i2m1, hDD00_i0p1_i2p1, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0011 = SIMD_fd_function_dDD11_fdorder2(hDD00, hDD00_i1m1, hDD00_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0012 =
            SIMD_fd_function_dDD12_fdorder2(hDD00_i1m1_i2m1, hDD00_i1m1_i2p1, hDD00_i1p1_i2m1, hDD00_i1p1_i2p1, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0022 = SIMD_fd_function_dDD22_fdorder2(hDD00, hDD00_i2m1, hDD00_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0100 = SIMD_fd_function_dDD00_fdorder2(hDD01, hDD01_i0m1, hDD01_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD0101 =
            SIMD_fd_function_dDD01_fdorder2(hDD01_i0m1_i1m1, hDD01_i0m1_i1p1, hDD01_i0p1_i1m1, hDD01_i0p1_i1p1, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0102 =
            SIMD_fd_function_dDD02_fdorder2(hDD01_i0m1_i2m1, hDD01_i0m1_i2p1, hDD01_i0p1_i2m1, hDD01_i0p1_i2p1, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0111 = SIMD_fd_function_dDD11_fdorder2(hDD01, hDD01_i1m1, hDD01_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0112 =
            SIMD_fd_function_dDD12_fdorder2(hDD01_i1m1_i2m1, hDD01_i1m1_i2p1, hDD01_i1p1_i2m1, hDD01_i1p1_i2p1, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0122 = SIMD_fd_function_dDD22_fdorder2(hDD01, hDD01_i2m1, hDD01_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0200 = SIMD_fd_function_dDD00_fdorder2(hDD02, hDD02_i0m1, hDD02_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD0201 =
            SIMD_fd_function_dDD01_fdorder2(hDD02_i0m1_i1m1, hDD02_i0m1_i1p1, hDD02_i0p1_i1m1, hDD02_i0p1_i1p1, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0202 =
            SIMD_fd_function_dDD02_fdorder2(hDD02_i0m1_i2m1, hDD02_i0m1_i2p1, hDD02_i0p1_i2m1, hDD02_i0p1_i2p1, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0211 = SIMD_fd_function_dDD11_fdorder2(hDD02, hDD02_i1m1, hDD02_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD0212 =
            SIMD_fd_function_dDD12_fdorder2(hDD02_i1m1_i2m1, hDD02_i1m1_i2p1, hDD02_i1p1_i2m1, hDD02_i1p1_i2p1, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD0222 = SIMD_fd_function_dDD22_fdorder2(hDD02, hDD02_i2m1, hDD02_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1100 = SIMD_fd_function_dDD00_fdorder2(hDD11, hDD11_i0m1, hDD11_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD1101 =
            SIMD_fd_function_dDD01_fdorder2(hDD11_i0m1_i1m1, hDD11_i0m1_i1p1, hDD11_i0p1_i1m1, hDD11_i0p1_i1p1, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD1102 =
            SIMD_fd_function_dDD02_fdorder2(hDD11_i0m1_i2m1, hDD11_i0m1_i2p1, hDD11_i0p1_i2m1, hDD11_i0p1_i2p1, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1111 = SIMD_fd_function_dDD11_fdorder2(hDD11, hDD11_i1m1, hDD11_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD1112 =
            SIMD_fd_function_dDD12_fdorder2(hDD11_i1m1_i2m1, hDD11_i1m1_i2p1, hDD11_i1p1_i2m1, hDD11_i1p1_i2p1, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1122 = SIMD_fd_function_dDD22_fdorder2(hDD11, hDD11_i2m1, hDD11_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1200 = SIMD_fd_function_dDD00_fdorder2(hDD12, hDD12_i0m1, hDD12_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD1201 =
            SIMD_fd_function_dDD01_fdorder2(hDD12_i0m1_i1m1, hDD12_i0m1_i1p1, hDD12_i0p1_i1m1, hDD12_i0p1_i1p1, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD1202 =
            SIMD_fd_function_dDD02_fdorder2(hDD12_i0m1_i2m1, hDD12_i0m1_i2p1, hDD12_i0p1_i2m1, hDD12_i0p1_i2p1, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1211 = SIMD_fd_function_dDD11_fdorder2(hDD12, hDD12_i1m1, hDD12_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD1212 =
            SIMD_fd_function_dDD12_fdorder2(hDD12_i1m1_i2m1, hDD12_i1m1_i2p1, hDD12_i1p1_i2m1, hDD12_i1p1_i2p1, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD1222 = SIMD_fd_function_dDD22_fdorder2(hDD12, hDD12_i2m1, hDD12_i2p1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD2200 = SIMD_fd_function_dDD00_fdorder2(hDD22, hDD22_i0m1, hDD22_i0p1, invdxx0);
        const REAL_SIMD_ARRAY hDD_dDD2201 =
            SIMD_fd_function_dDD01_fdorder2(hDD22_i0m1_i1m1, hDD22_i0m1_i1p1, hDD22_i0p1_i1m1, hDD22_i0p1_i1p1, invdxx0, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD2202 =
            SIMD_fd_function_dDD02_fdorder2(hDD22_i0m1_i2m1, hDD22_i0m1_i2p1, hDD22_i0p1_i2m1, hDD22_i0p1_i2p1, invdxx0, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD2211 = SIMD_fd_function_dDD11_fdorder2(hDD22, hDD22_i1m1, hDD22_i1p1, invdxx1);
        const REAL_SIMD_ARRAY hDD_dDD2212 =
            SIMD_fd_function_dDD12_fdorder2(hDD22_i1m1_i2m1, hDD22_i1m1_i2p1, hDD22_i1p1_i2m1, hDD22_i1p1_i2p1, invdxx1, invdxx2);
        const REAL_SIMD_ARRAY hDD_dDD2222 = SIMD_fd_function_dDD22_fdorder2(hDD22, hDD22_i2m1, hDD22_i2p1, invdxx2);
        const REAL_SIMD_ARRAY lambdaU_dD00 = SIMD_fd_function_dD0_fdorder2(lambdaU0_i0m1, lambdaU0_i0p1, invdxx0);
        const REAL_SIMD_ARRAY lambdaU_dD01 = SIMD_fd_function_dD1_fdorder2(lambdaU0_i1m1, lambdaU0_i1p1, invdxx1);
        const REAL_SIMD_ARRAY lambdaU_dD02 = SIMD_fd_function_dD2_fdorder2(lambdaU0_i2m1, lambdaU0_i2p1, invdxx2);
        const REAL_SIMD_ARRAY lambdaU_dD10 = SIMD_fd_function_dD0_fdorder2(lambdaU1_i0m1, lambdaU1_i0p1, invdxx0);
        const REAL_SIMD_ARRAY lambdaU_dD11 = SIMD_fd_function_dD1_fdorder2(lambdaU1_i1m1, lambdaU1_i1p1, invdxx1);
        const REAL_SIMD_ARRAY lambdaU_dD12 = SIMD_fd_function_dD2_fdorder2(lambdaU1_i2m1, lambdaU1_i2p1, invdxx2);
        const REAL_SIMD_ARRAY lambdaU_dD20 = SIMD_fd_function_dD0_fdorder2(lambdaU2_i0m1, lambdaU2_i0p1, invdxx0);
        const REAL_SIMD_ARRAY lambdaU_dD21 = SIMD_fd_function_dD1_fdorder2(lambdaU2_i1m1, lambdaU2_i1p1, invdxx1);
        const REAL_SIMD_ARRAY lambdaU_dD22 = SIMD_fd_function_dD2_fdorder2(lambdaU2_i2m1, lambdaU2_i2p1, invdxx2);

        /*
         * NRPy+-Generated GF Access/FD Code, Step 2 of 2:
         * Evaluate SymPy expressions and write to main memory.
         */
        const double dblFDPart3_Integer_1 = 1.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_1 = ConstSIMD(dblFDPart3_Integer_1);

        const double dblFDPart3_Integer_2 = 2.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_2 = ConstSIMD(dblFDPart3_Integer_2);

        const double dblFDPart3_Integer_3 = 3.0;
        const REAL_SIMD_ARRAY FDPart3_Integer_3 = ConstSIMD(dblFDPart3_Integer_3);

        const double dblFDPart3_NegativeOne_ = -1.0;
        const REAL_SIMD_ARRAY FDPart3_NegativeOne_ = ConstSIMD(dblFDPart3_NegativeOne_);

        const double dblFDPart3_Rational_1_2 = 1.0 / 2.0;
        const REAL_SIMD_ARRAY FDPart3_Rational_1_2 = ConstSIMD(dblFDPart3_Rational_1_2);

        const REAL_SIMD_ARRAY FDPart3tmp0 = AddSIMD(FDPart3_Integer_1, hDD00);
        const REAL_SIMD_ARRAY FDPart3tmp2 = AddSIMD(FDPart3_Integer_1, hDD11);
        const REAL_SIMD_ARRAY FDPart3tmp3 = AddSIMD(FDPart3_Integer_1, hDD22);
        const REAL_SIMD_ARRAY FDPart3tmp15 = MulSIMD(FDPart3_Rational_1_2, FDPart3_Rational_1_2);
        const REAL_SIMD_ARRAY FDPart3tmp16 = AddSIMD(hDD_dD120, SubSIMD(hDD_dD021, hDD_dD012));
        const REAL_SIMD_ARRAY FDPart3tmp18 = AddSIMD(hDD_dD120, SubSIMD(hDD_dD012, hDD_dD021));
        const REAL_SIMD_ARRAY FDPart3tmp24 = AddSIMD(hDD_dD021, SubSIMD(hDD_dD012, hDD_dD120));
        const REAL_SIMD_ARRAY FDPart3tmp44 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD010, hDD_dD001);
        const REAL_SIMD_ARRAY FDPart3tmp45 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD020, hDD_dD002);
        const REAL_SIMD_ARRAY FDPart3tmp76 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD011, hDD_dD110);
        const REAL_SIMD_ARRAY FDPart3tmp77 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD121, hDD_dD112);
        const REAL_SIMD_ARRAY FDPart3tmp89 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD022, hDD_dD220);
        const REAL_SIMD_ARRAY FDPart3tmp90 = FusedMulSubSIMD(FDPart3_Integer_2, hDD_dD122, hDD_dD221);
        const REAL_SIMD_ARRAY FDPart3tmp40 = MulSIMD(FDPart3_Integer_2, FDPart3tmp15);
        const REAL_SIMD_ARRAY FDPart3tmp8 = DivSIMD(
            FDPart3_Integer_1, FusedMulAddSIMD(MulSIMD(FDPart3_Integer_2, hDD01), MulSIMD(hDD02, hDD12),
                                               FusedMulSubSIMD(FDPart3tmp0, MulSIMD(FDPart3tmp2, FDPart3tmp3),
                                                               FusedMulAddSIMD(FDPart3tmp2, MulSIMD(hDD02, hDD02),
                                                                               FusedMulAddSIMD(FDPart3tmp3, MulSIMD(hDD01, hDD01),
                                                                                               MulSIMD(FDPart3tmp0, MulSIMD(hDD12, hDD12)))))));
        const REAL_SIMD_ARRAY FDPart3tmp17 =
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD001, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD110, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02))),
                                            MulSIMD(FDPart3tmp16, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))))));
        const REAL_SIMD_ARRAY FDPart3tmp19 =
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD002, FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))),
                            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD220, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                            MulSIMD(FDPart3tmp18, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))))));
        const REAL_SIMD_ARRAY FDPart3tmp20 =
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD002, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD220, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01))),
                                            MulSIMD(FDPart3tmp18, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))))));
        const REAL_SIMD_ARRAY FDPart3tmp21 = FusedMulAddSIMD(
            FDPart3tmp8, MulSIMD(hDD_dD002, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD220, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                            MulSIMD(FDPart3tmp18, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02))))));
        const REAL_SIMD_ARRAY FDPart3tmp25 =
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD112, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02))),
                            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD221, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                                            MulSIMD(FDPart3tmp24, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))))));
        const REAL_SIMD_ARRAY FDPart3tmp26 =
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD112, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD221, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01))),
                                            MulSIMD(FDPart3tmp24, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))))));
        const REAL_SIMD_ARRAY FDPart3tmp27 = FusedMulAddSIMD(
            FDPart3tmp8, MulSIMD(hDD_dD112, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD221, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                            MulSIMD(FDPart3tmp24, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))))));
        const REAL_SIMD_ARRAY FDPart3tmp31 =
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD001, FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))),
                            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD110, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                                            MulSIMD(FDPart3tmp16, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))))));
        const REAL_SIMD_ARRAY FDPart3tmp32 = FusedMulAddSIMD(
            FDPart3tmp8, MulSIMD(hDD_dD001, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD110, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                            MulSIMD(FDPart3tmp16, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01))))));
        const REAL_SIMD_ARRAY FDPart3tmp46 =
            FusedMulAddSIMD(FDPart3tmp45, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01))),
                            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD000, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                            MulSIMD(FDPart3tmp44, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))))));
        const REAL_SIMD_ARRAY FDPart3tmp49 = FusedMulAddSIMD(
            FDPart3tmp45, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD000, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                            MulSIMD(FDPart3tmp44, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02))))));
        const REAL_SIMD_ARRAY FDPart3tmp58 =
            FusedMulAddSIMD(FDPart3tmp45, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD000, FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))),
                                            MulSIMD(FDPart3tmp44, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))))));
        const REAL_SIMD_ARRAY FDPart3tmp78 =
            FusedMulAddSIMD(FDPart3tmp77, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD111, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02))),
                                            MulSIMD(FDPart3tmp76, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))))));
        const REAL_SIMD_ARRAY FDPart3tmp79 =
            FusedMulAddSIMD(FDPart3tmp77, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01))),
                            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD111, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                                            MulSIMD(FDPart3tmp76, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))))));
        const REAL_SIMD_ARRAY FDPart3tmp80 = FusedMulAddSIMD(
            FDPart3tmp77, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD111, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                            MulSIMD(FDPart3tmp76, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))))));
        const REAL_SIMD_ARRAY FDPart3tmp91 =
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(FDPart3tmp90, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02))),
                            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD222, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                                            MulSIMD(FDPart3tmp8, MulSIMD(FDPart3tmp89, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))))));
        const REAL_SIMD_ARRAY FDPart3tmp92 =
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(FDPart3tmp90, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD222, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01))),
                                            MulSIMD(FDPart3tmp8, MulSIMD(FDPart3tmp89, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))))));
        const REAL_SIMD_ARRAY FDPart3tmp93 = FusedMulAddSIMD(
            FDPart3tmp8, MulSIMD(FDPart3tmp90, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
            FusedMulAddSIMD(FDPart3tmp8, MulSIMD(hDD_dD222, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                            MulSIMD(FDPart3tmp8, MulSIMD(FDPart3tmp89, FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))))));
        const REAL_SIMD_ARRAY FDPart3tmp115 =
            MulSIMD(FDPart3_Integer_3, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))));
        const REAL_SIMD_ARRAY FDPart3tmp118 =
            MulSIMD(FDPart3_Integer_3, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01))));
        const REAL_SIMD_ARRAY FDPart3tmp120 =
            MulSIMD(FDPart3_Integer_3, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02))));
        const REAL_SIMD_ARRAY FDPart3tmp122 =
            MulSIMD(FDPart3_Integer_3, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))));
        const REAL_SIMD_ARRAY FDPart3tmp124 =
            MulSIMD(FDPart3_Integer_3, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))));
        const REAL_SIMD_ARRAY FDPart3tmp222 =
            MulSIMD(FDPart3_Integer_3, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))));
        const REAL_SIMD_ARRAY FDPart3tmp22 =
            FusedMulAddSIMD(FDPart3tmp2, FDPart3tmp21, FusedMulAddSIMD(FDPart3tmp20, hDD12, MulSIMD(FDPart3tmp19, hDD01)));
        const REAL_SIMD_ARRAY FDPart3tmp28 =
            FusedMulAddSIMD(FDPart3tmp25, hDD01, FusedMulAddSIMD(FDPart3tmp26, hDD02, MulSIMD(FDPart3tmp0, FDPart3tmp27)));
        const REAL_SIMD_ARRAY FDPart3tmp29 = MulSIMD(FDPart3tmp15, FDPart3tmp17);
        const REAL_SIMD_ARRAY FDPart3tmp33 =
            FusedMulAddSIMD(FDPart3tmp3, FDPart3tmp32, FusedMulAddSIMD(FDPart3tmp31, hDD02, MulSIMD(FDPart3tmp17, hDD12)));
        const REAL_SIMD_ARRAY FDPart3tmp35 = MulSIMD(FDPart3tmp15, FDPart3tmp20);
        const REAL_SIMD_ARRAY FDPart3tmp48 = MulSIMD(FDPart3tmp40, FDPart3tmp46);
        const REAL_SIMD_ARRAY FDPart3tmp59 =
            FusedMulAddSIMD(FDPart3tmp46, hDD12, FusedMulAddSIMD(FDPart3tmp58, hDD01, MulSIMD(FDPart3tmp2, FDPart3tmp49)));
        const REAL_SIMD_ARRAY FDPart3tmp65 =
            FusedMulAddSIMD(FDPart3tmp49, hDD12, FusedMulAddSIMD(FDPart3tmp58, hDD02, MulSIMD(FDPart3tmp3, FDPart3tmp46)));
        const REAL_SIMD_ARRAY FDPart3tmp81 =
            FusedMulAddSIMD(FDPart3tmp78, hDD01, FusedMulAddSIMD(FDPart3tmp79, hDD02, MulSIMD(FDPart3tmp0, FDPart3tmp80)));
        const REAL_SIMD_ARRAY FDPart3tmp94 =
            FusedMulAddSIMD(FDPart3tmp91, hDD01, FusedMulAddSIMD(FDPart3tmp92, hDD02, MulSIMD(FDPart3tmp0, FDPart3tmp93)));
        const REAL_SIMD_ARRAY FDPart3tmp102 = MulSIMD(FDPart3tmp20, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))));
        const REAL_SIMD_ARRAY FDPart3tmp103 = MulSIMD(FDPart3tmp26, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))));
        const REAL_SIMD_ARRAY FDPart3tmp104 =
            MulSIMD(FDPart3tmp8, MulSIMD(FDPart3tmp92, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01))));
        const REAL_SIMD_ARRAY FDPart3tmp106 = MulSIMD(FDPart3tmp17, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))));
        const REAL_SIMD_ARRAY FDPart3tmp107 =
            MulSIMD(FDPart3tmp78, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02))));
        const REAL_SIMD_ARRAY FDPart3tmp112 =
            MulSIMD(FDPart3tmp58, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))));
        const REAL_SIMD_ARRAY FDPart3tmp136 = MulSIMD(FDPart3tmp15, FDPart3tmp78);
        const REAL_SIMD_ARRAY FDPart3tmp159 =
            FusedMulAddSIMD(FDPart3tmp78, hDD12, FusedMulAddSIMD(FDPart3tmp80, hDD02, MulSIMD(FDPart3tmp3, FDPart3tmp79)));
        const REAL_SIMD_ARRAY FDPart3tmp172 =
            FusedMulAddSIMD(FDPart3tmp92, hDD12, FusedMulAddSIMD(FDPart3tmp93, hDD01, MulSIMD(FDPart3tmp2, FDPart3tmp91)));
        const REAL_SIMD_ARRAY FDPart3tmp182 = MulSIMD(FDPart3tmp15, FDPart3tmp92);
        const REAL_SIMD_ARRAY FDPart3tmp204 = MulSIMD(FDPart3tmp15, FDPart3tmp26);
        const REAL_SIMD_ARRAY FDPart3tmp209 = MulSIMD(FDPart3tmp15, FDPart3tmp79);
        const REAL_SIMD_ARRAY FDPart3tmp237 = MulSIMD(FDPart3tmp15, FDPart3tmp91);
        const REAL_SIMD_ARRAY FDPart3tmp239 = MulSIMD(FDPart3tmp15, FDPart3tmp93);
        const REAL_SIMD_ARRAY FDPart3tmp52 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp15, FDPart3tmp49));
        const REAL_SIMD_ARRAY FDPart3tmp56 =
            FusedMulAddSIMD(FDPart3tmp17, hDD01, FusedMulAddSIMD(FDPart3tmp32, hDD02, MulSIMD(FDPart3tmp0, FDPart3tmp31)));
        const REAL_SIMD_ARRAY FDPart3tmp63 =
            FusedMulAddSIMD(FDPart3tmp20, hDD02, FusedMulAddSIMD(FDPart3tmp21, hDD01, MulSIMD(FDPart3tmp0, FDPart3tmp19)));
        const REAL_SIMD_ARRAY FDPart3tmp66 = MulSIMD(FDPart3tmp15, FDPart3tmp65);
        const REAL_SIMD_ARRAY FDPart3tmp68 = MulSIMD(FDPart3tmp15, FDPart3tmp59);
        const REAL_SIMD_ARRAY FDPart3tmp73 =
            FusedMulAddSIMD(FDPart3tmp31, hDD01, FusedMulAddSIMD(FDPart3tmp32, hDD12, MulSIMD(FDPart3tmp17, FDPart3tmp2)));
        const REAL_SIMD_ARRAY FDPart3tmp86 =
            FusedMulAddSIMD(FDPart3tmp20, FDPart3tmp3, FusedMulAddSIMD(FDPart3tmp21, hDD12, MulSIMD(FDPart3tmp19, hDD02)));
        const REAL_SIMD_ARRAY FDPart3tmp105 = FusedMulAddSIMD(
            FDPart3_Rational_1_2,
            AddSIMD(FDPart3tmp102,
                    FusedMulAddSIMD(FDPart3tmp32, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))), FDPart3tmp103)),
            MulSIMD(
                FDPart3tmp15,
                FusedMulAddSIMD(FDPart3tmp46, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))),
                                FusedMulAddSIMD(FDPart3tmp79, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02))),
                                                FDPart3tmp104))));
        const REAL_SIMD_ARRAY FDPart3tmp108 = FusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(
                FDPart3tmp21, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                FusedMulAddSIMD(FDPart3tmp25, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))), FDPart3tmp106)),
            MulSIMD(
                FDPart3tmp15,
                FusedMulAddSIMD(FDPart3tmp49, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))),
                                FusedMulAddSIMD(FDPart3tmp8, MulSIMD(FDPart3tmp91, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01))),
                                                FDPart3tmp107))));
        const REAL_SIMD_ARRAY FDPart3tmp113 = FusedMulAddSIMD(
            FDPart3_Rational_1_2,
            FusedMulAddSIMD(FDPart3tmp27, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                            FusedMulAddSIMD(FDPart3tmp31, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                                            MulSIMD(FDPart3tmp19, MulSIMD(FDPart3tmp8, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)))))),
            MulSIMD(
                FDPart3tmp15,
                FusedMulAddSIMD(FDPart3tmp8, MulSIMD(FDPart3tmp80, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02))),
                                FusedMulAddSIMD(FDPart3tmp8, MulSIMD(FDPart3tmp93, FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01))),
                                                FDPart3tmp112))));
        const REAL_SIMD_ARRAY FDPart3tmp125 =
            FusedMulAddSIMD(FDPart3tmp46, hDD02, FusedMulAddSIMD(FDPart3tmp49, hDD01, MulSIMD(FDPart3tmp0, FDPart3tmp58)));
        const REAL_SIMD_ARRAY FDPart3tmp133 =
            FusedMulAddSIMD(FDPart3tmp26, hDD12, FusedMulAddSIMD(FDPart3tmp27, hDD01, MulSIMD(FDPart3tmp2, FDPart3tmp25)));
        const REAL_SIMD_ARRAY FDPart3tmp143 =
            FusedMulAddSIMD(FDPart3tmp79, hDD12, FusedMulAddSIMD(FDPart3tmp80, hDD01, MulSIMD(FDPart3tmp2, FDPart3tmp78)));
        const REAL_SIMD_ARRAY FDPart3tmp149 = MulSIMD(FDPart3tmp19, FDPart3tmp28);
        const REAL_SIMD_ARRAY FDPart3tmp152 = MulSIMD(FDPart3tmp28, FDPart3tmp31);
        const REAL_SIMD_ARRAY FDPart3tmp170 =
            FusedMulAddSIMD(FDPart3tmp26, FDPart3tmp3, FusedMulAddSIMD(FDPart3tmp27, hDD02, MulSIMD(FDPart3tmp25, hDD12)));
        const REAL_SIMD_ARRAY FDPart3tmp186 =
            FusedMulAddSIMD(FDPart3tmp91, hDD12, FusedMulAddSIMD(FDPart3tmp93, hDD02, MulSIMD(FDPart3tmp3, FDPart3tmp92)));
        const REAL_SIMD_ARRAY FDPart3tmp210 = MulSIMD(FDPart3_Integer_2, FDPart3tmp209);
        const REAL_SIMD_ARRAY FDPart3tmp238 = MulSIMD(FDPart3_Integer_2, FDPart3tmp237);
        const REAL_SIMD_ARRAY FDPart3tmp38 = MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp21, FDPart3tmp22));
        const REAL_SIMD_ARRAY FDPart3tmp42 = MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp32, FDPart3tmp33));
        const REAL_SIMD_ARRAY FDPart3tmp87 = MulSIMD(FDPart3tmp15, FDPart3tmp86);
        const REAL_SIMD_ARRAY FDPart3tmp100 = MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp49, FDPart3tmp73));
        const REAL_SIMD_ARRAY FDPart3tmp114 = MulSIMD(FDPart3tmp19, FDPart3tmp56);
        const REAL_SIMD_ARRAY FDPart3tmp116 = MulSIMD(FDPart3tmp31, FDPart3tmp63);
        const REAL_SIMD_ARRAY FDPart3tmp117 = MulSIMD(FDPart3tmp19, FDPart3tmp63);
        const REAL_SIMD_ARRAY FDPart3tmp119 = MulSIMD(FDPart3tmp31, FDPart3tmp56);
        const REAL_SIMD_ARRAY FDPart3tmp134 = MulSIMD(FDPart3tmp133, FDPart3tmp21);
        const REAL_SIMD_ARRAY FDPart3tmp138 = MulSIMD(FDPart3tmp133, FDPart3tmp17);
        const REAL_SIMD_ARRAY FDPart3tmp148 = MulSIMD(FDPart3tmp27, FDPart3tmp63);
        const REAL_SIMD_ARRAY FDPart3tmp156 = MulSIMD(FDPart3tmp27, FDPart3tmp56);
        const REAL_SIMD_ARRAY FDPart3tmp171 = MulSIMD(FDPart3tmp170, FDPart3tmp20);
        const REAL_SIMD_ARRAY FDPart3tmp187 = MulSIMD(FDPart3tmp186, MulSIMD(FDPart3tmp32, FDPart3tmp40));
        const REAL_SIMD_ARRAY FDPart3tmp188 = MulSIMD(FDPart3_Integer_2, FDPart3tmp186);
        const REAL_SIMD_ARRAY FDPart3tmp189 = FusedMulAddSIMD(FDPart3tmp17, FDPart3tmp22, MulSIMD(FDPart3tmp17, FDPart3tmp33));
        const REAL_SIMD_ARRAY FDPart3tmp190 = FusedMulAddSIMD(FDPart3tmp31, FDPart3tmp33, FDPart3tmp152);
        const REAL_SIMD_ARRAY FDPart3tmp193 = FusedMulAddSIMD(FDPart3tmp31, FDPart3tmp86, MulSIMD(FDPart3tmp31, FDPart3tmp94));
        const REAL_SIMD_ARRAY FDPart3tmp194 = FusedMulAddSIMD(FDPart3tmp17, FDPart3tmp170, MulSIMD(FDPart3tmp17, FDPart3tmp172));
        const REAL_SIMD_ARRAY FDPart3tmp207 = MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp27, FDPart3tmp28));
        const REAL_SIMD_ARRAY FDPart3tmp221 = MulSIMD(FDPart3tmp133, FDPart3tmp25);
        const REAL_SIMD_ARRAY FDPart3tmp240 = MulSIMD(FDPart3_Integer_3, FDPart3tmp186);
        const REAL_SIMD_ARRAY FDPart3tmp75 = MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp21, FDPart3tmp73));
        const REAL_SIMD_ARRAY FDPart3tmp180 = MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp170, FDPart3tmp32));
        const REAL_SIMD_ARRAY FDPart3tmp200 = FusedMulAddSIMD(FDPart3tmp31, FDPart3tmp65, FDPart3tmp116);
        const REAL_SIMD_ARRAY FDPart3tmp201 = FusedMulAddSIMD(FDPart3tmp159, FDPart3tmp17, FDPart3tmp138);
        const REAL_SIMD_ARRAY FDPart3tmp226 = MulSIMD(FDPart3_Integer_2, FDPart3tmp87);
        const REAL_SIMD_ARRAY FDPart3tmp179 = MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp32, FDPart3tmp87));
        const REAL_SIMD_ARRAY __RHS_exp_0 = FusedMulAddSIMD(
            FDPart3tmp8,
            MulSIMD(
                FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp28, FDPart3tmp29), MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp17, FDPart3tmp22)))),
            FusedMulAddSIMD(
                FDPart3tmp8,
                MulSIMD(FusedMulAddSIMD(FDPart3tmp32, FDPart3tmp87, MulSIMD(FDPart3tmp32, MulSIMD(FDPart3tmp40, FDPart3tmp94))),
                        FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                FusedMulAddSIMD(
                    FDPart3tmp8,
                    MulSIMD(FusedMulAddSIMD(FDPart3tmp46, FDPart3tmp87, MulSIMD(FDPart3tmp48, FDPart3tmp94)),
                            FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                    FusedMulAddSIMD(
                        FDPart3tmp8,
                        MulSIMD(FusedMulAddSIMD(FDPart3tmp29, FDPart3tmp59, MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp29, FDPart3tmp56))),
                                FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                        FusedMulAddSIMD(
                            FDPart3tmp8,
                            MulSIMD(FusedMulAddSIMD(FDPart3tmp32, FDPart3tmp66, MulSIMD(FDPart3tmp32, MulSIMD(FDPart3tmp40, FDPart3tmp63))),
                                    FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                            FusedMulAddSIMD(
                                FDPart3tmp8,
                                MulSIMD(FusedMulAddSIMD(FDPart3tmp28, FDPart3tmp48, MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp33, FDPart3tmp46))),
                                        FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                                FusedMulAddSIMD(
                                    FDPart3tmp8,
                                    MulSIMD(FusedMulAddSIMD(FDPart3tmp28, FDPart3tmp52, MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp22, FDPart3tmp49))),
                                            FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                    FusedMulAddSIMD(
                                        FDPart3tmp8,
                                        MulSIMD(FusedMulAddSIMD(FDPart3tmp20, FDPart3tmp66,
                                                                MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp35, FDPart3tmp63))),
                                                FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp8,
                                            MulSIMD(FusedMulAddSIMD(FDPart3tmp21, FDPart3tmp68,
                                                                    MulSIMD(FDPart3tmp21, MulSIMD(FDPart3tmp40, FDPart3tmp56))),
                                                    FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                            FusedMulAddSIMD(
                                                FDPart3tmp8,
                                                MulSIMD(FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12)),
                                                        FusedMulAddSIMD(FDPart3tmp46, FDPart3tmp66, MulSIMD(FDPart3tmp48, FDPart3tmp63))),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp8,
                                                    MulSIMD(FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12)),
                                                            FusedMulAddSIMD(FDPart3tmp49, FDPart3tmp68, MulSIMD(FDPart3tmp52, FDPart3tmp56))),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp8,
                                                        MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01)),
                                                                FusedMulAddSIMD(FDPart3tmp20, FDPart3tmp87,
                                                                                MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp35, FDPart3tmp94)))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp8,
                                                            MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02)),
                                                                    FusedMulAddSIMD(FDPart3tmp29, FDPart3tmp73,
                                                                                    MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp29, FDPart3tmp81)))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp8,
                                                                MulSIMD(
                                                                    FusedMulAddSIMD(FDPart3tmp28, MulSIMD(FDPart3tmp32, FDPart3tmp40), FDPart3tmp42),
                                                                    FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp8,
                                                                    MulSIMD(FusedMulAddSIMD(FDPart3tmp21, MulSIMD(FDPart3tmp40, FDPart3tmp81),
                                                                                            FDPart3tmp75),
                                                                            FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp8,
                                                                        MulSIMD(FusedMulAddSIMD(FDPart3tmp52, FDPart3tmp81, FDPart3tmp100),
                                                                                FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp8,
                                                                            MulSIMD(FusedMulAddSIMD(FDPart3tmp21, MulSIMD(FDPart3tmp28, FDPart3tmp40),
                                                                                                    FDPart3tmp38),
                                                                                    FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01))),
                                                                            FusedMulAddSIMD(
                                                                                hDD01, lambdaU_dD10,
                                                                                FusedMulAddSIMD(
                                                                                    hDD02, lambdaU_dD20,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp0, lambdaU_dD00,
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp15,
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp122, MulSIMD(FDPart3tmp56, FDPart3tmp58),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3_Integer_3,
                                                                                                    MulSIMD(FDPart3tmp112, FDPart3tmp125),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp122,
                                                                                                        MulSIMD(FDPart3tmp125, FDPart3tmp31),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp117, FDPart3tmp118,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp119, FDPart3tmp120,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp124,
                                                                                                                    MulSIMD(FDPart3tmp125,
                                                                                                                            FDPart3tmp19),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp124,
                                                                                                                        MulSIMD(FDPart3tmp58,
                                                                                                                                FDPart3tmp63),
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp114,
                                                                                                                            FDPart3tmp115,
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3tmp115,
                                                                                                                                FDPart3tmp116))))))))),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp8,
                                                                                                MulSIMD(FusedMulSubSIMD(hDD01, hDD02,
                                                                                                                        MulSIMD(FDPart3tmp0, hDD12)),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3_Integer_2,
                                                                                                            MulSIMD(FDPart3tmp28, FDPart3tmp35),
                                                                                                            MulSIMD(FDPart3tmp15,
                                                                                                                    MulSIMD(FDPart3tmp20,
                                                                                                                            FDPart3tmp33)))),
                                                                                                FusedMulSubSIMD(
                                                                                                    FDPart3_Integer_2,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp108,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3_Rational_1_2,
                                                                                                            MulSIMD(FDPart3tmp17, hDD01),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3_Rational_1_2,
                                                                                                                MulSIMD(FDPart3tmp32, hDD02),
                                                                                                                MulSIMD(FDPart3_Rational_1_2,
                                                                                                                        MulSIMD(FDPart3tmp0,
                                                                                                                                FDPart3tmp31)))),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp113,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3_Rational_1_2,
                                                                                                                MulSIMD(FDPart3tmp46, hDD02),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                    MulSIMD(FDPart3tmp49, hDD01),
                                                                                                                    MulSIMD(FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(FDPart3tmp0,
                                                                                                                                    FDPart3tmp58)))),
                                                                                                            MulSIMD(
                                                                                                                FDPart3tmp105,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                    MulSIMD(FDPart3tmp20, hDD02),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                        MulSIMD(FDPart3tmp21, hDD01),
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3tmp0,
                                                                                                                                FDPart3tmp19))))))),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp8,
                                                                                                        MulSIMD(hDD_dDD0002,
                                                                                                                FusedMulSubSIMD(
                                                                                                                    hDD01, hDD12,
                                                                                                                    MulSIMD(FDPart3tmp2, hDD02))),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp8,
                                                                                                            MulSIMD(hDD_dDD0012,
                                                                                                                    FusedMulSubSIMD(
                                                                                                                        hDD01, hDD02,
                                                                                                                        MulSIMD(FDPart3tmp0, hDD12))),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3_Rational_1_2,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp8,
                                                                                                                    MulSIMD(
                                                                                                                        hDD_dDD0011,
                                                                                                                        FusedMulSubSIMD(
                                                                                                                            FDPart3tmp0, FDPart3tmp3,
                                                                                                                            MulSIMD(hDD02, hDD02))),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp8,
                                                                                                                        MulSIMD(hDD_dDD0022,
                                                                                                                                FusedMulSubSIMD(
                                                                                                                                    FDPart3tmp0,
                                                                                                                                    FDPart3tmp2,
                                                                                                                                    MulSIMD(hDD01,
                                                                                                                                            hDD01))),
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3tmp8,
                                                                                                                            MulSIMD(
                                                                                                                                hDD_dDD0000,
                                                                                                                                FusedMulSubSIMD(
                                                                                                                                    FDPart3tmp2,
                                                                                                                                    FDPart3tmp3,
                                                                                                                                    MulSIMD(
                                                                                                                                        hDD12,
                                                                                                                                        hDD12)))))),
                                                                                                                MulSIMD(
                                                                                                                    FDPart3tmp8,
                                                                                                                    MulSIMD(
                                                                                                                        hDD_dDD0001,
                                                                                                                        FusedMulSubSIMD(
                                                                                                                            hDD02, hDD12,
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3tmp3,
                                                                                                                                hDD01))))))))))))))))))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_1 = FusedMulAddSIMD(
            FDPart3tmp8,
            MulSIMD(FusedMulAddSIMD(FDPart3tmp28, FDPart3tmp29, MulSIMD(FDPart3tmp133, MulSIMD(FDPart3tmp40, FDPart3tmp49))),
                    FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
            FusedMulAddSIMD(
                FDPart3tmp8,
                MulSIMD(FusedMulAddSIMD(FDPart3tmp136, FDPart3tmp56, MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp29, FDPart3tmp73))),
                        FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                FusedMulAddSIMD(
                    FDPart3tmp8,
                    MulSIMD(FusedMulAddSIMD(FDPart3tmp143, FDPart3tmp52, MulSIMD(FDPart3tmp29, FDPart3tmp81)),
                            FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                    FusedMulAddSIMD(
                        FDPart3tmp8,
                        MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02)),
                                FusedMulAddSIMD(FDPart3tmp136, FDPart3tmp81, MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp143, FDPart3tmp29)))),
                        FusedMulAddSIMD(
                            FDPart3tmp8,
                            MulSIMD(FusedMulAddSIMD(FDPart3tmp136, FDPart3tmp28, MulSIMD(FDPart3tmp138, FDPart3tmp40)),
                                    FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                            FusedMulAddSIMD(
                                FDPart3tmp8,
                                MulSIMD(FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp75, MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp25, FDPart3tmp56))),
                                        FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                FusedMulAddSIMD(
                                    FDPart3tmp8,
                                    MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01)),
                                            FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp134, FDPart3tmp15),
                                                            MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp25, FDPart3tmp28)))),
                                    FusedMulAddSIMD(
                                        FDPart3tmp15,
                                        FusedMulAddSIMD(
                                            FDPart3tmp8,
                                            MulSIMD(FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01)),
                                                    FusedMulAddSIMD(FDPart3tmp125, FDPart3tmp80,
                                                                    FusedMulAddSIMD(FDPart3tmp31, FDPart3tmp59, FDPart3tmp119))),
                                            FusedMulAddSIMD(
                                                FDPart3tmp8,
                                                MulSIMD(FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01)),
                                                        FusedMulAddSIMD(FDPart3tmp58, FDPart3tmp73,
                                                                        FusedMulAddSIMD(FDPart3tmp58, FDPart3tmp81, FDPart3tmp119))),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp8,
                                                    MulSIMD(FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp172, FDPart3tmp46,
                                                                FusedMulAddSIMD(FDPart3tmp32, FDPart3tmp94, MulSIMD(FDPart3tmp170, FDPart3tmp46)))),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp8,
                                                        MulSIMD(FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)),
                                                                FusedMulAddSIMD(FDPart3tmp20, FDPart3tmp33,
                                                                                FusedMulAddSIMD(FDPart3tmp26, FDPart3tmp63,
                                                                                                MulSIMD(FDPart3tmp20, FDPart3tmp22)))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp8,
                                                            MulSIMD(FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)),
                                                                    FusedMulAddSIMD(FDPart3tmp125, FDPart3tmp27,
                                                                                    FusedMulAddSIMD(FDPart3tmp19, FDPart3tmp59, FDPart3tmp114))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp8,
                                                                MulSIMD(FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)),
                                                                        FusedMulAddSIMD(FDPart3tmp22, FDPart3tmp58,
                                                                                        FusedMulAddSIMD(FDPart3tmp28, FDPart3tmp58, FDPart3tmp116))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp8,
                                                                    MulSIMD(FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                                                            FusedMulAddSIMD(FDPart3tmp159, FDPart3tmp20,
                                                                                            FusedMulAddSIMD(FDPart3tmp26, FDPart3tmp28,
                                                                                                            MulSIMD(FDPart3tmp133, FDPart3tmp20)))),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp8,
                                                                        MulSIMD(
                                                                            FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                                                            FusedMulAddSIMD(FDPart3tmp172, FDPart3tmp32,
                                                                                            FusedMulAddSIMD(FDPart3tmp79, FDPart3tmp94,
                                                                                                            MulSIMD(FDPart3tmp170, FDPart3tmp32)))),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp8,
                                                                            MulSIMD(FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp22, FDPart3tmp31,
                                                                                        FusedMulAddSIMD(FDPart3tmp63, FDPart3tmp80, FDPart3tmp152))),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp8,
                                                                                MulSIMD(FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                                                                        FusedMulAddSIMD(FDPart3tmp19, FDPart3tmp73,
                                                                                                        FusedMulAddSIMD(FDPart3tmp19, FDPart3tmp81,
                                                                                                                        FDPart3tmp156))),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp8,
                                                                                    MulSIMD(
                                                                                        FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3,
                                                                                                        MulSIMD(hDD12, hDD12)),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp56, FDPart3tmp58,
                                                                                            FusedMulAddSIMD(FDPart3tmp58, FDPart3tmp59,
                                                                                                            MulSIMD(FDPart3tmp125, FDPart3tmp31)))),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp8,
                                                                                        MulSIMD(FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3,
                                                                                                                MulSIMD(hDD12, hDD12)),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp32, FDPart3tmp63,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp33, FDPart3tmp46,
                                                                                                        MulSIMD(FDPart3tmp22, FDPart3tmp46)))),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp8,
                                                                                            MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3,
                                                                                                                    MulSIMD(hDD02, hDD02)),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp159, FDPart3tmp32,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp28, FDPart3tmp79,
                                                                                                            MulSIMD(FDPart3tmp133, FDPart3tmp32)))),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp8,
                                                                                                MulSIMD(
                                                                                                    FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3,
                                                                                                                    MulSIMD(hDD02, hDD02)),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp31, FDPart3tmp81,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp56, FDPart3tmp80,
                                                                                                            MulSIMD(FDPart3tmp31, FDPart3tmp73)))),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp8,
                                                                                                    MulSIMD(
                                                                                                        FusedMulSubSIMD(hDD02, hDD12,
                                                                                                                        MulSIMD(FDPart3tmp3, hDD01)),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp159, FDPart3tmp46,
                                                                                                            FusedMulAddSIMD(FDPart3tmp28,
                                                                                                                            FDPart3tmp32,
                                                                                                                            MulSIMD(FDPart3tmp133,
                                                                                                                                    FDPart3tmp46)))),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp8,
                                                                                                        MulSIMD(FusedMulSubSIMD(
                                                                                                                    hDD02, hDD12,
                                                                                                                    MulSIMD(FDPart3tmp3, hDD01)),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp32, FDPart3tmp33,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp63, FDPart3tmp79,
                                                                                                                        MulSIMD(FDPart3tmp22,
                                                                                                                                FDPart3tmp32)))),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp8,
                                                                                                            MulSIMD(FusedMulSubSIMD(
                                                                                                                        FDPart3tmp0, FDPart3tmp2,
                                                                                                                        MulSIMD(hDD01, hDD01)),
                                                                                                                    AddSIMD(FDPart3tmp148,
                                                                                                                            FusedMulAddSIMD(
                                                                                                                                FDPart3tmp19,
                                                                                                                                FDPart3tmp22,
                                                                                                                                FDPart3tmp149))),
                                                                                                            MulSIMD(
                                                                                                                FDPart3tmp8,
                                                                                                                MulSIMD(
                                                                                                                    FusedMulSubSIMD(
                                                                                                                        FDPart3tmp0, FDPart3tmp2,
                                                                                                                        MulSIMD(hDD01, hDD01)),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp172, FDPart3tmp20,
                                                                                                                        FusedMulAddSIMD(
                                                                                                                            FDPart3tmp26,
                                                                                                                            FDPart3tmp94,
                                                                                                                            FDPart3tmp171))))))))))))))))))))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp8,
                                            MulSIMD(FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp100, MulSIMD(FDPart3tmp29, FDPart3tmp56)),
                                                    FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))),
                                            FusedMulAddSIMD(
                                                FDPart3tmp8,
                                                MulSIMD(FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                                        FusedMulAddSIMD(FDPart3tmp143, MulSIMD(FDPart3tmp21, FDPart3tmp40),
                                                                        MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp25, FDPart3tmp81)))),
                                                FusedMulSubSIMD(
                                                    FDPart3_Rational_1_2,
                                                    FusedMulAddSIMD(
                                                        hDD02, lambdaU_dD21,
                                                        FusedMulAddSIMD(
                                                            hDD01, lambdaU_dD00,
                                                            FusedMulAddSIMD(
                                                                hDD01, lambdaU_dD11,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp113, AddSIMD(FDPart3tmp56, FDPart3tmp59),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp2, lambdaU_dD10,
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp105, AddSIMD(FDPart3tmp22, FDPart3tmp28),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp108, AddSIMD(FDPart3tmp73, FDPart3tmp81),
                                                                                FusedMulAddSIMD(
                                                                                    hDD12, lambdaU_dD20,
                                                                                    FusedMulSubSIMD(
                                                                                        FDPart3tmp0, lambdaU_dD01,
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp8,
                                                                                            MulSIMD(hDD_dDD0111,
                                                                                                    FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3,
                                                                                                                    MulSIMD(hDD02, hDD02))),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp8,
                                                                                                MulSIMD(hDD_dDD0122,
                                                                                                        FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2,
                                                                                                                        MulSIMD(hDD01, hDD01))),
                                                                                                MulSIMD(
                                                                                                    FDPart3tmp8,
                                                                                                    MulSIMD(hDD_dDD0100,
                                                                                                            FusedMulSubSIMD(
                                                                                                                FDPart3tmp2, FDPart3tmp3,
                                                                                                                MulSIMD(hDD12, hDD12))))))))))))))),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp8, MulSIMD(hDD_dDD0102, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp8,
                                                            MulSIMD(hDD_dDD0112, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                                                            MulSIMD(FDPart3tmp8,
                                                                    MulSIMD(hDD_dDD0101,
                                                                            FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01)))))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_2 = FusedMulAddSIMD(
            FDPart3tmp8,
            MulSIMD(FusedMulAddSIMD(FDPart3tmp186, FDPart3tmp48, MulSIMD(FDPart3tmp35, FDPart3tmp94)),
                    FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
            FusedMulAddSIMD(
                FDPart3tmp8,
                MulSIMD(FusedMulAddSIMD(FDPart3tmp171, FDPart3tmp40, MulSIMD(FDPart3tmp182, FDPart3tmp28)),
                        FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                FusedMulAddSIMD(
                    FDPart3tmp8,
                    MulSIMD(FusedMulAddSIMD(FDPart3tmp182, FDPart3tmp63, MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp20, FDPart3tmp87))),
                            FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                    FusedMulAddSIMD(
                        FDPart3tmp8,
                        MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01)),
                                FusedMulAddSIMD(FDPart3tmp182, FDPart3tmp94, MulSIMD(FDPart3tmp188, FDPart3tmp35))),
                        FusedMulAddSIMD(
                            FDPart3tmp8,
                            MulSIMD(FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12)),
                                    FusedMulAddSIMD(FDPart3tmp35, FDPart3tmp63, MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp46, FDPart3tmp87)))),
                            FusedMulAddSIMD(
                                FDPart3tmp8,
                                MulSIMD(FusedMulAddSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp26, FDPart3tmp94), FDPart3tmp187),
                                        FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                                FusedMulAddSIMD(
                                    FDPart3tmp8,
                                    MulSIMD(
                                        FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp180, MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp26, FDPart3tmp28))),
                                        FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02))),
                                    FusedMulAddSIMD(
                                        FDPart3tmp15,
                                        FusedMulAddSIMD(
                                            FDPart3tmp8,
                                            MulSIMD(
                                                FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)),
                                                FusedMulAddSIMD(FDPart3tmp172, FDPart3tmp49,
                                                                FusedMulAddSIMD(FDPart3tmp21, FDPart3tmp28, MulSIMD(FDPart3tmp170, FDPart3tmp49)))),
                                            FusedMulAddSIMD(
                                                FDPart3tmp8,
                                                MulSIMD(FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp21, FDPart3tmp33,
                                                            FusedMulAddSIMD(FDPart3tmp56, FDPart3tmp91, MulSIMD(FDPart3tmp21, FDPart3tmp22)))),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp8,
                                                    MulSIMD(FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)),
                                                            FusedMulAddSIMD(FDPart3tmp125, FDPart3tmp93,
                                                                            FusedMulAddSIMD(FDPart3tmp19, FDPart3tmp65, FDPart3tmp117))),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp8,
                                                        MulSIMD(FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)),
                                                                FusedMulAddSIMD(FDPart3tmp58, FDPart3tmp86,
                                                                                FusedMulAddSIMD(FDPart3tmp58, FDPart3tmp94, FDPart3tmp117))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp8,
                                                            MulSIMD(FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                                                    FusedMulAddSIMD(FDPart3tmp159, FDPart3tmp21,
                                                                                    FusedMulAddSIMD(FDPart3tmp81, FDPart3tmp91, FDPart3tmp134))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp8,
                                                                MulSIMD(FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                                                        FusedMulAddSIMD(FDPart3tmp19, FDPart3tmp33,
                                                                                        FusedMulAddSIMD(FDPart3tmp56, FDPart3tmp93, FDPart3tmp149))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp8,
                                                                    MulSIMD(FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12)),
                                                                            FusedMulAddSIMD(FDPart3tmp58, FDPart3tmp63,
                                                                                            FusedMulAddSIMD(FDPart3tmp58, FDPart3tmp65,
                                                                                                            MulSIMD(FDPart3tmp125, FDPart3tmp19)))),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp8,
                                                                        MulSIMD(
                                                                            FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12)),
                                                                            FusedMulAddSIMD(FDPart3tmp22, FDPart3tmp49,
                                                                                            FusedMulAddSIMD(FDPart3tmp33, FDPart3tmp49,
                                                                                                            MulSIMD(FDPart3tmp21, FDPart3tmp56)))),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp8,
                                                                            MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01)),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp172, FDPart3tmp21,
                                                                                        FusedMulAddSIMD(FDPart3tmp28, FDPart3tmp91,
                                                                                                        MulSIMD(FDPart3tmp170, FDPart3tmp21)))),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp8,
                                                                                MulSIMD(
                                                                                    FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01)),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp19, FDPart3tmp94,
                                                                                        FusedMulAddSIMD(FDPart3tmp63, FDPart3tmp93,
                                                                                                        MulSIMD(FDPart3tmp19, FDPart3tmp86)))),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp8,
                                                                                    MulSIMD(
                                                                                        FusedMulAddSIMD(FDPart3tmp125, FDPart3tmp27, FDPart3tmp200),
                                                                                        FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp8,
                                                                                        MulSIMD(FusedMulAddSIMD(FDPart3tmp25, FDPart3tmp81,
                                                                                                                FDPart3tmp201),
                                                                                                FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3,
                                                                                                                MulSIMD(hDD02, hDD02))),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp8,
                                                                                            MulSIMD(FusedMulAddSIMD(FDPart3tmp25, FDPart3tmp56,
                                                                                                                    FDPart3tmp189),
                                                                                                    FusedMulSubSIMD(hDD02, hDD12,
                                                                                                                    MulSIMD(FDPart3tmp3, hDD01))),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp8,
                                                                                                MulSIMD(FusedMulAddSIMD(FDPart3tmp25, FDPart3tmp28,
                                                                                                                        FDPart3tmp194),
                                                                                                        FusedMulSubSIMD(hDD01, hDD02,
                                                                                                                        MulSIMD(FDPart3tmp0, hDD12))),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp8,
                                                                                                    MulSIMD(
                                                                                                        FusedMulSubSIMD(hDD02, hDD12,
                                                                                                                        MulSIMD(FDPart3tmp3, hDD01)),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp28, FDPart3tmp58,
                                                                                                            FusedMulAddSIMD(FDPart3tmp33,
                                                                                                                            FDPart3tmp58,
                                                                                                                            FDPart3tmp114))),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp8,
                                                                                                        MulSIMD(FusedMulSubSIMD(
                                                                                                                    hDD02, hDD12,
                                                                                                                    MulSIMD(FDPart3tmp3, hDD01)),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp159, FDPart3tmp49,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp21, FDPart3tmp81,
                                                                                                                        MulSIMD(FDPart3tmp133,
                                                                                                                                FDPart3tmp49)))),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp8,
                                                                                                            MulSIMD(
                                                                                                                AddSIMD(FDPart3tmp148, FDPart3tmp193),
                                                                                                                FusedMulSubSIMD(
                                                                                                                    hDD01, hDD02,
                                                                                                                    MulSIMD(FDPart3tmp0, hDD12))),
                                                                                                            MulSIMD(
                                                                                                                FDPart3tmp8,
                                                                                                                MulSIMD(
                                                                                                                    AddSIMD(FDPart3tmp156,
                                                                                                                            FDPart3tmp190),
                                                                                                                    FusedMulSubSIMD(
                                                                                                                        FDPart3tmp0, FDPart3tmp3,
                                                                                                                        MulSIMD(
                                                                                                                            hDD02,
                                                                                                                            hDD02))))))))))))))))))))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp8,
                                            MulSIMD(FusedMulAddSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp26, FDPart3tmp63), FDPart3tmp179),
                                                    FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                                            FusedMulAddSIMD(
                                                FDPart3tmp8,
                                                MulSIMD(FusedMulAddSIMD(FDPart3tmp28, FDPart3tmp35,
                                                                        MulSIMD(FDPart3tmp170, MulSIMD(FDPart3tmp40, FDPart3tmp46))),
                                                        FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                                                FusedMulSubSIMD(
                                                    FDPart3_Rational_1_2,
                                                    FusedMulAddSIMD(
                                                        hDD02, lambdaU_dD22,
                                                        FusedMulAddSIMD(
                                                            hDD01, lambdaU_dD12,
                                                            FusedMulAddSIMD(
                                                                hDD02, lambdaU_dD00,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp113, AddSIMD(FDPart3tmp63, FDPart3tmp65),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp3, lambdaU_dD20,
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp105, AddSIMD(FDPart3tmp86, FDPart3tmp94),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp108, AddSIMD(FDPart3tmp28, FDPart3tmp33),
                                                                                FusedMulAddSIMD(
                                                                                    hDD12, lambdaU_dD10,
                                                                                    FusedMulSubSIMD(
                                                                                        FDPart3tmp0, lambdaU_dD02,
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp8,
                                                                                            MulSIMD(hDD_dDD0211,
                                                                                                    FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3,
                                                                                                                    MulSIMD(hDD02, hDD02))),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp8,
                                                                                                MulSIMD(hDD_dDD0222,
                                                                                                        FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2,
                                                                                                                        MulSIMD(hDD01, hDD01))),
                                                                                                MulSIMD(
                                                                                                    FDPart3tmp8,
                                                                                                    MulSIMD(hDD_dDD0200,
                                                                                                            FusedMulSubSIMD(
                                                                                                                FDPart3tmp2, FDPart3tmp3,
                                                                                                                MulSIMD(hDD12, hDD12))))))))))))))),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp8, MulSIMD(hDD_dDD0202, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp8,
                                                            MulSIMD(hDD_dDD0212, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                                                            MulSIMD(FDPart3tmp8,
                                                                    MulSIMD(hDD_dDD0201,
                                                                            FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01)))))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_3 = FusedMulAddSIMD(
            FDPart3tmp8,
            MulSIMD(FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01)),
                    FusedMulAddSIMD(FDPart3tmp133, MulSIMD(FDPart3tmp32, FDPart3tmp40), MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp159, FDPart3tmp32)))),
            FusedMulAddSIMD(
                FDPart3tmp8,
                MulSIMD(
                    FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                    FusedMulAddSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp28, FDPart3tmp80), MulSIMD(FDPart3tmp22, MulSIMD(FDPart3tmp40, FDPart3tmp80)))),
                FusedMulAddSIMD(
                    FDPart3tmp8,
                    MulSIMD(FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01)),
                            FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp68, FDPart3tmp80),
                                            MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp56, FDPart3tmp80)))),
                    FusedMulAddSIMD(
                        FDPart3tmp8,
                        MulSIMD(FusedMulAddSIMD(FDPart3tmp209, FDPart3tmp33, MulSIMD(FDPart3tmp210, FDPart3tmp22)),
                                FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                        FusedMulAddSIMD(
                            FDPart3tmp8,
                            MulSIMD(FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                    FusedMulAddSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp27, FDPart3tmp81),
                                                    MulSIMD(FDPart3tmp27, MulSIMD(FDPart3tmp40, FDPart3tmp73)))),
                            FusedMulAddSIMD(
                                FDPart3tmp8,
                                MulSIMD(FusedMulAddSIMD(FDPart3tmp170, FDPart3tmp209, MulSIMD(FDPart3tmp172, FDPart3tmp210)),
                                        FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                                FusedMulAddSIMD(
                                    FDPart3tmp8,
                                    MulSIMD(FusedMulAddSIMD(FDPart3tmp204, FDPart3tmp33,
                                                            MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp204, FDPart3tmp22))),
                                            FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                    FusedMulAddSIMD(
                                        FDPart3tmp8,
                                        MulSIMD(
                                            FusedMulAddSIMD(FDPart3tmp15, FDPart3tmp156, MulSIMD(FDPart3tmp27, MulSIMD(FDPart3tmp40, FDPart3tmp59))),
                                            FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp8,
                                            MulSIMD(FusedMulAddSIMD(FDPart3tmp159, FDPart3tmp204,
                                                                    MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp133, FDPart3tmp204))),
                                                    FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                                            FusedMulAddSIMD(
                                                FDPart3tmp8,
                                                MulSIMD(FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12)),
                                                        FusedMulAddSIMD(FDPart3tmp119, FDPart3tmp15,
                                                                        MulSIMD(FDPart3tmp31, MulSIMD(FDPart3tmp40, FDPart3tmp59)))),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp8,
                                                    MulSIMD(FusedMulAddSIMD(FDPart3tmp15, FDPart3tmp152,
                                                                            MulSIMD(FDPart3tmp22, MulSIMD(FDPart3tmp31, FDPart3tmp40))),
                                                            FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp8,
                                                        MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02)),
                                                                FusedMulAddSIMD(FDPart3tmp133, FDPart3tmp210, MulSIMD(FDPart3tmp159, FDPart3tmp209))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp8,
                                                            MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02)),
                                                                    FusedMulAddSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp80, FDPart3tmp81),
                                                                                    MulSIMD(FDPart3tmp40, MulSIMD(FDPart3tmp73, FDPart3tmp80)))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp8,
                                                                MulSIMD(
                                                                    FusedMulAddSIMD(FDPart3tmp22, MulSIMD(FDPart3tmp32, FDPart3tmp40), FDPart3tmp42),
                                                                    FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp8,
                                                                    MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01)),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp170, FDPart3tmp204,
                                                                                MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp172, FDPart3tmp204)))),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp8,
                                                                        MulSIMD(FusedMulAddSIMD(FDPart3tmp172, MulSIMD(FDPart3tmp32, FDPart3tmp40),
                                                                                                FDPart3tmp180),
                                                                                FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp8,
                                                                            MulSIMD(FusedMulAddSIMD(FDPart3tmp22, MulSIMD(FDPart3tmp27, FDPart3tmp40),
                                                                                                    FDPart3tmp207),
                                                                                    FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01))),
                                                                            FusedMulAddSIMD(
                                                                                hDD01, lambdaU_dD01,
                                                                                FusedMulAddSIMD(
                                                                                    hDD12, lambdaU_dD21,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp15,
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp122, MulSIMD(FDPart3tmp73, FDPart3tmp78),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp115, MulSIMD(FDPart3tmp133, FDPart3tmp78),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp115,
                                                                                                    MulSIMD(FDPart3tmp143, FDPart3tmp25),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3_Integer_3,
                                                                                                        MulSIMD(FDPart3tmp106, FDPart3tmp143),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3_Integer_3,
                                                                                                            MulSIMD(FDPart3tmp107, FDPart3tmp143),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp124,
                                                                                                                MulSIMD(FDPart3tmp25, FDPart3tmp73),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp17,
                                                                                                                    MulSIMD(FDPart3tmp222,
                                                                                                                            FDPart3tmp73),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp118, FDPart3tmp221,
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3tmp124,
                                                                                                                            FDPart3tmp138))))))))),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp2, lambdaU_dD11,
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp8,
                                                                                                MulSIMD(FusedMulSubSIMD(hDD02, hDD12,
                                                                                                                        MulSIMD(FDPart3tmp3, hDD01)),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp15,
                                                                                                            MulSIMD(FDPart3tmp31, FDPart3tmp81),
                                                                                                            MulSIMD(FDPart3tmp31,
                                                                                                                    MulSIMD(FDPart3tmp40,
                                                                                                                            FDPart3tmp73)))),
                                                                                                FusedMulSubSIMD(
                                                                                                    FDPart3_Integer_2,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp108,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3_Rational_1_2,
                                                                                                            MulSIMD(FDPart3tmp79, hDD12),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3_Rational_1_2,
                                                                                                                MulSIMD(FDPart3tmp80, hDD01),
                                                                                                                MulSIMD(FDPart3_Rational_1_2,
                                                                                                                        MulSIMD(FDPart3tmp2,
                                                                                                                                FDPart3tmp78)))),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp113,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3_Rational_1_2,
                                                                                                                MulSIMD(FDPart3tmp31, hDD01),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                    MulSIMD(FDPart3tmp32, hDD12),
                                                                                                                    MulSIMD(FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(FDPart3tmp17,
                                                                                                                                    FDPart3tmp2)))),
                                                                                                            MulSIMD(
                                                                                                                FDPart3tmp105,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                    MulSIMD(FDPart3tmp26, hDD12),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                        MulSIMD(FDPart3tmp27, hDD01),
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3tmp2,
                                                                                                                                FDPart3tmp25))))))),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp8,
                                                                                                        MulSIMD(hDD_dDD1102,
                                                                                                                FusedMulSubSIMD(
                                                                                                                    hDD01, hDD12,
                                                                                                                    MulSIMD(FDPart3tmp2, hDD02))),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp8,
                                                                                                            MulSIMD(hDD_dDD1112,
                                                                                                                    FusedMulSubSIMD(
                                                                                                                        hDD01, hDD02,
                                                                                                                        MulSIMD(FDPart3tmp0, hDD12))),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3_Rational_1_2,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp8,
                                                                                                                    MulSIMD(
                                                                                                                        hDD_dDD1111,
                                                                                                                        FusedMulSubSIMD(
                                                                                                                            FDPart3tmp0, FDPart3tmp3,
                                                                                                                            MulSIMD(hDD02, hDD02))),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp8,
                                                                                                                        MulSIMD(hDD_dDD1122,
                                                                                                                                FusedMulSubSIMD(
                                                                                                                                    FDPart3tmp0,
                                                                                                                                    FDPart3tmp2,
                                                                                                                                    MulSIMD(hDD01,
                                                                                                                                            hDD01))),
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3tmp8,
                                                                                                                            MulSIMD(
                                                                                                                                hDD_dDD1100,
                                                                                                                                FusedMulSubSIMD(
                                                                                                                                    FDPart3tmp2,
                                                                                                                                    FDPart3tmp3,
                                                                                                                                    MulSIMD(
                                                                                                                                        hDD12,
                                                                                                                                        hDD12)))))),
                                                                                                                MulSIMD(
                                                                                                                    FDPart3tmp8,
                                                                                                                    MulSIMD(
                                                                                                                        hDD_dDD1101,
                                                                                                                        FusedMulSubSIMD(
                                                                                                                            hDD02, hDD12,
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3tmp3,
                                                                                                                                hDD01))))))))))))))))))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_4 = FusedMulAddSIMD(
            FDPart3tmp8,
            MulSIMD(FusedMulAddSIMD(FDPart3tmp182, FDPart3tmp22, MulSIMD(FDPart3tmp226, FDPart3tmp26)),
                    FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
            FusedMulAddSIMD(
                FDPart3tmp8,
                MulSIMD(FusedMulAddSIMD(FDPart3tmp133, FDPart3tmp182, MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp170, FDPart3tmp204))),
                        FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                FusedMulAddSIMD(
                    FDPart3tmp8,
                    MulSIMD(FusedMulAddSIMD(FDPart3tmp172, FDPart3tmp204, MulSIMD(FDPart3tmp188, FDPart3tmp209)),
                            FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                    FusedMulAddSIMD(
                        FDPart3tmp8,
                        MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01)),
                                FusedMulAddSIMD(FDPart3tmp172, FDPart3tmp182, MulSIMD(FDPart3tmp188, FDPart3tmp204))),
                        FusedMulAddSIMD(
                            FDPart3tmp8,
                            MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02)),
                                    FusedMulAddSIMD(FDPart3tmp133, FDPart3tmp204, MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp170, FDPart3tmp209)))),
                            FusedMulAddSIMD(
                                FDPart3tmp8,
                                MulSIMD(FusedMulAddSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp172, FDPart3tmp20), FDPart3tmp187),
                                        FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                FusedMulAddSIMD(
                                    FDPart3tmp8,
                                    MulSIMD(FusedMulAddSIMD(FDPart3_Integer_2, FDPart3tmp180,
                                                            MulSIMD(FDPart3tmp133, MulSIMD(FDPart3tmp15, FDPart3tmp20))),
                                            FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                                    FusedMulAddSIMD(
                                        FDPart3tmp15,
                                        FusedMulAddSIMD(
                                            FDPart3tmp8,
                                            MulSIMD(FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)),
                                                    FusedMulAddSIMD(FDPart3tmp27, FDPart3tmp65,
                                                                    FusedMulAddSIMD(FDPart3tmp59, FDPart3tmp93, FDPart3tmp148))),
                                            FusedMulAddSIMD(
                                                FDPart3tmp8,
                                                MulSIMD(FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp25, FDPart3tmp33,
                                                            FusedMulAddSIMD(FDPart3tmp73, FDPart3tmp91, MulSIMD(FDPart3tmp22, FDPart3tmp25)))),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp8,
                                                    MulSIMD(FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp80, FDPart3tmp86,
                                                                FusedMulAddSIMD(FDPart3tmp80, FDPart3tmp94, MulSIMD(FDPart3tmp22, FDPart3tmp27)))),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp8,
                                                        MulSIMD(FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                                                FusedMulAddSIMD(FDPart3tmp27, FDPart3tmp33,
                                                                                FusedMulAddSIMD(FDPart3tmp73, FDPart3tmp93,
                                                                                                MulSIMD(FDPart3tmp27, FDPart3tmp28)))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp8,
                                                            MulSIMD(FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                                                    FusedMulAddSIMD(FDPart3tmp143, FDPart3tmp91,
                                                                                    FusedMulAddSIMD(FDPart3tmp159, FDPart3tmp25, FDPart3tmp221))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp8,
                                                                MulSIMD(FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                                                        FusedMulAddSIMD(FDPart3tmp170, FDPart3tmp78,
                                                                                        FusedMulAddSIMD(FDPart3tmp172, FDPart3tmp78, FDPart3tmp221))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp8,
                                                                    MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02)),
                                                                            FusedMulAddSIMD(FDPart3tmp143, FDPart3tmp25,
                                                                                            FusedMulAddSIMD(FDPart3tmp159, FDPart3tmp78,
                                                                                                            MulSIMD(FDPart3tmp133, FDPart3tmp78)))),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp8,
                                                                        MulSIMD(
                                                                            FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02)),
                                                                            FusedMulAddSIMD(FDPart3tmp28, FDPart3tmp80,
                                                                                            FusedMulAddSIMD(FDPart3tmp33, FDPart3tmp80,
                                                                                                            MulSIMD(FDPart3tmp27, FDPart3tmp73)))),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp8,
                                                                            MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01)),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp170, FDPart3tmp25,
                                                                                        FusedMulAddSIMD(FDPart3tmp172, FDPart3tmp25,
                                                                                                        MulSIMD(FDPart3tmp133, FDPart3tmp91)))),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp8,
                                                                                MulSIMD(
                                                                                    FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01)),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp27, FDPart3tmp86,
                                                                                        FusedMulAddSIMD(FDPart3tmp27, FDPart3tmp94,
                                                                                                        MulSIMD(FDPart3tmp22, FDPart3tmp93)))),
                                                                                FusedMulAddSIMD(
                                                                                    FDPart3tmp8,
                                                                                    MulSIMD(
                                                                                        FusedMulAddSIMD(FDPart3tmp19, FDPart3tmp59, FDPart3tmp200),
                                                                                        FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3,
                                                                                                        MulSIMD(hDD12, hDD12))),
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp8,
                                                                                        MulSIMD(FusedMulAddSIMD(FDPart3tmp143, FDPart3tmp21,
                                                                                                                FDPart3tmp201),
                                                                                                FusedMulSubSIMD(hDD02, hDD12,
                                                                                                                MulSIMD(FDPart3tmp3, hDD01))),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp8,
                                                                                            MulSIMD(FusedMulAddSIMD(FDPart3tmp19, FDPart3tmp73,
                                                                                                                    FDPart3tmp190),
                                                                                                    FusedMulSubSIMD(hDD02, hDD12,
                                                                                                                    MulSIMD(FDPart3tmp3, hDD01))),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp8,
                                                                                                MulSIMD(FusedMulAddSIMD(FDPart3tmp19, FDPart3tmp22,
                                                                                                                        FDPart3tmp193),
                                                                                                        FusedMulSubSIMD(hDD01, hDD12,
                                                                                                                        MulSIMD(FDPart3tmp2, hDD02))),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp8,
                                                                                                    MulSIMD(
                                                                                                        FusedMulSubSIMD(hDD02, hDD12,
                                                                                                                        MulSIMD(FDPart3tmp3, hDD01)),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp25, FDPart3tmp73,
                                                                                                            FusedMulAddSIMD(FDPart3tmp33,
                                                                                                                            FDPart3tmp78,
                                                                                                                            MulSIMD(FDPart3tmp22,
                                                                                                                                    FDPart3tmp78)))),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp8,
                                                                                                        MulSIMD(FusedMulSubSIMD(
                                                                                                                    hDD02, hDD12,
                                                                                                                    MulSIMD(FDPart3tmp3, hDD01)),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp63, FDPart3tmp80,
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp65, FDPart3tmp80,
                                                                                                                        MulSIMD(FDPart3tmp27,
                                                                                                                                FDPart3tmp59)))),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp8,
                                                                                                            MulSIMD(
                                                                                                                AddSIMD(FDPart3tmp134, FDPart3tmp194),
                                                                                                                FusedMulSubSIMD(
                                                                                                                    hDD01, hDD12,
                                                                                                                    MulSIMD(FDPart3tmp2, hDD02))),
                                                                                                            MulSIMD(
                                                                                                                FDPart3tmp8,
                                                                                                                MulSIMD(
                                                                                                                    FusedMulAddSIMD(FDPart3tmp21,
                                                                                                                                    FDPart3tmp73,
                                                                                                                                    FDPart3tmp189),
                                                                                                                    FusedMulSubSIMD(
                                                                                                                        FDPart3tmp2, FDPart3tmp3,
                                                                                                                        MulSIMD(
                                                                                                                            hDD12,
                                                                                                                            hDD12))))))))))))))))))))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp8,
                                            MulSIMD(FusedMulAddSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp20, FDPart3tmp22), FDPart3tmp179),
                                                    FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))),
                                            FusedMulAddSIMD(
                                                FDPart3tmp8,
                                                MulSIMD(FusedMulAddSIMD(FDPart3tmp204, FDPart3tmp22, MulSIMD(FDPart3tmp226, FDPart3tmp79)),
                                                        FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                                                FusedMulSubSIMD(
                                                    FDPart3_Rational_1_2,
                                                    FusedMulAddSIMD(
                                                        hDD12, lambdaU_dD11,
                                                        FusedMulAddSIMD(
                                                            hDD01, lambdaU_dD02,
                                                            FusedMulAddSIMD(
                                                                hDD02, lambdaU_dD01,
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp2, lambdaU_dD12,
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp3, lambdaU_dD21,
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp108, AddSIMD(FDPart3tmp133, FDPart3tmp159),
                                                                            FusedMulAddSIMD(
                                                                                FDPart3tmp113, AddSIMD(FDPart3tmp22, FDPart3tmp33),
                                                                                FusedMulAddSIMD(
                                                                                    hDD12, lambdaU_dD22,
                                                                                    FusedMulSubSIMD(
                                                                                        FDPart3tmp105, AddSIMD(FDPart3tmp170, FDPart3tmp172),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp8,
                                                                                            MulSIMD(hDD_dDD1211,
                                                                                                    FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3,
                                                                                                                    MulSIMD(hDD02, hDD02))),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp8,
                                                                                                MulSIMD(hDD_dDD1222,
                                                                                                        FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2,
                                                                                                                        MulSIMD(hDD01, hDD01))),
                                                                                                MulSIMD(
                                                                                                    FDPart3tmp8,
                                                                                                    MulSIMD(hDD_dDD1200,
                                                                                                            FusedMulSubSIMD(
                                                                                                                FDPart3tmp2, FDPart3tmp3,
                                                                                                                MulSIMD(hDD12, hDD12))))))))))))))),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp8, MulSIMD(hDD_dDD1202, FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp8,
                                                            MulSIMD(hDD_dDD1212, FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                                                            MulSIMD(FDPart3tmp8,
                                                                    MulSIMD(hDD_dDD1201,
                                                                            FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01)))))))))))))))));
        const REAL_SIMD_ARRAY __RHS_exp_5 = FusedMulAddSIMD(
            FDPart3tmp8,
            MulSIMD(FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)),
                    FusedMulAddSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp19, FDPart3tmp94), MulSIMD(FDPart3tmp19, MulSIMD(FDPart3tmp40, FDPart3tmp86)))),
            FusedMulAddSIMD(
                FDPart3tmp8,
                MulSIMD(FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)),
                        FusedMulAddSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp66, FDPart3tmp93),
                                        MulSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp63, FDPart3tmp93)))),
                FusedMulAddSIMD(
                    FDPart3tmp8,
                    MulSIMD(FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02)),
                            FusedMulAddSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp172, FDPart3tmp21),
                                            MulSIMD(FDPart3tmp170, MulSIMD(FDPart3tmp21, FDPart3tmp40)))),
                    FusedMulAddSIMD(
                        FDPart3tmp8,
                        MulSIMD(FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                FusedMulAddSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp172, FDPart3tmp25),
                                                MulSIMD(FDPart3tmp170, MulSIMD(FDPart3tmp25, FDPart3tmp40)))),
                        FusedMulAddSIMD(
                            FDPart3tmp8,
                            MulSIMD(FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12)),
                                    FusedMulAddSIMD(FDPart3tmp15, MulSIMD(FDPart3tmp27, FDPart3tmp94),
                                                    MulSIMD(FDPart3tmp27, MulSIMD(FDPart3tmp40, FDPart3tmp86)))),
                            FusedMulAddSIMD(
                                FDPart3tmp8,
                                MulSIMD(FusedMulAddSIMD(FDPart3tmp22, FDPart3tmp237, MulSIMD(FDPart3tmp238, FDPart3tmp33)),
                                        FusedMulSubSIMD(hDD01, hDD12, MulSIMD(FDPart3tmp2, hDD02))),
                                FusedMulAddSIMD(
                                    FDPart3tmp8,
                                    MulSIMD(FusedMulAddSIMD(FDPart3tmp239, FDPart3tmp28,
                                                            MulSIMD(FDPart3_Integer_2, MulSIMD(FDPart3tmp239, FDPart3tmp33))),
                                            FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                                    FusedMulAddSIMD(
                                        FDPart3tmp8,
                                        MulSIMD(
                                            FusedMulAddSIMD(FDPart3tmp149, FDPart3tmp15, MulSIMD(FDPart3tmp19, MulSIMD(FDPart3tmp33, FDPart3tmp40))),
                                            FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                                        FusedMulAddSIMD(
                                            FDPart3tmp8,
                                            MulSIMD(FusedMulAddSIMD(FDPart3tmp159, FDPart3tmp238,
                                                                    MulSIMD(FDPart3tmp133, MulSIMD(FDPart3tmp15, FDPart3tmp91))),
                                                    FusedMulSubSIMD(hDD01, hDD02, MulSIMD(FDPart3tmp0, hDD12))),
                                            FusedMulAddSIMD(
                                                FDPart3tmp8,
                                                MulSIMD(FusedMulAddSIMD(FDPart3tmp134, FDPart3tmp15,
                                                                        MulSIMD(FDPart3tmp159, MulSIMD(FDPart3tmp21, FDPart3tmp40))),
                                                        FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                                                FusedMulAddSIMD(
                                                    FDPart3tmp8,
                                                    MulSIMD(FusedMulAddSIMD(FDPart3tmp148, FDPart3tmp15,
                                                                            MulSIMD(FDPart3tmp27, MulSIMD(FDPart3tmp40, FDPart3tmp65))),
                                                            FusedMulSubSIMD(hDD02, hDD12, MulSIMD(FDPart3tmp3, hDD01))),
                                                    FusedMulAddSIMD(
                                                        FDPart3tmp8,
                                                        MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02)),
                                                                FusedMulAddSIMD(FDPart3tmp15, FDPart3tmp221,
                                                                                MulSIMD(FDPart3tmp159, MulSIMD(FDPart3tmp25, FDPart3tmp40)))),
                                                        FusedMulAddSIMD(
                                                            FDPart3tmp8,
                                                            MulSIMD(FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12)),
                                                                    FusedMulAddSIMD(FDPart3tmp117, FDPart3tmp15,
                                                                                    MulSIMD(FDPart3tmp19, MulSIMD(FDPart3tmp40, FDPart3tmp65)))),
                                                            FusedMulAddSIMD(
                                                                FDPart3tmp8,
                                                                MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01)),
                                                                        FusedMulAddSIMD(FDPart3tmp170, FDPart3tmp238,
                                                                                        MulSIMD(FDPart3tmp172, FDPart3tmp237))),
                                                                FusedMulAddSIMD(
                                                                    FDPart3tmp8,
                                                                    MulSIMD(FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp2, MulSIMD(hDD01, hDD01)),
                                                                            FusedMulAddSIMD(FDPart3tmp226, FDPart3tmp93,
                                                                                            MulSIMD(FDPart3tmp239, FDPart3tmp94))),
                                                                    FusedMulAddSIMD(
                                                                        FDPart3tmp8,
                                                                        MulSIMD(FusedMulAddSIMD(FDPart3tmp27, MulSIMD(FDPart3tmp33, FDPart3tmp40),
                                                                                                FDPart3tmp207),
                                                                                FusedMulSubSIMD(FDPart3tmp0, FDPart3tmp3, MulSIMD(hDD02, hDD02))),
                                                                        FusedMulAddSIMD(
                                                                            FDPart3tmp8,
                                                                            MulSIMD(FusedMulAddSIMD(FDPart3tmp21, MulSIMD(FDPart3tmp33, FDPart3tmp40),
                                                                                                    FDPart3tmp38),
                                                                                    FusedMulSubSIMD(FDPart3tmp2, FDPart3tmp3, MulSIMD(hDD12, hDD12))),
                                                                            FusedMulAddSIMD(
                                                                                hDD02, lambdaU_dD02,
                                                                                FusedMulAddSIMD(
                                                                                    hDD12, lambdaU_dD12,
                                                                                    FusedMulAddSIMD(
                                                                                        FDPart3tmp15,
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp122, MulSIMD(FDPart3tmp26, FDPart3tmp86),
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp115, MulSIMD(FDPart3tmp170, FDPart3tmp92),
                                                                                                FusedMulAddSIMD(
                                                                                                    FDPart3tmp120,
                                                                                                    MulSIMD(FDPart3tmp170, FDPart3tmp26),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp104, FDPart3tmp240,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp122, FDPart3tmp171,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3tmp124,
                                                                                                                MulSIMD(FDPart3tmp86, FDPart3tmp92),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp20,
                                                                                                                    MulSIMD(FDPart3tmp222,
                                                                                                                            FDPart3tmp86),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp102, FDPart3tmp240,
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3tmp103,
                                                                                                                            FDPart3tmp240))))))))),
                                                                                        FusedMulAddSIMD(
                                                                                            FDPart3tmp3, lambdaU_dD22,
                                                                                            FusedMulAddSIMD(
                                                                                                FDPart3tmp8,
                                                                                                MulSIMD(FusedMulSubSIMD(hDD02, hDD12,
                                                                                                                        MulSIMD(FDPart3tmp3, hDD01)),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp15,
                                                                                                            MulSIMD(FDPart3tmp22, FDPart3tmp25),
                                                                                                            MulSIMD(FDPart3tmp25,
                                                                                                                    MulSIMD(FDPart3tmp33,
                                                                                                                            FDPart3tmp40)))),
                                                                                                FusedMulSubSIMD(
                                                                                                    FDPart3_Integer_2,
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp108,
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3_Rational_1_2,
                                                                                                            MulSIMD(FDPart3tmp26, FDPart3tmp3),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3_Rational_1_2,
                                                                                                                MulSIMD(FDPart3tmp27, hDD02),
                                                                                                                MulSIMD(
                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                    MulSIMD(FDPart3tmp25, hDD12)))),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp113,
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3_Rational_1_2,
                                                                                                                MulSIMD(FDPart3tmp20, FDPart3tmp3),
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                    MulSIMD(FDPart3tmp21, hDD12),
                                                                                                                    MulSIMD(FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(FDPart3tmp19,
                                                                                                                                    hDD02)))),
                                                                                                            MulSIMD(
                                                                                                                FDPart3tmp105,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3_Rational_1_2,
                                                                                                                    MulSIMD(FDPart3tmp91, hDD12),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3_Rational_1_2,
                                                                                                                        MulSIMD(FDPart3tmp93, hDD02),
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3_Rational_1_2,
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3tmp3,
                                                                                                                                FDPart3tmp92))))))),
                                                                                                    FusedMulAddSIMD(
                                                                                                        FDPart3tmp8,
                                                                                                        MulSIMD(hDD_dDD2202,
                                                                                                                FusedMulSubSIMD(
                                                                                                                    hDD01, hDD12,
                                                                                                                    MulSIMD(FDPart3tmp2, hDD02))),
                                                                                                        FusedMulAddSIMD(
                                                                                                            FDPart3tmp8,
                                                                                                            MulSIMD(hDD_dDD2212,
                                                                                                                    FusedMulSubSIMD(
                                                                                                                        hDD01, hDD02,
                                                                                                                        MulSIMD(FDPart3tmp0, hDD12))),
                                                                                                            FusedMulAddSIMD(
                                                                                                                FDPart3_Rational_1_2,
                                                                                                                FusedMulAddSIMD(
                                                                                                                    FDPart3tmp8,
                                                                                                                    MulSIMD(
                                                                                                                        hDD_dDD2211,
                                                                                                                        FusedMulSubSIMD(
                                                                                                                            FDPart3tmp0, FDPart3tmp3,
                                                                                                                            MulSIMD(hDD02, hDD02))),
                                                                                                                    FusedMulAddSIMD(
                                                                                                                        FDPart3tmp8,
                                                                                                                        MulSIMD(hDD_dDD2222,
                                                                                                                                FusedMulSubSIMD(
                                                                                                                                    FDPart3tmp0,
                                                                                                                                    FDPart3tmp2,
                                                                                                                                    MulSIMD(hDD01,
                                                                                                                                            hDD01))),
                                                                                                                        MulSIMD(
                                                                                                                            FDPart3tmp8,
                                                                                                                            MulSIMD(
                                                                                                                                hDD_dDD2200,
                                                                                                                                FusedMulSubSIMD(
                                                                                                                                    FDPart3tmp2,
                                                                                                                                    FDPart3tmp3,
                                                                                                                                    MulSIMD(
                                                                                                                                        hDD12,
                                                                                                                                        hDD12)))))),
                                                                                                                MulSIMD(
                                                                                                                    FDPart3tmp8,
                                                                                                                    MulSIMD(
                                                                                                                        hDD_dDD2201,
                                                                                                                        FusedMulSubSIMD(
                                                                                                                            hDD02, hDD12,
                                                                                                                            MulSIMD(
                                                                                                                                FDPart3tmp3,
                                                                                                                                hDD01))))))))))))))))))))))))))))));

        WriteSIMD(&RbarDD00GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_0);
        WriteSIMD(&RbarDD01GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_1);
        WriteSIMD(&RbarDD02GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_2);
        WriteSIMD(&RbarDD11GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_3);
        WriteSIMD(&RbarDD12GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_4);
        WriteSIMD(&RbarDD22GF[CCTK_GFINDEX3D(cctkGH, i0, i1, i2)], __RHS_exp_5);

      } // END LOOP: for (int i0 = cctk_nghostzones[0]; i0 < cctk_lsh[0]-cctk_nghostzones[0]; i0 += simd_width)
    } // END LOOP: for (int i1 = cctk_nghostzones[1]; i1 < cctk_lsh[1]-cctk_nghostzones[1]; i1++)
  } // END LOOP: for (int i2 = cctk_nghostzones[2]; i2 < cctk_lsh[2]-cctk_nghostzones[2]; i2++)
}
