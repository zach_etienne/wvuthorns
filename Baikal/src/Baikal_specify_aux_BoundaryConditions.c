#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Faces.h"
#include "cctk_Parameters.h"
#include "stdio.h"
#include "util_Table.h"
/**
 *
 *
 * This code is based on Kranc's McLachlan/ML_BSSN/src/Boundaries.cc code.
 */
void Baikal_specify_aux_BoundaryConditions(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS_Baikal_specify_aux_BoundaryConditions;
  DECLARE_CCTK_PARAMETERS;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  const CCTK_INT bndsize = FD_order / 2 + 1; // <- bndsize = number of ghostzones

  ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, bndsize, -1, "Baikal::HGF", "flat");
  if (ierr < 0)
    CCTK_ERROR("Failed to register BC with Boundary for Baikal::HGF!");

  ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, bndsize, -1, "Baikal::MSQUAREDGF", "flat");
  if (ierr < 0)
    CCTK_ERROR("Failed to register BC with Boundary for Baikal::MSQUAREDGF!");

  ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, bndsize, -1, "Baikal::MU0GF", "flat");
  if (ierr < 0)
    CCTK_ERROR("Failed to register BC with Boundary for Baikal::MU0GF!");

  ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, bndsize, -1, "Baikal::MU1GF", "flat");
  if (ierr < 0)
    CCTK_ERROR("Failed to register BC with Boundary for Baikal::MU1GF!");

  ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, bndsize, -1, "Baikal::MU2GF", "flat");
  if (ierr < 0)
    CCTK_ERROR("Failed to register BC with Boundary for Baikal::MU2GF!");
}
